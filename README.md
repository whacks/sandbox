### Running Java from Intellij (recommended)

Options:
1. Right click on the test-file -> Run
1. Right click on the test package -> Run
1. Right click on the `main` method -> Run

### Running Java from command-line

`mvn clean install -DrunIT`

### Running Scala from Intellij

For some reason, running `src/main/scala` via Intellij does not work.

However, running anything in `src/main/java`, including the scala stuff, works on Intellij.

If getting weird compile errors only on Intellij and not on sbt, try `Build` -> `Rebuild Project`

### Running Scala from command-line

1. `cd $PROJECT_ROOT`
1. `sbt`
1. `run`
1. select class to run

### Compiling proto files

```
protoc -I=src/main/proto/ --java_out=src/main/java/ src/main/proto/*/*
```