package org.rajivprab.sandbox.census_analysis;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;

import java.util.Optional;

public class Entry implements Comparable<Entry> {
    private static final Logger log = LogManager.getLogger(Entry.class);

    private final String id;
    private final double whitePercentage, otherPercentage, mixedPercentage;

    static Optional<Entry> build(String id, double whitePercentage, double otherPercentage, double mixedPercentage) {
        id = id.replaceAll("Metro Area", "").replaceAll("Micro Area", "").trim();
        Entry entry = new Entry(id, whitePercentage, otherPercentage, mixedPercentage);
        if (Double.isNaN(otherPercentage) || Double.isNaN(mixedPercentage) || (otherPercentage < 1)) {
            log.debug("Excluding entry because of small sample size: {}", entry);
            return Optional.empty();
        }
        Validatec.greaterOrEqual(100.0, whitePercentage);
        Validatec.greaterOrEqual(100.0, otherPercentage);
        Validatec.greaterOrEqual(100.0, mixedPercentage);
        return Optional.of(entry);
    }

    private Entry(String id, double whitePercentage, double otherPercentage, double mixedPercentage) {
        this.id = id;
        this.whitePercentage = whitePercentage;
        this.otherPercentage = otherPercentage;
        this.mixedPercentage = mixedPercentage;
    }

    double getScore() {
        return 100 * mixedPercentage / getExpectedMixedPercentage();
    }

    double getExpectedMixedPercentage() {
        double whiteFraction = whitePercentage / 100;
        double otherFraction = otherPercentage / 100;
        double expectedMixedFraction = whiteFraction * otherFraction * 2;
        return expectedMixedFraction * 100;
    }

    @Override
    public int compareTo(Entry o) {
        return Double.compare(getScore(), o.getScore());
    }

    public String prettyPrint(String otherName) {
        return String.format("[%46s] score: %4.1f, white: %4.1f, %s: %4.1f, biracial: %3.1f, expected: %4.1f",
                             id, getScore(), whitePercentage, otherName, otherPercentage, mixedPercentage, getExpectedMixedPercentage());
    }

    @Override
    public String toString() {
        return "Entry{" +
                "id='" + id + '\'' +
                ", whitePercentage=" + whitePercentage +
                ", otherPercentage=" + otherPercentage +
                ", mixedPercentage=" + mixedPercentage +
                '}';
    }
}
