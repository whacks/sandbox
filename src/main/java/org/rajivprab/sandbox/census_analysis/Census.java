package org.rajivprab.sandbox.census_analysis;

import com.google.common.collect.Lists;
import org.rajivprab.cava.FileUtilc;
import org.rajivprab.cava.Validatec;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Dataset derived from: https://data.census.gov/cedsci/table?q=DP05&t=Race%20and%20Ethnicity&tid=ACSDP1Y2018.DP05&hidePreview=false
 * -> Select "MAPS"
 * -> Select "State" or "Metro/Micro Area" from the dropdown menu
 * -> "Select" and drag-select the entire US, along with Alaska, Hawaii and Puerto Rico
 * -> Download selected
 */
public class Census {
    public static void main(String... args) throws IOException {
        // parse2010("metro");
        parse2010("state");

        parse2018("metro");
        parse2018("state");
    }

    private static void parse2010(String granularity) throws IOException {
        System.out.println("=============== 2010 ==================");
        String fileRoot = String.format("census_analysis/ACSDP1Y_DP05/%s/2010", granularity);
        File dataFile = FileUtilc.getClasspathFile(fileRoot + "/data_with_overlays.csv");
        File metadata = FileUtilc.getClasspathFile(fileRoot + "/metadata.csv");

        Data data = new Data(metadata, dataFile);

        parse("black",
              "Percent!!",
              "RACE!!One race!!White",
              "RACE!!One race!!Black or African American",
              "RACE!!Two or more races!!White and Black or African American",
              data);

        parse("asian",
              "Percent!!",
              "RACE!!One race!!White",
              "RACE!!One race!!Asian",
              "RACE!!Two or more races!!White and Asian",
              data);
    }

    private static void parse2018(String granularity) throws IOException {
        System.out.println("=============== 2018 ==================");
        String fileRoot = String.format("census_analysis/ACSDP1Y_DP05/%s/2018", granularity);
        File dataFile = FileUtilc.getClasspathFile(fileRoot + "/data_with_overlays.csv");
        File metadata = FileUtilc.getClasspathFile(fileRoot + "/metadata.csv");

        Data data = new Data(metadata, dataFile);

        parse("black",
              "Percent Estimate!!",
              "RACE!!Total population!!One race!!White",
              "RACE!!Total population!!One race!!Black or African American",
              "RACE!!Total population!!Two or more races!!White and Black or African American",
              data);

        parse("asian",
              "Percent Estimate!!",
              "RACE!!Total population!!One race!!White",
              "RACE!!Total population!!One race!!Asian",
              "RACE!!Total population!!Two or more races!!White and Asian",
              data);
    }

    private static void parse(String otherName,
                              String percentagePrefix,
                              String whiteColumnDescriptionSuffix,
                              String otherColumnDescriptionSuffix,
                              String mixedColumnDescriptionSuffix, Data data) throws IOException {
        List<String> geographicAreaName = data.getColumn("Geographic Area Name");
        List<Double> whitePercentage = data.getColumnAsDouble(percentagePrefix + whiteColumnDescriptionSuffix);
        List<Double> otherPercentage = data.getColumnAsDouble(percentagePrefix + otherColumnDescriptionSuffix);
        List<Double> otherPopulation = data.getColumnAsDouble("Estimate!!" + otherColumnDescriptionSuffix);
        List<Double> biracialPercentage = data.getColumnAsDouble(percentagePrefix + mixedColumnDescriptionSuffix);

        Validatec.size(whitePercentage, geographicAreaName.size());
        Validatec.size(otherPercentage, geographicAreaName.size());
        Validatec.size(biracialPercentage, geographicAreaName.size());

        List<Entry> entries = Lists.newArrayList();
        for (int i=0; i<geographicAreaName.size(); i++) {
            if (otherPopulation.get(i) > 1000) {
                Entry.build(geographicAreaName.get(i),
                            whitePercentage.get(i),
                            otherPercentage.get(i),
                            biracialPercentage.get(i))
                     .ifPresent(e -> entries.add(e));
            }
        }
        Collections.sort(entries);

        System.out.println("----------------- Printing stats for: " + otherName + " -------------------");
        entries.forEach(e -> System.out.println(e.prettyPrint(otherName)));
    }
}
