package org.rajivprab.sandbox.census_analysis;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Data {
    private static final Logger log = LogManager.getLogger(Data.class);

    private final Map<String, String> descriptionToColumnName;
    private final Map<String, Integer> columnNameToIndex;   // Starts with 0
    private final File data;

    Data(File metadata, File data) throws IOException {
        this.descriptionToColumnName = ImmutableMap.copyOf(parseMetadata(metadata));
        this.columnNameToIndex = ImmutableMap.copyOf(parseColumnIndexes(data));
        this.data = data;
    }

    List<Double> getColumnAsDouble(String description) throws IOException {
        List<String> columns = getColumn(description);
        try {
            return columns.stream().map(s -> parseDouble(s)).collect(Collectors.toList());
        } catch (RuntimeException e) {
            log.info("Problem parsing column: {}. Found: {}", description, columns);
            throw e;
        }
    }

    List<String> getColumn(String description) throws IOException {
        Validatec.contains(descriptionToColumnName.keySet(), description);
        int index = columnNameToIndex.get(descriptionToColumnName.get(description));
        List<String> column = Lists.newArrayList();
        Scanner scanner = new Scanner(new FileInputStream(data));
        scanner.nextLine();     // Column-names
        scanner.nextLine();     // Column-descriptions
        while (scanner.hasNextLine()) {
            column.add(getNextLine(scanner)[index]);
        }
        return column;
    }

    private static Double parseDouble(String doubleText) {
        if ("N".equals(doubleText)) { return Double.NaN; }
        return Double.parseDouble(doubleText);
    }

    private static Map<String, Integer> parseColumnIndexes(File data) throws IOException {
        Map<String, Integer> columnIndexes = Maps.newHashMap();
        String[] columns = getNextLine(new Scanner(new FileInputStream(data)));
        for (int index=0; index< columns.length; index++) {
            columnIndexes.put(columns[index], index);
        }
        return columnIndexes;
    }

    private static Map<String, String> parseMetadata(File metadata) throws IOException {
        Scanner scanner = new Scanner(new FileInputStream(metadata));
        Map<String, String> descriptionToColumnName = Maps.newHashMap();
        while ((scanner.hasNextLine())) {
            String[] line = getNextLine(scanner);
            descriptionToColumnName.put(line[1], line[0]);
        }
        return descriptionToColumnName;
    }

    private static String[] getNextLine(Scanner scanner) {
        return getNextLine(scanner.nextLine());
    }

    @VisibleForTesting
    static String[] getNextLine(String line) {
        String[] column = line.split("\",\"");
        column[0] = column[0].replaceFirst("\"", "");
        column[column.length - 1] = column[column.length - 1].replaceFirst("\"", "");
        return column;
    }
}