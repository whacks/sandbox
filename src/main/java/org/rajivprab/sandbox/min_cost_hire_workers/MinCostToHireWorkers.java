package org.rajivprab.sandbox.min_cost_hire_workers;

import com.google.common.collect.MinMaxPriorityQueue;
import org.rajivprab.cava.Validatec;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Problem: https://leetcode.com/problems/minimum-cost-to-hire-k-workers/
 *
 * There are N workers.  The i-th worker has a quality[i] and a minimum wage expectation wage[i].
 *
 * Now we want to hire exactly K workers to form a paid group.  When hiring a group of K workers, we must pay them
 * according to the following rules:
 *
 * Every worker in the paid group should be paid in the ratio of their quality compared to other workers in the paid
 * group.
 * Every worker in the paid group must be paid at least their minimum wage expectation.
 * Return the least amount of money needed to form a paid group satisfying the above conditions.
 *
 * Solution: https://leetcode.com/problems/minimum-cost-to-hire-k-workers/discuss/141768/Detailed-explanation-O(NlogN)
 */
class MinCostToHireWorkers {
    static double getCost(List<Integer> quality, List<Integer> wages, int numWorkers) {
        List<Worker> workers = IntStream.range(0, quality.size())
                 .mapToObj(i -> new Worker(quality.get(i), wages.get(i)))
                 .collect(Collectors.toList());
        return getCost(workers, numWorkers);
    }

    static double getCost(List<Worker> workers, int numWorkers) {
        workers.sort(Comparator.comparingDouble(Worker::getDollarsPerQuality));

        PriorityQueue<Integer> qualitiesChosen = new PriorityQueue<>(Collections.reverseOrder());
        double curDollarsPerQuality = 0;
        int curSumQuality = 0;

        for (int i=0; i<numWorkers; i++) {
            Worker worker = workers.get(i);
            curDollarsPerQuality = worker.getDollarsPerQuality();
            qualitiesChosen.add(worker.quality);
            curSumQuality += worker.quality;
        }

        for (int i=numWorkers; i<workers.size(); i++) {
            Worker newWorker = workers.get(i);
            double newDollarsPerQuality = newWorker.getDollarsPerQuality();

            int curHighestQuality = qualitiesChosen.peek();
            double curHighestWage = curHighestQuality * curDollarsPerQuality;

            double wageIncreaseForExistingWorkers =
                    (curSumQuality - curHighestQuality) * (newDollarsPerQuality - curDollarsPerQuality);

            if (wageIncreaseForExistingWorkers + newWorker.minWage < curHighestWage) {
                qualitiesChosen.poll();
                qualitiesChosen.add(newWorker.quality);
                curSumQuality -= curHighestQuality;
                curSumQuality += newWorker.quality;
                curDollarsPerQuality = newWorker.getDollarsPerQuality();
            }
        }

        return curDollarsPerQuality * curSumQuality;
    }
}
