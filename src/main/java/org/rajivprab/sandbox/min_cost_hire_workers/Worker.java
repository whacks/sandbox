package org.rajivprab.sandbox.min_cost_hire_workers;

class Worker {
    final int quality;
    final int minWage;

    Worker(int quality, int minWage) {
        this.quality = quality;
        this.minWage = minWage;
    }

    double getDollarsPerQuality() {
        return minWage * 1.0 / quality;
    }
}
