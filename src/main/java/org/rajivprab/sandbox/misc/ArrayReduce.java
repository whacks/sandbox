package org.rajivprab.sandbox.misc;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;

public class ArrayReduce {
    static int reductionCost(int[] num) {
        Queue<Integer> queue = new PriorityQueue<>();
        Arrays.stream(num).forEach(queue::add);
        int cost = 0;
        while (!queue.isEmpty()) {
            int smallest = queue.poll();
            if (queue.isEmpty()) {
                return cost;
            }
            int nextSmallest = queue.poll();
            cost += smallest + nextSmallest;
            int cur = smallest + nextSmallest;
            queue.add(cur);
        }
        throw new IllegalStateException("Should have returned");
    }
}
