package org.rajivprab.sandbox.misc;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Sort {
    public static void main(String args[]) {
        String input = new Scanner(System.in).next();
        String output = Joiner.on("\\s").join(output(ImmutableList.copyOf(input.split("\\s"))));
        System.out.println(output);
    }

    private static List<String> output(List<String> input) {
        List<EntryType> inputTypes = Lists.newArrayList();
        List<String> words = Lists.newArrayList();
        List<Integer> numbers = Lists.newArrayList();
        List<String> output = Lists.newArrayList();

        for (String cur : input) {
            try {
                numbers.add(Integer.parseInt(cur));
                inputTypes.add(EntryType.NUMBER);
            } catch (NumberFormatException e) {
                words.add(cur);
                inputTypes.add(EntryType.WORD);
            }
        }

        Collections.sort(numbers);
        Collections.sort(words);
        Collections.reverse(numbers);   // Pop from the end, not front, to prevent bad performance in ArrayList
        Collections.reverse(words);
        for (EntryType type : inputTypes) {
            String entry = type == EntryType.WORD ? words.remove(words.size() - 1) :
                    numbers.remove(numbers.size() - 1) + "";
            output.add(entry);
        }
        assert output.size() == input.size();
        return output;
    }

    private enum EntryType { WORD, NUMBER }
}
