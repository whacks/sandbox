package org.rajivprab.sandbox.misc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Decode {
    static String decode(List<String> codes, String encoded) {
        Map<String, String> dictionary = new HashMap<>();
        for (String code : codes) {
            String[] components = code.split("\\s");
            assert components.length == 2;
            String value = components[0].equals("[newline]") ? "\n" : components[0];
            dictionary.put(components[1], value);
        }
        StringBuilder decoded = new StringBuilder();
        StringBuilder curSymbol = new StringBuilder();
        for (int i=0; i<encoded.length(); i++) {
            curSymbol.append(encoded.charAt(i));
            if (dictionary.containsKey(curSymbol.toString())) {
                decoded.append(dictionary.get(curSymbol.toString()));
                curSymbol = new StringBuilder();
            }
        }
        return decoded.toString();
    }
}
