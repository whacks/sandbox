package org.rajivprab.sandbox.misc;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MovieTitles {
    static String[] getMovieTitles(String substr) {
        try {
            JSONObject json = getFromUrl(substr, 1);
            List<String> titles = getTitles(json);
            int numPages = json.getInt("total_pages");
            for (int i = 2; i <= numPages; i++) {
                titles.addAll(getTitles(getFromUrl(substr, i)));
            }
            return titles.toArray(new String[titles.size()]);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static List<String> getTitles(JSONObject json) {
        List<String> titles = new ArrayList<>();
        JSONArray data = json.getJSONArray("data");
        for (int i=0; i<data.length(); i++) {
            JSONObject entry = data.getJSONObject(i);
            titles.add(entry.getString("Title"));
        }
        return titles;
    }

    private static JSONObject getFromUrl(String substr, int pageNumber) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(getURL(substr, pageNumber));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return new JSONObject(result.toString());
    }

    private static String getURL(String substr, int pageNumber) {
        return "https://jsonmock.hackerrank.com/api/movies/search/?Title=" + substr + "&page=" + pageNumber;
    }
}
