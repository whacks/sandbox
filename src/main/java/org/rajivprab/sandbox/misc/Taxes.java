package org.rajivprab.sandbox.misc;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Taxes {
    // Complete the calculateTax function below.
    static String calculateTax(List<String> trades) {
        Map<String, OpenPositions> openPositions = new HashMap<>();
        double realizedGains2015 = 0;
        for (String trade : trades) {
            String[] split = trade.split(",");
            assert split.length == 5;
            int tradeYear = Integer.valueOf(split[0].split("-")[0]);
            String ticker = split[1];
            boolean isBuy = split[2].equals("B");
            int quantity = Integer.valueOf(split[3]);
            double price = Double.valueOf(split[4]);

            if (!openPositions.containsKey(ticker)) {
                openPositions.put(ticker, new OpenPositions());
            }
            Position latestTrade = new Position(isBuy, quantity, price);
            double realizedGain = openPositions.get(ticker).addPosition(latestTrade);
            if (tradeYear == 2015) {
                // Only calculate taxes for gains/losses realized in 2015
                realizedGains2015 += realizedGain;
            }
        }

        double taxes = realizedGains2015 * 0.25;
        if (taxes < 0) { taxes = 0; }
        return getTaxAmountString(taxes);
    }

    private static String getTaxAmountString(double taxAmount) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);

        boolean isNegative = taxAmount < 0.00;
        String result = df.format(Math.abs(taxAmount));
        result = "$" + result;
        if (isNegative) {
            result = "(" + result + ")";
        }
        return result;
    }

    private static class OpenPositions {
        // Invariant: All Positions in list should have matching isLongPosition
        LinkedList<Position> openPositions = new LinkedList<>();

        // Returns the realized profit/loss. If no profit/loss realized, returns 0
        double addPosition(Position latestTrade) {
            if (openPositions.isEmpty() || latestTrade.isLongPosition == openPositions.getFirst().isLongPosition) {
                // No realized gains/losses. Just expand the list of positions
                openPositions.add(latestTrade);
                return 0;
            } else if (openPositions.getFirst().numShares > latestTrade.numShares) {
                // Earliest open position is big enough to "consume" entirety of the new position
                Position oldFirst = openPositions.pollFirst();
                Position newFirst = new Position(oldFirst.isLongPosition,
                                                 oldFirst.numShares - latestTrade.numShares,
                                                 oldFirst.price);
                openPositions.addFirst(newFirst);
                double realizedGains = (latestTrade.price - oldFirst.price) * latestTrade.numShares;
                return latestTrade.isLongPosition ? realizedGains * -1 : realizedGains;
            } else {
                // Close out the oldest position entirely, then repeat this entire method
                Position first = openPositions.pollFirst();
                double realizedGains = (latestTrade.price - first.price) * first.numShares;
                realizedGains = latestTrade.isLongPosition ? realizedGains * -1 : realizedGains;
                Position newLatestTrade = new Position(latestTrade.isLongPosition,
                                                       latestTrade.numShares - first.numShares,
                                                       latestTrade.price);
                return realizedGains + addPosition(newLatestTrade);
            }
        }
    }

    private static class Position {
        final boolean isLongPosition;
        final int numShares;
        final double price;

        Position(boolean isLong, int numShares, double price) {
            "".chars();
            assert numShares > 0;
            assert price >= 0;
            this.isLongPosition = isLong;
            this.numShares = numShares;
            this.price = price;
        }
    }
}
