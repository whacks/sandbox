package org.rajivprab.sandbox.misc;

public class RollingString {
    static String rollingString(String s, String[] operations) {
        char[] characters = s.toCharArray();
        for (String operation : operations) {
            String[] components = operation.split("\\s");
            if (components.length != 3) {
                throw new IllegalArgumentException("Found unexpected operation: " + operation);
            }
            int startIndexInclusive = Integer.valueOf(components[0]);
            int endIndexInclusive = Integer.valueOf(components[1]);
            int delta = components[2].equals("R") ? 1 : -1;
            for (int i=startIndexInclusive; i<=endIndexInclusive; i++) {
                if (characters[i] < 'a' || characters[i] > 'z') {
                    throw new IllegalArgumentException("Expecting only lower case letters. Found: " + characters[i]);
                }
                if (characters[i] == 'a' && delta == -1) {
                    characters[i] = 'z';
                } else if (characters[i] == 'z' && delta == 1) {
                    characters[i] = 'a';
                } else {
                    characters[i] += delta;
                }
            }
        }
        return String.valueOf(characters);
    }
}
