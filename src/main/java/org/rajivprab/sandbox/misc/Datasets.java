package org.rajivprab.sandbox.misc;

import com.google.common.base.Joiner;
import com.google.common.collect.*;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Datasets {
    private Map<Integer, Integer> datasetToOwningCenter;
    private SetMultimap<Integer, Integer> datacenterToDatasetsPresent;

    // Some minor corner-case bugs have not been addressed below. Specifically for datacenters with no datasets at all
    public static void main(String args[]) {
        Datasets solver = new Datasets();
        solver.parseInput(new Scanner(System.in));
        String output = Joiner.on("\n").join(solver.getMoveInstructions());
        System.out.println(output);
        System.out.println("done");
    }

    private void parseInput(Scanner scanner) {
        Map<Integer, Integer> datasetToOwningCenter = Maps.newHashMap();
        SetMultimap<Integer, Integer> datacenterToDatasetsPresent = HashMultimap.create();
        int numDatacenters = scanner.nextInt();
        for (int datacenter = 1; datacenter <= numDatacenters; datacenter++) {
            int numDatasets = scanner.nextInt();
            for (int i=0; i<numDatasets; i++) {
                int dataset = scanner.nextInt();
                datasetToOwningCenter.put(dataset, datacenter);
                datacenterToDatasetsPresent.put(datacenter, dataset);
            }
        }
        assert !scanner.hasNext();
        this.datasetToOwningCenter = ImmutableMap.copyOf(datasetToOwningCenter);
        this.datacenterToDatasetsPresent = ImmutableSetMultimap.copyOf(datacenterToDatasetsPresent);
    }

    private List<String> getMoveInstructions() {
        Set<Integer> allDatasets = datasetToOwningCenter.keySet();
        List<String> output = Lists.newArrayList();
        for (int datacenter : datacenterToDatasetsPresent.keySet()) {
            Set<Integer> datasetsMissing = Sets.difference(allDatasets, datacenterToDatasetsPresent.get(datacenter));
            for (int dataset : datasetsMissing) {
                output.add(String.format("%d %d %d", dataset, datasetToOwningCenter.get(dataset), datacenter));
            }
        }
        return ImmutableList.copyOf(output);
    }
}
