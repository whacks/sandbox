package org.rajivprab.sandbox.n_queens;

import com.google.common.collect.Sets;
import org.rajivprab.cava.Validatec;

import java.util.*;

public class Cell {
    final int row;
    final int column;

    Cell(int row, int column) {
        this.row = row;
        this.column = column;
    }

    boolean conflictsWith(Collection<Cell> queens) {
        return queens.stream().anyMatch(this::conflictsWith);
    }

    boolean conflictsWith(Cell queen) {
        if (queen.row == row) { return true; }
        if (queen.column == column) { return true; }

        int rowDelta = Math.abs(queen.row - row);
        int columnDelta = Math.abs(queen.column - column);
        return rowDelta == columnDelta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return column == cell.column && row == cell.row;
    }

    @Override
    public int hashCode() {
        return Objects.hash(column, row);
    }

    @Override
    public String toString() {
        return "{" + row +
                ", " + column +
                '}';
    }
}
