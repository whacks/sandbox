package org.rajivprab.sandbox.n_queens;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Set;

/**
 * https://leetcode.com/problems/n-queens/
 *
 * The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two queens attack each
 * other.
 *
 * Given an integer n, return all distinct solutions to the n-queens puzzle. You may return the answer in any order.
 *
 * Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' both indicate
 * a queen and an empty space, respectively.
 */
public class NQueens {
    private final int numRows;
    private final Set<Set<Cell>> solutionsFound = Sets.newHashSet();

    public NQueens(int numRows) { this.numRows = numRows; }

    static Set<Set<Cell>> getSolutions(int numRows) {
        NQueens solver = new NQueens(numRows);
        solver.getSolutions(0, Sets.newHashSet());
        return solver.solutionsFound;
    }

    private void getSolutions(int curRow, Set<Cell> curQueens) {
        for (int column = 0; column < numRows; column++) {
            Cell prospect = new Cell(curRow, column);
            if (!prospect.conflictsWith(curQueens)) {
                curQueens.add(prospect);
                if (curRow == numRows - 1) {
                    solutionsFound.add(ImmutableSet.copyOf(curQueens));
                } else {
                    getSolutions(curRow+1, curQueens);
                }
                curQueens.remove(prospect);
            }
        }
    }
}
