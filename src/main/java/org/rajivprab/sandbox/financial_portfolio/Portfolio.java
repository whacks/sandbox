package org.rajivprab.sandbox.financial_portfolio;

import org.rajivprab.cava.Validatec;

import java.util.Random;

public class Portfolio {
    private static final double AVERAGE_ANNUAL_DRAWDOWN = 0.04;

    private final double equityFraction;

    private double currentWealth = 1000 * 1000;
    private double totalMoneySpent;

    Portfolio(double equityFraction) {
        this.equityFraction = equityFraction;
    }

    void simYear(double equityReturns, double bondReturns) {
        Validatec.greaterOrEqual(equityFraction, 0.0);
        Validatec.greaterOrEqual(1.0, equityFraction);

        double equityHoldings = equityFraction * currentWealth;
        double bondHoldings = (1.0 - equityFraction) * currentWealth;

        double equityProfit = equityHoldings * equityReturns;
        double bondProfit = bondHoldings * bondReturns;
        double moneySpent = currentWealth * AVERAGE_ANNUAL_DRAWDOWN;

        totalMoneySpent += moneySpent;
        currentWealth += equityProfit;
        currentWealth += bondProfit;
        currentWealth -= moneySpent;
    }

    double dollarValue() {
        return currentWealth + totalMoneySpent;
    }
}
