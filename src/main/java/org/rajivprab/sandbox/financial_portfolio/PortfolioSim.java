package org.rajivprab.sandbox.financial_portfolio;

import com.google.common.collect.Lists;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.Random;

// Compares performance of all-equity vs 60-40 portfolio
public class PortfolioSim {

    private enum EquityBondCorrelation {Uncorrelated, InverselyCorrelated, PerfectlyCorrelated}

    private static final EquityBondCorrelation EQUITY_BOND_CORRELATION = EquityBondCorrelation.InverselyCorrelated;
    private static final int NUM_SIMULATIONS = 100000;
    private static final int NUM_YEARS_TO_SIMULATE = 45;

    private static final Random RNG = new Random();

    // Use real-returns here so inflation isn't a concern
    private static final double AVERAGE_EQUITY_RETURNS = 0.05;
    private static final double AVERAGE_BOND_RETURNS = 0.02;

    public static void main(String... args) {
        List<Double> sixtyFortyDollars = Lists.newArrayList();
        List<Double> allEquityDollars = Lists.newArrayList();
        int allEquityWins = 0;

        for (int i=0; i<NUM_SIMULATIONS; i++) {
            Portfolio sixtyForty = new Portfolio(0.6);
            Portfolio allEquity = new Portfolio(1.0);

            for (int year=0; year<NUM_YEARS_TO_SIMULATE; year++) {
                double equitySeed = randomNumMinusPlusOne();

                double bondSeed;
                if (EQUITY_BOND_CORRELATION == EquityBondCorrelation.Uncorrelated) {
                    bondSeed = randomNumMinusPlusOne();
                } else if (EQUITY_BOND_CORRELATION == EquityBondCorrelation.InverselyCorrelated) {
                    bondSeed = equitySeed * -1;
                } else {
                    bondSeed = equitySeed;
                }

                double equityReturns = equityReturns(equitySeed);
                double bondReturns = bondReturns(bondSeed);

                sixtyForty.simYear(equityReturns, bondReturns);
                allEquity.simYear(equityReturns, bondReturns);
            }

            sixtyFortyDollars.add(sixtyForty.dollarValue());
            allEquityDollars.add(allEquity.dollarValue());
            if (allEquity.dollarValue() > sixtyForty.dollarValue()) {
                allEquityWins++;
            }
        }

        int averageAllEquityDollars = (int) allEquityDollars.stream().mapToDouble(i -> i).average().getAsDouble();
        int average6040Dollars = (int) sixtyFortyDollars.stream().mapToDouble(i -> i).average().getAsDouble();

        double averageAllEquityMillions = (averageAllEquityDollars * 100 / 1000000) / 100.0;
        double average6040Millions = (average6040Dollars * 100 / 1000000) / 100.0;

        System.out.println("All equity outperformed in " + allEquityWins * 100 / NUM_SIMULATIONS + "% of simulations");

        System.out.println("Starting with a $1M portfolio and withdrawing 4% every year:");
        System.out.println("All equity average final portfolio: $" + averageAllEquityMillions + "M");
        System.out.println("60/40 average final portfolio:      $" + average6040Millions + "M");
    }

    // Assumes bond returns range uniformly from [AVG-10%,AVG+10%]
    // So if average is 2%, range uniformly from [-8%,12%]
    private static double bondReturns(double randomNumMinusPlusOne) {
        return randomNumMinusPlusOne/10.0 + AVERAGE_BOND_RETURNS;
    }

    // Assumes equity returns range uniformly from [AVG-40%,AVG+40%]
    // So if average is 6%, range uniformly from [-34%,46%]
    private static double equityReturns(double randomNumMinusPlusOne) {
        return randomNumMinusPlusOne/2.5 + AVERAGE_EQUITY_RETURNS;
    }

    private static double randomNumMinusPlusOne() {
        return RNG.nextDouble() * 2.0 - 1.0;
    }
}
