package org.rajivprab.sandbox.language_trivia.dynamic_dispatch;

public class Overloads {
    public void foo(Object object) {
        System.out.println("foo:object:" + object);
    }

    public void foo(Integer integer) {
        System.out.println("foo:integer:" + integer);
    }

    public static void main(String... args) {
        String stringParam = "my string";
        Integer intParam = 1;
        new Overloads().foo(stringParam);
        new Overloads().foo(intParam);

        Object objectParam = stringParam;
        new Overloads().foo(objectParam);

        objectParam = intParam;
        new Overloads().foo(objectParam);
    }
}
