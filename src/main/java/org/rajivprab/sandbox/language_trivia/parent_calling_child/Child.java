package org.rajivprab.sandbox.language_trivia.parent_calling_child;

public class Child extends Parent {
    @Override
    public String bar() {
        return "Child bar";
    }

    public static void main(String... args) {
        System.out.println(new Child().fooBar());
    }
}
