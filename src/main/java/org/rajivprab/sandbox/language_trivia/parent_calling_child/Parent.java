package org.rajivprab.sandbox.language_trivia.parent_calling_child;

public class Parent {
    public String foo() {
        return "Base foo";
    }

    public String bar() {
        return "Base bar";
    }

    public String fooBar() {
        return foo() + " " + bar();
    }

    public static void main(String... args) {
        System.out.println(new Parent().fooBar());
    }
}
