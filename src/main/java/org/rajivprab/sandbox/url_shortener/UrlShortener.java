package org.rajivprab.sandbox.url_shortener;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Prototype for a URL shortening service that utilizes the shortest available suffix to map to a unique URL
 */
public class UrlShortener {
    @VisibleForTesting
    static final String URL_PREFIX = "https://shortened-url.com/";

    private final UniqueString uniqueString = new UniqueString();

    // In production, this mapping would be stored in a single database table,
    // with both suffix and fullUrl marked as unique, so as to prevent duplicates.
    private final Map<String, String> suffixToFullUrl = Maps.newConcurrentMap();
    // Not using BiMap, because there is no concurrent BiMap implementation
    private final Map<String, String> fullUrlToSuffix = Maps.newConcurrentMap();

    @VisibleForTesting
    final Map<String, AtomicInteger> suffixToNumClicks = Maps.newConcurrentMap();

    // In production, this would probably be a REST API, as a POST request,
    // with the fullUrl provided as a form-parameter
    public String shorten(String fullUrl) {
        // TODO: Perform some pre-processing on the fullUrl,
        // so that identical URLs with different case or other minor differences, do not trigger a new suffix generation
        String suffix = fullUrlToSuffix.computeIfAbsent(fullUrl, url -> uniqueString.getUniqueString());
        String existingFullUrl = suffixToFullUrl.put(suffix, fullUrl);
        assert existingFullUrl == null || existingFullUrl.equals(fullUrl);
        return URL_PREFIX + suffix;
    }

    // In production, this would probably be a REST API, as a GET request,
    // with the list of suffixes provided as query-parameters.
    // The database lookups can then be done in a single query, to avoid making numerous DB calls
    public List<String> getFullUrl(List<String> suffixes) {
        return suffixes.stream().map(this::getFullUrl).collect(Collectors.toList());
    }

    private String getFullUrl(String suffix) {
        suffixToNumClicks.computeIfAbsent(suffix, k -> new AtomicInteger()).getAndIncrement();
        return suffixToFullUrl.get(suffix);
    }
}
