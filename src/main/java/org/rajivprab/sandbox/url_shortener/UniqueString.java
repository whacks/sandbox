package org.rajivprab.sandbox.url_shortener;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Returns the shortest possible alphanumeric string, that hasn't previously been returned by this instance.
 * Is thread-safe.
 *
 * In production, the nextAvailable AtomicLong would be implemented as a Database.
 *
 * If a single database does not support the desired throughput, it can be shared into multiple partitions,
 * where each partition starts off with a consecutive value, and increments by N upon every new String generated.
 * Eg: With 3 partitions, you'd have:
 * DB-0 starting off with nextAvailable = 0,
 * DB-1 starting off with nextAvailable = 1,
 * DB-2 starting off with nextAvailable = 2,
 * each database will increment by 3, whenever it generates a new unique String
 */
class UniqueString {
    private static final int NUM_ALPHABETS = 26;
    private static final int NUM_DIGITS = 10;
    private static final int NUM_CHARACTERS = NUM_ALPHABETS * 2 + NUM_DIGITS;   // Both upper/lower case

    private final AtomicLong nextAvailable = new AtomicLong(0);

    String getUniqueString() {
        return getString(nextAvailable.getAndIncrement());
    }

    private static String getString(long value) {
        StringBuilder builder = new StringBuilder();
        if (value == 0) { return "0"; }
        while (value > 0) {
            long cur = value % NUM_CHARACTERS;
            value = value / NUM_CHARACTERS;
            builder.append(getChar(cur));
        }
        return builder.reverse().toString();
    }

    private static char getChar(long value) {
        assert value < NUM_CHARACTERS;
        if (value < NUM_DIGITS) {
            return Character.forDigit((int) value, NUM_DIGITS);
        }
        value -= NUM_DIGITS;
        char letter = (char) ('a' + (value % NUM_ALPHABETS));
        return value >= NUM_ALPHABETS ? Character.toUpperCase(letter) : letter;
    }
}
