package org.rajivprab.sandbox.kth_min_path;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by rprabhakar on 2/19/16.
 */
public class KthMinPath {
    public static String[] findPaths(String[] input) {
        List<String> output = Lists.newArrayList();
        for (String entry : input) {
            output.add(findPath(parseSingleInput(entry)));
        }
        return output.toArray(new String[input.length]);
    }

    private static String findPath(List<Integer> input) {
        int numH = input.get(0);
        int numV = input.get(1);
        int k = input.get(2);
        return findPath(numH, numV, k);
    }

    private static String findPath(int numH, int numV, int k) {
        int numPaths = numPaths(numH, numV);
        Validatec.greaterThan(numPaths, k);
        if (numPaths == 1) {
            return buildPath(numH, numV);
        }
        int numPathsFirstH = numPaths(numH - 1, numV);
        if (numPathsFirstH > k) {
            return "H" + findPath(numH - 1, numV, k);
        } else {
            return "V" + findPath(numH, numV - 1, k - numPathsFirstH);
        }
    }

    private static List<Integer> parseSingleInput(String input) {
        List<Integer> elements = Lists.newArrayList();
        for (String element : input.split(" ")) {
            elements.add(Integer.valueOf(element));
        }
        Validatec.size(elements, 3);
        return elements;
    }

    // TODO Optimize using Math formula, or by caching recursive-call-results in a table
    private static int numPaths(int numH, int numV) {
        if (numH == 0 || numV == 0) {
            return 1;
        }
        return numPaths(numH - 1, numV) + numPaths(numH, numV - 1);
    }

    private static String buildPath(int numH, int numV) {
        Validate.isTrue(numH == 0 || numV == 0, "Expecting either NumH or NumV to be 0, in order to build one path");
        String myChar = numH == 0 ? "V" : "H";
        StringBuilder sb = new StringBuilder();
        int range = Integer.max(numH, numV);
        IntStream.range(0, range).forEach(i -> sb.append(myChar));
        return sb.toString();
    }
}
