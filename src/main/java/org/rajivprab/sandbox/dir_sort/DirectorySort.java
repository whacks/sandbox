package org.rajivprab.sandbox.dir_sort;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.Validate;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

public class DirectorySort {
    private static final int NUM_LINES_PER_CHUNK = 100;
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(100);

    public static void main(String... args) {
        sort(new File(args[0]), new File(args[1]));
    }

    static void sort(File inputDirectory, File outputFile) {
        List<File> inputFiles = ImmutableList.copyOf(inputDirectory.listFiles());
        inputFiles = sortByChunks(inputFiles);
        File output = merge(inputFiles);
        Validate.isTrue(output.renameTo(outputFile));
    }

    // Can be used even if input files are not sorted
    private static List<File> sortByChunks(Collection<File> unsortedFiles) {
        return unsortedFiles.stream()
                            .map(inputFile -> EXECUTOR.submit(() -> sortByChunks(inputFile)))
                            // Without following line, everything will be serialized, and all parallelism will be lost
                            .collect(ImmutableList.toImmutableList()).stream()
                            .flatMap(future -> futureGet(future).stream())
                            .collect(ImmutableList.toImmutableList());
    }

    // Can be used even if input file is not sorted
    // Though to meet the question requirements, exception will be thrown if file is not sorted
    private static Collection<File> sortByChunks(File inputFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            List<File> outputFiles = Lists.newArrayList();
            LinkedHashSet<String> linesBuffered = Sets.newLinkedHashSet();
            String curLine = readNonEmptyLine(reader);
            while (curLine != null) {
                linesBuffered.add(curLine);
                if (linesBuffered.size() == NUM_LINES_PER_CHUNK) {
                    outputFiles.add(sortAndSave(linesBuffered));
                    linesBuffered = Sets.newLinkedHashSet();
                }
                curLine = readNonEmptyLine(reader);
            }
            outputFiles.add(sortAndSave(linesBuffered));
            if (outputFiles.contains(null)) {
                throw new IllegalArgumentException("Input files are not sorted");
            }
            return ImmutableList.copyOf(outputFiles);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String readNonEmptyLine(BufferedReader reader) throws IOException {
        String line = "";
        while (line != null && line.isEmpty()) {
            line = reader.readLine();
        }
        return line;
    }

    // Only accepts Set, to ensure no duplicates in chunk. Required for the no-duplicates invariant in merge(File, File)
    private static File sortAndSave(LinkedHashSet<String> chunk) throws IOException {
        List<String> originalOrdering = ImmutableList.copyOf(chunk);
        List<String> sortedOrdering = Lists.newArrayList(chunk);
        Collections.sort(sortedOrdering);
        if (!originalOrdering.equals(sortedOrdering)) { return null; }

        File outputFile = new File("/tmp/" + UUID.randomUUID());
        outputFile.deleteOnExit();
        try (PrintWriter writer = new PrintWriter(outputFile)) {
            sortedOrdering.forEach(writer::println);
        }
        return outputFile;
    }

    private static File merge(List<File> sortedFiles) {
        while (sortedFiles.size() > 1) {
            sortedFiles = mergeGeneration(sortedFiles);
        }
        Validate.notEmpty(sortedFiles);
        return sortedFiles.iterator().next();
    }

    private static List<File> mergeGeneration(List<File> sortedFiles) {
        List<Future<File>> outputFutures = Lists.newArrayList();
        Validate.isTrue(sortedFiles.size() > 1);
        for (int i=0; i<sortedFiles.size(); i+=2) {
            if (sortedFiles.size() == i + 1) {
                outputFutures.add(CompletableFuture.completedFuture(sortedFiles.get(i)));
            } else {
                File file1 = sortedFiles.get(i);
                File file2 = sortedFiles.get(i+1);
                outputFutures.add(EXECUTOR.submit(() -> merge(file1, file2)));
            }
        }
        return outputFutures.stream().map(DirectorySort::futureGet).collect(ImmutableList.toImmutableList());
    }

    private static File merge(File one, File two) {
        one.deleteOnExit();
        two.deleteOnExit();

        // Do not delete on exit, because could be final output
        File outputFile = new File("/tmp/" + UUID.randomUUID());

        try (BufferedReader reader1 = new BufferedReader(new FileReader(one));
             BufferedReader reader2 = new BufferedReader(new FileReader(two));
             PrintWriter writer = new PrintWriter(outputFile)) {
            String line1 = reader1.readLine();
            String line2 = reader2.readLine();
            while (line1 != null || line2 != null) {
                if (line1 == null) {
                    writer.println(line2);
                    line2 = reader2.readLine();
                } else if (line2 == null) {
                    writer.println(line1);
                    line1 = reader1.readLine();
                } else if (line1.equals(line2)) {
                    // Ensure no duplicate lines in output files
                    // Invariant only holds if each input file contains no duplicates
                    writer.println(line1);
                    line1 = reader1.readLine();
                    line2 = reader2.readLine();
                } else if (line1.compareTo(line2) < 0) {
                    writer.println(line1);
                    line1 = reader1.readLine();
                } else {
                    writer.println(line2);
                    line2 = reader2.readLine();
                }
            }
            return outputFile;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static <T> T futureGet(Future<T> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
