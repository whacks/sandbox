package org.rajivprab.sandbox.construct_tree;

import com.google.common.collect.Maps;
import org.javatuples.Pair;

import java.util.Map;

/**
 * Question: Given a series of parent-child relationships, construct a tree that reflects those relationships and return the root.
 * Assume: You have access to Node (value, children), and Pair(parent, child).
 *
 * Example Input:
 * [D-C, B-A, B-D]
 *
 * Example Tree:
 *    B  <- return the root
 *   / \
 *  A   D
 *       \
 *        C
 *
 */
public class ConstructTree {
    public static <T> T getRoot(Iterable<Pair<T, T>> links) {
        Map<T, Node<T>> nodesMap = Maps.newHashMap();
        for (Pair<T, T> link : links) {
            Node<T> parent = getWithPopulate(nodesMap, link.getValue0());
            Node<T> child = getWithPopulate(nodesMap, link.getValue1());
            child.withParent(parent);
        }
        Node<T> curNode = nodesMap.values().iterator().next();
        while (curNode.getParent() != null) {
            curNode = curNode.getParent();
        }
        return curNode.getValue();
    }

    private static <T> Node<T> getWithPopulate(Map<T, Node<T>> nodesMap, T value) {
        if (!nodesMap.containsKey(value)) {
            nodesMap.put(value, new Node<T>(value));
        }
        return nodesMap.get(value);
    }

    private static class Node<T> {
        private T value;
        private Node<T> parent;

        public Node(T value) {
            this.value = value;
        }

        public Node<T> withParent(Node<T> parent) {
            this.parent = parent;
            return this;
        }

        public Node<T> getParent() {
            return parent;
        }

        public T getValue() {
            return value;
        }
    }
}
