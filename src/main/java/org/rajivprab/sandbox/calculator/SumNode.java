package org.rajivprab.sandbox.calculator;

import com.google.common.collect.ImmutableList;

import java.util.List;

class SumNode implements Node {
    private final List<Node> nodes;

    SumNode(Node... nodes) {
        this.nodes = ImmutableList.copyOf(nodes);
    }

    @Override
    public double evaluate() {
        return nodes.stream().map(n -> n.evaluate()).mapToDouble(d -> d).sum();
    }
}
