package org.rajivprab.sandbox.calculator;

class SubtractNode {
    static Node build(Node first, Node second) {
        second = new MultiplyNode(second, new NumberNode(-1));
        return new SumNode(first, second);
    }
}
