package org.rajivprab.sandbox.calculator;

class DivideNode {
    static Node build(Node numerator, Node denominator) {
        Node inverse = new InverseNode(denominator);
        return new MultiplyNode(numerator, inverse);
    }
}
