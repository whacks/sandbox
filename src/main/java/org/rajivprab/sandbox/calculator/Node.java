package org.rajivprab.sandbox.calculator;

interface Node {
    double evaluate();
}
