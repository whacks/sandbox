package org.rajivprab.sandbox.calculator;

class InverseNode implements Node {
    private final Node node;

    InverseNode(Node node) {
        this.node = node;
    }

    @Override
    public double evaluate() {
        return 1 / node.evaluate();
    }
}
