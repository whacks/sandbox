package org.rajivprab.sandbox.calculator;

class NumberNode implements Node {
    private final double val;

    NumberNode(double val) {
        this.val = val;
    }

    @Override
    public double evaluate() {
        return val;
    }
}
