package org.rajivprab.sandbox.calculator;

import com.google.common.collect.ImmutableList;

import java.util.List;

class MultiplyNode implements Node {
    private final List<Node> nodes;

    MultiplyNode(Node... nodes) {
        this.nodes = ImmutableList.copyOf(nodes);
    }

    @Override
    public double evaluate() {
        double val = 1;
        for (Node node : nodes) {
            val *= node.evaluate();
        }
        return val;
    }
}
