package org.rajivprab.sandbox.min_index_sum;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MinIndexSum {
    static List<String> getMinIndexes(List<String> one, List<String> two) {
        return new Solver(one, two).solve();
    }

    private static class Solver {
        private final List<String> one;
        private final List<String> two;

        private final Map<String,Integer> indexes = Maps.newHashMap();
        private int resultIndexSum = Integer.MAX_VALUE;
        private List<String> result = Lists.newArrayList();

        private Solver(List<String> one, List<String> two) {
            // TODO Enhancement - validate no duplicates in each list
            this.one = ImmutableList.copyOf(one);
            this.two = ImmutableList.copyOf(two);
        }

        List<String> solve() {
            int maxSize = Math.max(one.size(), two.size());
            for (int i=0; i<maxSize; i++) {
                if (i > resultIndexSum) {
                    return result();    // Only needed for optimization
                }

                if (one.size() > i) {
                    parseEntry(one.get(i), i);
                }

                if (two.size() > i) {
                    parseEntry(two.get(i), i);
                }
            }

            return result();
        }

        private void parseEntry(String entry, int i) {
            Integer existingIndex = indexes.putIfAbsent(entry, i);
            if (existingIndex != null) {
                // Found a match
                int combinedIndex = existingIndex + i;
                if (combinedIndex < resultIndexSum) {
                    resultIndexSum = combinedIndex;
                    result = Lists.newArrayList(entry);
                } else if (combinedIndex == resultIndexSum) {
                    result.add(entry);
                }
            }
        }

        private List<String> result() {
            return result.stream().sorted().collect(Collectors.toList());
        }
    }
}
