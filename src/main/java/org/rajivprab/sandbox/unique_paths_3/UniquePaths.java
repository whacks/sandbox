package org.rajivprab.sandbox.unique_paths_3;

import com.google.common.collect.Sets;
import org.javatuples.Pair;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.Set;

class UniquePaths {
    static int getNumPaths(List<List<Integer>> map) {
        return new UniquePaths(map).getNumPaths();
    }

    private final Set<Pair<Integer, Integer>> visitedCells = Sets.newHashSet();
    private final List<List<Integer>> map;
    private final int numCellsToVisit;  // Includes starting cell, but not finish-cell

    private UniquePaths(List<List<Integer>> map) {
        this.map = map; // TODO make immutable copy
        this.numCellsToVisit = getNumCellsToVisit(map);
    }

    private static int getNumCellsToVisit(List<List<Integer>> map) {
        int numCells = 0;
        for (List<Integer> row : map) {
            for (Integer cell : row) {
                if (cell == 0 || cell == 1) { numCells++; }
            }
        }
        return numCells;
    }

    private int getNumPaths() {
        int row = 0;
        int col = 0;
        int terrain = map.get(row).get(col);
        while (terrain != 1) {
            col++;
            try {
                terrain = map.get(row).get(col);
            } catch (IndexOutOfBoundsException e) {
                row++;
                col = 0;
            }
        }
        return getNumPaths(row, col);
    }

    // TODO replace tuple with coordinate class
    private int getNumPaths(int curRow, int curCol) {
        Pair<Integer, Integer> cell = Pair.with(curRow, curCol);
        Validatec.doesNotContain(visitedCells, cell);
        visitedCells.add(cell);
        int numPaths = 0;

        numPaths += considerNeighbor(curRow-1, curCol);
        numPaths += considerNeighbor(curRow+1, curCol);
        numPaths += considerNeighbor(curRow, curCol-1);
        numPaths += considerNeighbor(curRow, curCol+1);

        // TODO if performance is not a concern, fork the visitedCells instead of trying to "undo" changes
        visitedCells.remove(cell);
        return numPaths;
    }

    private int considerNeighbor(int row, int col) {
        int terrain;
        try {
            terrain = map.get(row).get(col);
        } catch (IndexOutOfBoundsException e) {
            return 0;
        }
        if (terrain == -1) { return 0; }
        if (terrain == 2) {
            if (numCellsToVisit == visitedCells.size()) { return 1; }
             else { return 0; }
        }
        if (visitedCells.contains(Pair.with(row, col))) { return 0; }
        Validatec.equals(terrain, 0);
        return getNumPaths(row, col);
    }
}
