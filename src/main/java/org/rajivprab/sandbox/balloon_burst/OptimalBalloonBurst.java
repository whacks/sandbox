package org.rajivprab.sandbox.balloon_burst;

import com.google.common.collect.Lists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;

import java.util.List;

/**
 * https://leetcode.com/problems/burst-balloons/discuss/76228/Share-some-analysis-and-explanations
 */
public class OptimalBalloonBurst {
    private static final Logger log = LogManager.getLogger(OptimalBalloonBurst.class);

    public static int getMax(List<Integer> balloons) {
        List<Integer> balloonsWithSentinels = Lists.newArrayList(1);
        balloonsWithSentinels.addAll(balloons);
        balloonsWithSentinels.add(1);
        log.info("Starting recursion");
        return getMax(balloonsWithSentinels, 0, balloonsWithSentinels.size() - 1);
    }

    // Can only burst balloons in between left and right, exclusive
    // TODO Memoize
    public static int getMax(List<Integer> balloons, int leftIndexFixed, int rightIndexFixed) {
        if (leftIndexFixed + 1 == rightIndexFixed) { return 0; }
        Validatec.greaterOrEqual(rightIndexFixed, leftIndexFixed + 2);

        int maxScore = Integer.MIN_VALUE;
        int finalLastToPop = -1;
        for (int lastToPop=leftIndexFixed+1; lastToPop<rightIndexFixed; lastToPop++) {
            int leftScore = getMax(balloons, leftIndexFixed, lastToPop);
            int rightScore = getMax(balloons, lastToPop, rightIndexFixed);
            int totalScore = leftScore + rightScore + (
                    balloons.get(lastToPop) *
                            balloons.get(leftIndexFixed) *
                            balloons.get(rightIndexFixed));
            if (totalScore > maxScore) {
                finalLastToPop = lastToPop;
                maxScore = totalScore;
            }
        }
        log.info("Calling with balloons {}, left-fixed: {}, right-fixed: {}",
                 balloons, leftIndexFixed, rightIndexFixed);
        log.info("Best last index to pop: {}, producing score: {}", finalLastToPop, maxScore);
        return maxScore;
    }
}
