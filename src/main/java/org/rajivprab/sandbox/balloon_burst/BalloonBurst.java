package org.rajivprab.sandbox.balloon_burst;

import com.google.common.collect.Lists;
import org.rajivprab.cava.Validatec;

import java.util.List;

/**
 * https://leetcode.com/problems/burst-balloons/
 *
 * Given n balloons, indexed from 0 to n-1. Each balloon is painted with a number on it represented by array nums.
 * You are asked to burst all the balloons. If the you burst balloon i you will get nums[left] * nums[i] *
 * nums[right] coins. Here left and right are adjacent indices of i. After the burst, the left and right then becomes
 * adjacent.
 *
 * Find the maximum coins you can collect by bursting the balloons wisely.
 *
 * Note:
 *
 *     You may imagine nums[-1] = nums[n] = 1. They are not real therefore you can not burst them.
 *     0 ≤ n ≤ 500, 0 ≤ nums[i] ≤ 100
 *
 * Example:
 *
 * Input: [3,1,5,8]
 * Output: 167
 * Explanation: nums = [3,1,5,8] --> [3,5,8] -->   [3,8]   -->  [8]  --> []
 *              coins =  3*1*5      +  3*5*8    +  1*3*8      + 1*8*1   = 167
 */
public class BalloonBurst {
    public static int getMax(List<Integer> balloons) {
        List<Integer> balloonsWithSentinels = Lists.newArrayList(1);
        balloonsWithSentinels.addAll(balloons);
        balloonsWithSentinels.add(1);
        return getMaxWithSentinels(balloonsWithSentinels);
    }

    // TODO Cache result of this call
    private static int getMaxWithSentinels(List<Integer> balloons) {
        int size = balloons.size();
        Validatec.greaterOrEqual(size, 2);
        Validatec.equals(balloons.get(0), 1);
        Validatec.equals(balloons.get(size - 1), 1);

        if (size == 2) { return 0; }    // Terminal case

        int maxScore = Integer.MIN_VALUE;
        for (int i=1; i<size-1; i++) {
            int left = balloons.get(i-1);
            int right = balloons.get(i+1);
            int popped = balloons.remove(i);

            int downstreamScore = getMaxWithSentinels(balloons);
            int totalScore = left * popped * right + downstreamScore;
            if (totalScore > maxScore) {
                maxScore = totalScore;
            }

            balloons.add(i, popped);    // Undo all changes before next iteration
        }

        return maxScore;
    }
}
