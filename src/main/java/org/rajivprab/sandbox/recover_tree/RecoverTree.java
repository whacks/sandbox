package org.rajivprab.sandbox.recover_tree;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.rajivprab.cava.Validatec;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

class RecoverTree {
    static List<Integer> parse(String input) {
        Node root = buildTree(input);
        return printTree(root);
    }

    private static Node buildTree(String input) {
        int number = getNum(input, 0);
        int curIndex = (number + "").length();
        Node root = new Node(number);

        Map<Integer, Node> generationToMostRecentNode = Maps.newHashMap();
        generationToMostRecentNode.put(0, root);

        while (curIndex < input.length()) {
            int generation = getNumDashes(input, curIndex);
            curIndex += generation;

            number = getNum(input, curIndex);
            curIndex += (number + "").length();

            Node cur = new Node(number);

            Node parent = generationToMostRecentNode.get(generation - 1);
            parent.put(cur);

            generationToMostRecentNode.put(generation, cur);
        }

        return root;
    }

    @VisibleForTesting
    static int getNumDashes(String input, int index) {
        int num = 0;
        while (input.charAt(index) == '-') {
            num++;
            index++;
        }
        return num;
    }

    @VisibleForTesting
    static int getNum(String input, final int startIndex) {
        int curIndex = startIndex;
        int length = 0;
        while (curIndex < input.length() && input.charAt(curIndex) != '-') {
            length++;
            curIndex++;
        }
        Validatec.greaterThan(length, 0);
        String num = input.substring(startIndex, startIndex + length);
        return Integer.parseInt(num);
    }

    static List<Integer> printTree(Node root) {
        List<Integer> result = Lists.newArrayList();
        Queue<Node> nodesToVisit = new LinkedList<>();
        nodesToVisit.add(root);
        while (!nodesToVisit.isEmpty()) {
            Node cur = nodesToVisit.poll();
            if (cur == null) {
                result.add(null);
            } else {
                result.add(cur.value);
                nodesToVisit.add(cur.left);
                nodesToVisit.add(cur.right);
            }
        }
        // TODO remove all at once for performance
        while (result.get(result.size() - 1) == null) {
            result.remove(result.size() - 1);
        }
        return result;
    }
}
