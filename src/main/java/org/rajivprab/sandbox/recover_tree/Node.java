package org.rajivprab.sandbox.recover_tree;

import org.rajivprab.cava.Validatec;

class Node {
    final int value;

    Node left;  // TODO make private, add getters
    Node right;

    Node(int value) {
        this.value = value;
    }

    void put(Node child) {
            if (left == null) {
                left = child;
            } else {
                Validatec.isNull(right);
                right = child;
            }
    }
}
