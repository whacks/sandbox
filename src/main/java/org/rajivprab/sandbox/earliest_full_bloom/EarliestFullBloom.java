package org.rajivprab.sandbox.earliest_full_bloom;

import com.google.common.collect.Sets;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.Set;

/**
 * https://leetcode.com/problems/earliest-possible-day-of-full-bloom/
 *
 * You have n flower seeds. Every seed must be planted first before it can begin to grow, then bloom. Planting a seed takes time and so does the growth of a seed. You are given two 0-indexed integer arrays plantTime and growTime, of length n each:
 *
 * plantTime[i] is the number of full days it takes you to plant the ith seed. Every day, you can work on planting exactly one seed. You do not have to work on planting the same seed on consecutive days, but the planting of a seed is not complete until you have worked plantTime[i] days on planting it in total.
 * growTime[i] is the number of full days it takes the ith seed to grow after being completely planted. After the last day of its growth, the flower blooms and stays bloomed forever.
 * From the beginning of day 0, you can plant the seeds in any order.
 *
 * Return the earliest possible day where all seeds are blooming.
 *
 * Hints:
 *
 * - List the planting like the diagram above shows, where a row represents the timeline of a seed. A row i is
 * above another row j if the last day planting seed i is ahead of the last day for seed j. Does it have any
 * advantage to spend some days to plant seed j before completely planting seed i?
 *
 * - No. It does not help seed j but could potentially delay the completion of seed i, resulting in a worse final
 * answer. Remaining focused is a part of the optimal solution.
 *
 * - Sort the seeds by their growTime in descending order. Can you prove why this strategy is the other part of the
 * optimal solution? Note the bloom time of a seed is the sum of plantTime of all seeds preceding this seed plus
 * the growTime of this seed.
 *
 * - There is no way to improve this strategy. The seed to bloom last dominates the final answer. Exchanging the
 * planting of this seed with another seed with either a larger or smaller growTime will result in a potentially
 * worse answer.
 */
public class EarliestFullBloom {

    // Note that this returns the day index of the final bloom. This is one off from the number of days
    public static int solve(List<Integer> plantTime, List<Integer> growTime) {
        return numDaysNeeded(plantTime, growTime, Sets.newHashSet()) - 1;
    }

    // Returns the number of days (not the day index) needed for all seeds to be planted and bloomed.
    // Ignores all seeds in ignoreIndexes
    private static int numDaysNeeded(List<Integer> plantTime, List<Integer> growTime, Set<Integer> ignoreIndexes) {
        if (ignoreIndexes.size() == plantTime.size()) { return 0; }
        int seedToPlant = getSeedIndexWithLongestGrowTime(growTime, ignoreIndexes);

        int seedBloomTime = plantTime.get(seedToPlant) + growTime.get(seedToPlant) + 1;

        ignoreIndexes.add(seedToPlant);
        int remainingSeedsBloomTime = plantTime.get(seedToPlant) + numDaysNeeded(plantTime, growTime, ignoreIndexes);

        return Math.max(seedBloomTime, remainingSeedsBloomTime);
    }

    private static int getSeedIndexWithLongestGrowTime(List<Integer> growTime, Set<Integer> alreadyPlanted) {
        int maxGrowTime = Integer.MIN_VALUE;
        int index = -1;
        for (int i=0; i<growTime.size(); i++) {
            if (!alreadyPlanted.contains(i) && growTime.get(i) > maxGrowTime) {
                index = i;
                maxGrowTime = growTime.get(i);
            }
        }
        Validatec.greaterOrEqual(index, 0);
        return index;
    }
}
