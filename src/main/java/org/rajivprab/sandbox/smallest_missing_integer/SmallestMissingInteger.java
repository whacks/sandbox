package org.rajivprab.sandbox.smallest_missing_integer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

// Given an unsorted integer array, find the smallest missing positive integer.
// Your algorithm should run in O(n) time and uses constant extra space.
class SmallestMissingInteger {
    private static final Log log = LogFactory.getLog(SmallestMissingInteger.class);

    static int getSmallestMissing(List<Integer> input) {
        for (int i=0; i<input.size(); i++) {
            place(input, input.get(i));
        }

        for (int index=0; index<input.size(); index++) {
            int expected = index + 1;
            if (input.get(index) != expected) {
                return expected;
            }
        }

        return input.size() + 1;
    }

    private static void place(List<Integer> input, int value) {
        log.info("Placing " + value + " into " + input);

        int index = value - 1;
        if (index >= input.size()) { return; }
        if (index < 0) { return; }

        int existing = input.get(index);
        input.set(index, value);

        if (existing != value) {
            place(input, existing);
        }
    }
}
