package org.rajivprab.sandbox.room_cleaner;

import java.util.Set;

/**
 * Created by rajivprab on 4/29/17.
 */
public class RoutePlanner {
    private final Map map = new Map();
    private final Mover mover;
    private final Explorer explorer;

    public RoutePlanner(RobotAPI robotAPI) {
        this.mover = new Mover(map, robotAPI);
        this.explorer = new Explorer(map);
    }

    public void run() {
        findWall();
        traceWall();
        cleanAll();
    }

    private void findWall() {
        // TODO go in straight line till we hit a wall
    }

    private void traceWall() {
        // TODO follow wall until we are back where we started
        // TODO also need to check that the wall outline encloses the origin completely
    }

    private void cleanAll() {
        Set<Coordinates> cleanedPoints = mover.getCleanedPoints();
        boolean progressMade = true;
        while (progressMade) {
            progressMade = explorer.getReachablePointsOrderedByDistance(mover.getCurrentLocation()).stream()
                                   .filter(coordinates -> !cleanedPoints.contains(coordinates))
                                   .findFirst()
                                   .map(destination -> explorer.getPath(mover.getCurrentLocation(), destination))
                                   .map(path -> mover.goTo(path.iterator()))
                                   .isPresent();
        }
    }
}
