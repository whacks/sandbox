package org.rajivprab.sandbox.room_cleaner;

/**
 * Created by rajivprab on 4/28/17.
 */
public interface RobotAPI {
    boolean move();
    void turnLeft(int numTurns);
    void turnRight(int numTurns);
    void clean();
}
