package org.rajivprab.sandbox.room_cleaner;

import com.google.common.collect.Sets;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.Validatec;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by rajivprab on 4/29/17.
 */
public class Mover {
    private final Map map;
    private final RobotAPI robotAPI;
    private final Set<Coordinates> cleanedPoints = Sets.newHashSet();

    private Direction currentlyFacing = Direction.NORTH;
    private Coordinates currentLocation = new Coordinates(0, 0);

    public Mover(Map map, RobotAPI robotAPI) {
        this.map = map;
        this.robotAPI = robotAPI;
    }

    public Coordinates getCurrentLocation() {
        return currentLocation;
    }

    public Set<Coordinates> getCleanedPoints() {
        return Collections.unmodifiableSet(cleanedPoints);
    }

    public boolean goTo(Iterator<Coordinates> coordinates) {
        Validate.isTrue(coordinates.hasNext());
        boolean success = true;
        while (success && coordinates.hasNext()) {
            success = goTo(coordinates.next());
        }
        return success;
    }

    public boolean goTo(Coordinates coordinates) {
        Validatec.equals(currentLocation.getDistanceSq(coordinates), 1);
        if (map.getCell(coordinates).equals(Cell.BLOCKED)) {
            return false;
        }
        if (coordinates.x - currentLocation.x == 1) {
            faceEast();
        } else if (coordinates.x - currentLocation.x == -1) {
            faceWest();
        } else if (coordinates.y - currentLocation.y == 1) {
            faceNorth();
        } else if (coordinates.y - currentLocation.y == -1) {
            faceSouth();
        }
        boolean moved = robotAPI.move();
        if (moved) {
            currentLocation = coordinates;
            cleanIfNeeded();
            map.setCell(coordinates, Cell.EMPTY);
            return true;
        } else {
            map.setCell(coordinates, Cell.BLOCKED);
            return false;
        }
    }

    private void cleanIfNeeded() {
        if (!cleanedPoints.contains(currentLocation)) {
            robotAPI.clean();
        }
    }

    private void faceNorth() {
        if (currentlyFacing.equals(Direction.NORTH)) {
        } else if (currentlyFacing.equals(Direction.EAST)) {
            robotAPI.turnLeft(1);
        } else if (currentlyFacing.equals(Direction.SOUTH)) {
            robotAPI.turnLeft(2);
        } else if (currentlyFacing.equals(Direction.WEST)) {
            robotAPI.turnRight(1);
        }
        currentlyFacing = Direction.NORTH;
    }

    private void faceEast() {
        if (currentlyFacing.equals(Direction.NORTH)) {
            robotAPI.turnRight(1);
        } else if (currentlyFacing.equals(Direction.EAST)) {
        } else if (currentlyFacing.equals(Direction.SOUTH)) {
            robotAPI.turnLeft(1);
        } else if (currentlyFacing.equals(Direction.WEST)) {
            robotAPI.turnRight(2);
        }
        currentlyFacing = Direction.EAST;
    }

    private void faceSouth() {
        if (currentlyFacing.equals(Direction.NORTH)) {
            robotAPI.turnRight(2);
        } else if (currentlyFacing.equals(Direction.EAST)) {
            robotAPI.turnRight(1);
        } else if (currentlyFacing.equals(Direction.SOUTH)) {
        } else if (currentlyFacing.equals(Direction.WEST)) {
            robotAPI.turnLeft(1);
        }
        currentlyFacing = Direction.SOUTH;
    }

    private void faceWest() {
        if (currentlyFacing.equals(Direction.NORTH)) {
            robotAPI.turnLeft(1);
        } else if (currentlyFacing.equals(Direction.EAST)) {
            robotAPI.turnRight(2);
        } else if (currentlyFacing.equals(Direction.SOUTH)) {
            robotAPI.turnRight(1);
        } else if (currentlyFacing.equals(Direction.WEST)) {
        }
        currentlyFacing = Direction.WEST;
    }

    private enum Direction {
        EAST, WEST, NORTH, SOUTH
    }
}
