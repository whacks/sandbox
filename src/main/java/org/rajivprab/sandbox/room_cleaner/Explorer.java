package org.rajivprab.sandbox.room_cleaner;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.rajivprab.cava.Validatec;

import java.util.*;

/**
 * Created by rajivprab on 4/29/17.
 */
public class Explorer {
    private final Map map;

    public Explorer(Map map) {
        this.map = map;
    }

    // If a path does exist, it is guaranteed to find it
    // In the absence of barriers, will always find the shortest path
    // In the presence of barriers, reasonable attempts will be made to find shortest path, but not guaranteed
    public List<Coordinates> getPath(Coordinates start, Coordinates dest) {
        LinkedList<Coordinates> path = Lists.newLinkedList();
        getPath(path, Sets.newHashSet(), start, dest);
        return path;
    }

    private boolean getPath(LinkedList<Coordinates> pathSoFar, Set<Coordinates> visitedPoints,
                            Coordinates cur, Coordinates dest) {
        Validatec.doesNotContain(visitedPoints, cur);
        visitedPoints.add(cur);
        pathSoFar.add(cur);
        if (cur.equals(dest)) {
            return true;
        }
        for (Coordinates next : getPointsOrderedByDistance(cur.getNeighbors(), dest)) {
            if (!map.getCell(next).equals(Cell.BLOCKED) && !visitedPoints.contains(next)) {
                if (getPath(pathSoFar, visitedPoints, next, dest)) {
                    return true;
                }
            }
        }
        Coordinates removed = pathSoFar.removeLast();
        Validatec.equals(removed, cur);
        return false;
    }

    public PriorityQueue<Coordinates> getReachablePointsOrderedByDistance(Coordinates coordinates) {
        return getPointsOrderedByDistance(getReachablePoints(), coordinates);
    }

    private PriorityQueue<Coordinates> getPointsOrderedByDistance(Collection<Coordinates> points, Coordinates ref) {
        PriorityQueue<Coordinates> result = new PriorityQueue<>(new DistanceComparator(ref));
        points.forEach(result::add);
        return result;
    }

    // Will avoid adding blocked-points, but will include unknown-points that are potentially blocked
    // Will fail if called with a map that doesn't have a full enclosure of blocked points around the origin
    // Consider caching the results and reusing it if no further map-updates have occurred
    public Collection<Coordinates> getReachablePoints() {
        Set<Coordinates> visitedOrMarkedToVisit = Sets.newHashSet();
        Queue<Coordinates> pointsToVisit = Lists.newLinkedList();
        visitedOrMarkedToVisit.add(new Coordinates(0, 0));
        pointsToVisit.add(new Coordinates(0, 0));
        while (!pointsToVisit.isEmpty()) {
            Coordinates cur = pointsToVisit.remove();
            Validatec.contains(visitedOrMarkedToVisit, cur);
            for (Coordinates neighbor : cur.getNeighbors()) {
                Cell cell = map.getCell(neighbor);
                if (!visitedOrMarkedToVisit.contains(neighbor) && !cell.equals(Cell.BLOCKED)) {
                    visitedOrMarkedToVisit.add(neighbor);
                    pointsToVisit.add(neighbor);
                }
            }
        }
        return visitedOrMarkedToVisit;
    }

    private static class DistanceComparator implements Comparator<Coordinates> {
        private final Coordinates ref;

        public DistanceComparator(Coordinates ref) {
            this.ref = ref;
        }

        // TODO Right ordering of minus sign? Write unit test
        @Override
        public int compare(Coordinates o1, Coordinates o2) {
            return o1.getDistanceSq(ref) - o2.getDistanceSq(ref);
        }
    }
}
