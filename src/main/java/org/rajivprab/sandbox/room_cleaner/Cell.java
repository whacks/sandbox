package org.rajivprab.sandbox.room_cleaner;

/**
 * Created by rajivprab on 4/28/17.
 */
public enum Cell {
    UNKNOWN, BLOCKED, EMPTY
}
