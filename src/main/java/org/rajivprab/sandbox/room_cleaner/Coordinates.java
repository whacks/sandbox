package org.rajivprab.sandbox.room_cleaner;

import com.google.common.collect.ImmutableList;

import java.util.Collection;

/**
 * Created by rajivprab on 4/29/17.
 */
public class Coordinates {
    public final int x;
    public final int y;

    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordinates)) return false;

        Coordinates that = (Coordinates) o;

        if (x != that.x) return false;
        return y == that.y;
    }

    public Collection<Coordinates> getNeighbors() {
        return ImmutableList.of(
                new Coordinates(x + 1, y),
                new Coordinates(x - 1, y),
                new Coordinates(x, y + 1),
                new Coordinates(x, y - 1));
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    public int getDistanceSq(Coordinates coordinates) {
        int horizontal = this.x - coordinates.x;
        int vertical = this.y - coordinates.y;
        return horizontal * horizontal + vertical * vertical;
    }

    @Override
    public String toString() {
        return "Coordinates{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
