package org.rajivprab.sandbox.room_cleaner;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.Validate;
import org.javatuples.Pair;

import java.util.List;

/**
 * Created by rajivprab on 4/28/17.
 */
public class Map {
    // Cell cell = map.get(x_index).get(y_index);
    private final List<List<Cell>> map = Lists.newArrayList(Lists.newArrayList());

    private Pair<Integer, Integer> originIndex = Pair.with(0, 0);

    public Map() {
        setCell(new Coordinates(0, 0), Cell.EMPTY);
    }

    public Cell getCell(Coordinates coordinates) {
        try {
            return map.get(getXIndex(coordinates)).get(getYIndex(coordinates));
        } catch (IndexOutOfBoundsException e) {
            throw new OutOfMapException("Cannot get " + coordinates);
        }
    }

    public Map setCell(Coordinates coordinates, Cell cell) {
        while (getXIndex(coordinates) < 0) {
            appendColumnLeft();
        }
        while (getXIndex(coordinates) >= map.size()) {
            appendColumnRight();
        }
        List<Cell> column = map.get(getXIndex(coordinates));
        while (getYIndex(coordinates) < 0) {
            appendRowBottom();
        }
        while (getYIndex(coordinates) >= column.size()) {
            appendRowTop(coordinates);
        }
        Cell existing = getCell(coordinates);
        Validate.isTrue(existing.equals(Cell.UNKNOWN) || existing.equals(cell));
        map.get(getXIndex(coordinates)).set(getYIndex(coordinates), cell);
        return this;
    }

    private int getYIndex(Coordinates coordinates) {
        return coordinates.y + originIndex.getValue1();
    }

    private int getXIndex(Coordinates coordinates) {
        return coordinates.x + originIndex.getValue0();
    }

    private void appendRowTop(Coordinates coordinates) {
        map.get(getXIndex(coordinates)).add(Cell.UNKNOWN);
    }

    private void appendRowBottom() {
        for (int x = 0; x < map.size(); x++) {
            map.get(x).add(0, Cell.UNKNOWN);
        }
        originIndex = Pair.with(originIndex.getValue0(), originIndex.getValue1() + 1);
    }

    private void appendColumnRight() {
        map.add(Lists.newArrayList());
    }

    private void appendColumnLeft() {
        map.add(0, Lists.newArrayList());
        originIndex = Pair.with(originIndex.getValue0() + 1, originIndex.getValue1());
    }

    public static class OutOfMapException extends RuntimeException {
        public OutOfMapException(String message) {
            super(message);
        }
    }
}
