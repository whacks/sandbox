package org.rajivprab.sandbox.one_four_three;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import java.util.*;

public class Predictions {
    private static final Multimap<Integer, Character> NUMBER_TO_CHARACTERS =
            ImmutableMultimap.<Integer, Character>builder()
                             .put(2, 'a')
                             .put(2, 'b')
                             .put(2, 'c')
                             .put(3, 'd')
                             .put(3, 'e')
                             .put(3, 'f')
                             .put(4, 'g')
                             .put(4, 'h')
                             .put(4, 'i')
                             .put(5, 'j')
                             .put(5, 'k')
                             .put(5, 'l')
                             .put(6, 'm')
                             .put(6, 'n')
                             .put(6, 'o')
                             .put(7, 'p')
                             .put(7, 'q')
                             .put(7, 'r')
                             .put(7, 's')
                             .put(8, 't')
                             .put(8, 'u')
                             .put(8, 'v')
                             .put(9, 'w')
                             .put(9, 'x')
                             .put(9, 'y')
                             .put(9, 'z')
                             .build();

    public static Collection<String> getLetterCombinationsFromStdin() {
        String input = new Scanner(System.in).next();
        LinkedList<Integer> intInput = new LinkedList<>();
        for (int i=0; i<input.length(); i++) {
            char cur = input.charAt(i);
            intInput.add(Integer.valueOf(cur + ""));
        }
        return getLetterCombinations(intInput);
    }

    static Collection<String> getLetterCombinations(LinkedList<Integer> input) {
        if (input.size() == 1) {
            return NUMBER_TO_CHARACTERS.get(input.poll()).stream()
                                       .map(String::valueOf)
                                       .collect(ImmutableList.toImmutableList());
        }
        int prefix = input.pollFirst();
        Collection<String> suffixCombinations = getLetterCombinations(input);
        List<String> combinations = Lists.newArrayList();
        for (char prefixChar : NUMBER_TO_CHARACTERS.get(prefix)) {
            suffixCombinations.stream().map(suffix -> prefixChar + suffix).forEach(combinations::add);
        }
        return ImmutableList.copyOf(combinations);
    }
}
