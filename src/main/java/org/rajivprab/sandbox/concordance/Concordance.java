package org.rajivprab.sandbox.concordance;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Instructions:
 * 1. Copy-paste this entire file into your favorite Java 1.7 IDE (I used Intellij)
 * 2. Include Guava, Apache Commons and JUnit as your dependencies if not already done so
 * 3. Change the package reference given in the very first line of this file
 * 4. Run all the unit tests shown in ConcordanceTest
 *
 * General algorithm flow:
 * 1. Normalize the text. Convert problematic words into special sequences
 * 2. Break down the text into sentences, using sentence delimiters
 * 3. Break down the sentences into words, after removing unneeded punctuation, using \s as delimiter
 * 4. Convert individual special-sequence words back into their original form
 * 5. Parse each word and track all statistics
 * 6. Output statistics
 *
 * Potential problems:
 * All problematic words, like i.e. need to be hardcoded.
 * No special handling provided for quotations, since it's unclear how sentences within quotes should be counted.
 * Proper grammar rules assumed: Commas, colons etc should be followed by a space, before the next word
 *
 * Started reading the problems at 10.32pm. It is now 12.28am.
 * Most of my time was spent trying to handle the special cases described above, in as elegant a way as possible
 *
 * Created by rprabhakar on 3/29/16.
 */
class Concordance {
    private static final BiMap<String, String> SPECIAL_WORDS_ENCODE = ImmutableBiMap.of(
            "i\\.e\\.", "iespecialsequence",
            "e\\.g\\.", "egspecialsequence"
    );

    private static final BiMap<String, String> SPECIAL_WORDS_DECODE = ImmutableBiMap.of(
            "iespecialsequence", "i.e.",
            "egspecialsequence", "e.g."
    );

    final Map<String, Entry> concordance = Maps.newHashMap();

    public Concordance(String text) {
        text = normalizeText(text);
        List<String> sentences = decomposeIntoSentences(text);
        parseSentences(sentences);
    }

    public void printConcordance() {
        for (Entry entry : getConcordance()) {
            System.out.println(entry);
        }
    }

    public List<Entry> getConcordance() {
        List<Entry> entries = Lists.newArrayList(concordance.values());
        Collections.sort(entries);
        return entries;
    }

    private static String normalizeText(String text) {
        text = text.toLowerCase();
        text = text.replaceAll("[\n\t]", " ");
        for (String specialSequence : SPECIAL_WORDS_ENCODE.keySet()) {
            text = text.replaceAll(specialSequence, SPECIAL_WORDS_ENCODE.get(specialSequence));
        }
        return text;
    }

    private static List<String> decomposeIntoSentences(String text) {
        return Lists.newArrayList(text.split("[!\\?\\.] "));
    }

    private void parseSentences(List<String> sentences) {
        for (int i=0; i<sentences.size(); i++) {
            for (String word : getWords(sentences.get(i))) {
                if (!word.trim().isEmpty()) {
                    getEntry(word).addSentenceSeen(i + 1);
                }
            }
        }
    }

    private Collection<String> getWords(String sentence) {
        // Deletes all non-alphabetic/space characters, to eliminate punctuation and any non-word characters
        sentence = sentence.replaceAll("[^a-zA-Z ]", "");
        List<String> words = Lists.newArrayList(sentence.split(" "));
        for (int i=0; i<words.size(); i++) {
            String word = words.get(i);
            String transformed = SPECIAL_WORDS_DECODE.get(word);
            if (transformed != null) {
                words.set(i, transformed);
            }
        }
        return words;
    }

    private Entry getEntry(String word) {
        Entry entry = concordance.get(word);
        if (entry == null) {
            entry = new Entry(word);
            concordance.put(word, entry);
        }
        return entry;
    }

    public static class Entry implements Comparable<Entry> {
        private final String word;
        private final List<Integer> sentences = Lists.newArrayList();

        public Entry(String word) {
            // Assuming that all entries can only contain alphabetic characters.
            // Exception made only for "i.e." Any other exceptions will need to be hard-coded here
            Validate.isTrue(StringUtils.isAlpha(word) || SPECIAL_WORDS_DECODE.values().contains(word),
                    "Only alphabets allowed in a word. Found: '" + word + "'");
            Validate.isTrue(word.toLowerCase().equals(word), "Expecting word to be lower-case for consistency: " + word);
            this.word = word;
        }

        public String getWord() {
            return word;
        }

        public int getFrequency() {
            return sentences.size();
        }

        public List<Integer> getSentences() {
            return Lists.newArrayList(sentences);
        }

        public void addSentenceSeen(int sentenceSeen) {
            sentences.add(sentenceSeen);
        }

        @Override
        public String toString() {
            return word + " {" + getFrequency() + ": " + sentences + "}";
        }

        @Override
        public int compareTo(Entry o) {
            return getWord().compareTo(o.getWord());
        }
    }

}
