package org.rajivprab.sandbox.password_entropy;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Password must be a 9-digit number.
 *
 * Extra restriction: include at least 4 different numbers but cannot include the same number more than three times
 *
 * How much is the entropy reduction due to the extra restriction?
 *
 * Created by rajivprab on 7/13/17.
 */
public class PasswordRunner {
    public static void main(String... args) {
        int numTrials = 1000000;
        Instant start = Instant.now();
        int numRejects = (int) IntStream.range(0, numTrials)    // .parallel()
                                        .filter(i -> !meetsRestriction(getRandomPassword())).count();
        Instant end = Instant.now();
        System.out.println("Percentage rejects: " + numRejects * 100.0 / numTrials +
                                   ", in time: " + Duration.between(start, end));
    }

    private static String getRandomPassword() {
        return RandomStringUtils.randomNumeric(9);
    }

    private static boolean meetsRestriction(String password) {
        Map<Integer, Integer> counts = Maps.newHashMap();
        password.chars().forEach(cur -> counts.put(cur, counts.getOrDefault(cur, 0) + 1));
        return (counts.keySet().size() >= 4) && counts.values().stream().noneMatch(count -> count > 3);
    }
}
