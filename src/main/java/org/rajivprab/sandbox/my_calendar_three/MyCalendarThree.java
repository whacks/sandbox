package org.rajivprab.sandbox.my_calendar_three;

import org.rajivprab.cava.Validatec;

import java.time.Duration;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

class MyCalendarThree {
    private final NavigableMap<Integer, Integer> timeToCount = new TreeMap<>();

    int book(int start, int end) {
        Entry<Integer, Integer> mostRecentPastEntry = timeToCount.floorEntry(start);
        if (mostRecentPastEntry == null) {
            timeToCount.put(start, 0);
        } else {

            int mostRecentPastTime = mostRecentPastEntry.getKey();

            long profileInnerStart = System.nanoTime();
            Validatec.greaterOrEqual(start, mostRecentPastTime);
            long profileInnerMid = System.nanoTime();
            Validatec.greaterOrEqual(start, mostRecentPastTime);
            long profileInnerEnd = System.nanoTime();

            System.out.println("Validatec.greaterOrEqual 1st time took " + Duration.ofNanos(profileInnerMid - profileInnerStart));
            System.out.println("Validatec.greaterOrEqual 2nd time took " + Duration.ofNanos(profileInnerEnd - profileInnerMid));

            timeToCount.put(start, mostRecentPastEntry.getValue()); // Noop if mostRecentPastEntry.getKey() == start
        }

        int lastCount = Integer.MIN_VALUE;
        for (Entry<Integer, Integer> entry : timeToCount.subMap(start, end + 1).entrySet()) {
            lastCount = entry.getValue() + 1;
            timeToCount.put(entry.getKey(), lastCount);
        }

        timeToCount.put(end, lastCount - 1);
        // System.out.println("Updated values: " + timeToCount.entrySet());

        // If we don't need to worry about removing bookings, then we can technically store the max value as an
        // instance variable, and then search only the updated entries to see if we have a new max-value
        return timeToCount.values().stream().mapToInt(e -> e).max().getAsInt();
    }
}
