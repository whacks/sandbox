package org.rajivprab.sandbox.eight_queens;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.Validatec;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Implementation of a chess board, with pieces on it.
 * Not thread safe.
 *
 * Created by rajivprab on 4/14/17.
 */
public class Board {
    public static final int MAX_WIDTH = 8;
    private static final Set<Tile> ALL_TILES = getAllTiles();
    private final static Map<Tile, ImmutableSet<Tile>> CACHED_TILE_TO_TILES_UNDER_FIRE = Maps.newHashMap();

    private final Set<Tile> filledTiles = Sets.newHashSet();

    public int getNumPieces() {
        return filledTiles.size();
    }

    public void removePiece(Tile tile) {
        Validatec.contains(filledTiles, tile);
        filledTiles.remove(tile);
    }

    public void addPiece(Tile tile) {
        Validate.isTrue(isValidTile(tile));
        Validatec.doesNotContain(filledTiles, tile);
        // Performance optimization: Validatec.doesNotContain(getTilesUnderFire(), tile);
        filledTiles.add(tile);
    }

    public Collection<Tile> getOpenTiles() {
        return Sets.difference(ALL_TILES, getTilesUnderFire());
    }

    private Set<Tile> getTilesUnderFire() {
        return filledTiles.stream().flatMap(tile -> getTilesUnderFire(tile).stream()).collect(Collectors.toSet());
    }

    private static Set<Tile> getTilesUnderFire(Tile tile) {
        ImmutableSet<Tile> result = CACHED_TILE_TO_TILES_UNDER_FIRE.get(tile);
        if (result == null) {
            result = ImmutableSet.<Tile>builder()
                    .addAll(getDiagonalTiles(tile))
                    .addAll(getHorizontalTiles(tile))
                    .addAll(getVerticalTiles(tile))
                    .build();
            CACHED_TILE_TO_TILES_UNDER_FIRE.putIfAbsent(tile, result);
        }
        return result;
    }

    private static Set<Tile> getDiagonalTiles(Tile tile) {
        Set<Tile> tiles = Sets.newHashSet(new Tile(tile.x, tile.y));
        for (int x=tile.x, y=tile.y; isValidTile(x, y); x++, y++) {
            tiles.add(new Tile(x, y));
        }
        for (int x=tile.x, y=tile.y; isValidTile(x, y); x--, y--) {
            tiles.add(new Tile(x, y));
        }
        for (int x=tile.x, y=tile.y; isValidTile(x, y); x++, y--) {
            tiles.add(new Tile(x, y));
        }
        for (int x=tile.x, y=tile.y; isValidTile(x, y); x--, y++) {
            tiles.add(new Tile(x, y));
        }
        return tiles;
    }

    private static Set<Tile> getVerticalTiles(Tile tile) {
        return IntStream.range(0, MAX_WIDTH).mapToObj(y -> new Tile(tile.x, y)).collect(Collectors.toSet());
    }

    private static Set<Tile> getHorizontalTiles(Tile tile) {
        return IntStream.range(0, MAX_WIDTH).mapToObj(x -> new Tile(x, tile.y)).collect(Collectors.toSet());
    }

    private static boolean isValidTile(Tile tile) {
        return isValidTile(tile.x, tile.y);
    }

    private static boolean isValidTile(int x, int y) {
        return x < MAX_WIDTH && y < MAX_WIDTH && x >= 0 && y >= 0;
    }

    private static Set<Tile> getAllTiles() {
        ImmutableSet.Builder<Tile> tiles = ImmutableSet.builder();
        for (int x = 0; x < MAX_WIDTH; x++) {
            for (int y=0; y<MAX_WIDTH; y++) {
                tiles.add(new Tile(x, y));
            }
        }
        return tiles.build();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("\n|-------------------------------|\n");
        for (int y=0; y<MAX_WIDTH; y++) {
            builder.append("|");
            for (int x=0; x<MAX_WIDTH; x++) {
                builder.append(filledTiles.contains(new Tile(x, y)) ? " Q |" : "   |");
            }
            builder.append("\n|-------------------------------|\n");
        }
        return builder.toString();
    }
}
