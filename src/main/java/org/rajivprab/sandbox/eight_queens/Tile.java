package org.rajivprab.sandbox.eight_queens;

import org.rajivprab.cava.Validatec;

/**
 * Created by rajivprab on 4/14/17.
 */
public class Tile {
    public final int x;
    public final int y;

    public Tile(int x, int y) {
        this.x = x;
        this.y = y;
        Validatec.greaterThan(x, -1);
        Validatec.greaterThan(y, -1);
        Validatec.greaterThan(Board.MAX_WIDTH, x);
        Validatec.greaterThan(Board.MAX_WIDTH, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tile)) return false;

        Tile tile = (Tile) o;

        if (x != tile.x) return false;
        return y == tile.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
