package org.rajivprab.sandbox.eight_queens;

import org.rajivprab.cava.Validatec;

import java.time.Duration;
import java.time.Instant;

/**
 * Solver for placing 8-queens in a chess board, without any of them being able to capture the other.
 *
 * Created by rajivprab on 4/14/17.
 */
public class EightQueens {
    private static final int NUM_PIECES = 8;

    private final Board board = new Board();

    public static void main(String... args) {
        Instant start = Instant.now();
        System.out.println(new EightQueens().solve());
        System.out.println("Found solution in: " + Duration.between(start, Instant.now()));
    }

    public Board solve() {
        Validatec.isTrue(populatePiece(), "Unable to find solution for " + NUM_PIECES + " pieces!");
        return board;
    }

    private boolean populatePiece() {
        if (board.getNumPieces() == NUM_PIECES) { return true; }
        Validatec.greaterThan(NUM_PIECES, board.getNumPieces());
        for (Tile tile : board.getOpenTiles()) {
            board.addPiece(tile);
            if (populatePiece()) { return true; }
            board.removePiece(tile);
        }
        return false;
    }
}
