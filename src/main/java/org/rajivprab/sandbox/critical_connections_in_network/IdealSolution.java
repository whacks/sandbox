package org.rajivprab.sandbox.critical_connections_in_network;

import com.google.common.collect.Sets;

import java.util.*;
import java.util.stream.Collectors;

/**
 * https://leetcode.com/problems/critical-connections-in-a-network/discuss/382638/DFS-detailed-explanation-O(orEor)-solution
 *
 * First thought
 * Thiking for a little while, you will easily find out this theorem on a connected graph:
 *
 * An edge is a critical connection, if and only if it is not in a cycle.
 * So, if we know how to find cycles, and discard all edges in the cycles, then the remaining connections are a
 * complete collection of critical connections.
 *
 * How to find eges in cycles, and remove them
 * We will use DFS algorithm to find cycles and decide whether or not an edge is in a cycle.
 *
 * Define rank of a node: The depth of a node during a DFS. The starting node has a rank 0.
 *
 * Only the nodes on the current DFS path have non-special ranks. In other words, only the nodes that we've started
 * visiting, but haven't finished visiting, have ranks. So 0 <= rank < n.
 *
 * (For coding purpose, if a node is not visited yet, it has a special rank -2; if we've fully completed the visit of
 * a node, it has a special rank n.)
 *
 * How can "rank" help us with removing cycles? Imagine you have a current path of length k during a DFS. The nodes
 * on the path has increasing ranks from 0 to kand incrementing by 1. Surprisingly, your next visit finds a node that
 * has a rank of p where 0 <= p < k. Why does it happen? Aha! You found a node that is on the current search path!
 * That means, congratulations, you found a cycle!
 *
 * But only the current level of search knows it finds a cycle. How does the upper level of search knows, if you
 * backtrack? Let's make use of the return value of DFS: dfs function returns the minimum rank it finds. During a
 * step of search from node u to its neighbor v, if dfs(v) returns something smaller than or equal to rank(u), then u
 * knows its neighbor v helped it to find a cycle back to u or u's ancestor. So u knows it should discard the edge
 * (u, v) which is in a cycle.
 *
 * After doing dfs on all nodes, all edges in cycles are discarded. So the remaining edges are critical connections.
 */
class IdealSolution {
    static Set<Set<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        List<Integer>[] graph = new ArrayList[n];
        for (int i = 0; i < n; i++) {
            graph[i] = new ArrayList<>();
        }
        for(List<Integer> oneConnection :connections) {
            graph[oneConnection.get(0)].add(oneConnection.get(1));
            graph[oneConnection.get(1)].add(oneConnection.get(0));
        }
        HashSet<List<Integer>> connectionsSet = new HashSet<>(connections);
        int[] rank = new int[n];
        Arrays.fill(rank, -2);
        dfs(graph, 0, 0, rank, connectionsSet);

        return connectionsSet.stream().map(l -> Sets.newHashSet(l)).collect(Collectors.toSet());
    }

    private static int dfs(List<Integer>[] graph, int node, int depth, int[] rank, HashSet<List<Integer>> connectionsSet){
        if (rank[node]>=0){
            return rank[node]; // already visited node. return its rank
        }
        rank[node] = depth;
        int minDepthFound = Integer.MAX_VALUE;
        for(Integer neighbor: graph[node]){
            if (rank[neighbor] == depth-1){ // ignore parent
                continue;
            }
            int minDepth = dfs(graph, neighbor, depth+1, rank, connectionsSet);
            minDepthFound = Math.min(minDepthFound, minDepth);
            if (minDepth <= depth){
                // to avoid the sorting just try to remove both combinations. of (x,y) and (y,x)
                connectionsSet.remove(Arrays.asList(node, neighbor));
                connectionsSet.remove(Arrays.asList(neighbor, node));
            }
        }
        return minDepthFound;
    }
}
