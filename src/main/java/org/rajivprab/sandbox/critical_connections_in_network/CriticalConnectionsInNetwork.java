package org.rajivprab.sandbox.critical_connections_in_network;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.Validatec;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

class CriticalConnectionsInNetwork {
    static Set<Set<Integer>> criticalConnections(int n, Collection<List<Integer>> connections) {
        Set<Set<Integer>> result = Sets.newHashSet();
        CriticalConnectionsInNetwork solver = new CriticalConnectionsInNetwork(n, connections);
        for (Collection<Integer> connection : connections) {
            if (solver.isCritical(connection)) {
                result.add(Sets.newHashSet(connection));
            }
        }
        return result;
    }

    private final Multimap<Integer, Integer> connections = HashMultimap.create();

    private CriticalConnectionsInNetwork(int n, Collection<List<Integer>> connections) {
        for (List<Integer> connection : connections) {
            Validatec.size(connection, 2);
            addConnection(connection.get(0), connection.get(1));
        }
        Validatec.size(this.connections.keySet(), n);
    }

    boolean isCritical(Collection<Integer> connection) {
        int a = Iterables.get(connection, 0);
        int b = Iterables.getLast(connection);

        removeConnection(a, b);
        boolean isCritical = !pathExists(a, b, Sets.newHashSet());
        addConnection(a, b);
        return isCritical;
    }

    private boolean pathExists(int source, int dest, Set<Integer> visitedNodes) {
        Validatec.notEquals(source, dest);
        if (connections.containsEntry(source, dest)) { return true; }
        visitedNodes.add(source);
        for (int neighbor: connections.get(source)) {
            if (!visitedNodes.contains(neighbor)) {
                if (pathExists(neighbor, dest, visitedNodes)) { return true; }
            }
        }
        return false;
    }

    private void addConnection(int a, int b) {
        connections.put(a, b);
        connections.put(b, a);
    }

    private void removeConnection(int a, int b) {
        connections.remove(a, b);
        connections.remove(b, a);
    }
}
