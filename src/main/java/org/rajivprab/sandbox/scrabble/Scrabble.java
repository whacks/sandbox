package org.rajivprab.sandbox.scrabble;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Given a list of words, list of  single letters (might be repeating) and score of every character.
 *
 * Return the maximum score of any valid set of words formed by using the given letters (words[i] cannot be used two
 * or more times).
 *
 * It is not necessary to use all characters in letters and each letter can only be used once. Score of letters 'a',
 * 'b', 'c', ... ,'z' is given by score[0], score[1], ... , score[25] respectively.
 *
 * https://leetcode.com/problems/maximum-score-words-formed-by-letters/
 */
public class Scrabble {
    private final List<Integer> scores;
    private final Map<String, Integer> wordScores = Maps.newHashMap();

    private Scrabble(List<Integer> scores) {
        this.scores = ImmutableList.copyOf(scores);
    }

    public static int getMaxScore(List<String> words, List<String> letters, List<Integer> scores) {
        return new Scrabble(scores).getMaxScore(Lists.newLinkedList(words), Lists.newLinkedList(letters));
    }

    private int getMaxScore(List<String> words, List<String> letters) {
        if (words.isEmpty()) { return 0; }

        String candidate = words.remove(0);
        int scoreWithoutCandidate = getMaxScore(Lists.newLinkedList(words), Lists.newLinkedList(letters));

        Optional<List<String>> lettersRemoved = removeLetters(candidate, letters);
        if (!lettersRemoved.isPresent()) { return scoreWithoutCandidate; }
        int scoreWithCandidate = getMaxScore(words, lettersRemoved.get()) + getScore(candidate);

        return Math.max(scoreWithCandidate, scoreWithoutCandidate);
    }

    private static Optional<List<String>> removeLetters(String word, List<String> letters) {
        // TODO Use HashMap to make lookup and removal more efficient
        for (char letter : word.toCharArray()) {
            // TODO more efficient way to do char->string conversion?
            boolean exists = letters.remove(letter + "");
            if (!exists) { return Optional.empty(); }
        }

        return Optional.of(letters);
    }

    private int getScore(String word) {
        Integer cached = wordScores.get(word);
        if (cached != null) { return cached; }

        int score = 0;
        for (char letter : word.toCharArray()) {
            int index = letter - 'a';
            score += scores.get(index);
        }

        wordScores.put(word, score);
        return score;
    }
}