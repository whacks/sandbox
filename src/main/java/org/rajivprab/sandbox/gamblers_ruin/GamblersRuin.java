package org.rajivprab.sandbox.gamblers_ruin;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.LongStream;

/**
 * Simulate your likelihood of winning/losing money based on
 * specific stack size, odds of winning, and when you decide to walk away
 */
public class GamblersRuin {
    private static final int STACK = 200;
    private static final int WALK_AWAY_POINT = 2*STACK;
    private static final int BET = 40;
    private static final double ODDS_OF_WINNING = 0.493;

    private static final long NUM_SIMULATIONS = 10000000;

    public static void main(String... args) {
        long numWins = LongStream.range(0, NUM_SIMULATIONS).parallel().filter(i -> isSuccess()).count();
        System.out.println("Percentage won: " + numWins * 100.0 / NUM_SIMULATIONS + "%");
    }

    private static boolean isSuccess() {
        int curStack = STACK;
        while (curStack > 0) {
            if (curStack >= WALK_AWAY_POINT) {
                return true;
            }

            if (wonBet()) {
                curStack += BET;
            } else {
                curStack -= BET;
            }
        }
        return false;
    }

    private static boolean wonBet() {
        return ThreadLocalRandom.current().nextDouble() < ODDS_OF_WINNING;
    }
}
