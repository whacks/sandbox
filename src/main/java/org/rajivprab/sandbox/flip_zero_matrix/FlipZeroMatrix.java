package org.rajivprab.sandbox.flip_zero_matrix;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.Map;
import java.util.Set;

class FlipZeroMatrix {
    // TODO find way to better reuse across multiple instances of same problem
    // TODO verify that List<List<>> handles hashCode and equals correctly
    private final Map<List<List<Boolean>>, Integer> results = Maps.newHashMap();
    private final Set<List<List<Boolean>>> visited = Sets.newHashSet();

    // TODO Create a matrix class
    static int minStepsNeeded(List<List<Boolean>> matrix) {
        int stepsNeeded = new FlipZeroMatrix().minStepsNeededHelper(matrix);
        return stepsNeeded == Integer.MAX_VALUE ? -1 : stepsNeeded;
    }

    // Returns Integer.MAX_VALUE if no result exists
    private int minStepsNeededHelper(List<List<Boolean>> matrix) {
        if (isZero(matrix)) {
            return 0;
        }
        if (results.containsKey(matrix)) {
            return results.get(matrix);
        }
        if (!visited.add(matrix)) {
            throw new VisitedException();
        }

        int minSteps = Integer.MAX_VALUE;
        for (int row=0; row<getRows(matrix); row++) {
            for (int col=0; col<getColumns(matrix); col++) {
                // TODO optimization. Instead of forking, make all changes, then undo changes later
                List<List<Boolean>> forkedMatrix = clone(matrix);
                flipAll(forkedMatrix, row, col);
                try {
                    int childStepsNeeded = minStepsNeededHelper(forkedMatrix);
                    if (childStepsNeeded != Integer.MAX_VALUE) {
                        if (minSteps > childStepsNeeded + 1) {
                            minSteps = childStepsNeeded + 1;
                        }
                    }
                } catch (VisitedException ignored) {}
            }
        }
        results.put(matrix, minSteps);
        return minSteps;
    }

    private static class VisitedException extends RuntimeException {}

    private static List<List<Boolean>> clone(List<List<Boolean>> matrix) {
        List<List<Boolean>> clone = Lists.newArrayList();
        for (List<Boolean> row : matrix) {
            clone.add(Lists.newArrayList(row));
        }
        return clone;
    }

    private static void flipAll(List<List<Boolean>> matrix, int row, int col) {
        flip(matrix, row, col);
        flip(matrix, row-1, col);
        flip(matrix, row+1, col);
        flip(matrix, row, col-1);
        flip(matrix, row, col+1);
    }

    private static void flip(List<List<Boolean>> matrix, int row, int col) {
        try {
            boolean original = matrix.get(row).get(col);
            boolean updated = !original;
            matrix.get(row).set(col, updated);
        } catch (IndexOutOfBoundsException ignored) {}
    }

    private static int getColumns(List<List<Boolean>> matrix) {
        // TODO Check that all rows have same number of columns
        Validatec.notEmpty(matrix);
        return matrix.get(0).size();
    }

    private static int getRows(List<List<Boolean>> matrix) {
        return matrix.size();
    }

    private static boolean isZero(List<List<Boolean>> matrix) {
        for (List<Boolean> row : matrix) {
            for (boolean cell : row) {
                if (cell) { return false; }
            }
        }
        return true;
    }
}
