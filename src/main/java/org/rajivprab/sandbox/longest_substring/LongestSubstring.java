package org.rajivprab.sandbox.longest_substring;

import java.util.Iterator;

/**
 * Given a stream of characters, find the longest substring that contains at most M distinct characters
 *
 * Note: Solution given here was my 1st attempt at solving this problem.
 * It works for reasonable m, but for pathological cases like m=24,
 * it will lead to an explosion of active-running-tallies, thus blowing up the time complexity and memory space.
 *
 * Recommended alternative solution that gives length of longest substring in guaranteed O(N) time:
 * For each character, track the index at which it was last seen.
 * If we're currently at index 20, with m=3, and the highest values in the index are 20, 17, 15, 11 ...
 * then the cur-longest length is (20-15+1) = 6
 * Though to be fair, if there are O(N) distinct "characters",
 * then this approach can take n^2 time and N memory-storage, even for very small values of m
 *
 * Created by rajivprab on 12/21/16.
 */
public class LongestSubstring {
    public static String getLongestSubstring(Iterator<Character> chars, int m) {
        AllTallies tallies = new AllTallies(m);
        while (chars.hasNext()) {
            tallies.addCharacter(chars.next());
        }
        return tallies.getChampion();
    }
}
