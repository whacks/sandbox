package org.rajivprab.sandbox.longest_substring;

import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by rajivprab on 12/21/16.
 */
class AllTallies {
    private final Collection<SingleRunningTally> activeTallies = Lists.newLinkedList();
    private final int maxDistinctCharacters;

    private String champion = "";   // Updated lazily

    public AllTallies(int maxDistinctCharacters) {
        this.maxDistinctCharacters = maxDistinctCharacters;
    }

    public AllTallies addCharacter(char newChar) {
        activeTallies.forEach(tally -> tally.addCharacter(newChar));
        Collection<SingleRunningTally> deadTallies = activeTallies.stream().filter(tally -> !tally.isStillValid())
                .collect(Collectors.toList());
        deadTallies.forEach(this::updateChampionIfNeeded);
        activeTallies.removeAll(deadTallies);
        if (!activeTallies.stream().anyMatch(tally -> tally.containsOnlyCharacter(newChar))) {
            activeTallies.add(new SingleRunningTally(newChar, maxDistinctCharacters));
        }
        return this;
    }

    public String getChampion() {
        activeTallies.forEach(this::updateChampionIfNeeded);
        return champion;
    }

    private void updateChampionIfNeeded(SingleRunningTally tally) {
        if (tally.getLength() > champion.length()) {
            champion = tally.getValidSubstring();
        }
    }
}
