package org.rajivprab.sandbox.longest_substring;

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Created by rajivprab on 12/21/16.
 */
class SingleRunningTally {
    private final int maxDistinctCharacters;
    private final Set<Character> charsSeen = Sets.newHashSet();
    private final StringBuilder validSubstring = new StringBuilder();

    public SingleRunningTally(char startCharacter, int maxDistinctCharacters) {
        this.maxDistinctCharacters = maxDistinctCharacters;
        charsSeen.add(startCharacter);
        validSubstring.append(startCharacter);
    }

    // Only appends to substring if we're within the bounds
    public SingleRunningTally addCharacter(char character) {
        charsSeen.add(character);
        if (isStillValid()) {
            validSubstring.append(character);
        }
        return this;
    }

    public boolean isStillValid() {
        return charsSeen.size() <= maxDistinctCharacters;
    }

    public int getLength() {
        return validSubstring.length();
    }

    public String getValidSubstring() {
        return validSubstring.toString();
    }

    public boolean containsOnlyCharacter(char canary) {
        return charsSeen.size() == 1 && charsSeen.contains(canary);
    }
}
