package org.rajivprab.sandbox.strong_password_checker;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.Objects;

/**
 * https://leetcode.com/problems/strong-password-checker/
 *
 * A password is considered strong if the below conditions are all met:
 *
 * It has at least 6 characters and at most 20 characters.
 * It contains at least one lowercase letter, at least one uppercase letter, and at least one digit.
 * It does not contain three repeating characters in a row (i.e., "Baaabb0" is weak, but "Baaba0" is strong).
 * Given a string password, return the minimum number of steps required to make password strong. if password is already strong, return 0.
 *
 * In one step, you can:
 *
 * Insert one character to password,
 * Delete one character from password, or
 * Replace one character of password with another character.
 */
public class StrongPasswordChecker {
    public static int minChangesNeeded(String candidate) {
        if (candidate.length() >= 6 && candidate.length() <= 20) {
            return Math.max(numSpecialCharactersMissing(candidate), mutationsNeededToBreakSequences(sequences(candidate)));
        }
        if (candidate.length() < 6) {
            int numInsertsNeeded = 6 - candidate.length();
            numInsertsNeeded = Math.max(numInsertsNeeded, numSpecialCharactersMissing(candidate));
            numInsertsNeeded = Math.max(numInsertsNeeded, insertsNeededToBreakSequences(candidate));
            return numInsertsNeeded;
        }
        Validatec.greaterOrEqual(candidate.length(), 21);
        int numDeletesNeeded = candidate.length() - 20;
        int numMutationsNeeded = Math.max(numSpecialCharactersMissing(candidate), mutationsNeededToBreakSequencesAfterDelete(candidate, numDeletesNeeded));
        return numDeletesNeeded + numMutationsNeeded;
    }

    private static int mutationsNeededToBreakSequencesAfterDelete(String candidate, int numDeletions) {
        List<Integer> sequences = sequences(candidate);
        while (numDeletions > 0 && !sequences.isEmpty()) {
            numDeletions--;
            Integer sequenceToDelete = getSequenceToDelete(sequences);
            boolean removed = sequences.remove(sequenceToDelete);   // sequenceToDelete must be object and not int
            Validatec.isTrue(removed);
            if (sequenceToDelete == 3) {
                // Do nothing. Problem solved entirely by deleting
            } else {
                sequences.add(sequenceToDelete - 1);
            }
        }
        return mutationsNeededToBreakSequences(sequences);
    }

    private static int getSequenceToDelete(List<Integer> sequences) {
        for (int cur : sequences) {
            if (cur % 3 == 0) { return cur; }
        }
        for (int cur : sequences) {
            if (cur % 3 == 1) { return cur; }
        }
        return sequences.get(0);
    }

    private static int mutationsNeededToBreakSequences(List<Integer> sequences) {
        int changesNeeded = 0;
        for (int sequence : sequences) {
            // "333" needs exactly 1 change. "4444" needs 1. "55555" needs 1. "666666" needs 2
            changesNeeded += sequence / 3;
        }
        return changesNeeded;
    }

    private static int insertsNeededToBreakSequences(String candidate) {
        List<Integer> sequences = sequences(candidate);
        int changesNeeded = 0;
        for (int sequence : sequences) {
            // "22" needs 0 change. "333" needs exactly 1 change. "4444" needs 1. "55555" needs 2. "666666" needs 2
            int numFull2s = sequence / 2;
            int numPartial2s = sequence % 2;
            changesNeeded += numFull2s + numPartial2s - 1;
        }
        return changesNeeded;
    }

    private static List<Integer> sequences(String candidate) {
        List<Integer> sequences = Lists.newArrayList();
        Character curSequence = null;
        int sequenceLength = 0;

        for (int i=0; i<candidate.length(); i++) {
            char cur = candidate.charAt(i);
            if (Objects.equals(cur, curSequence)) {
                sequenceLength++;
            } else {
                if (sequenceLength >= 3) { sequences.add(sequenceLength); }
                curSequence = cur;
                sequenceLength = 1;
            }
        }
        if (sequenceLength >= 3) { sequences.add(sequenceLength); }

        return sequences;
    }

    private static int numSpecialCharactersMissing(String candidate) {
        boolean lowerCaseExists = false;
        boolean upperCaseExists = false;
        boolean digitExists = false;

        for (int i=0; i<candidate.length(); i++) {
            String cur = candidate.substring(i, i+1);
            if (StringUtils.isAllLowerCase(cur)) {
                lowerCaseExists = true;
            }
            if (StringUtils.isAllUpperCase(cur)) {
                upperCaseExists = true;
            }
            if (StringUtils.isNumeric(cur)) {
                digitExists = true;
            }
        }

        int numMissing = 3;
        if (upperCaseExists) { numMissing--; }
        if (lowerCaseExists) { numMissing--; }
        if (digitExists) { numMissing--; }

        return numMissing;
    }
}
