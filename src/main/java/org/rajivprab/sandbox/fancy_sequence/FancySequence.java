package org.rajivprab.sandbox.fancy_sequence;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Write an API that generates fancy sequences using the append, addAll, and multAll operations.
 *
 * Implement the Fancy class:
 *
 * Fancy() Initializes the object with an empty sequence.
 * void append(val) Appends an integer val to the end of the sequence.
 * void addAll(inc) Increments all existing values in the sequence by an integer inc.
 * void multAll(m) Multiplies all existing values in the sequence by an integer m.
 * int getIndex(idx) Gets the current value at index idx (0-indexed) of the sequence modulo 109 + 7. If the index
 * is greater or equal than the length of the sequence, return -1.
 */
public class FancySequence {

    // Naive algorithm: Simply store all values in a list. When addAll/multAll is called, update every element
    // in the list. Time complexity:
    // - O(1) for append
    // - O(N) for all add/mult operations, where N is the number of elements appended till now
    // - O(1) for getIndex

    // Better algorithm implemented below:
    // - O(1) for all append operations.
    // - O(logN) for all addAll/multAll operations. Where N is the number of elements appended till now.
    // - O(N) for getIndex operation. Where N is the number of commands applicable to that index.
    //   All prior commands do not impact the time complexity.

    // If you append A elements, and C commands, and then get a single index:
    // Naive algorithm: O(A*C)
    // Better algorithm: O(C*logA + C)

    // If you append A elements, and C commands, and then get all indexes (not as much performance benefit):
    // Naive algorithm: O(A*C + A)
    // Better algorithm: O(C*logA + A*C)

    private final List<Integer> appends = Lists.newArrayList();
    private final TreeMap<Integer, List<Command>> commandsStartingWithIndex = Maps.newTreeMap();

    public FancySequence() {}

    public void append(int val) {
        appends.add(val);
    }

    public void addAll(int inc) {
        List<Command> commands = commandsStartingWithIndex.computeIfAbsent(appends.size() - 1, k -> Lists.newArrayList());
        commands.add(new Command(true, inc));
    }

    public void multAll(int m) {
        List<Command> commands = commandsStartingWithIndex.computeIfAbsent(appends.size() - 1, k -> Lists.newArrayList());
        commands.add(new Command(false, m));
    }

    public int getIndex(int idx) {
        if (idx >= appends.size()) { return -1; }
        int cur = appends.get(idx);
        Map<Integer, List<Command>> commandsToExecute = commandsStartingWithIndex.tailMap(idx, true);
        for (int index : commandsToExecute.keySet()) {
            for (Command command : commandsToExecute.get(index)) {
                if (command.isAdd) { cur += command.value; }
                else { cur *= command.value; }
            }
        }
        return cur;
    }

    private static class Command {
        private final boolean isAdd;
        private final int value;

        private Command(boolean isAdd, int value) {
            this.isAdd = isAdd;
            this.value = value;
        }
    }
}
