package org.rajivprab.sandbox.fancy_sequence;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;

/**
 * https://leetcode.com/problems/fancy-sequence/discuss/898861/C%2B%2B-Math-Solution-O(1)-extra-space-and-O(1)-time-for-each
 *
 * See FancySequence for problem description.
 *
 * This solution uses O(1) space/time for storing and applying all commands, regardless of the number
 * of commands given. Unlike the sub-optimal algorithm, this has time complexity:
 *
 * - O(1) for append
 * - O(1) for all add/mult operations
 * - O(1) for getIndex
 *
 * If you append A elements, and C commands, and then get all indexes: O(A + C + A)
 */
public class FancySequenceOptimal {

    private final List<Double> modifiedAppends = Lists.newArrayList();
    private int multiplier = 1;
    private int increment = 0;

    public FancySequenceOptimal() {}

    // We will later on return A*mult + increment
    // Hence store (val - increment)/mult
    // ((val - increment)/mult) * mult + increment = val => restores intended value
    public void append(int val) {
        // Need to convert to double because (5/3)*3 will produce 3, not 5, when using ints
        modifiedAppends.add((val - increment) * 1.0 / multiplier);
    }

    // Currently returning A*mult + increment
    // After this, we want to return A*mult + (increment + inc)
    public void addAll(int inc) {
        increment += inc;
    }

    // Currently returning A*mult + increment
    // After this, we want to return (A*mult + increment)*m = A*mult*m + increment*m
    public void multAll(int m) {
        multiplier *= m;
        increment *= m;
    }

    // If A is the value stored in list, we return A*mult + increment
    public int getIndex(int idx) {
        if (idx >= modifiedAppends.size()) { return -1; }
        return (int) (modifiedAppends.get(idx) * multiplier + increment);
    }
}
