package org.rajivprab.sandbox.predictive_signal;

import com.google.common.collect.Lists;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javatuples.Triplet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

/**
 * Computes correlation
 * <p>
 * Created by rprabhakar on 3/31/16.
 */
class PredictiveSignal {
    private static final Logger log = LogManager.getLogger(PredictiveSignal.class);

    static void parseDataset(String filePath) throws IOException {
        CSVParser parser = new CSVParser(
                new InputStreamReader(ClassLoader.getSystemResourceAsStream(filePath)),
                CSVFormat.DEFAULT.withHeader());
        String signalName = "Signal";
        String closePrice = "ClosePrice";
        List<Double> signals = Lists.newArrayList();
        List<Double> closePrices = Lists.newArrayList();
        for (CSVRecord record : parser.getRecords()) {
            double mySignal = Double.valueOf(record.get(signalName));
            double myPrice = Double.valueOf(record.get(closePrice));
            if (mySignal > 1 && mySignal < 10 && myPrice > 50 && myPrice < 400) {
                signals.add(mySignal);
                closePrices.add(myPrice);
            } else {
                log.error("Filtering out what is likely a bad value: " + record);
            }
        }
        log.info("Signals -> (Max, Min, NumEntries): " +
                Triplet.with(Collections.max(signals), Collections.min(signals), signals.size()));
        log.info("ClosePrices -> (Max, Min, NumEntries): " +
                Triplet.with(Collections.max(closePrices), Collections.min(closePrices), closePrices.size()));
        double correlation = new PearsonsCorrelation().correlation(getArray(signals), getArray(closePrices));
        log.info("Correlation found: " + correlation);
    }

    private static double[] getArray(List<Double> list) {
        double[] array = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }
}
