package org.rajivprab.sandbox.wow.Strategy;

import org.rajivprab.sandbox.wow.ability.Revenge;
import org.rajivprab.sandbox.wow.ability.ShieldSlam;
import org.rajivprab.sandbox.wow.ability.Sunder;

public class PerfectSlam implements Strategy {
    @Override
    public double press(double time) {
        if (ShieldSlam.SHIELD_SLAM.isAvailable(time)) {
            return ShieldSlam.SHIELD_SLAM.press(time);
        }

        if (Revenge.REVENGE.isAvailable(time)) {
            return Revenge.REVENGE.press(time);
        }

        return Sunder.SUNDER.press(time);
    }
}
