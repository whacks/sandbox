package org.rajivprab.sandbox.wow.Strategy;

import org.rajivprab.sandbox.wow.ability.SlamSunderMacro;

public class SlamSunderOnly implements Strategy {
    @Override
    public double press(double time) {
        return SlamSunderMacro.SLAM_SUNDER_MACRO.press(time);
    }
}
