package org.rajivprab.sandbox.wow.Strategy;

import org.rajivprab.sandbox.wow.ability.Sunder;

public class SunderOnly implements Strategy {
    @Override
    public double press(double time) {
        return Sunder.SUNDER.press(time);
    }
}
