package org.rajivprab.sandbox.wow.Strategy;

public interface Strategy {
    double press(double time);
}
