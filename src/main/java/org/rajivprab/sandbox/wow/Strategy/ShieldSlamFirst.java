package org.rajivprab.sandbox.wow.Strategy;

import org.rajivprab.sandbox.wow.ability.SlamSunderMacro;
import org.rajivprab.sandbox.wow.ability.Revenge;
import org.rajivprab.sandbox.wow.ability.ShieldSlam;

public class ShieldSlamFirst implements Strategy {
    @Override
    public double press(double time) {
        if (ShieldSlam.SHIELD_SLAM.isAvailable(time)) {
            return SlamSunderMacro.SLAM_SUNDER_MACRO.press(time);
        }

        if (Revenge.REVENGE.isAvailable(time)) {
            return Revenge.REVENGE.press(time);
        }

        return SlamSunderMacro.SLAM_SUNDER_MACRO.press(time);
    }
}
