package org.rajivprab.sandbox.wow.Strategy;

import org.rajivprab.sandbox.wow.ability.SlamSunderMacro;
import org.rajivprab.sandbox.wow.ability.Revenge;

public class RevengeFirst implements Strategy {
    @Override
    public double press(double time) {
        return Revenge.REVENGE.isAvailable(time) ?
                Revenge.REVENGE.press(time) :
                SlamSunderMacro.SLAM_SUNDER_MACRO.press(time);
    }
}
