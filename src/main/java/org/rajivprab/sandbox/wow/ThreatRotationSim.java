package org.rajivprab.sandbox.wow;

import org.rajivprab.sandbox.wow.Strategy.Strategy;
import org.rajivprab.sandbox.wow.ability.Ability.NoEffectException;
import org.rajivprab.sandbox.wow.ability.Revenge;
import org.rajivprab.sandbox.wow.ability.ShieldSlam;
import org.rajivprab.sandbox.wow.ability.SlamSunderMacro;
import org.rajivprab.sandbox.wow.ability.Sunder;

class ThreatRotationSim {
    private static final double REACTION_TIME = 0.2;

    static double getTpsPerRage(Strategy strategy, double time) {
        double tps = getTps(strategy, time);
        double rageCost = Revenge.REVENGE.getRageUsed() + Sunder.SUNDER.getRageUsed() + ShieldSlam.SHIELD_SLAM.getRageUsed();
        return tps / rageCost;
    }

    static double getTps(Strategy strategy, double time) {
        return getThreat(strategy, time) / time;
    }

    private static double getThreat(Strategy strategy, double time) {
        Revenge.REVENGE.reset();
        ShieldSlam.SHIELD_SLAM.reset();
        Sunder.SUNDER.reset();
        SlamSunderMacro.SLAM_SUNDER_MACRO.reset();

        double curTime = 0;
        double threat = 0;
        while (curTime < time) {
            double curThreat = -1;
            while (curThreat < 0) {
                try {
                    curThreat = strategy.press(curTime);
                } catch (NoEffectException e) {
                    curTime += 0.1;
                }
            }
            threat += curThreat;
            curTime += 1.5;
            curTime += REACTION_TIME;
        }
        return threat;
    }
}
