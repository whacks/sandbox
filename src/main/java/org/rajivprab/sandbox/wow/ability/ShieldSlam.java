package org.rajivprab.sandbox.wow.ability;

public class ShieldSlam extends AbstractAbility {
    public static final Ability SHIELD_SLAM = new ShieldSlam();

    private ShieldSlam() {
        super(6.0, (250 + 350) * 1.45, 20);
    }
}
