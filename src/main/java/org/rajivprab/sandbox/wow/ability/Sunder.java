package org.rajivprab.sandbox.wow.ability;

public class Sunder extends AbstractAbility {
    public static final Ability SUNDER = new Sunder();

    private Sunder() {
        super(1.5, 261 * 1.45, 12);
    }
}
