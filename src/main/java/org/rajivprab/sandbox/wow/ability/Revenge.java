package org.rajivprab.sandbox.wow.ability;

public class Revenge extends AbstractAbility {
    public static final Ability REVENGE = new Revenge();

    private Revenge() {
        super(5.0, (315 + 71) * 1.45, 5);
    }
}
