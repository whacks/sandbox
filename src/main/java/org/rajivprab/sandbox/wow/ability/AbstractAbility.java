package org.rajivprab.sandbox.wow.ability;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

abstract class AbstractAbility implements Ability {
    private static final Logger log = LogManager.getLogger(AbstractAbility.class);

    private final double cooldown;
    private final double threat;
    private final int rageCost;

    private double lastPressed;
    private int rageUsed;

    AbstractAbility(double cooldown, double threat, int rageCost) {
        this.cooldown = cooldown;
        this.threat = threat;
        this.rageCost = rageCost;

        reset();
    }

    @Override
    public int getRageUsed() {
        return rageUsed;
    }

    @Override
    public void reset() {
        lastPressed = -100;
        rageUsed = 0;
    }

    @Override
    public double press(double time) {
        if (!isAvailable(time)) { throw new NoEffectException(); }

        // log.info("[{}] Triggered {}", (int) time, this.getClass().getSimpleName());
        lastPressed = time;
        rageUsed += rageCost;
        return threat;
    }

    @Override
    public boolean isAvailable(double time) {
        return time - lastPressed > cooldown;
    }
}
