package org.rajivprab.sandbox.wow.ability;

import com.google.common.collect.Lists;
import org.rajivprab.cava.Validatec;

import java.util.List;

// Castsequence Shield Slam, Sunder, Sunder
public class SlamSunderMacro implements Ability {
    public static final SlamSunderMacro SLAM_SUNDER_MACRO = new SlamSunderMacro();

    private final List<Ability> history = Lists.newArrayList();    // First element is most recent

    @Override
    public void reset() {
        history.clear();
    }

    @Override
    public int getRageUsed() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double press(double time) {
        while (history.size() > 2) {
            history.remove(2);
        }

        Ability next = getNext();
        double threat = next.press(time);
        history.add(0, next);
        return threat;
    }

    @Override
    public boolean isAvailable(double time) {
        return getNext().isAvailable(time);
    }

    private Ability getNext() {
        Validatec.greaterOrEqual(2, history.size());
        return history.contains(ShieldSlam.SHIELD_SLAM) ? Sunder.SUNDER : ShieldSlam.SHIELD_SLAM;
    }
}
