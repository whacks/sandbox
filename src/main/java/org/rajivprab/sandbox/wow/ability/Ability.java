package org.rajivprab.sandbox.wow.ability;

public interface Ability {
    double press(double time);

    boolean isAvailable(double time);

    void reset();

    int getRageUsed();

    class NoEffectException extends RuntimeException {}
}
