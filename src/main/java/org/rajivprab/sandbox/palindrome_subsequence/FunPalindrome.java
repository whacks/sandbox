package org.rajivprab.sandbox.palindrome_subsequence;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.rajivprab.cava.Validatec;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 A palindrome is a word, phrase, number, or other sequence of characters which reads the same forward and backwards. For example: "madam" and "dad" are palindromes, but "sir" and "sad" are not.

 The fun score for two subsequences, A and B, in string s is the product of their respective lengths. There are many ways to choose A and B, but your goal is to maximize the fun score. There can't be any overlap or crossover between the two subsequences.

 Given string s, select exactly two non-overlapping palindromic subsequences, A and B, from s to maximize the fun score.

 Constraints
 1 < |s| ≤ 3000
 s is composed of lowercase English characters.

 Input Format
 Complete a function named funPal which takes a single string (s) as a parameter.

 Output Format
 Return a single integer denoting the maximum possible fun score for s.

 * Created by rprabhakar on 2/21/16.
 */
public class FunPalindrome {
    public static int getMaxFunScore(String input) {
        Collection<Subsequence> subsequences = new StringIndex(input).getAllSubsequences();
        int winnerScore = 1;
        for (Subsequence subA : subsequences) {
            for (Subsequence subB : subsequences) {
                if (!subA.intersects(subB)) {
                    int myScore = subA.getCombinedScore(subB);
                    if (myScore > winnerScore) {
                        winnerScore = myScore;
                    }
                }
                if (subA.length > winnerScore && subA.range() < input.length()) {
                    winnerScore = subA.length;
                }
            }
        }
        return winnerScore;
    }

    private static class Subsequence {
        private final int startInclusive;
        private final int endInclusive;
        public final int length;

        public Subsequence(int startInclusive, int endInclusive, int length) {
            Validatec.greaterThan(length, 1);
            Validatec.greaterThan(endInclusive, startInclusive);
            this.startInclusive = startInclusive;
            this.endInclusive = endInclusive;
            this.length = length;
        }

        public boolean intersects(Subsequence other) {
            return !distinct(other);
        }

        private boolean distinct(Subsequence other) {
            boolean left = endInclusive < other.startInclusive;
            boolean right = startInclusive > other.endInclusive;
            return left || right;
        }

        public int getCombinedScore(Subsequence other) {
            Validatec.isTrue(!intersects(other));
            return this.length * other.length;
        }

        public int range() {
            return endInclusive - startInclusive + 1;
        }
    }

    private static class StringIndex {
        private final Map<Character, List<Integer>> index = Maps.newHashMap();
        private final String sequence;

        public StringIndex(String sequence) {
            char[] chars = sequence.toCharArray();
            for (int i=0; i<chars.length; i++) {
                populateIndex(sequence.charAt(i), i);
            }
            this.sequence = sequence;
        }

        // Will not return degenerate SubSequences where length == 1
        public Collection<Subsequence> getAllSubsequences() {
            return index.keySet().stream().map(this::getAllSubsequences).flatMap(Collection::stream).collect(Collectors.toList());
        }

        // Will not return degenerate SubSequences where length == 1
        private Collection<Subsequence> getAllSubsequences(char startChar) {
            List<Integer> positions = index.get(startChar);
            List<Subsequence> subsequences = Lists.newArrayList();
            for (int start : positions) {
                for (int end : positions) {
                    if (end > start) {
                        int length = end == (start + 1) ? 2 : 2 + getLongestSubPalindrome(start + 1, end);
                        subsequences.add(new Subsequence(start, end, length));
                    }
                }
            }
            return subsequences;
        }

        private int getLongestSubPalindrome(int startInclusive, int endExclusive) {
            Validatec.greaterThan(endExclusive, startInclusive);
            if (endExclusive ==  startInclusive + 1) {
                return 1;
            }
            return IntStream
                    .range(startInclusive, endExclusive)
                    .map(sequence::charAt).boxed()
                    .collect(Collectors.toSet()).stream()
                    .mapToInt(c -> getLongestSubPalindrome(startInclusive, endExclusive, (char) c.intValue()))
                    .max().getAsInt();
        }

        private int getLongestSubPalindrome(int startInclusive, int endExclusive, char startChar) {
            List<Integer> positions = index.get(startChar).stream()
                    .filter(i -> i >= startInclusive && i < endExclusive).collect(Collectors.toList());
            Validatec.notEmpty(positions, "Expecting at least one instance of startChar");
            if (positions.size() == 1) {
                return 1;
            }
            int min = positions.get(0);
            int max = positions.get(positions.size() - 1);
            Validatec.greaterThan(max, min);
            return getLongestSubPalindrome(min + 1, max) + 2;
        }

        private void populateIndex(char character, int position) {
            List<Integer> positions = index.get(character);
            if (positions == null) {
                positions = Lists.newArrayList();
                index.put(character, positions);
            }
            positions.add(position);
        }
    }
}
