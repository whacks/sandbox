package org.rajivprab.sandbox.negabinary;

// https://darrenkopp.com/posts/2024/05/01/coding-interviews-are-stupid
// Note: this is an extremely inefficient implementation that takes O(2^N), where N is the number of bits
// in the solution. An O(N) solution exists.
// Hint: Consider how you would print a number as binary in O(N). You can then do something very similar.
public class Negabinary {
    public static String getBaseMinusTwo(int num) {
        if (num == 0) {
            return "0";
        }
        return getBaseMinusTwo(num, "", 0);
    }

    static String getBaseMinusTwo(int target, String suffix, int suffixVal) {
        if (suffixVal == target) {
            return suffix;
        }

        int delta = Math.abs(target - suffixVal);
        int prependValue = 1 << suffix.length();
        if (delta < prependValue) {
            return null;
        }

        // Prepend 0
        String result0 = getBaseMinusTwo(target, "0" + suffix, suffixVal);
        if (result0 != null) {
            return result0;
        }

        // Prepend 1
        if (suffix.length() % 2 == 1) {
            prependValue *= -1;
        }
        return getBaseMinusTwo(target, "1" + suffix, prependValue + suffixVal);
    }
}
