package org.rajivprab.sandbox.vote_count;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.json.JSONArray;

import java.util.Map;
import java.util.UUID;

public class VoteCounter {
    public static Table<UUID, String, Integer> countVotes(JSONArray records) {
        int numRecords = records.length();
        Table<UUID, String, Integer> contentEmojiCounts = HashBasedTable.create();
        for (int i=0; i<numRecords; i++) {
            JSONArray record = records.getJSONArray(i);
            UUID contentID = UUID.fromString(record.getString(1));
            String emoji = record.getString(4);
            Integer existingCount = contentEmojiCounts.get(contentID, emoji);
            if (existingCount == null) {
                existingCount = 0;
            }
            contentEmojiCounts.put(contentID, emoji, existingCount + 1);
        }
        return contentEmojiCounts;
    }

    public static Map<String, Integer> getEmojiCounts(Table<UUID, String, Integer> contentEmojiCounts, UUID contentID) {
        return contentEmojiCounts.row(contentID);
    }
}
