package org.rajivprab.sandbox.topo_sort;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.javatuples.Pair;

import java.util.*;

/**
 * Question: Given a list of nodes, and their dependencies, construct a dependency graph for use in a dependency manager.
 * Given: a string key, and a set of string dependencies
 * Assume: the result graph is a DAG
 * Assume: You are given a node class Node: (parents, name, state)
 *
 * Input:
 * B -> C, D
 * A -> B
 * C -> D
 * D
 *
 * Graph:
 *    D
 *   / \
 *  C   )
 *   \ /
 *    B
 *     \
 *      A
 *
 */
public class TopologicalSortDfs {

    // This solution uses the DFS algorithm described here: https://en.wikipedia.org/wiki/Topological_sorting

    public static <T> List<T> createGraph(Iterable<Pair<T, T>> links) {
        Map<T, Node<T>> nodesMap = Maps.newHashMap();
        for (Pair<T, T> link : links) {
            Node<T> parent = getWithPopulate(nodesMap, link.getValue0());
            Node<T> child = getWithPopulate(nodesMap, link.getValue1());
            child.withParent(parent);
        }

        List<T> orderedResults = Lists.newArrayList();
        Set<T> addedResults = Sets.newHashSet();
        for (Node<T> element : nodesMap.values()) {
            visitNode(orderedResults, addedResults, element);
        }
        return orderedResults;
    }

    private static <T> void visitNode(List<T> orderedResults, Set<T> addedResults, Node<T> cur) {
        for (Node<T> parent : cur.getParents()) {
            if (!addedResults.contains(parent.getValue())) {
                visitNode(orderedResults, addedResults, parent);
            }
        }
        orderedResults.add(cur.getValue());
        addedResults.add(cur.getValue());
    }

    private static <T> Node<T> getWithPopulate(Map<T, Node<T>> nodesMap, T value) {
        if (!nodesMap.containsKey(value)) {
            nodesMap.put(value, new Node<T>(value));
        }
        return nodesMap.get(value);
    }

    private static class Node<T> {
        private T value;
        private final Collection<Node<T>> parents = Lists.newArrayList();

        public Node(T value) {
            this.value = value;
        }

        public Node<T> withParent(Node<T> parent) {
            this.parents.add(parent);
            return this;
        }

        public Collection<Node<T>> getParents() {
            return Collections.unmodifiableCollection(parents);
        }

        public T getValue() {
            return value;
        }
    }
}
