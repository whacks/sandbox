package org.rajivprab.sandbox.topo_sort;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;
import java.util.Set;

public class TopologicalSortKahns {

    // See Kahn's algorithm: https://en.wikipedia.org/wiki/Topological_sorting

    public static List<Node> topologicalSort(List<Node> nodes) {
        List<Node> pendingNodes = Lists.newArrayList(nodes);
        List<Node> orderedNodes = Lists.newArrayList();
        Set<Node> completedNodes = Sets.newHashSet();

        while (!pendingNodes.isEmpty()) {
            List<Node> recentlyOrdered = Lists.newArrayList();
            for (Node cur: pendingNodes) {
                Set<Node> waitingFor = Sets.difference(cur.dependencies, completedNodes);
                if (waitingFor.isEmpty()) {
                    orderedNodes.add(cur);
                    completedNodes.add(cur);
                    recentlyOrdered.add(cur);
                }
            }
            pendingNodes.removeAll(recentlyOrdered);
        }

        return orderedNodes;
    }

    public static class Node {
        private final String name;

        public final Set<Node> dependencies = Sets.newHashSet();

        public Node(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
