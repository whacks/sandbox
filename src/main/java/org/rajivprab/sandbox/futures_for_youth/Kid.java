package org.rajivprab.sandbox.futures_for_youth;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

import static org.rajivprab.sandbox.futures_for_youth.Parameters.RATIO;

public class Kid {
    private Map<Integer, Long> yearToNetRevenue;

    public Kid(int startYear, long startingSalary, int startAge, int careerEndAge, List<Long> expenses) {
        expenses = Lists.newLinkedList(expenses);
        this.yearToNetRevenue = buildProjections(startYear, startingSalary, startAge, careerEndAge, expenses);
    }

    public long getNetRevenue(int year) {
        Long revenue = yearToNetRevenue.get(year);
        return revenue == null ? 0 : revenue;
    }

    // Ignores educational expenses
    public long getGrossRevenue(int year) {
        long netRevenue = getNetRevenue(year);
        return netRevenue > 0 ? netRevenue : 0;
    }

    private Map<Integer, Long> buildProjections(int startYear, long startingSalary, int startAge,
                                                int careerEndAge, List<Long> expenses) {
        int year = startYear;
        int age = startAge;
        long salary = startingSalary;
        Map<Integer, Long> revenue = Maps.newHashMap();
        while (age <= careerEndAge) {
            year++;
            age++;
            if (expenses.isEmpty()) {
                revenue.put(year, Math.round(salary * RATIO));
                if (salary < Parameters.RATIO_MAX_MIN_SALARY * startingSalary) {
                    salary *= Parameters.INFLATION_ADJUSTED_YEARLY_WAGE_GROWTH;
                }
            } else {
                revenue.put(year, expenses.remove(0) * -1);
            }
        }
        return ImmutableMap.copyOf(revenue);
    }
}
