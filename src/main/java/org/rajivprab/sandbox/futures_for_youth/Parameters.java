package org.rajivprab.sandbox.futures_for_youth;

import com.google.common.collect.ImmutableList;

import java.util.List;

public class Parameters {
    // https://www.equidam.com/average-growth-rate-for-startups/
    static final List<Double> GROWTH_RATE =
            ImmutableList.of(8.0, 3.0, 2.0, 2.0, 2.0,
                             2.0, 2.0, 1.5, 1.5, 1.5,
                             1.5, 1.2, 1.2, 1.2, 1.2,
                             1.2, 1.2, 1.2, 1.2, 1.2,
                             1.2, 1.2, 1.2, 1.2, 1.2,
                             1.2, 1.2, 1.2, 1.2, 1.2);

    static final int PROGRAM_END_AGE = 60;

    static final int START_YEAR = 2020;
    static final int START_NUM_KIDS = 10;

    static final int YEARLY_IMMIGRATION_CAP = 200000;

    static final double RATIO = 0.09;

    static final double INFLATION_ADJUSTED_YEARLY_WAGE_GROWTH = 1.05;
    static final double RATIO_MAX_MIN_SALARY = 2;
}
