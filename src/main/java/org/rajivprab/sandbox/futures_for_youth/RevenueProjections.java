package org.rajivprab.sandbox.futures_for_youth;

import com.google.common.collect.ImmutableList;

import java.util.Collection;

import static org.rajivprab.sandbox.futures_for_youth.Parameters.START_YEAR;

public class RevenueProjections {

    public static void main(String... args) {
        Collection<BusinessModel> models = ImmutableList.of(
                new LocalCollegeLocalCareer(),
                new ForeignMastersAndCareer(),
                new LocalCollegeForeignMastersAndCareer());

        for (BusinessModel model : models) {
            System.out.println("--------------------- " + model + " ----------------");
            printNumberKids(model);
            System.out.println("---------------------");
            printRevenues(model);
        }
    }

    private static void printRevenues(BusinessModel model) {
        for (int i = 0; i <= 30; i++) {
            int year = START_YEAR + i;
            long revenue = model.getGrossRevenue(year);
            System.out.println("$" +  (revenue / 1000000L) + "M");
        }
    }

    private static void printNumberKids(BusinessModel model) {
        for (int i = 0; i < 30; i++) {
            int year = START_YEAR + i;
            int kidsEnrolled = model.getNumberKidsEnrolled(year);
            System.out.println(kidsEnrolled);
        }
    }
}
