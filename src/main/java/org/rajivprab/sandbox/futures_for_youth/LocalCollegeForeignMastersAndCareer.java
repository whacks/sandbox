package org.rajivprab.sandbox.futures_for_youth;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

import static org.rajivprab.sandbox.futures_for_youth.Parameters.*;

public class LocalCollegeForeignMastersAndCareer implements BusinessModel {
    private static final int AGE_AT_ENROLLMENT = 18;
    private static final List<Long> COLLEGE_COSTS = ImmutableList.of(2000L, 2000L, 2000L, 2000L, 75000L);
    private static final long STARTING_SALARY = 100000;

    private final Map<Integer, Integer> yearNumberKidsEnrolled = Maps.newHashMap();
    private final Map<Kid, Integer> portfolioKidsToNumber = Maps.newHashMap();

    LocalCollegeForeignMastersAndCareer() {
        yearNumberKidsEnrolled.put(START_YEAR, START_NUM_KIDS);
        for (int i=0; i<GROWTH_RATE.size(); i++) {
            int year = START_YEAR + i + 1;
            double growth = GROWTH_RATE.get(i);
            int previousYearNumKids = yearNumberKidsEnrolled.get(year - 1);
            int projectedNumKids = Math.toIntExact(Math.round(previousYearNumKids * growth));
            int numKidsEnrolled = Math.min(projectedNumKids, YEARLY_IMMIGRATION_CAP);
            yearNumberKidsEnrolled.put(year, numKidsEnrolled);
        }
        for (int year : yearNumberKidsEnrolled.keySet()) {
            Kid kid = new Kid(year, STARTING_SALARY, AGE_AT_ENROLLMENT,
                              PROGRAM_END_AGE, COLLEGE_COSTS);
            portfolioKidsToNumber.put(kid, yearNumberKidsEnrolled.get(year));
        }
    }

    @Override
    public long getNetRevenue(int year) {
        long revenue = 0;
        for (Kid kid : portfolioKidsToNumber.keySet()) {
            revenue += kid.getNetRevenue(year) * portfolioKidsToNumber.get(kid);
        }
        return revenue;
    }

    @Override
    public long getGrossRevenue(int year) {
        long revenue = 0;
        for (Kid kid : portfolioKidsToNumber.keySet()) {
            revenue += kid.getGrossRevenue(year) * portfolioKidsToNumber.get(kid);
        }
        return revenue;
    }

    @Override
    public int getNumberKidsEnrolled(int year) {
        return yearNumberKidsEnrolled.get(year);
    }

    @Override
    public String toString() {
        return "Local College Foreign Masters - Foreign Career";
    }
}
