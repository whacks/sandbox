package org.rajivprab.sandbox.futures_for_youth;

public interface BusinessModel {
    long getNetRevenue(int year);
    long getGrossRevenue(int year);
    int getNumberKidsEnrolled(int year);
}
