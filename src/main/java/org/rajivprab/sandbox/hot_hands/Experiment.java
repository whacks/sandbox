package org.rajivprab.sandbox.hot_hands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;
import java.util.stream.IntStream;

/**
 * http://www.wsj.com/articles/the-hot-hand-debate-gets-flipped-on-its-head-1443465711
 *
 * Toss a coin four times. Write down the percentage of heads on the flips coming immediately after heads.
 * Repeat that process one million times. On average, what is that percentage?
 * The obvious answer is 50%. That answer is also wrong. The real answer is 40% —
 * and the authors say their correction should alter years of thinking about the hot hand.
 *
 * Created by rajivprab on 12/25/16.
 */
public class Experiment {
    private static final Logger log = LogManager.getLogger(Experiment.class);

    private static final int NUM_ITERATIONS = 1000000;
    private static final int NUM_TOSSES = 4;

    public static void main(String... args) {
        double percentageHeads = IntStream.range(0, NUM_ITERATIONS)
                .mapToObj(i -> CoinToss.tossSeries(NUM_TOSSES))
                .mapToDouble(CoinToss::getPercentageHeads)
                .average().orElseThrow(IllegalStateException::new);
        log.info("Percentage of tosses landing as heads: " + percentageHeads);

        getPercentageHeadsAfterHeads(NUM_TOSSES);
        getPercentageHeadsAfterHeads(2 * NUM_TOSSES);
        getPercentageHeadsAfterHeads(10 * NUM_TOSSES);
        getPercentageHeadsAfterHeads(100 * NUM_TOSSES);
    }

    private static void getPercentageHeadsAfterHeads(int numToss) {
        double percentageHeadsAfterHeads = IntStream.range(0, NUM_ITERATIONS)
                .mapToObj(i -> CoinToss.tossSeries(numToss))
                .map(CoinToss::getPercentageHeadsAfterHeads)
                .filter(Optional::isPresent)
                .mapToDouble(percentage -> percentage.orElseThrow(IllegalStateException::new))
                .average().orElseThrow(IllegalStateException::new);
        log.info("With NumToss: " + numToss +
                ", Percentage of tosses after a heads, landing as heads: " + percentageHeadsAfterHeads);

        int numHeadsAfterHeads = IntStream.range(0, NUM_ITERATIONS)
                .mapToObj(i -> CoinToss.tossSeries(numToss))
                .mapToInt(CoinToss::getNumHeadsAfterHeads)
                .sum();
        int numTossesAfterHeads = IntStream.range(0, NUM_ITERATIONS)
                .mapToObj(i -> CoinToss.tossSeries(numToss))
                .mapToInt(CoinToss::getNumTossesAfterHeads)
                .sum();
        log.info("Percentage heads out of combined-post-heads tosses: " + numHeadsAfterHeads * 1.0 / numTossesAfterHeads);
    }
}
