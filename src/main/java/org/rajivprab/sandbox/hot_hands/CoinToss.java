package org.rajivprab.sandbox.hot_hands;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Toss a coin N times, and parse resulting statistics
 *
 * Created by rajivprab on 12/25/16.
 */
public class CoinToss {
    public static CoinToss tossSeries(int numTosses) {
        return new CoinToss(numTosses);
    }

    public enum Coin {HEADS, TAILS};

    private static final Random RNG = new Random();

    private final List<Coin> tosses;
    private List<Coin> tossesAfterHeads;

    private CoinToss(int numToss) {
        List<Coin> tosses = Lists.newArrayList();
        IntStream.range(0, numToss).forEach(i -> tosses.add(RNG.nextBoolean() ? Coin.HEADS : Coin.TAILS));
        this.tosses = ImmutableList.copyOf(tosses);
    }

    public double getPercentageHeads() {
        return getPercentageHeads(tosses).orElseThrow(IllegalStateException::new);
    }

    public Optional<Double> getPercentageHeadsAfterHeads() {
        return getPercentageHeads(getTossesAfterHeads());
    }

    public int getNumHeadsAfterHeads() {
        return getNumHeads(getTossesAfterHeads());
    }

    public int getNumTossesAfterHeads() {
        return getTossesAfterHeads().size();
    }

    private List<Coin> getTossesAfterHeads() {
        if (tossesAfterHeads == null) {
            tossesAfterHeads = Lists.newArrayList();
            for (int i = 0; i < tosses.size() - 2; i++) {
                if (tosses.get(i).equals(Coin.HEADS)) {
                    tossesAfterHeads.add(tosses.get(i + 1));
                }
            }
        }
        return tossesAfterHeads;
    }

    private static int getNumHeads(List<Coin> tosses) {
        return (int) tosses.stream().filter(toss -> toss.equals(Coin.HEADS)).count();
    }

    private static Optional<Double> getPercentageHeads(List<Coin> tosses) {
        return tosses.isEmpty() ? Optional.empty() :
                Optional.of(getNumHeads(tosses) * 1.0 / tosses.size());
    }
}
