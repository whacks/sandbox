package org.rajivprab.sandbox.stock_returns;

import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.rajivprab.cava.FileUtilc;
import org.rajivprab.cava.JSONObjectImmutable;
import org.rajivprab.cava.Validatec;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Blog write-up: https://outlookzen.com/2018/10/27/correlation-between-projected-vs-actual-stock-market-returns/
 * 
 * Thesis: Long term (nominal) stock market returns, closely follow the sum of:
 * 1. Long-term earnings yield (ie, E/P ratio)
 * 2. Nominal GDP growth rate
 * <p>
 * Changes in the PE ratio can trigger short-term fluctuations, or a one-time boost/loss.
 * But they cannot significantly impact long-term returns
 * <p>
 * --------------------
 * <p>
 * Methodology:
 * 1. Pick a year X
 * 2. Look up stock market's PE ratio on year X
 * 3. Look up USA's nominal GDP on year X
 * 4. Compare that with nominal GDP on year X+1, to calculate GDP growth rate
 * 5. Projected returns for year X = GDP_growth_rate + earnings_yield
 * 6. Repeat for all X between start/end years, to calculate projected portfolio value at end-year
 * 7. Use projected_portfolio_value to calculate annualized returns between start/end years
 * 8. Look up actual returns between start/end years
 * 9. Compare the delta between projected and actual returns
 * <p>
 * ---------------------
 * <p>
 * Sources:
 * GDP: https://www.thebalance.com/us-gdp-by-year-3305543
 * Actual returns: https://dqydj.com/sp-500-return-calculator/
 * PE ratio: http://www.multpl.com/table
 * Current GDP: https://fred.stlouisfed.org/series/GDP
 * <p>
 * ----------------------
 * <p>
 * Examples using older course-grain methodology:
 * PE Ratio in 09/2018: 25
 * Nominal GDP in 2018: 20.411 trillion
 * <p>
 * ---
 * <p>
 * PE Ratio 01/1931: 17
 * 01/1931 to 09/2018 (with dividend reinvestment): 477274% total returns
 * 10.145% per year (87.66 years)
 * Total returns if today's PE ratio were 17: 477274 * 17/25 = 324546%
 * 9.662% per year
 * <p>
 * ----
 * <p>
 * Nominal GDP 01/1931: 0.085 trillion (averaging 1930 and 1931 numbers, because not sure which month they represent)
 * GDP Growth: 20.411/0.085 * 100 = 24012.9411765%
 * Per year: 6.452%
 * Earnings yield = 1/17 * 100% = 5.882%
 * <p>
 * Expected investment returns = 5.882 + 6.452 = 12.334%
 * Actual returns: 9.662%
 * Returns Slippage: 12.334 - 9.662 = 2.67%
 * Correlation = 9.662 / 12.334 = 0.78
 * <p>
 * ------
 * <p>
 * 09/1918 to 09/2018 (with dividend reinvestment): 1986882% total returns
 * 10.4% per year
 */
public class StockReturnsPredictions {
    private static final Logger log = LogManager.getLogger(StockReturnsPredictions.class);

    private static final JSONObject PE_RATIOS
            = JSONObjectImmutable.build(FileUtilc.readClasspathFile("stock_returns/pe.json"));
    private static final JSONObject GDP_HISTORY
            = JSONObjectImmutable.build(FileUtilc.readClasspathFile("stock_returns/gdp.json"));

    private static final int START_YEAR_INCLUSIVE = 1930;
    private static final int END_YEAR_EXCLUSIVE = 2018;
    private static final int MIN_NUM_YEARS = 40;

    public static void main(String... args) {
        printTableHeadings();
        generateTable();
    }

    private static void printTableHeadings() {
        log.info("------------------------------------------------------------------------------------------");

        StringBuilder headings = new StringBuilder("EndYear   = ---- | ");
        for (int endYear = START_YEAR_INCLUSIVE + MIN_NUM_YEARS; endYear <= END_YEAR_EXCLUSIVE; endYear++) {
            headings.append(endYear).append(" | ");
        }
        log.info(headings);

        log.info("------------------------------------------------------------------------------------------");
    }

    private static void generateTable() {
        for (int startYear = START_YEAR_INCLUSIVE; startYear <= END_YEAR_EXCLUSIVE; startYear++) {
            StringBuilder row = new StringBuilder("StartYear = ").append(startYear).append(" | ");
            for (int endYear = START_YEAR_INCLUSIVE + MIN_NUM_YEARS; endYear <= END_YEAR_EXCLUSIVE; endYear++) {
                if (endYear - startYear < MIN_NUM_YEARS) {
                    row.append("----");
                } else {
                    row.append(String.format("%3.2f", getCorrelation(startYear, endYear)));
                }
                row.append(" | ");
            }
            log.info(row.append("StartYear = ").append(startYear).toString());
        }
    }

    // Ideally we would do a linear regression and find individual multipliers for each of
    // earnings_yield, real_gdp_growth, inflation. But for now, we are simplifying and assuming a single
    // multiplier for the entire projected_returns
    static double getCorrelation(int startYearInclusive, int endYearExclusive) {
        /*
         * A = (1 + Ra) ^ y     where A is actual portfolio value after Y years, assuming starting with $1
         * Ra = k * Rp          where Ra (Actual annuallized Returns) ~ Rp (Projected annuallized Returns) ~ 0.1
         * =>
         * A = (1 + k*Rp) ^ y
         * lgA = y * lg(1 + k*Rp)
         * lgA / y = lg(1 + k*Rp)
         * e^(lgA / y) = 1 + k*Rp
         * e^(lgA / y) - 1 = k*Rp
         * (e^(lgA / y) - 1) / Rp = k
         */
        double projectedReturns = projectedAnnualizedReturns(startYearInclusive, endYearExclusive) - 1.0;
        double actualPortfolioValue = actualPortfolioValue(startYearInclusive, endYearExclusive);
        int numYears = endYearExclusive - startYearInclusive;
        double step1 = Math.log(actualPortfolioValue) / numYears;
        double step2 = Math.exp(step1);
        return (step2 - 1) / projectedReturns;
    }

    /**
     * Given $1 portfolio at start of startYear, returns portfolio value at start of endYear
     */
    static double actualPortfolioValue(int startYearInclusive, int endYearExclusive) {
        ChromeDriver browser = ReturnsBrowserHolder.BROWSER;
        new Select(browser.findElementById("STARTMONTH")).selectByVisibleText("January");
        new Select(browser.findElementById("ENDMONTH")).selectByVisibleText("January");
        new Select(browser.findElementById("STARTYEAR")).selectByVisibleText(startYearInclusive + "");
        new Select(browser.findElementById("ENDYEAR")).selectByVisibleText(endYearExclusive + "");

        // browser.findElementById("CALC").click();     // Sometimes fails
        // https://stackoverflow.com/a/48667924/4816322
        browser.executeScript("arguments[0].click();", browser.findElementById("CALC"));

        // String annualizedReturns = driver.findElementById("DANNRETURN").getAttribute("value");
        String totalReturns = browser.findElementById("DTOTRETURN").getAttribute("value");
        // For 1 year with returns of 10%, DTOTRETURN = 10%
        return 1.0 + (Double.valueOf(totalReturns.replace("%", "")) / 100.0);
    }

    /**
     * Returns 1.10 to represent annualized returns of 10%
     */
    static double projectedAnnualizedReturns(int startYearInclusive, int endYearExclusive) {
        double projectedPortfolio = projectedPortfolio(startYearInclusive, endYearExclusive);
        int numYears = endYearExclusive - startYearInclusive;
        /*
         * r^n = pp
         * nlogr = logpp
         * logr = logpp / n
         * r = e^(log pp / n)
         */
        return Math.exp(Math.log(projectedPortfolio) / numYears);
    }

    private static double projectedPortfolio(int startYearInclusive, int endYearExclusive) {
        double projectedPortfolio = 1;
        for (int year = startYearInclusive; year < endYearExclusive; year++) {
            projectedPortfolio = projectedReturns(year) * projectedPortfolio;
        }
        return projectedPortfolio;
    }

    private static final Map<Integer, Double> PROJECTED_RETURNS_FOR_YEAR = Maps.newHashMap();

    /**
     * Returns 1.1 to represent 10% projected returns
     */
    static double projectedReturns(int year) {
        return PROJECTED_RETURNS_FOR_YEAR.computeIfAbsent(year, StockReturnsPredictions::computeProjectedReturns);
    }

    private static double computeProjectedReturns(int year) {
        // Use {year-1}, since that represents GDP at end-of-year for {year-1}
        // Ie, GDP at start-of-year for {year}
        double startGDP = GDP_HISTORY.getDouble((year - 1) + "");
        double endGDP = GDP_HISTORY.getDouble(year + "");
        double gdpGrowth = (endGDP - startGDP) / startGDP;

        // Use {year} because that represents PE ratio at start-of-year
        double curEarningsYield = 1.0 / PE_RATIOS.getDouble(year + "");
        double nextEarningsYield = 1.0 / PE_RATIOS.getDouble((year + 1) + "");
        double averageEarningsYield = (curEarningsYield + nextEarningsYield) / 2;

        return 1.0 + averageEarningsYield + gdpGrowth;
    }

    private static class ReturnsBrowserHolder {
        static final ChromeDriver BROWSER = buildBrowser();

        private static ChromeDriver buildBrowser() {
            ChromeDriver browser = new ChromeDriver();
            Runtime.getRuntime().addShutdownHook(new Thread(browser::close));

            browser.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            browser.get("https://dqydj.com/sp-500-return-calculator/");
            Validatec.contains(browser.getTitle(), "S&P 500 Return Calculator, with Dividend Reinvestment");
            browser.switchTo().frame(0);
            return browser;
        }
    }
}
