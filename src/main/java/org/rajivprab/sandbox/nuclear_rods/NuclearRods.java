package org.rajivprab.sandbox.nuclear_rods;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import org.apache.commons.lang3.Validate;
import org.javatuples.Pair;
import org.rajivprab.cava.Validatec;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A core meltdown has occurred at the Fubaru nuclear plant. There are n nuclear fuel rods that are damaged and need
 * to be removed using specialized radiation-hardened robotic equipment with solid-lead isolation chambers. Remote
 * imaging has already uniquely identified every damaged fuel rod and assigned it a number between 1 and n. The
 * imaging data also records which fuel rods were fused to each other during the meltdown. Every recovery sortie by
 * the robot can pick up one set of nuclear fuel rods that are directly or indirectly fused to each other.
 * <p>
 * The recovery costs per sortie are proportional to the square root of the number of fused rods recovered. So the
 * cost is K to recover K^2 rods. Isolation chambers are available for all positive integer costs (1, 2, 3, …). An
 * isolation chamber can be used multiple times, and each use will incur the same cost. The robot can also recover a
 * lower number of rods than a chamber's capacity on a sortie.
 * <p>
 * Find the minimal cost to recover all the radioactive rods by completing the given function.
 * <p>
 * Input
 * The first parameter integer n specifies the number of rods. The second parameter pairs is an array of pairs of
 * rods that are fused together. Each item in the array contains exactly two integers, P and Q separated by a space
 * (" "), which means that the rod numbered P is fused to the rod numbered Q. *Note - Each item in the array is a
 * string which needs to be parsed to P and Q
 * <p>
 * Output
 * Print the minimal cost for recovering all the rods as an integer.
 * <p>
 * Constraints
 * 2 ≤ N ≤ 100,000
 * 1 ≤ P, Q ≤ N
 * P ≠ Q
 * <p>
 * Created by rprabhakar on 2/28/16.
 */
public class NuclearRods {
    public static int getMinCost(String... input) {
        return new NuclearRods(input).getMinCost();
    }

    private final Map<Integer, Set<Integer>> fusedSets = Maps.newHashMap();
    private final int numRods;
    private boolean stateCleared = false;

    NuclearRods(String... input) {
        numRods = Integer.valueOf(input[0]);
        int numPairs = Integer.valueOf(input[1]);
        Validatec.equals(input.length, numPairs + 2);
        IntStream.range(0, numPairs)
                 .mapToObj(i -> getPair(input[i + 2]))
                 .forEach(this::mergeSets);
    }

    public int getMinCost() {
        Validate.isTrue(!stateCleared, "Can only be called once, because state is cleared on 1st call");
        stateCleared = true;
        return IntStream.range(1, numRods)
                        .mapToObj(this::getSet).sequential()
                        .mapToInt(NuclearRods::clearSetGetCost)
                        .sum();
    }

    static int clearSetGetCost(Set<Integer> fusedSet) {
        long cost = Math.round(Math.ceil(Math.sqrt(fusedSet.size())));
        fusedSet.clear();
        return Ints.checkedCast(cost);
    }

    private void mergeSets(Pair<Integer, Integer> pair) {
        Set<Integer> setP = getSet(pair.getValue0());
        Set<Integer> setQ = getSet(pair.getValue1());
        if (setP != setQ) {     // Performance optimization. Do not merge a set with itself, if rods are indirectly
            // fused
            Validate.isTrue(!setP.equals(setQ), "Sets should be distinct: " + Pair.with(setP, setQ));
            assert Sets.intersection(setP, setQ).isEmpty();     // O(N) operation. Do not enable in production
            setP.addAll(setQ);
            setQ.stream().forEach(i -> fusedSets.put(i, setP));
            // fusedSets.put(pair.getValue1(), setP);
        }
    }

    private Set<Integer> getSet(int rod) {
        if (!fusedSets.containsKey(rod)) {
            fusedSets.put(rod, Sets.newHashSet(rod));
        }
        return fusedSets.get(rod);
    }

    private static Pair<Integer, Integer> getPair(String pair) {
        List<Integer> rods = Arrays.stream(pair.split(" ")).map(Integer::valueOf).collect(Collectors.toList());
        return Pair.with(rods.get(0), rods.get(1));
    }
}
