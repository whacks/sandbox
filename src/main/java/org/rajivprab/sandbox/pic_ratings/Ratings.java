package org.rajivprab.sandbox.pic_ratings;

import com.google.common.collect.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;

import java.util.*;

/**
 * Created by rajivprab on 8/17/17.
 */
public class Ratings {
    private static final Logger log = LogManager.getLogger(Ratings.class);

    private enum Pic {CAR_SOULFUL, HEADSHOT_LESS_LIGHT, SNOW_TIME_SQUARE, THAILAND_SUNGLASS, GENGHIS};

    public static void main(String... args) {
        List<Map<Pic, Double>> matchups = ImmutableList.of(
                ImmutableMap.of(Pic.HEADSHOT_LESS_LIGHT, 39.0, Pic.CAR_SOULFUL, 61.0),
                ImmutableMap.of(Pic.THAILAND_SUNGLASS, 36.0, Pic.GENGHIS, 64.0),
                ImmutableMap.of(Pic.SNOW_TIME_SQUARE, 18.0, Pic.CAR_SOULFUL, 82.0),
                ImmutableMap.of(Pic.GENGHIS, 50.0, Pic.CAR_SOULFUL, 50.0)   // TODO dummy result
        );

        // TODO Shuffle the MATCHUPS order, run multiple times, and average all results
        Map<Pic, Double> scores = compute(matchups);
        for (Pic pic : scores.keySet()) {
            System.out.println("Relative score for pic: " + pic + "\t is: " + scores.get(pic));
        }
    }

    public static Map<Pic, Double> compute(List<Map<Pic, Double>> matchups) {
        Map<Pic, Double> scores = Maps.newHashMap(matchups.get(0));
        LinkedList<Map<Pic, Double>> pendingProcess = Lists.newLinkedList(matchups);
        while (!pendingProcess.isEmpty()) {
            log.info("Remaining to process: " + pendingProcess);
            Iterator<Map<Pic, Double>> iterator = pendingProcess.iterator();
            while (iterator.hasNext()) {
                Map<Pic, Double> cur = iterator.next();
                List<Pic> curPics = ImmutableList.copyOf(cur.keySet());
                Validatec.size(curPics, 2);
                Set<Pic> intersect = Sets.intersection(cur.keySet(), scores.keySet());
                switch (intersect.size()) {
                    case 2:
                        double existingScoreRatio = getPic0Ratio(scores, curPics);
                        double curPic0Ratio = getPic0Ratio(cur, curPics);
                        Validatec.greaterThan(1.0, existingScoreRatio);
                        Validatec.greaterThan(1.0, curPic0Ratio);
                        // TODO To be really accurate, if changing existing scores,
                        // should change all pics influenced by that score as well
                        double averageRatio = (existingScoreRatio + curPic0Ratio) / 2;
                        double sum = scores.get(curPics.get(0)) + scores.get(curPics.get(1));
                        scores.put(curPics.get(0), averageRatio * sum);
                        scores.put(curPics.get(1), (1 - averageRatio) * sum);
                        iterator.remove();
                        break;
                    case 1:
                        curPic0Ratio = getPic0Ratio(cur, curPics);
                        double existingScore = scores.get(intersect.iterator().next());
                        if (scores.containsKey(curPics.get(0))) {
                            scores.put(curPics.get(1), existingScore * (1 - curPic0Ratio) / curPic0Ratio);
                        } else {
                            scores.put(curPics.get(0), existingScore * curPic0Ratio / (1 - curPic0Ratio));
                        }
                        iterator.remove();
                        break;
                    case 0: break;
                    default: throw new IllegalStateException("Bad intersect size");
                }
            }
        }
        return scores;
    }

    // For scores of 38-62, will return 0.38
    private static double getPic0Ratio(Map<Pic, Double> scores, List<Pic> curPics) {
        Validatec.size(curPics, 2);
        double pic0 = scores.get(curPics.get(0));
        double pic1 = scores.get(curPics.get(1));
        return pic0 / (pic0 + pic1);
    }
}
