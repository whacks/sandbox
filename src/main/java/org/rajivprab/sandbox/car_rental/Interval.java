package org.rajivprab.sandbox.car_rental;

import java.time.Duration;
import java.time.Instant;

public class Interval {
    final Instant start;
    final Instant end;

    public Interval(Instant start, Instant end) {
        this.start = start;
        this.end = end;
    }

    public Duration getDuration() {
        return Duration.between(start, end);
    }
}
