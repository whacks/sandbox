package org.rajivprab.sandbox.car_rental;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.stream.Collectors;

public class RentalCompany {
    private final Collection<Car> fleet = Lists.newArrayList();

    public Collection<Car> getCars(Requirements requirements) {
        return fleet.stream().filter(requirements::meetsRequirements).collect(Collectors.toList());
    }

    private Multimap<Car, Interval> getBookings(Interval bookingInterval) {
        Multimap<Car, Interval> map = ArrayListMultimap.create();
        getBookings(map, bookingInterval);
        return map;
    }

    private void getBookings(Multimap<Car, Interval> map, Interval bookingInterval) {
        for (Car car : fleet) {
            if (car.bookingSchedule().isAvailable(bookingInterval)) {
                map.put(car, bookingInterval);
                return;
            }
        }

        Car longestCar = null;
        Interval longestInterval = new Interval(Instant.MIN, Instant.MIN);
        for (Car car : fleet) {
            Instant end = car.bookingSchedule().getLongestAvailabilityAfter(bookingInterval.start);
            if (Duration.between(bookingInterval.start, end).compareTo(longestInterval.getDuration()) > 0) {
                longestCar = car;
                longestInterval = new Interval(bookingInterval.start, end);
            }

            Instant start = car.bookingSchedule().getLongestAvailabilityBefore(bookingInterval.end);
            if (Duration.between(start, bookingInterval.end).compareTo(longestInterval.getDuration()) > 0) {
                longestCar = car;
                longestInterval = new Interval(start, bookingInterval.end);
            }
        }
        map.put(longestCar, longestInterval);

        if (longestInterval.start.equals(bookingInterval.start)) {
            getBookings(map, new Interval(longestInterval.end, bookingInterval.end));
        } else {
            getBookings(map, new Interval(bookingInterval.start, longestInterval.start));
        }
    }
}
