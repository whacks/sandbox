package org.rajivprab.sandbox.car_rental;

public class UberRequirements implements Requirements {
    @Override
    public boolean meetsRequirements(Car car) {
        return car.numDoors() == 4 && car.age() <= 4;
    }
}
