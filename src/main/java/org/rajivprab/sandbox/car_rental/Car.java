package org.rajivprab.sandbox.car_rental;

public interface Car {
    int numDoors();
    int age();
    Schedule bookingSchedule();
}
