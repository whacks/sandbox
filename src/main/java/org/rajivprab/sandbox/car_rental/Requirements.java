package org.rajivprab.sandbox.car_rental;

public interface Requirements {
    boolean meetsRequirements(Car car);
}
