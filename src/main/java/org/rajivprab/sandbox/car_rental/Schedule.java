package org.rajivprab.sandbox.car_rental;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

public class Schedule {
    private final List<Interval> bookings = Lists.newArrayList();

    boolean isAvailable(Interval interval) {
        return bookings.stream().noneMatch(
                booking -> overlaps(interval.start, interval.end, booking.start, booking.end));
    }

    Instant getLongestAvailabilityAfter(Instant start) {
        return null;
    }

    Instant getLongestAvailabilityBefore(Instant end) {
        return null;
    }

    Collection<Interval> getBookings() {
        return ImmutableList.copyOf(bookings);
    }

    boolean book(Interval interval) {
        if (!isAvailable(interval)) { return false; }
        bookings.add(interval);
        return true;
    }

    @VisibleForTesting
    static boolean overlaps(Instant start1, Instant end1, Instant start2, Instant end2) {
        return start2.isBefore(end1) && end2.isAfter(start1);
    }
}
