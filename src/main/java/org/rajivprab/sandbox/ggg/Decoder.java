package org.rajivprab.sandbox.ggg;

import com.google.common.collect.Lists;
import org.rajivprab.cava.Validatec;
import org.rajivprab.sandbox.trie.Trie;

import java.util.ListIterator;

/**
 * https://www.reddit.com/r/dailyprogrammer/comments/3x3hqa/20151216_challenge_245_intermediate_ggggggg_gggg/
 *
 * Created by rajivprab on 6/24/17.
 */
public class Decoder {
    public static void main(String... args) {
        System.out.println(decode(args[0], args[1]));
    }

    static String decode(String encoding, String alienMessage) {
        Trie<Character, Character> alienToHumanDictionary = buildDictionary(encoding);
        return decode(alienMessage, alienToHumanDictionary);
    }

    // Returns mapping of gGg... -> human-asci-character
    private static Trie<Character, Character> buildDictionary(String encoding) {
        String[] elements = encoding.split(" ");
        Trie<Character, Character> dictionary = Trie.newSimpleTrie();
        for (int i=0; i<elements.length; i+=2) {
            Validatec.length(elements[i], 1);
            Validatec.matches(elements[i+1], "[gG]+");
            dictionary.put(elements[i].charAt(0), Lists.charactersOf(elements[i+1]));
        }
        return dictionary;
    }

    private static String decode(String alienMessage, Trie<Character, Character> dictionary) {
        StringBuilder decoded = new StringBuilder();
        ListIterator<Character> inputIterator = Lists.charactersOf(alienMessage).listIterator();
        while (inputIterator.hasNext()) {
            decoded.append(getNextHumanCharacter(inputIterator, dictionary));
        }
        return decoded.toString();
    }

    // When function returns, input.next() will point towards the next input-char that needs to be parsed
    private static Character getNextHumanCharacter(ListIterator<Character> input, Trie<Character, Character> lookup) {
        if (lookup.getValue().isPresent()) { return lookup.getValue().get(); }

        Validatec.isTrue(input.hasNext());
        Character curChar = input.next();
        return lookup.get(curChar).map(trie -> getNextHumanCharacter(input, trie)).orElse(curChar);
    }
}
