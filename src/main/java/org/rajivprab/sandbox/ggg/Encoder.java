package org.rajivprab.sandbox.ggg;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.Validatec;
import org.rajivprab.sandbox.trie.Trie;

import java.util.*;

/**
 * Created by rajivprab on 6/24/17.
 */
public class Encoder {
    private static final Random RNG = new Random();

    public static String printEncoding(Map<Character, String> encoding) {
        StringBuilder output = new StringBuilder();
        for (char character : encoding.keySet()) {
            output.append(character).append(" ").append(encoding.get(character)).append(" ");
        }
        return output.toString().trim();
    }

    public static Map<Character, String> generateEncoding(String humanMessage) {
        Trie<Character, Character> alienToHumanDictionary = Trie.newSimpleTrie();
        Set<Character> humanCharactersParsed = Sets.newHashSet();
        for (char character : humanMessage.toCharArray()) {
            if (Character.isLetter(character) && humanCharactersParsed.add(character)) {
                populate(character, alienToHumanDictionary);
            }
        }
        return getHumanToAlienEncoding(alienToHumanDictionary);
    }

    public static String getAlienMessage(String humanMessage, Map<Character, String> encoding) {
        StringBuilder alienMessage = new StringBuilder();
        for (int i = 0; i < humanMessage.length(); i++) {
            char cur = humanMessage.charAt(i);
            alienMessage.append(Optional.ofNullable(encoding.get(cur)).orElse(cur + ""));
        }
        return alienMessage.toString();
    }

    // --------

    private static void populate(char character, Trie<Character, Character> alienToHumanDictionary) {
        Collection<Character> children = alienToHumanDictionary.getKeys();
        if (children.isEmpty()) {
            Optional<Character> existing = alienToHumanDictionary.remove();
            if (existing.isPresent()) {
                char randomChild = getRandomAlienCharacter();
                Optional<Character> existing1 = alienToHumanDictionary.put(existing.get(), randomChild);
                Optional<Character> existing2 = alienToHumanDictionary.put(character, getComplement(randomChild));
                Validate.isTrue(!existing1.isPresent() && !existing2.isPresent());
            } else {
                existing = alienToHumanDictionary.put(character);
                Validate.isTrue(!existing.isPresent());
            }
        } else if (children.size() == 1) {
            char child = getComplement(alienToHumanDictionary.getKeys().iterator().next());
            Optional<Character> existing = alienToHumanDictionary.put(character, child);
            Validate.isTrue(!existing.isPresent());
        } else {
            Validatec.size(alienToHumanDictionary.getKeys(), 2);
            populate(character, alienToHumanDictionary.get(getRandomAlienCharacter()).get());
        }
    }

    private static Map<Character, String> getHumanToAlienEncoding(Trie<Character, Character> alienToHumanDictionary) {
        Map<Character, String> result = Maps.newHashMap();
        getEncodingHelper(result, alienToHumanDictionary, "");
        return result;
    }

    private static void getEncodingHelper(Map<Character, String> humanToAlien,
                                          Trie<Character, Character> alienToHuman,
                                          String prefix) {
        if (alienToHuman.getValue().isPresent()) {
            Validatec.size(alienToHuman.getKeys(), 0);
            humanToAlien.put(alienToHuman.getValue().get(), prefix);
        } else {
            for (char child : alienToHuman.getKeys()) {
                getEncodingHelper(humanToAlien, alienToHuman.get(child).get(), prefix + child);
            }
        }
    }

    private static char getComplement(char cur) {
        Validate.isTrue(cur == 'g' || cur == 'G', "Found: " + cur);
        return cur == 'G' ? 'g' : 'G';
    }

    private static char getRandomAlienCharacter() {
        return RNG.nextBoolean() ? 'g' : 'G';
    }
}
