package org.rajivprab.sandbox.risk;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by rajivprab on 5/21/17.
 */
public class Battle {
    private static final Random RNG = new Random();

    public static int rollDie() {
        return RNG.nextInt(6) + 1;
    }

    // List of integer is sorted from highest to lowest
    public static List<Integer> rollDie(int numDie) {
        List<Integer> rolls = IntStream.range(0, numDie).mapToObj(i -> rollDie()).collect(Collectors.toList());
        Collections.sort(rolls);
        Collections.reverse(rolls);
        return rolls;
    }

    public static Outcome getSingleOutcome(int numAttackers, int numDefenders) {
        List<Integer> attackerRolls = rollDie(Math.min(numAttackers, 3));
        List<Integer> defenderRolls = rollDie(Math.min(numDefenders, 2));
        return Outcome.compute(attackerRolls.iterator(), defenderRolls.iterator());
    }

}
