package org.rajivprab.sandbox.risk;

import org.rajivprab.cava.Validatec;

/**
 * Created by rajivprab on 5/21/17.
 */
public class Campaign {
    public static Outcome wageCampaign(int numAttackers, int numDefenders) {
        Validatec.greaterThan(numAttackers, 0);
        Validatec.greaterThan(numDefenders, 0);

        Outcome outcome = new Outcome(0, 0);
        while (numAttackers > outcome.numDeadAttacker && numDefenders > outcome.numDeadDefender) {
            outcome = outcome.merge(Battle.getSingleOutcome(numAttackers - outcome.numDeadAttacker,
                                                            numDefenders - outcome.numDeadDefender));
        }

        Validatec.greaterOrEqual(numAttackers, outcome.numDeadAttacker);
        Validatec.greaterOrEqual(numDefenders, outcome.numDeadDefender);
        Validatec.isTrue((outcome.numDeadAttacker == numAttackers) ^ (outcome.numDeadDefender == numDefenders));
        return outcome;
    }

    public static boolean attackerWins(int numAttackers, int numDefenders) {
        return wageCampaign(numAttackers, numDefenders).numDeadAttacker < numAttackers;
    }
}
