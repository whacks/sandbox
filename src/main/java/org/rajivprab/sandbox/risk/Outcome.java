package org.rajivprab.sandbox.risk;

import java.util.Iterator;

/**
 * Created by rajivprab on 5/21/17.
 */
public class Outcome {
    public final int numDeadAttacker;
    public final int numDeadDefender;

    public Outcome(int numDeadAttacker, int numDeadDefender) {
        this.numDeadAttacker = numDeadAttacker;
        this.numDeadDefender = numDeadDefender;
    }

    public static Outcome compute(Iterator<Integer> attackerRolls, Iterator<Integer> defenderRolls) {
        int numDeadAttacker = 0;
        int numDeadDefender = 0;
        while (attackerRolls.hasNext() && defenderRolls.hasNext()) {
            int attacker = attackerRolls.next();
            int defender = defenderRolls.next();
            if (attacker > defender) {
                numDeadDefender++;
            } else {
                numDeadAttacker++;
            }
        }
        return new Outcome(numDeadAttacker, numDeadDefender);
    }

    public Outcome merge(Outcome outcome) {
        return new Outcome(numDeadAttacker + outcome.numDeadAttacker,
                           numDeadDefender + outcome.numDeadDefender);
    }
}
