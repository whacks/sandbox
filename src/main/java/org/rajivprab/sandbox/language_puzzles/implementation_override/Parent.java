package org.rajivprab.sandbox.language_puzzles.implementation_override;

public class Parent {
    void bar () { foo(); }
    void foo () { System.out.println("I am Parent!"); }
}
