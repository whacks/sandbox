package org.rajivprab.sandbox.language_puzzles.hidden_override;

public class Main {
    public static void main(String... args) {
        // What gets invoked?
        String string = "";
        Parent parent = new Child();
        parent.print(string);		// Answer: Parent prints Object
    }
    /*
    Because "parent" is actually an instance of Child, and "string" is of type String,
    it's tempting to think that Child::print(String) will be invoked.
    However, because the only method available in Parent is print(Object), Parent::print(Object) is what gets invoked.
     */
}
