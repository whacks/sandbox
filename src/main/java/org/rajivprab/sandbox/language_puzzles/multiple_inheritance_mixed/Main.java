package org.rajivprab.sandbox.language_puzzles.multiple_inheritance_mixed;

public class Main {
    public static void main(String... args) {
        // What gets invoked?
        new Child().print();
    }
}
