package org.rajivprab.sandbox.language_puzzles.multiple_inheritance_mixed;

public interface ParentInterface {
    default void print() { System.out.println("I am a interface!"); }
}
