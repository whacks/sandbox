package org.rajivprab.sandbox.language_puzzles.exposed_override;

class Parent {
    void print(Object a) { System.out.println("Parent prints Object!"); }
    void print(String a) { System.out.println("Parent prints String!"); }
}
