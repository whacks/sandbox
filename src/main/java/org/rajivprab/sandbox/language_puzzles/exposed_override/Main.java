package org.rajivprab.sandbox.language_puzzles.exposed_override;

public class Main {
    public static void main(String... args) {
        // What gets invoked?
        String string = "";
        Parent parent = new Child();
        parent.print(string);		// Answer: Child prints String
    }
    /*
    The code here is identical to the earlier one in hidden_override, except for one difference:
    A new Parent.print(String) method has been added.
    It is tempting to think that simply adding a new method will not cause *some other* method to get run,
    but that is exactly what happens here.
    Because this new method was added, Parent.print(String) is preferred to Parent.print(Object),
    and Child.print(String) overrides Parent.print(String), thus resulting in Child.print(String) being invoked.
     */
}
