package org.rajivprab.sandbox.language_puzzles;

import java.util.Optional;

public class OptionalPuzzles {

    public static void main(String... args) {
        Optional<String> opt = Optional.ofNullable("abc");
        int lengthNull = opt.map(OptionalPuzzles::transform).map(String::length).orElse(-1);
        System.out.println(lengthNull);
    }

    private static String transform(String input) {
        return null;
    }
}
