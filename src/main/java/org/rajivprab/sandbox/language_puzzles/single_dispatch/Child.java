package org.rajivprab.sandbox.language_puzzles.single_dispatch;

class Child extends Parent {
    void print(String a) {
        System.out.println("Child prints String");
    }

    void print(Object a) {
        System.out.println("Child prints Object");
    }
}
