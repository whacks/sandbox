package org.rajivprab.sandbox.language_puzzles.single_dispatch;

public class Main {
    public static void main(String... args) {
        String string = "";
        Object stringObject = string;

        // What gets invoked?
        Child child = new Child();
        child.print(string);		// Answer: Child prints String
        child.print(stringObject);	// Answer: Parent prints Object

        Parent parent = new Child();
        parent.print(string);		// Answer: Child prints String
        parent.print(stringObject);	// Answer: Parent prints Object
    }
    /*
    Java does not have double dispatch. The method selected depends on the parameter's declared type, not actual type.
    Hence, child.print(stringObject) invokes a different method compared to child.print(string), even though
    stringObject == string
     */
}
