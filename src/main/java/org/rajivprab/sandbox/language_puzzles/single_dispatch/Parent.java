package org.rajivprab.sandbox.language_puzzles.single_dispatch;

class Parent {
    void print(String a) {
        System.out.println("Parent prints String");
    }

    void print(Object a) {
        System.out.println("Parent prints Object");
    }
}
