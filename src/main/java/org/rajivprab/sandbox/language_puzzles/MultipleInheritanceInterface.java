package org.rajivprab.sandbox.language_puzzles;

public class MultipleInheritanceInterface {
    interface Foo {
        default void print() { System.out.println("I am Foo!"); }
    }

    interface Bar {
        default void print() { System.out.println("I am Bar!"); }
    }

    // static class Baz implements Foo, Bar {}

    public static void main(String... args) {
        // What gets invoked?
        // new Baz().print();      // Answer: Does not compile
    }
}
