package org.rajivprab.sandbox.language_puzzles;

public class Simple {
    static class Parent {
        void print(String a) { System.out.println("I am Parent!"); }
    }

    static class Child extends Parent {
        void print(String a) { System.out.println("I am Child!"); }
    }

    public static void main(String... args) {
        String string = "";

        // What gets invoked?
        Child child = new Child();
        Parent parent = child;
        child.print(string);		// Answer: Child::print
        parent.print(string);		// Answer: Child::print
    }
    /*
    Explanation: Simple polymorphism. The method invoked depends on the actual instance type, not the declared type.
    Even if "parent" is declared as type Parent, because its actual type is Child, Child::print is what gets invoked
     */
}
