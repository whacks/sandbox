package org.rajivprab.sandbox.language_puzzles;

import java.util.HashMap;
import java.util.Map;

public class AmbiguousParameter {
    static class Foo {
        void print(Cloneable a) { System.out.println("I am cloneable!"); }
        void print(Map a) { System.out.println("I am Map!"); }
    }

    public static void main(String... args) {
        HashMap cloneableMap = new HashMap();
        Cloneable cloneable = cloneableMap;
        Map map = cloneableMap;

        // What gets invoked?
        Foo foo = new Foo();
        foo.print(map);            // Answer: foo(Map)
        foo.print(cloneable);      // Answer: foo(Cloneable)
        // foo.print(cloneableMap);   // Answer: Does not compile

        /*
        Similar to the single_dispatch example, what matters here is the declared type of the parameter,
        not the actual type. In addition, if there are multiple methods that are equally valid for a given parameter,
        Java throws a compile error and forces you to specify which one should be called.
         */
    }
}
