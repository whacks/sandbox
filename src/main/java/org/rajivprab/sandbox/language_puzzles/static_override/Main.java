package org.rajivprab.sandbox.language_puzzles.static_override;

public class Main {
    public static void main(String... args) {
        Child child = new Child();
        Parent parent = child;

        Parent.foo();
        Child.foo();
        parent.foo();
        child.foo();
    }
}
