package org.rajivprab.sandbox.language_puzzles.static_linking;

public class Child extends Parent {
    public static void staticMethod() { System.out.println("I am Child::staticMethod!"); }
    public void instanceMethod() { System.out.println("I am Child::instanceMethod!"); }
}