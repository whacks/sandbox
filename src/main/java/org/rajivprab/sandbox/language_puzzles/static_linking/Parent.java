package org.rajivprab.sandbox.language_puzzles.static_linking;

public class Parent {
    public void foo() { staticMethod(); instanceMethod(); }
    public static void staticMethod() { System.out.println("I am Parent::staticMethod!"); }
    public void instanceMethod() { System.out.println("I am Parent::instanceMethod!"); }
}