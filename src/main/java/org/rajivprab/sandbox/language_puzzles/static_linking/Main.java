package org.rajivprab.sandbox.language_puzzles.static_linking;

public class Main {
    public static void main(String... args) {
        Child child = new Child();
        Parent parent = child;

        parent.foo();
        child.foo();
    }
}
