package org.rajivprab.sandbox.language_puzzles.private_implementation_override;

public class Parent {
    void bar () { foo(); }
    private void foo () { System.out.println("I am Parent!"); }
}
