package org.rajivprab.sandbox.language_puzzles.private_implementation_override;

public class Child extends Parent {
    void foo () { System.out.println("I am Child!"); }
}
