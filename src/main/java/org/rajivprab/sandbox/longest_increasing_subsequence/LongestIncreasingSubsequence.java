package org.rajivprab.sandbox.longest_increasing_subsequence;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.Map;

/**
 * https://leetcode.com/problems/longest-increasing-subsequence-ii/
 *
 * You are given an integer array nums and an integer k.
 *
 * Find the longest subsequence of nums that meets the following requirements:
 *
 * The subsequence is strictly increasing and
 * The difference between adjacent elements in the subsequence is at most k.
 * Return the length of the longest subsequence that meets the requirements.
 *
 * A subsequence is an array that can be derived from another array by deleting some or no elements without
 * changing the order of the remaining elements.
 */
public class LongestIncreasingSubsequence {

    private static final Logger log = LogManager.getLogger(LongestIncreasingSubsequence.class);

    // Solution:
    // - Create method to compute the longest subsequence-length that can be constructed if the first element in
    // the subsequence is index i.
    // - To implement this method, find all elements after i, which are larger than but
    // within k of the i-th element.
    // - Use recursion to find the longest length when starting with each of those elements
    // - The longest length will then be 1 + largest_recursive_return_value
    // 2. Cache the result of the above call. For each index i, the method will then be computed at most once
    public static int lengthOfLIS(List<Integer> nums, int k) {
        return new LongestIncreasingSubsequence(nums, k).lengthOfLis();
    }

    private final List<Integer> nums;
    private final int k;
    private final Map<Integer, Integer> startingWithIndexToLength = Maps.newHashMap();

    private LongestIncreasingSubsequence(List<Integer> nums, int k) {
        this.nums = ImmutableList.copyOf(nums);
        this.k = k;
    }

    private int lengthOfLis() {
        int maxLength = Integer.MIN_VALUE;
        for (int startingWith=0; startingWith<nums.size(); startingWith++) {
            int candidateLength = lengthOfLIS(startingWith);
            maxLength = Math.max(maxLength, candidateLength);
        }
        return maxLength;
    }

    private int lengthOfLIS(int startingWithIndex) {
        Integer cached = startingWithIndexToLength.get(startingWithIndex);
        if (cached != null) {
            log.info("Cache hit!");
            return cached;
        }

        int result = lengthOfLISWorker(startingWithIndex);
        startingWithIndexToLength.put(startingWithIndex, result);
        return result;
    }

    private int lengthOfLISWorker(int startingWithIndex) {
        Validatec.greaterThan(nums.size(), startingWithIndex);
        int longestLengthFound = 1;
        int startingValue = nums.get(startingWithIndex);
        for (int next=startingWithIndex+1; next<nums.size(); next++) {
            int nextVal = nums.get(next);
            if ((nextVal > startingValue) && (nextVal - startingValue <= k)) {
                int candidateLength = 1 + lengthOfLIS(next);
                longestLengthFound = Math.max(longestLengthFound, candidateLength);
            }
        }
        log.info("Longest length starting with index {} is {}", startingWithIndex, longestLengthFound);
        return longestLengthFound;
    }
}
