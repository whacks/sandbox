package org.rajivprab.sandbox.combinatorial;

import com.google.common.collect.Sets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Set;

// https://projecteuler.net/problem=53
class Selections {
    private static final Logger log = LogManager.getLogger(Selections.class);

    static int getPairs() {
        int found = 0;
        for (long n=1; n<=100; n++) {
            int curFound = getRValuesExceedingMillion(n);
            found += curFound;
        }
        return found;
    }

    private static int getRValuesExceedingMillion(long n) {
        // Can cut down number of iterations by 2, but keep it simple to avoid off-by-1 error
        for (long r=1; r<=n; r++) {
            long result = Choose.get(n, r);
            if (result > 1000 * 1000) {
                log.info(n + " choose " + r + " = " + result);
                // Can cut down number of iterations by 2, but keep it simple to avoid off-by-1 error
                Set<Long> rValues = Sets.newHashSet();
                for (long rNeighbor=r; rNeighbor<=n-r; rNeighbor++) {
                    rValues.add(rNeighbor);
                    rValues.add(n-rNeighbor);
                }
                log.info("Found candidates: " + rValues + " for n=" + n);
                return rValues.size();
            }
        }
        return 0;
    }
}
