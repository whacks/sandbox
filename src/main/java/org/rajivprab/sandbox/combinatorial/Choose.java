package org.rajivprab.sandbox.combinatorial;

import com.google.common.collect.Maps;
import org.rajivprab.cava.Validatec;

import java.util.Map;

public class Choose {
    private static final Map<CacheKey, Long> chooseCache = Maps.newConcurrentMap();

    static long get(long n, long r) {
        CacheKey key = new CacheKey(n, r);
        return chooseCache.computeIfAbsent(key, k -> computeChoose(n, r));
    }

    // (n choose r) == n! / ( r! * (n-r)! )
    private static long computeChoose(long n, long r) {
        if (n==r) { return 1; }

        // N choose R == N choose (N-R)
        // Computation is faster when we use the larger value of R vs (N-R)
        long inverseR = n-r;
        r = Math.max(r, inverseR);

        Validatec.greaterThan(n, r);
        // return Factorial.get(n) / (Factorial.get(r) * Factorial.get(n-r)); very inefficient

        // n! / r! == n * (n-1) * (n-2) * .... * (r+1)
        long numerator = Factorial.multiplySequentially(r+1, n);
        long denominator = Factorial.get(n-r);
        return numerator / denominator;
    }
}
