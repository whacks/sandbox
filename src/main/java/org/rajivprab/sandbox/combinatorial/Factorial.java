package org.rajivprab.sandbox.combinatorial;

import com.google.common.collect.Maps;
import org.rajivprab.cava.Validatec;

import java.util.Map;

public class Factorial {
    private static final Map<CacheKey, Long> factorialCache = Maps.newConcurrentMap();

    static long get(long val) {
        return multiplySequentially(1, val);
    }

    static long multiplySequentially(long startInclusive, long endInclusive) {
        CacheKey key = new CacheKey(startInclusive, endInclusive);
        return factorialCache.computeIfAbsent(key, k -> computeMultiplySequentially(startInclusive, endInclusive));
    }

    private static long computeMultiplySequentially(long startInclusive, long endInclusive) {
        Validatec.greaterOrEqual(endInclusive, startInclusive);
        long val = startInclusive;
        long cur = startInclusive;
        while (cur < endInclusive) {
            cur++;
            long newVal = val * cur;
            Validatec.greaterOrEqual(newVal, val, "Likely failing because of overflow error");
            val = newVal;
        }
        return val;
    }

}
