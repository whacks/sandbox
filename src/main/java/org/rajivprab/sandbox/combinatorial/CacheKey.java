package org.rajivprab.sandbox.combinatorial;

import java.util.Objects;

class CacheKey {
    private final long a;
    private final long b;

    CacheKey(long a, long b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CacheKey)) return false;
        CacheKey cacheKey = (CacheKey) o;
        return a == cacheKey.a &&
                b == cacheKey.b;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }
}
