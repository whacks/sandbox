package org.rajivprab.sandbox.composition_over_inheritance;

import java.util.LinkedList;

public class SimpleStack<T> implements Stack<T> {
    private final LinkedList<T> list = new LinkedList<>();

    @Override
    public void push(T val) {
        list.addLast(val);
    }

    @Override
    public T pop() {
        return list.removeLast();
    }
}
