package org.rajivprab.sandbox.composition_over_inheritance.inheritance;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class LoggingSafeStack<T> extends SafeStack<T> {
    private static final Logger log = LogManager.getLogger(LoggingSafeStack.class);

    @Override
    public void push(T val) {
        super.push(val);
        log.info("Pushed " + val + " into stack");
    }

    @Override
    public T pop() {
        T value = super.pop();
        log.info("Popped value: " + value);
        return value;
    }
}
