package org.rajivprab.sandbox.composition_over_inheritance.inheritance;

import org.rajivprab.sandbox.composition_over_inheritance.SimpleStack;

import java.util.NoSuchElementException;

class SafeStack<T> extends SimpleStack<T> {
    // Returns null instead of throwing exception
    @Override
    public T pop() {
        try {
            return super.pop();
        } catch (NoSuchElementException e) {
            return null;
        }
    }
}
