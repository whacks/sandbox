package org.rajivprab.sandbox.composition_over_inheritance;

public interface Stack<T> {
    void push(T val);
    T pop();
}
