package org.rajivprab.sandbox.composition_over_inheritance.composition;

import org.rajivprab.sandbox.composition_over_inheritance.SimpleStack;
import org.rajivprab.sandbox.composition_over_inheritance.Stack;

public class Composition {
    public static void main(String... args) {
        Stack<Integer> exceptionThrowingStack = new SimpleStack<>();
        Stack<Integer> safeStack = new SafeStack<>(exceptionThrowingStack);
        Stack<Integer> safeStackWithLogging = new LoggingStack<>(safeStack);

        // Not possible in the inheritance example
        Stack<Integer> exceptionThrowingStackWithLogging = new LoggingStack<>(exceptionThrowingStack);
    }
}
