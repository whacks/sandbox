package org.rajivprab.sandbox.composition_over_inheritance.composition;

import org.rajivprab.sandbox.composition_over_inheritance.Stack;

import java.util.NoSuchElementException;

class SafeStack<T> implements Stack<T> {
    private final Stack<T> underlying;

    SafeStack(Stack<T> underlying) {
        this.underlying = underlying;
    }

    @Override
    public void push(T val) {
        underlying.push(val);
    }

    // Returns null instead of throwing exception
    @Override
    public T pop() {
        try {
            return underlying.pop();
        } catch (NoSuchElementException e) {
            return null;
        }
    }
}
