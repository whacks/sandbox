package org.rajivprab.sandbox.composition_over_inheritance.composition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.sandbox.composition_over_inheritance.Stack;

class LoggingStack<T> implements Stack<T> {
    private static final Logger log = LogManager.getLogger(LoggingStack.class);

    private final Stack<T> underlying;

    LoggingStack(Stack<T> underlying) {
        this.underlying = underlying;
    }

    @Override
    public void push(T val) {
        underlying.push(val);
        log.info("Pushed " + val + " into stack");
    }

    @Override
    public T pop() {
        T value = underlying.pop();
        log.info("Popped value: " + value);
        return value;
    }
}
