package org.rajivprab.sandbox.pi_estimate;

import org.javatuples.Pair;

import java.util.Random;
import java.util.stream.LongStream;

/**
 * Unit circle with radius 1: area = pi * 1^2 = pi
 * Square with length 2: area = 2*2 = 4
 *
 * Consider a square, with a unit circle positioned exactly inside the square.
 * The square's edges are located at [-1, -1], [-1, 1], [1, -1], [1, 1]
 * And the unit circle's origin is located at [0, 0]
 *
 * Pick a random point anywhere within the square. The odds of that point lying within the unit circle, is given by
 * => circle_area / square_area
 * => pi / 4
 *
 * We can run an experiment, where we use Java's RNG to pick N random points, and compute the fraction of those points
 * which are located within the circle. This fraction would then give us an estimate for {pi/4}
 */
public class PiEstimate {
    private static final Random RNG = new Random();

    // TODO add unit test
    public static void main(String... args) {
        System.out.println("Pi is approximately " + estimate(1000000));
    }

    static double estimate(long sampleSize) {
        return fractionWithinCircle(sampleSize) * 4;
    }

    private static double fractionWithinCircle(long numPoints) {
        long withinCircle = LongStream.range(0, numPoints)
                .mapToObj(i -> getRandomPoint())
                .map(PiEstimate::isWithinCircle)
                .filter(isWithinCircle -> isWithinCircle)
                .count();

        return withinCircle * 1.0 / numPoints;
    }

    private static boolean isWithinCircle(Pair<Double, Double> point) {
        double distanceFromOrigin = Math.pow(point.getValue0(), 2) + Math.pow(point.getValue1(), 2);
        return distanceFromOrigin < 1;
    }

    private static Pair<Double, Double> getRandomPoint() {
        double x = RNG.nextDouble() * 2 - 1;
        double y = RNG.nextDouble() * 2 - 1;
        return Pair.with(x, y);
    }
}
