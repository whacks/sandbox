package org.rajivprab.sandbox.dom_text_compare;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.Validate;

import java.util.List;
import java.util.Optional;

// Each node can have text content, or children htmlnodes, but not both
class HtmlNode {
    final String tagName;
    final Optional<String> textContent;
    final ImmutableList<HtmlNode> children;

    static HtmlNode buildNode(String text) {
        return new HtmlNode("random tag name", Optional.of(text), ImmutableList.of());
    }

    static HtmlNode buildNode(HtmlNode... children) {
        return new HtmlNode("random tag name", Optional.empty(), ImmutableList.copyOf(children));
    }

    private HtmlNode(String tagName, Optional<String> textContent, List<HtmlNode> children) {
        Validate.isTrue(!textContent.isPresent() || children.isEmpty());
        this.tagName = tagName;
        this.textContent = textContent;
        this.children = ImmutableList.copyOf(children);
    }
}
