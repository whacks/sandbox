package org.rajivprab.sandbox.dom_text_compare;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.Validatec;

import java.util.Iterator;

public class HtmlTextIterator implements Iterator<Character> {
    private final HtmlNode node;

    private int curChild;
    private Iterator<Character> curIterator;

    public static Iterator<Character> getTextIterator(HtmlNode node) {
        if (node.textContent.isPresent()) {
            return Lists.charactersOf(node.textContent.get()).iterator();
        } else if (node.children.isEmpty()) {
            return Lists.charactersOf("").iterator();
        } else {
            return new HtmlTextIterator(node);
        }
    }

    public HtmlTextIterator(HtmlNode node) {
        Validatec.isFalse(node.textContent.isPresent());
        Validatec.notEmpty(node.children);

        this.node = node;
        this.curChild = 0;
        updateCurIterator();
    }

    @Override
    public boolean hasNext() {
        if (curIterator.hasNext()) { return true; }

        // Need to advance to the next child if available, since curIterator is depleted
        while (curChild < node.children.size() - 1) {
            curChild++;
            updateCurIterator();
            if (curIterator.hasNext()) { return true; }
        }

        // All children have been depleted
        return false;
    }

    @Override
    public Character next() {
        Validate.isTrue(hasNext());
        return curIterator.next();
    }

    private void updateCurIterator() {
        curIterator = getTextIterator(node.children.get(curChild));
    }
}
