package org.rajivprab.sandbox.dom_text_compare;

import java.util.Iterator;

class Compare {

    // Uses constant memory space to compare the plain-text values of 2 HtmlNodes
    static boolean textEquals(HtmlNode a, HtmlNode b) {
        Iterator<Character> aText = HtmlTextIterator.getTextIterator(a);
        Iterator<Character> bText = HtmlTextIterator.getTextIterator(b);

        while (aText.hasNext() && bText.hasNext()) {
            if (!aText.next().equals(bText.next())) {
                return false;
            }
        }
        return !aText.hasNext() && !bText.hasNext();
    }
}
