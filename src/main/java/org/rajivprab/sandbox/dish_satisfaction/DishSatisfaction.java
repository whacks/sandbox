package org.rajivprab.sandbox.dish_satisfaction;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import org.apache.commons.lang3.Validate;

import java.util.Collections;
import java.util.List;

// https://leetcode.com/problems/reducing-dishes/
public class DishSatisfaction {

    // For more efficient solution, see: https://leetcode.com/problems/reducing-dishes/discuss/563384/JavaC%2B%2BPython-Easy-and-Concise
    static int maxSatisfaction(List<Integer> dishSatisfaction) {
        List<Integer> copy = Lists.newArrayList(dishSatisfaction);
        Collections.sort(copy);

        return maxSatisfaction(ImmutableList.copyOf(copy), 0, 0);
    }

    private static int maxSatisfaction(List<Integer> dishSatisfaction, int timeSpentSoFar, int startingDish) {
        if (dishSatisfaction.size() == startingDish) { return 0; }

        // Remove validation if trying to improve performance
        Validate.isTrue(Ordering.natural().isOrdered(dishSatisfaction), "Dishes are not sorted: %s", dishSatisfaction);

        int cur = dishSatisfaction.get(startingDish);

        int scoreWithDishCooked = (timeSpentSoFar + 1) * cur +
                maxSatisfaction(dishSatisfaction, timeSpentSoFar + 1, startingDish + 1);
        if (cur >= 0) { return scoreWithDishCooked; }

        int scoreWithoutDishCooked = maxSatisfaction(dishSatisfaction, timeSpentSoFar, startingDish + 1);
        return Math.max(scoreWithDishCooked, scoreWithoutDishCooked);
    }
}
