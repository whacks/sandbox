package org.rajivprab.sandbox.order_book;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class OrderBook {
    private static final int MAX_LEVELS = 10;

    private final File inputFile;
    private final File outputFile;

    // Should perform some benchmarking to see if LinkedLists will perform better here.
    // Potentially faster inserts/deletes, though slower on all lookups and updates
    private List<Order> bids = new ArrayList<>();
    private List<Order> asks = new ArrayList<>();
    private boolean bookIsKnown = false;

    public static void main(String... args) throws FileNotFoundException {
        File input = new File(args[0]);
        File output = new File(args[1]);
        new OrderBook(input, output).run();
    }

    public OrderBook(File inputFile, File outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }

    public void run() throws FileNotFoundException {
        for (int i = 0; i < MAX_LEVELS; i++) {
            // Populate the OrderBook with all MAX_LEVELS orders, with state set to unknown
            // These unknown orders can then be updated as more information arrives
            bids.add(new Order());
            asks.add(new Order());
        }
        PacketIterator packetIterator = new PacketIterator(inputFile);
        outputFile.delete();
        try (PrintWriter outputWriter = new PrintWriter(outputFile)) {
            while (packetIterator.hasNext()) {
                List<Instruction> instructions = packetIterator.next();
                // Apply all instructions with same sequenceNumber, prior to printing anything
                instructions.forEach(this::apply);
                print(instructions.get(0).sequenceNumber, outputWriter);
            }
        }
    }

    private void print(int sequenceNumber, PrintWriter writer) {
        // Only print if there are no unknown entries in the OrderBook
        // Once this condition is true once, it will always be true. Hence, set the flag, and never evaluate it again
        bookIsKnown = bookIsKnown || bookIsKnown();
        if (bookIsKnown) {
            StringJoiner outputs = new StringJoiner(", ");
            outputs.add(sequenceNumber + "");
            printOrders(bids, outputs);
            printOrders(asks, outputs);
            writer.println(outputs.toString());
        }
    }

    private boolean bookIsKnown() {
        for (Order order : bids) {
            if (!order.knownState) { return false; }
        }
        for (Order order : asks) {
            if (!order.knownState) { return false; }
        }
        return true;
    }

    private static void printOrders(List<Order> orders, StringJoiner output) {
        for (Order order : orders) {
            output.add(order.price + "");
            output.add(order.size + "");
        }
        // Populate dashes for any empty levels, as shown in the examples
        for (int i = orders.size(); i < MAX_LEVELS; i++) {
            output.add("-");
            output.add("-");
        }
    }

    private void apply(Instruction instruction) {
        List<Order> orders = instruction.side == Side.Bid ? bids : asks;
        if (instruction.action == Action.New) {
            // TODO Check that price is in between neighboring levels
            orders.add(instruction.level, instruction.order);
            if (orders.size() > MAX_LEVELS) {
                orders.remove(MAX_LEVELS);
            }
            assert orders.size() <= MAX_LEVELS;
        } else if (instruction.action == Action.Delete) {
            Order removed = orders.remove(instruction.level);
            if (removed.price != instruction.order.price || removed.size != instruction.order.size) {
                throw new IllegalArgumentException("Unexpected instruction: " + instruction);
            }
        } else if (instruction.action == Action.Update) {
            Order order = orders.get(instruction.level);
            order.knownState = true;
            order.size = instruction.order.size;
            order.price = instruction.order.price;
        } else {
            throw new IllegalStateException("Invalid instruction: " + instruction);
        }
    }

    private enum Side {Bid, Ask}

    private enum Action {New, Delete, Update}

    // When calling next(), returns all instructions with the same sequence number
    private static class PacketIterator implements Iterator<List<Instruction>> {
        private final Iterator<Instruction> instructionIterator;
        private Instruction nextInstruction;

        PacketIterator(File file) throws FileNotFoundException {
            this.instructionIterator = new InstructionIterator(file);
            updateNextInstruction();
        }

        @Override
        public boolean hasNext() {
            return nextInstruction != null;
        }

        @Override
        public List<Instruction> next() {
            assert hasNext();
            List<Instruction> packet = new ArrayList<>();
            Instruction startInstruction = nextInstruction;
            while (nextInstruction != null && nextInstruction.sequenceNumber == startInstruction.sequenceNumber) {
                packet.add(nextInstruction);
                updateNextInstruction();
            }
            return packet;
        }

        private void updateNextInstruction() {
            if (instructionIterator.hasNext()) {
                this.nextInstruction = instructionIterator.next();
            } else {
                this.nextInstruction = null;
            }
        }
    }

    // Returns the next Instruction (line) contained in the input file
    // Streams through the input file, and only buffers the minimum amount of data needed
    private static class InstructionIterator implements Iterator<Instruction> {
        private final Scanner scanner;

        InstructionIterator(File file) throws FileNotFoundException {
            this.scanner = new Scanner(file);
        }

        @Override
        public boolean hasNext() {
            return scanner.hasNextLine();
        }

        @Override
        public Instruction next() {
            assert hasNext();
            String line = scanner.nextLine();
            String[] split = line.split(",");
            if (split.length != 6) { throw new IllegalArgumentException("Invalid line: " + line); }
            int sequenceNumber = Integer.valueOf(split[0]);
            Action action = parseAction(split[1]);
            int level = Integer.valueOf(split[2]);
            Side side = parseSide(split[3]);
            double price = Double.valueOf(split[4]);
            int size = Integer.valueOf(split[5]);
            return new Instruction(sequenceNumber, action, level, side, new Order(price, size));
        }

        private static Action parseAction(String input) {
            switch (input) {
                case "U":
                    return Action.Update;
                case "N":
                    return Action.New;
                case "D":
                    return Action.Delete;
                default:
                    throw new IllegalArgumentException("Cannot parse action: " + input);
            }
        }

        private static Side parseSide(String input) {
            switch (input) {
                case "B":
                    return Side.Bid;
                case "A":
                    return Side.Ask;
                default:
                    throw new IllegalArgumentException("Cannot parse side: " + input);
            }
        }
    }

    private static class Instruction {
        final int sequenceNumber;
        final Action action;
        final int level;
        final Side side;
        final Order order;

        Instruction(int sequenceNumber, Action action, int level, Side side, Order order) {
            if (sequenceNumber < 0) { throw new IllegalArgumentException("Invalid sequenceNumber: " + sequenceNumber); }
            if (level < 0 || level >= MAX_LEVELS) { throw new IllegalArgumentException("Invalid level: " + level); }
            this.sequenceNumber = sequenceNumber;
            this.action = action;
            this.level = level;
            this.side = side;
            this.order = order;
        }

        @Override
        public String toString() {
            return "Instruction{" +
                    "sequenceNumber=" + sequenceNumber +
                    ", action=" + action +
                    ", level=" + level +
                    ", side=" + side +
                    ", order=" + order +
                    '}';
        }
    }

    private static class Order {
        // Made mutable, in order to support efficient Update operations
        boolean knownState;
        double price;
        int size;

        Order() {
            this.knownState = false;
        }

        Order(double price, int size) {
            if (price < 0) { throw new IllegalArgumentException("Invalid price: " + price); }
            if (size < 0) { throw new IllegalArgumentException("Invalid size: " + size); }
            this.price = price;
            this.size = size;
            this.knownState = true;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Order)) return false;
            Order order = (Order) o;
            return knownState == order.knownState &&
                    Double.compare(order.price, price) == 0 &&
                    size == order.size;
        }

        @Override
        public int hashCode() {
            return Objects.hash(knownState, price, size);
        }

        @Override
        public String toString() {
            return "Order{" +
                    "knownState=" + knownState +
                    ", price=" + price +
                    ", size=" + size +
                    '}';
        }
    }
}
