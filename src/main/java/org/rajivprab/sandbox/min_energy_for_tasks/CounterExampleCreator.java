package org.rajivprab.sandbox.min_energy_for_tasks;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.javatuples.Pair;

import java.util.List;
import java.util.Random;

class CounterExampleCreator {
    private static final Random RNG = new Random();

    public static void main(String... args) {
        int numEntries = 2;
        boolean success = false;
        while (numEntries < 10 && !success) {
            success = generateCounterExample(numEntries);
            numEntries++;
        }
        System.out.println("Failed to generate counter-example");
    }

    private static boolean generateCounterExample(int size) {
        for (int i=0; i<5; i++) {
            List<Pair<Integer, Integer>> input = ImmutableList.copyOf(genList(size));
            if (MinEnergyForTasksBruteForce.compute(input) != MinEnergyForTasksSimple.compute(input)) {
                System.out.println("Found counter-example: " + input);
                System.out.println("Comprehensive solution: " + MinEnergyForTasksBruteForce.compute(input));
                System.out.println("Simple solution: " + MinEnergyForTasksSimple.compute(input));
                return true;
            }
        }
        return false;
    }

    private static List<Pair<Integer, Integer>> genList(int size) {
        List<Pair<Integer, Integer>> list = Lists.newArrayList();
        for (int i=0; i<size; i++) {
            list.add(genEntry());
        }
        return list;
    }

    private static Pair<Integer, Integer> genEntry() {
        int planned = RNG.nextInt(10) + 1;
        int actual = RNG.nextInt(planned) + 1;
        return Pair.with(actual, planned);
    }
}
