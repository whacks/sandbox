package org.rajivprab.sandbox.min_energy_for_tasks;

import org.javatuples.Pair;

import java.util.List;

class MinEnergyForTasksWrong {
    static int compute(List<Pair<Integer, Integer>> actualAndMinEnergy) {
        int sumOfActuals = actualAndMinEnergy.stream().mapToInt(e -> e.getValue0()).sum();
        int smallestWastedEffort = actualAndMinEnergy.stream().mapToInt(e -> e.getValue1() - e.getValue0()).min().getAsInt();
        int maxPlanned = actualAndMinEnergy.stream().mapToInt(e -> e.getValue1()).max().getAsInt();
        return Math.max(sumOfActuals + smallestWastedEffort, maxPlanned);
    }
}
