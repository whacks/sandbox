package org.rajivprab.sandbox.min_energy_for_tasks;

import com.google.common.collect.Lists;
import org.javatuples.Pair;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * https://leetcode.com/problems/minimum-initial-energy-to-finish-tasks/
 *
 * You are given an array tasks where tasks[i] = [actuali, minimumi]:
 *
 * actuali is the actual amount of energy you spend to finish the ith task.
 * minimumi is the minimum amount of energy you require to begin the ith task.
 * For example, if the task is [10, 12] and your current energy is 11, you cannot start this task. However, if your
 * current energy is 13, you can complete this task, and your energy will be 3 after finishing it.
 *
 * You can finish the tasks in any order you like.
 *
 * Return the minimum initial amount of energy you will need to finish all the tasks.
 */
class MinEnergyForTasksSimple {
    static int compute(List<Pair<Integer, Integer>> actualAndMinEnergy) {
        actualAndMinEnergy = Lists.newArrayList(actualAndMinEnergy);
        Collections.sort(actualAndMinEnergy, Comparator.comparingInt(MinEnergyForTasksSimple::getSavings));

        int energyNeeded = 0;
        for (Pair<Integer, Integer> entry : actualAndMinEnergy) {
            energyNeeded = Math.max(entry.getValue0() + energyNeeded, entry.getValue1());
        }
        return energyNeeded;
    }

    private static int getSavings(Pair<Integer, Integer> entry) {
        return entry.getValue1() - entry.getValue0();
    }
}
