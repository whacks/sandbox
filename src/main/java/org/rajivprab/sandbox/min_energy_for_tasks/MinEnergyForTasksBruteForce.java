package org.rajivprab.sandbox.min_energy_for_tasks;

import com.google.common.collect.Lists;
import org.javatuples.Pair;

import java.util.LinkedList;
import java.util.List;

/**
 * https://leetcode.com/problems/minimum-initial-energy-to-finish-tasks/
 *
 * You are given an array tasks where tasks[i] = [actuali, minimumi]:
 *
 * actuali is the actual amount of energy you spend to finish the ith task.
 * minimumi is the minimum amount of energy you require to begin the ith task.
 * For example, if the task is [10, 12] and your current energy is 11, you cannot start this task. However, if your
 * current energy is 13, you can complete this task, and your energy will be 3 after finishing it.
 *
 * You can finish the tasks in any order you like.
 *
 * Return the minimum initial amount of energy you will need to finish all the tasks.
 */
class MinEnergyForTasksBruteForce {
    static int compute(List<Pair<Integer, Integer>> actualAndMinEnergy) {
        return worker(Lists.newLinkedList(actualAndMinEnergy));
    }

    // TODO Use HashMap to store results of all calls to worker. Check HashMap before computing, and populate it on exit
    // TODO Use "OrderedSet" class, that guarantees iteration order based on insertion order. But also guarantees
    // that different instances with a different ordering have the same hashcode and equals result,
    // to enable efficient use of HashMap

    private static int worker(LinkedList<Pair<Integer, Integer>> actualAndMinEnergy) {
        if (actualAndMinEnergy.isEmpty()) { return 0; }
        if (actualAndMinEnergy.size() == 1) { return actualAndMinEnergy.getFirst().getValue1(); }

        int minEnergyNeeded = Integer.MAX_VALUE;
        for (int i=0; i<actualAndMinEnergy.size(); i++) {
            Pair<Integer, Integer> entry = actualAndMinEnergy.removeFirst();

            int childEnergyNeeded = worker(actualAndMinEnergy);
            int energyNeeded = Math.max(childEnergyNeeded + entry.getValue0(), entry.getValue1());
            if (energyNeeded < minEnergyNeeded) { minEnergyNeeded = energyNeeded; }

            actualAndMinEnergy.addLast(entry);
        }

        return minEnergyNeeded;
    }
}
