package org.rajivprab.sandbox.bin_picker;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import org.apache.commons.lang3.mutable.MutableDouble;
import org.rajivprab.cava.Validatec;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/** Given a set of cupboards, each with an inventory of items,
 * and a shopping-cart which contains an inventory of items,
 * figure out the (approximately) minimum number of cupboards to visit in order to get all items in cart
 */
class BinPicker {
    // Find item that exists in the fewest number of cupboards
    // Pick one of those cupboards. Use recursive tie-breaker if needed
    // Pick all items available in chosen cupboard and remove from cart
    // Repeat until cart is empty
    public Collection<Long> getCupboards(Table<Long, UUID, Integer> cupboardSkuQuantity, Map<UUID, Integer> cart) {
        Validatec.doesNotContain(cart.values(), 0);
        cart = Maps.newHashMap(cart);
        Collection<Long> result = Lists.newArrayList();

        while (!cart.isEmpty()) {
            long cupboard = getCandidateCupboard(cupboardSkuQuantity, cart);
            pickFromCupboard(cupboardSkuQuantity.row(cupboard), cart);
            result.add(cupboard);
        }

        return result;
    }

    private void pickFromCupboard(Map<UUID, Integer> cupboard, Map<UUID, Integer> cart) {
        for (UUID item : cart.keySet()) {
            Integer stock = cupboard.get(item);
            if (stock != null) {
                int required = cart.get(item);
                if (stock > required) {
                    cart.remove(item);
                    cupboard.put(item, stock - required);
                } else if (stock == required) {
                    cart.remove(item);
                    cupboard.remove(item);
                } else {
                    cupboard.remove(item);
                    cart.put(item, required - stock);
                }
            }
        }
    }

    // Pick the item that is present in the fewest number of cupboards, and select one of those cupboards
    // Tiebreaker: Pick the cupboard that contains the most items, weighted by the rarity of the item
    private Long getCandidateCupboard(Table<Long, UUID, Integer> cupboardSkuQuantity, Map<UUID, Integer> cart) {
        Set<Long> candidates = cupboardSkuQuantity.rowKeySet();
        Map<Long, MutableDouble> cupboardScores = initializeCandidateScores(cupboardSkuQuantity.rowKeySet());

        for (UUID item : cart.keySet()) {
            Set<Long> curCandidates = getCandidates(cupboardSkuQuantity, item, cart.get(item));
            Validatec.notEmpty(curCandidates);  // No solution exists
            curCandidates.forEach(c -> cupboardScores.get(c).add(1.0 / curCandidates.size()));
            if (curCandidates.size() < candidates.size()) {
                candidates = curCandidates;
            }
        }

        return getCandidate(candidates, cupboardScores);
    }

    private long getCandidate(Set<Long> candidates, Map<Long, MutableDouble> scores) {
        double champScore = -1;
        long champ = 0;

        for (long candidate : candidates) {
            double score = scores.get(candidate).getValue();
            if (score > champScore) {
                champScore = score;
                champ = candidate;
            }
        }

        return champ;
    }

    private Map<Long, MutableDouble> initializeCandidateScores(Collection<Long> candidates) {
        Map<Long, MutableDouble> candidateScores = Maps.newHashMap();
        for (long candidate : candidates) {
            candidateScores.put(candidate, new MutableDouble(0));
        }
        return candidateScores;
    }

    private Set<Long> getCandidates(Table<Long, UUID, Integer> cupboardSkuQuantity, UUID item, int quantity) {
        // TODO Update implementation to take quantity into account?
        // Prefer full orders?
        // Or ignore candidates with less than half?
        // Or if all potential candidates sum up to the desired quantity, return just one of them, for immediate pick?
        return cupboardSkuQuantity.column(item).keySet();
    }
}
