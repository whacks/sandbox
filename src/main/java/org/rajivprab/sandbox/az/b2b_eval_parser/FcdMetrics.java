package org.rajivprab.sandbox.az.b2b_eval_parser;

import com.google.common.base.Joiner;
import com.google.common.collect.*;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.IOExceptionc;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

class FcdMetrics {
    final Table<Integer, Integer, Entry> entries;   // Lead, Span -> Entry

    FcdMetrics(String date) {
        String filePath = String.format(
                "/Users/prarajiv/Desktop/backtest-reports/_roots_prarajiv_fast-backtest_tsv_filtered_%s.tsv", date);
        try {
            this.entries = ImmutableTable.copyOf(parse(new File(filePath)));
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    private static Table<Integer, Integer, Entry> parse(File eval) throws IOException {
        Scanner scanner = new Scanner(eval);
        Table<Integer, Integer, Entry> entries = HashBasedTable.create();
        Map<String, Integer> columns = parseColumns(scanner.nextLine());
        while (scanner.hasNextLine()) {
            List<String> vals = parse(scanner.nextLine());
            Entry.parse(vals, columns).ifPresent(e -> Validatec.isNull(entries.put(e.lead, e.span, e)));
        }
        EvalNanFilter.debugLog(Joiner.on("\n").join(entries.cellSet()));
        return entries;
    }

    private static Map<String, Integer> parseColumns(String line) {
        Map<String, Integer> columns = Maps.newHashMap();
        List<String> columnNames = parse(line);
        IntStream.range(0, columnNames.size()).forEach(i -> Validatec.isNull(columns.put(columnNames.get(i), i)));
        return ImmutableMap.copyOf(columns);
    }

    private static List<String> parse(String line) {
        return Arrays.stream(line.split("\t")).map(String::trim).collect(ImmutableList.toImmutableList());
    }
}
