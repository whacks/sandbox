package org.rajivprab.sandbox.az.b2b_eval_parser;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.List;

public class EvalNanFilter {
    private static final List<String> FCDS = ImmutableList.of("2018-12-09",
                                                              "2018-12-30",
                                                              "2019-01-20",
                                                              "2019-02-10",
                                                              "2019-03-03",
                                                              "2019-03-24",
                                                              "2019-04-14",
                                                              "2019-05-05",
                                                              "2019-05-26",
                                                              "2019-06-16",
                                                              "2019-07-07",
                                                              "2019-07-28",
                                                              "2019-08-18",
                                                              "2019-09-08",
                                                              "2019-09-29",
                                                              "2019-10-20",
                                                              "2019-11-10",
                                                              "2019-12-01");

    public static void main(String... args) {
        for (String date : FCDS) {
            FcdMetrics metrics = new FcdMetrics(date);
            System.out.println(date + ", 3 lead, 1 span   ," + getMetrics(metrics.entries.get(3, 1)));
            System.out.println(date + ", 0 lead, all span ," + getMetrics(metrics.entries.row(0).values()));
            System.out.println(date + ", all lead, 1 span ," + getMetrics(metrics.entries.column(1).values()));
        }
    }

    private static String getMetrics(Entry entry) {
        return getMetrics(ImmutableList.of(entry));
    }

    private static String getMetrics(Collection<Entry> entries) {
        /*
        return Joiner.on("\t\t").join(
                String.format("P50 forecast change: %5.2f%%", getP50ForecastChangePercentage(entries)),
                String.format("P90 forecast change: %5.2f%%", getP90ForecastChangePercentage(entries)),
                String.format("P50 quantile loss diff: %7.2f basis points", getP50QlDiff(entries) * 10000),
                String.format("P90 quantile loss diff: %7.2f basis points", getP90QlDiff(entries) * 10000),
                String.format("P50 QL Diff percentage: %5.2f%%", getP50QlDiffPercentage(entries)),
                String.format("P90 QL Diff percentage: %5.2f%%", getP90QlDiffPercentage(entries))); */
        return Joiner.on(",").join(
                getP50ForecastChangePercentage(entries),
                getP90ForecastChangePercentage(entries),
                getP50QlDiff(entries) * 10000,
                getP90QlDiff(entries) * 10000,
                getP50QlDiffPercentage(entries),
                getP90QlDiffPercentage(entries));
    }

    private static double getP50ForecastChangePercentage(Collection<Entry> entries) {
        double sumP50Baseline = 0;
        double sumP50Eval = 0;
        for (Entry entry : entries) {
            sumP50Baseline += entry.baselineP50;
            sumP50Eval += entry.evalP50;
        }

        return (sumP50Eval - sumP50Baseline) * 100 / sumP50Baseline;
    }

    private static double getP90ForecastChangePercentage(Collection<Entry> entries) {
        double sumP90Baseline = 0;
        double sumP90Eval = 0;
        for (Entry entry : entries) {
            sumP90Baseline += entry.baselineP90;
            sumP90Eval += entry.evalP90;
        }

        return (sumP90Eval - sumP90Baseline) * 100 / sumP90Baseline;
    }

    private static double getP50BaselineQl(Collection<Entry> entries) {
        double sumDemand = 0;
        double sumBaselineP50Ql = 0;
        for (Entry entry : entries) {
            sumDemand += entry.demand;
            sumBaselineP50Ql += entry.baselineP50NewQl * entry.demand;
        }

        return sumBaselineP50Ql / sumDemand;
    }

    private static double getP50EvalQl(Collection<Entry> entries) {
        double sumDemand = 0;
        double sumEvalP50Ql = 0;
        for (Entry entry : entries) {
            sumDemand += entry.demand;
            sumEvalP50Ql += entry.evalP50NewQl * entry.demand;
        }

        return sumEvalP50Ql / sumDemand;
    }

    private static double getP50QlDiff(Collection<Entry> entries) {
        return getP50EvalQl(entries) - getP50BaselineQl(entries);
    }

    private static double getP50QlDiffPercentage(Collection<Entry> entries) {
        return getP50QlDiff(entries) * 100 / getP50BaselineQl(entries);
    }

    private static double getP90BaselineQl(Collection<Entry> entries) {
        double sumDemand = 0;
        double sumBaselineP90Ql = 0;
        for (Entry entry : entries) {
            sumDemand += entry.demand;
            sumBaselineP90Ql += entry.baselineP90NewQl * entry.demand;
        }

        return sumBaselineP90Ql / sumDemand;
    }

    private static double getP90EvalQl(Collection<Entry> entries) {
        double sumDemand = 0;
        double sumEvalP90Ql = 0;
        for (Entry entry : entries) {
            sumDemand += entry.demand;
            sumEvalP90Ql += entry.evalP90NewQl * entry.demand;
        }

        return sumEvalP90Ql / sumDemand;
    }

    private static double getP90QlDiff(Collection<Entry> entries) {
        return getP90EvalQl(entries) - getP90BaselineQl(entries);
    }

    private static double getP90QlDiffPercentage(Collection<Entry> entries) {
        return getP90QlDiff(entries) * 100 / getP90BaselineQl(entries);
    }

    static void print(String message) {
        System.out.println(message);
    }

    static void debugLog(String message) {
        // LogManager.getLogger(EvalNanFilter.class).info(message);
    }
}
