package org.rajivprab.sandbox.az.b2b_eval_parser;

import java.util.List;
import java.util.Map;
import java.util.Optional;

class Entry {
    final int lead;
    final int span;

    final double demand;
    final double evalP50;
    final double baselineP50;
    final double evalP90;
    final double baselineP90;

    final double evalP50NewQl;
    final double baselineP50NewQl;
    final double evalP90NewQl;
    final double baselineP90NewQl;

    static Optional<Entry> parse(List<String> line, Map<String, Integer> columns) {
        if (!get(line, columns, "GL").equals("ALL")) { return Optional.empty(); }
        String leadStr = get(line, columns, "Lead");
        String spanStr = get(line, columns, "Span");
        if (leadStr.equals("ALL") || spanStr.equals("ALL")) { return Optional.empty(); }
        int lead = Integer.parseInt(leadStr);
        int span = Integer.parseInt(spanStr);

        double demand = Double.parseDouble(get(line, columns, "Demand"));
        Double evalP50 = Double.parseDouble(get(line, columns, "Eval P50"));
        Double baselineP50 = Double.parseDouble(get(line, columns, "Baseline P50"));
        Double evalP90 = Double.parseDouble(get(line, columns, "Eval P90"));
        Double baselineP90 = Double.parseDouble(get(line, columns, "Baseline P90"));

        Double evalP50NewQl = Double.parseDouble(get(line, columns, "Eval P50 New_QL"));
        Double baselineP50NewQl = Double.parseDouble(get(line, columns, "Baseline P50 New_QL"));
        Double evalP90NewQl = Double.parseDouble(get(line, columns, "Eval P90 New_QL"));
        Double baselineP90NewQl = Double.parseDouble(get(line, columns, "Baseline P90 New_QL"));

        Entry entry = new Entry(lead, span,
                                demand, evalP50, baselineP50, evalP90, baselineP90,
                                evalP50NewQl, baselineP50NewQl, evalP90NewQl, baselineP90NewQl);
        if (evalP50NewQl.isNaN() || baselineP50NewQl.isNaN() || evalP90NewQl.isNaN() || baselineP90NewQl.isNaN()) {
            EvalNanFilter.debugLog("Found NaN. Discarding: " + entry);
            return Optional.empty();
        }
        return Optional.of(entry);
    }

    private static String get(List<String> line, Map<String, Integer> columns, String col) {
        return line.get(columns.get(col));
    }

    private Entry(int lead, int span, double demand, double evalP50, double baselineP50, double evalP90,
                  double baselineP90, double evalP50NewQl, double baselineP50NewQl, double evalP90NewQl, double baselineP90NewQl) {
        this.lead = lead;
        this.span = span;
        this.demand = demand;
        this.evalP50 = evalP50;
        this.baselineP50 = baselineP50;
        this.evalP90 = evalP90;
        this.baselineP90 = baselineP90;
        this.evalP50NewQl = evalP50NewQl;
        this.baselineP50NewQl = baselineP50NewQl;
        this.evalP90NewQl = evalP90NewQl;
        this.baselineP90NewQl = baselineP90NewQl;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "lead=" + lead +
                ", span=" + span +
                ", demand=" + demand +
                ", evalP50NewQl=" + evalP50NewQl +
                ", baselineP50NewQl=" + baselineP50NewQl +
                ", evalP90NewQl=" + evalP90NewQl +
                ", baselineP90NewQl=" + baselineP90NewQl +
                '}';
    }
}
