package org.rajivprab.sandbox.reverse_linklist;

import org.rajivprab.cava.Validatec;

import java.util.Iterator;

/**
 * Created by rprabhakar on 2/15/16.
 */
public class Reverse {
    public static <T> void reverse(Iterator<Node<T>> list) {
        Node<T> prev = null;
        while (list.hasNext()) {
            Node<T> cur = list.next();
            Node<T> next = cur.next;
            Validatec.equals(cur.next, next);
            Validatec.equals(cur.prev, prev);
            Validatec.equals(next.prev, cur);
            cur.next = prev;
            cur.prev = next;
            if (prev != null) {
                Validatec.equals(prev.prev, cur);
            }
        }
    }

    private static class Node<T> {
        Node<T> prev;
        Node<T> next;
        private final T val;

        public Node(Node<T> prev, Node<T> next, T val) {
            this.val = val;
            this.prev = prev;
            this.next = next;
        }
    }
}
