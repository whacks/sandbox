package org.rajivprab.sandbox.web_scrape;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.ConnectException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Scraper {
    private static final Logger log = LogManager.getLogger(Scraper.class);

    private static final String INPUT_URL = "https://s3.amazonaws.com/fieldlens-public/urls.txt";
    private static final String MATCH_TERM = "this";
    private static final int NUM_PARALLEL_REQUESTS = 20;

    public static void main(String... args) throws Exception {
        List<String> matchingURLs = getUrlsWithMatchingTerm(getUrls());
        log.info("Matching URLs that contain the term " + MATCH_TERM + ":\n" + matchingURLs);
        File outputFile = new File("results.txt");
        try (PrintWriter writer = new PrintWriter(outputFile)) {
            matchingURLs.forEach(writer::println);
            log.info("Results written to: " + outputFile);
        }
    }

    private static List<String> getUrls() {
        try {
            List<String> urls = Lists.newArrayList();
            BufferedReader reader = readFromUrl(INPUT_URL);
            String inputLine = reader.readLine();   // The first line is the header
            Validate.isTrue(inputLine.split(".,.")[1].equals("URL"));
            while ((inputLine = reader.readLine()) != null) {
                urls.add("http://" + inputLine.split(".,.")[1]);
            }
            log.info("Looking up URLs: " + urls);
            return urls;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static List<String> getUrlsWithMatchingTerm(List<String> urls) {
        Queue<String> taskQueue = Queues.newConcurrentLinkedQueue(urls);
        List<List<String>> partitionedResults = IntStream
                .range(0, NUM_PARALLEL_REQUESTS)
                .mapToObj(i -> Lists.<String>newArrayList())
                .collect(Collectors.toList());
        List<Thread> workers = IntStream
                .range(0, NUM_PARALLEL_REQUESTS)
                .mapToObj(i -> new Thread(() -> runWorker(taskQueue, partitionedResults.get(i))))
                .collect(Collectors.toList());
        workers.forEach(Thread::start);
        try {
            for (Thread thread : workers) {
                thread.join();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return partitionedResults.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    private static void runWorker(Queue<String> taskQueue, List<String> results) {
        String url = taskQueue.poll();
        while (url != null) {
            if (matchesTerm(url, MATCH_TERM)) {
                results.add(url);
            }
            url = taskQueue.poll();
        }
    }

    private static boolean matchesTerm(String url, String match) {
        try {
            BufferedReader br = readFromUrl(url);
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                if (inputLine.contains(match)) {
                    return true;
                }
            }
            return false;
        } catch (UnknownHostException e) {
            log.info("Unknown host", e);
            return false;
        } catch (ConnectException e) {
            log.info("Unable to connect to server", e);
            return false;
        } catch (IOException e) {
            if (e.getMessage().startsWith("Server returned HTTP response code")) {
                log.info("Server returned error status code", e);
            } else {
                log.error("Unknown error encountered", e);
            }
            return false;
        }
    }

    private static BufferedReader readFromUrl(String url) throws IOException {
        URLConnection connection = new URL(url).openConnection();
        return new BufferedReader(new InputStreamReader(connection.getInputStream()));
    }
}
