package org.rajivprab.sandbox.minimize_array_sum_difference;

import com.google.common.collect.*;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * https://leetcode.com/problems/partition-array-into-two-arrays-to-minimize-sum-difference/solutions/1513298/c-meet-in-middle/?orderBy=most_votes
 */
public class MinimizeArraySumDifferenceMeetInMiddle {
    public static int minimumDifference(List<Integer> nums) {
        List<Integer> left = nums.subList(0, nums.size() / 2);
        List<Integer> right = nums.subList(nums.size() / 2, nums.size());

        int minDiff = Integer.MAX_VALUE;
        double targetSum = nums.stream().mapToInt(i -> i).sum() * 0.5;
        ArrayListMultimap<Integer, Integer> leftSizeToSums = getSizeToSums(left);
        ArrayListMultimap<Integer, Integer> rightSizeToSums = getSizeToSums(right);
        for (int leftSize=0; leftSize<= nums.size() / 2; leftSize++) {
            int rightSize = (nums.size() / 2) - leftSize;
            List<Integer> rightSums = rightSizeToSums.get(rightSize);
            for (int leftSum : leftSizeToSums.get(leftSize)) {
                double targetRightSum = targetSum - leftSum;
                int closestRightSum = getClosest(rightSums, targetRightSum);
                int sum = leftSum + closestRightSum;
                int diff = (int) Math.abs((targetSum - sum) * 2);
                // if (diff == 0) { return 0; }    // Optimistic performance optimization
                if (diff < minDiff) { minDiff = diff; }
            }
        }
        return minDiff;
    }

    private static int getClosest(List<Integer> nums, double target) {
        Validatec.notEmpty(nums);
        int startIndexInclusive = 0;
        int endIndexExclusive = nums.size();
        while (true) {
            if (endIndexExclusive == startIndexInclusive + 1) { return nums.get(startIndexInclusive); }
            if (endIndexExclusive == startIndexInclusive + 2) {
                int firstValue = nums.get(startIndexInclusive);
                int secondValue = nums.get(startIndexInclusive + 1);
                double firstDiff = Math.abs(target - firstValue);
                double secondDiff = Math.abs(target - secondValue);
                return firstDiff < secondDiff ? firstValue : secondValue;
            }
            Validatec.greaterThan(endIndexExclusive, startIndexInclusive);
            int midIndex = (endIndexExclusive + startIndexInclusive) / 2;
            int midVal = nums.get(midIndex);
            if (Math.abs(midVal - target) <= 0.5) { return midVal; }

            if (midVal < target) { startIndexInclusive = midIndex; }
            else { endIndexExclusive = midIndex + 1; }
        }
    }

    // Returns a mapping of {num_elements} -> {all_possible_sums_possible_using_num_elements}. Includes 0 -> 0
    public static ArrayListMultimap<Integer, Integer> getSizeToSums(List<Integer> nums) {
        ArrayListMultimap<Integer, Integer> result = ArrayListMultimap.create();
        for (int i=0; i<=nums.size(); i++) {
            result.putAll(i, getSums(nums, i));
        }
        return result;
    }

    // Returns in sorted order to optimize later binary search
    public static List<Integer> getSums(List<Integer> nums, int numElements) {
        if (numElements == 0) { return ImmutableList.of(0); }
        List<Integer> cachedValue = sumsCache.get(nums, numElements);
        if (cachedValue == null) {
            System.out.printf("Cache miss for %d elements from %s\n", numElements, nums);
            cachedValue = getSumsWorker(nums, numElements);
            sumsCache.put(nums, numElements, cachedValue);
        } else {
            System.out.println("Cache hit");
        }
        return cachedValue;
    }

    private static final Table<List<Integer>, Integer, List<Integer>> sumsCache = HashBasedTable.create();

    public static List<Integer> getSumsWorker(List<Integer> nums, int numElements) {
        Validatec.greaterOrEqual(nums.size(), numElements);
        if (numElements == nums.size()) { return ImmutableList.of(nums.stream().mapToInt(i -> i).sum()); }

        List<Integer> suffixNums = nums.subList(1, nums.size());
        List<Integer> prefixNotChosen = getSums(suffixNums, numElements);
        List<Integer> prefixChosen = getSums(suffixNums, numElements-1)
                .stream()
                .map(i -> i + nums.get(0))
                .collect(Collectors.toList());
        return ImmutableSet.<Integer>builder()
                           .addAll(prefixChosen)
                           .addAll(prefixNotChosen)
                           .build()
                           .stream()
                           .sorted()
                           .collect(Collectors.toList());
    }
}
