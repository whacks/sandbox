package org.rajivprab.sandbox.minimize_array_sum_difference;

import com.google.common.collect.*;
import org.rajivprab.cava.Validatec;

import java.util.List;

/**
 * You are given an integer array nums of 2 * n integers. You need to partition nums into two arrays of length n to minimize the absolute difference of the sums of the arrays. To partition nums, put each element of nums into one of the two arrays.
 *
 * Return the minimum possible absolute difference.
 *
 * https://leetcode.com/problems/partition-array-into-two-arrays-to-minimize-sum-difference/
 */
public class MinimizeArraySumDifferenceNaive {

    // Naive solution: For each element, explore downstream possibilities if it were to be placed in 1st or 2nd set
    // Caching is only helpful if we have duplicate elements. Otherwise, each recursive call will always be unique
    public static int minimumDifference(List<Integer> nums) {
        return new MinimizeArraySumDifferenceNaive().
                minimumDifference(nums, 0, HashMultiset.create(), HashMultiset.create());
    }

    private final Table<Multiset<Integer>, Multiset<Integer>, Integer> resultsCache = HashBasedTable.create();

    private int minimumDifference(List<Integer> nums, int curIndex,
                                  Multiset<Integer> first, Multiset<Integer> second) {
        Integer cached = resultsCache.get(first, second);
        if (cached == null) {
            cached = minimumDifferenceWorker(nums, curIndex, first, second);
            resultsCache.put(first, second, cached);
        }
        return cached;
    }

    private int minimumDifferenceWorker(List<Integer> nums, int curIndex,
                                  Multiset<Integer> first, Multiset<Integer> second) {
        if (curIndex == nums.size()) {
            Validatec.size(first, second.size());
            return Math.abs(first.stream().mapToInt(i -> i).sum() - second.stream().mapToInt(i -> i).sum());
        }
        int cur = nums.get(curIndex);
        if (first.size() == nums.size() / 2) {
            second.add(cur);
            int result = minimumDifference(nums, curIndex + 1, first, second);
            second.remove(cur);
            return result;
        }
        if (second.size() == nums.size() / 2) {
            first.add(cur);
            int result = minimumDifference(nums, curIndex + 1, first, second);
            first.remove(cur);
            return result;
        }

        first.add(cur);
        int firstResult = minimumDifference(nums, curIndex + 1, first, second);
        first.remove(cur);

        second.add(cur);
        int secondResult = minimumDifference(nums, curIndex + 1, first, second);
        second.remove(cur);

        return Math.min(firstResult, secondResult);
    }
}
