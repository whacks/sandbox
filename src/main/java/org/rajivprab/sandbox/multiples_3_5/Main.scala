package org.rajivprab.sandbox.multiples_3_5

/**
 * https://projecteuler.net/problem=1
 *
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */
object Main extends App {
  println("Running multiple solver for 3|5 till 10: " + sum(3, 5, 10))
  println("Running multiple solver for 3|5 till 1000: " + sum(3, 5, 1000))

  def sum(multiple1: Int, multiple2: Int, limit: Int): Int = {
    val all = new Multiples(multiple1, limit).multiples ++ new Multiples(multiple2, limit).multiples;
    var sum = 0;
    all.foreach(i => sum += i)
    sum
  }
}
