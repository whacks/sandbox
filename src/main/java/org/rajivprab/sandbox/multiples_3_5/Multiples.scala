package org.rajivprab.sandbox.multiples_3_5

import scala.collection.mutable

class Multiples(private val multipleOf: Int, private val limit: Int) {
  val multiples = getMultiples();

  private def getMultiples(): Set[Int] = {
    val builder = new mutable.HashSet[Int]();
    var i = multipleOf;
    while (i < limit) {
      builder.add(i);
      i = i+multipleOf;
    }
    Set.from(builder);
  }
}
