package org.rajivprab.sandbox.separate_numbers;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;

import java.util.Map;
import java.util.Map.Entry;

/**
 * Similar to naive. Except we remove the "minNumberAllowed" worker input, since that produces a lot of cache misses.
 * For each string, we store the number of valid combinations, indexed by the smallest number in the combination.
 * Eg: "111" produces "1 -> 2, 111 -> 1". To represent the 3 combinations for "1, 1, 1", "1, 11" and "111"
 *
 * We then cache and reuse it, by eliminating all indexes that are invalid for the sequence taken thus far.
 */
public class SeparateNumbersBetter {
    private static final Logger log = LogManager.getLogger(SeparateNumbersBetter.class);

    private static final Table<String, Integer, Integer> stringFirstNumberToCombinations = HashBasedTable.create();

    public static int numberOfCombinations(String num) {
        return numberOfCombinations(num, 1) % (109 + 7);
    }

    // TODO enhancement: Instead of constructing a modified num input for each call, always use the same string, and pass a
    // startIndex instead. Improves performance by avoiding string-construction
    private static int numberOfCombinations(String num, int minNumberAllowed) {
        // Getting an empty string is a valid base-case for incrementing by 1.
        // When this is called with inputs ("12", 11): recursive call is only made once for ("", 12),
        // and should return 1
        if (num.isEmpty()) { return 1; }

        // Prevent "09" from being parsed as 9. Requirements say no leading 0s
        if (num.charAt(0) == '0') { return 0; }

        // Not needed, but early-return for cases where there is no point looping/recursing. O(1) check anyway
        if (minNumberAllowed > Integer.parseInt(num)) { return 0; }

        Map<Integer, Integer> cachedFirstNumberToCombinations = stringFirstNumberToCombinations.row(num);
        if (cachedFirstNumberToCombinations.isEmpty()) {
            cachedFirstNumberToCombinations.putAll(computeFirstNumberToCombinations(num));
        } else {
            log.info("cache hit!");
        }

        int numCombinations = 0;
        for (Entry<Integer, Integer> entry : cachedFirstNumberToCombinations.entrySet()) {
            if (entry.getKey() >= minNumberAllowed) {
                numCombinations += entry.getValue();
            }
        }
        return numCombinations;
    }

    private static Map<Integer, Integer> computeFirstNumberToCombinations(String num) {
        Map<Integer, Integer> combinationsFound = Maps.newHashMap();

        // Largest int is ~2^31: 2,000,000,000. 10 digits
        for (int numDigitsTaken=1; numDigitsTaken<=Math.min(10, num.length()); numDigitsTaken++) {
            int candidate = Integer.parseInt(num.substring(0, numDigitsTaken));
            int candidateResults = numberOfCombinations(num.substring(numDigitsTaken), candidate);
            if (candidateResults != 0) {
                combinationsFound.put(candidate, candidateResults);
            }
        }

        log.info("Found {} combinations for string {}", combinationsFound.entrySet(), num);
        Validatec.notEmpty(combinationsFound.keySet());
        return combinationsFound;
    }
}
