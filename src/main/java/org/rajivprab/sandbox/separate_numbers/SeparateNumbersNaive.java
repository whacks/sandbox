package org.rajivprab.sandbox.separate_numbers;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * https://leetcode.com/problems/number-of-ways-to-separate-numbers/
 *
 * You wrote down many positive integers in a string called num. However, you realized that you forgot to add
 * commas to seperate the different numbers. You remember that the list of integers was:
 * - non-decreasing and that
 * - no integer had leading zeros
 * - only positive integers (0 not allowed)
 *
 * Return the number of possible lists of integers that you could have written down to get the string num. Since
 * the answer may be large, return it modulo 109 + 7.
 */
public class SeparateNumbersNaive {
    private static final Logger log = LogManager.getLogger(SeparateNumbersNaive.class);

    private static final Table<String, Integer, Integer> cachedResults = HashBasedTable.create();

    public static int numberOfCombinations(String num) {
        return numberOfCombinations(num, 1) % (109 + 7);
    }

    // TODO enhancement: Instead of constructing a modified num input for each call, always use the same string, and pass a
    // startIndex instead. Improves performance by avoiding string-construction
    private static int numberOfCombinations(String num, int minNumberAllowed) {
        // Getting an empty string is a valid base-case for incrementing by 1.
        // When this is called with inputs ("12", 11): recursive call is only made once for ("", 12),
        // and should return 1
        if (num.isEmpty()) { return 1; }

        // Prevent "09" from being parsed as 9. Requirements say no leading 0s
        if (num.charAt(0) == '0') { return 0; }

        // Not needed, but early-return for cases where there is no point looping/recursing
        if (minNumberAllowed > Integer.parseInt(num)) { return 0; }

        Integer cached = cachedResults.get(num, minNumberAllowed);
        if (cached == null) {
            cached = numberOfCombinationsWorker(num, minNumberAllowed);
            cachedResults.put(num, minNumberAllowed, cached);
        } else {
            log.info("cache hit!");
        }

        return cached;
    }

    private static int numberOfCombinationsWorker(String num, int minNumberAllowed) {
        int combinationsFound = 0;

        // Largest int is ~2^31: 2,000,000,000. 10 digits
        for (int numDigitsTaken=1; numDigitsTaken<=Math.min(10, num.length()); numDigitsTaken++) {
            int candidate = Integer.parseInt(num.substring(0, numDigitsTaken));
            if (candidate >= minNumberAllowed) {
                int candidateResults = numberOfCombinations(num.substring(numDigitsTaken), candidate);
                combinationsFound += candidateResults;
            }
        }

        log.info("Found {} results for string {}, min-num: {}", combinationsFound, num, minNumberAllowed);
        return combinationsFound;
    }
}
