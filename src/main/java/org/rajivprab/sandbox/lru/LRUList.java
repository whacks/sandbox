package org.rajivprab.sandbox.lru;

import com.google.common.collect.Maps;
import org.rajivprab.cava.Validatec;

import java.util.Map;

class LRUList<K> {
    Map<K, Node> nodeMap = Maps.newHashMap();
    Node<K> start = new Node<>(null);
    Node<K> end = new Node<>(null);

    LRUList() {
        start.next = end;
        end.prev = start;
    }

    void update(K key) {
        Node<K> node = nodeMap.get(key);
        if (node == null) {
            node = new Node<>(key);
            nodeMap.put(key, node);
        } else {
            remove(node);
        }
        append(node);
    }

    K evict() {
        Node<K> evict = start.next;
        remove(evict);
        nodeMap.remove(evict.key);
        return evict.key;
    }

    private void append(Node<K> node) {
        Node<K> prev = end.prev;
        prev.next = node;
        end.prev = node;
        node.prev = prev;
        node.next = end;
    }

    private void remove(Node node) {
        Validatec.notEquals(node, start);
        Validatec.notEquals(node, end);
        Node prev = node.prev;
        Node next = node.next;

        prev.next = next;
        next.prev = prev;
    }

    private static class Node<K> {
        final K key;
        Node<K> prev;
        Node<K> next;

        Node(K key) {
            this.key = key;
        }
    }
}
