package org.rajivprab.sandbox.lru;

import com.google.common.collect.Maps;

import java.util.Map;

public class LRU<K, V> {
    private final LRUList<K> lru = new LRUList<>();
    private final Map<K, V> map = Maps.newConcurrentMap();
    private final int size;

    LRU(int size) {
        this.size = size;
    }

    void put(K key, V value) {
        map.put(key, value);
        lru.update(key);
        if (map.size() > size) {
            K evictKey = lru.evict();
            map.remove(evictKey);
        }
    }

    V get(K key) {
        lru.update(key);
        return map.get(key);
    }
}
