package org.rajivprab.sandbox.trie;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

/**
 * Created by rajivprab on 6/23/17.
 */
public interface Trie<K, V> {
    static <K, V> Trie<K, V> newSimpleTrie() { return new SimpleTrie<>(); }

    Optional<V> getValue(Iterator<K> keys);
    Optional<Trie<K, V>> get(Iterator<K> keys);
    Collection<K> getKeys();
    // Returns the existing value which was overwritten/removed
    Optional<V> put(V value, Iterator<K> keys);
    Optional<V> remove(Iterator<K> keys);

    default Optional<V> put(V value, K... keys) { return put(value, Arrays.asList(keys)); }
    default Optional<V> remove(K... keys) { return remove(Arrays.asList(keys)); }
    default Optional<Trie<K, V>> get(K... keys) { return get(Arrays.asList(keys)); }
    default Optional<V> getValue(K... keys) { return getValue(Arrays.asList(keys)); }

    default Optional<V> put(V value, Iterable<K> keys) { return put(value, keys.iterator()); }
    default Optional<V> remove(Iterable<K> keys) { return remove(keys.iterator()); }
    default Optional<Trie<K, V>> get(Iterable<K> keys) { return get(keys.iterator()); }
    default Optional<V> getValue(Iterable<K> keys) { return getValue(keys.iterator()); }
}
