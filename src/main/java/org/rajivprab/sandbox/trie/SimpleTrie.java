package org.rajivprab.sandbox.trie;

import com.google.common.collect.Maps;

import java.util.*;

/**
 * User is responsible for ensuring that:
 * - Data structure is only used by a single thread at a time
 * - Key is hashable, and implements equals
 * - Key is not modified while it's in the trie
 *
 * Created by rajivprab on 6/23/17.
 */
class SimpleTrie<K, V> implements Trie<K, V> {
    private final Map<K, SimpleTrie<K, V>> children = Maps.newHashMap();

    private Optional<V> rootValue = Optional.empty();

    @Override
    public Optional<V> getValue(Iterator<K> keys) {
        return keys.hasNext() ? get(keys).flatMap(trie -> trie.getValue()) : rootValue;
    }

    @Override
    public Optional<Trie<K, V>> get(Iterator<K> keys) {
        return keys.hasNext() ?
                Optional.ofNullable(children.get(keys.next())).flatMap(trie -> trie.get(keys)) :
                Optional.of(this);
    }

    @Override
    public Collection<K> getKeys() {
        return Collections.unmodifiableCollection(children.keySet());
    }

    @Override
    public Optional<V> put(V value, Iterator<K> keys) {
        if (!keys.hasNext()) {
            Optional<V> result = this.rootValue;
            this.rootValue = Optional.ofNullable(value);
            return result;
        }
        return children.computeIfAbsent(keys.next(), (key) -> new SimpleTrie<>()).put(value, keys);
    }

    @Override
    public Optional<V> remove(Iterator<K> keys) {
        return put(null, keys);
    }
}
