package org.rajivprab.sandbox.recover_tree_from_traversal;

public class Node {
    final int value;

    Node leftChild;
    Node rightChild;

    Node(int value) {
        this.value = value;
    }
}
