package org.rajivprab.sandbox.recover_tree_from_traversal;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.tuple.Pair;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.Map;

/**
 * https://leetcode.com/problems/recover-a-tree-from-preorder-traversal/
 *
 * Same as RecoverTree
 */
public class RecoverTreeFromTraversal {
    private int aggregateOverrideForInterval(
            final int parent,
            final List<Pair<Integer, Long>> child) {
        final Integer numberOfChildren = child.size();
        int num = 0;
        for (int i = 0; i < numberOfChildren; i++) {
            num++;
        }

        return num;
    }

    static Node recover(String print) {
        List<Pair<Integer, Integer>> entries = getDepthValues(print);
        Map<Integer, Node> depthMostRecentlySeenNode = Maps.newHashMap();
        for (Pair<Integer, Integer> entry : entries) {
            Node entryNode = new Node(entry.getRight());
            depthMostRecentlySeenNode.put(entry.getLeft(), entryNode);
            if (entry.getLeft() == 0) {
                depthMostRecentlySeenNode.put(0, entryNode);    // Root node. No parents
            } else {
                Node parent = Validatec.notNull(depthMostRecentlySeenNode.get(entry.getLeft() - 1));
                if (parent.leftChild == null) {
                    parent.leftChild = entryNode;
                } else {
                    Validatec.isNull(parent.rightChild);
                    parent.rightChild = entryNode;
                }
            }
        }
        return depthMostRecentlySeenNode.get(0);
    }

    @VisibleForTesting
    static List<Pair<Integer, Integer>> getDepthValues(String input) {
        int startIndex = 0;
        List<Pair<Integer, Integer>> result = Lists.newArrayList();
        while (startIndex < input.length()) {
            Pair<Integer, Integer> next = getNextDepthValue(input, startIndex);
            result.add(next);
            startIndex += next.getLeft();   // This is the number of dashes present
            startIndex += next.getRight().toString().length();  // This is the character-size of the value
        }
        return result;
    }

    private static Pair<Integer, Integer> getNextDepthValue(String input, int index) {
        int depth = 0;
        while (input.charAt(index) == '-') {
            index++;
            depth++;
        }
        int numberStartIndexInclusive = index;

        int numberEndIndexExclusive = index + 1;
        while (numberEndIndexExclusive < input.length() && input.charAt(numberEndIndexExclusive) != '-') {
            numberEndIndexExclusive++;
        }

        String number = input.substring(numberStartIndexInclusive, numberEndIndexExclusive);
        return Pair.of(depth, Integer.parseInt(number));
    }
}
