package org.rajivprab.sandbox.kth_highest_binary_tree;

import org.rajivprab.cava.Validatec;

public class Node {
    private Node left;
    private Node right;
    final int val;

    public Node(int val) {
        this.val = val;
    }

    public void put(int newVal) {
        Validatec.notEquals(val, newVal);
        if (newVal > val) {
            if (right == null) {
                right = new Node(newVal);
                return;
            }
            right.put(newVal);
            return;
        }

        if (left == null) {
            left = new Node(newVal);
            return;
        }

        left.put(newVal);
    }

    public Node left() {
        return left;
    }

    public Node right() {
        return right;
    }
}
