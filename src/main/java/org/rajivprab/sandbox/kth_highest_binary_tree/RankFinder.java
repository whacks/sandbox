package org.rajivprab.sandbox.kth_highest_binary_tree;

import org.rajivprab.cava.Validatec;

public class RankFinder {
    static int getRankedEntry(int rank, Node root) {
        Validatec.notNull(root);
        Validatec.greaterOrEqual(rank, 1);

        if (root.right() == null) {
            if (rank == 1) { return root.val; }
            else if (root.left() == null){ throw new NotEnoughEntriesException(1); }
            try {
                return getRankedEntry(rank - 1, root.left());
            } catch (NotEnoughEntriesException left) {
                throw new NotEnoughEntriesException(left.treeSize + 1);
            }
        }

        try {
            return getRankedEntry(rank, root.right());
        } catch (NotEnoughEntriesException right) {
            int midLeftRank = rank - right.treeSize;
            Validatec.greaterOrEqual(midLeftRank, 1);
            if (midLeftRank == 1) { return root.val; }
            if (root.left() == null) {
                throw new NotEnoughEntriesException(right.treeSize + 1);
            }
            int leftRank = midLeftRank - 1;
            try {
                return getRankedEntry(leftRank, root.left());
            } catch (NotEnoughEntriesException left) {
                throw new NotEnoughEntriesException(right.treeSize + left.treeSize + 1);
            }
        }
    }

    static class NotEnoughEntriesException extends RuntimeException {
        final int treeSize;
        NotEnoughEntriesException(int treeSize) {this.treeSize = treeSize;}
    }
}
