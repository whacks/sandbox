package org.rajivprab.sandbox.continental_divide;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;
import org.javatuples.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * My favorite interview questions
 * <p>
 * Created by rprabhakar on 2/15/16.
 */
class ContDivide {
    public static Collection<Pair<Integer, Integer>> getContDivides(List<List<Integer>> map) {
        return new ContDivide(map).getDivides();
    }

    private final List<List<Integer>> map;

    ContDivide(List<List<Integer>> map) {
        this.map = map;
    }

    private int numRows() {
        return map.size();
    }

    private int numCols() {
        return map.get(0).size();
    }

    Collection<Pair<Integer, Integer>> getDivides() {
        return Sets.intersection(getWestBound(), getEastBound());
    }

    private Set<Pair<Integer, Integer>> getWestBound() {
        return getPointsThatCanReach(getAllPointsInColumn(0));
    }

    private Set<Pair<Integer, Integer>> getEastBound() {
        return getPointsThatCanReach(getAllPointsInColumn(numCols() - 1));
    }

    private Set<Pair<Integer, Integer>> getPointsThatCanReach(Collection<Pair<Integer, Integer>> seedPoints) {
        Deque<Pair<Integer, Integer>> pointsToVisit = Queues.newLinkedBlockingDeque(seedPoints);
        Set<Pair<Integer, Integer>> queuedPoints = Sets.newHashSet(seedPoints);
        while (!pointsToVisit.isEmpty()) {
            Pair<Integer, Integer> curPoint = pointsToVisit.poll();
            List<Pair<Integer, Integer>> newPoints = getHigherOrEqualNeighbors(curPoint.getValue0(),
                                                                               curPoint.getValue1())
                    .stream().filter(p -> !queuedPoints.contains(p)).collect(Collectors.toList());
            queuedPoints.addAll(newPoints);
            pointsToVisit.addAll(newPoints);
        }
        return queuedPoints;
    }

    private Collection<Pair<Integer, Integer>> getHigherOrEqualNeighbors(int row, int col) {
        int myHeight = map.get(row).get(col);
        return getNeighbors(row, col).stream()
                                     .filter(p -> map.get(p.getValue0()).get(p.getValue1()) >= myHeight)
                                     .collect(Collectors.toList());
    }

    private Collection<Pair<Integer, Integer>> getNeighbors(int row, int col) {
        Collection<Pair<Integer, Integer>> neighbors = Lists.newArrayList();
        if (row > 0) { neighbors.add(Pair.with(row - 1, col)); }
        if (row < numRows() - 1) { neighbors.add(Pair.with(row + 1, col)); }
        if (col > 0) { neighbors.add(Pair.with(row, col - 1)); }
        if (col < numCols() - 1) { neighbors.add(Pair.with(row, col + 1)); }
        return neighbors;
    }

    private Collection<Pair<Integer, Integer>> getAllPointsInColumn(int column) {
        return IntStream.range(0, numRows()).mapToObj(row -> Pair.with(row, column)).collect(Collectors.toList());
    }
}
