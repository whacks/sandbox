package org.rajivprab.sandbox.continental_divide;

import java.util.Scanner;

/**
 * Solution provided by Interview candidate Reddy
 *
 * Created by rprabhakar on 4/5/16.
 */
public class ReddySolution {
    // to find neighbors
    static int xpoints[] = {0, 0, 1, -1};
    static int ypoints[] = {1, -1, 0, 0};

    public static void main(String args[] ) throws Exception {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter number of rows");
        int rows = s.nextInt();
        System.out.println("rows: " + rows);
        System.out.println("Enter number of columns");
        int columns = s.nextInt();

        System.out.println("columns: " + columns);

        int[][] input = new int[rows][columns];
        boolean[][] eastern = new boolean[rows][columns];
        boolean[][] western = new boolean[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                input[i][j] = s.nextInt();
            }
        }

        int validPoints = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (checkPointUtil(i, j, rows, columns, input, eastern, western)) {
                    validPoints++;
                }
            }
        }

        if (validPoints == 0) {
            System.out.println("()");
        }
    }

    private static boolean checkPointUtil(int i, int j, int rows, int columns, int[][] input,
                                          boolean[][] eastern, boolean[][] western) {
        boolean[][] bool = new boolean[rows][columns];

        // Checking eastern edge
        if (checkIfValid(i, j, rows, columns, bool, false, input, true)) {
            eastern[i][j] = true;
        }

        // Make the visited nodes false again
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                bool[r][c] = false;
            }
        }

        // Checking western edge
        if (checkIfValid(i, j, rows, columns, bool, false, input, false)) {
            western[i][j] = true;
        }

        // This is the candidate
        if (eastern[i][j] && western[i][j]) {
            System.out.println("(" + i + "," + j + ")");
            return true;
        }

        return false;
    }

    private static boolean checkIfValid(int i, int j, int rows, int columns, boolean[][] bool, boolean foundEdge,
                                        int[][] input, boolean isEast) {
        bool[i][j] = true;

        if (isEast) {
            if (checkEastern(i, j, rows, columns) == true) {
                foundEdge = true;
            }
        }
        else {
            if (checkWestern(i, j, rows, columns) == true) {
                foundEdge = true;
            }
        }

        if (foundEdge) {
            return true;
        }

        for (int k = 0; k < xpoints.length; k++) {
            if (((i + xpoints[k] < rows) && (i + xpoints[k] >= 0))
                    && ((j + ypoints[k] < columns) && (j + ypoints[k] >= 0))
                    && bool[i + xpoints[k]][j + ypoints[k]] == false
                    && input[i][j] >= input[i + xpoints[k]][j + ypoints[k]])
            {
                if (checkIfValid(i + xpoints[k], j + ypoints[k], rows, columns, bool, foundEdge, input, isEast)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static boolean checkWestern(int i, int j, int rows, int columns) {
        if ((j == 0) && (i >= 0 && i < rows)) {
            return true;
        }

        return false;
    }

    private static boolean checkEastern(int i, int j, int rows, int columns) {
        if ((j == columns - 1) && (i >= 0 && i < rows)) {
            return true;
        }

        return false;
    }
}
