package org.rajivprab.sandbox.booking_concert_tickets;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.rajivprab.cava.Validatec;

import java.util.Map;
import java.util.NavigableMap;

/**
 * https://leetcode.com/problems/booking-concert-tickets-in-groups/
 *
 * A concert hall has n rows numbered from 0 to n - 1, each with m seats, numbered from 0 to m - 1. You need to
 * design a ticketing system that can allocate seats in the following cases:
 *
 * If a group of k spectators can sit together in a row.
 * If every member of a group of k spectators can get a seat. They may or may not sit together.
 * Note that the spectators are very picky. Hence:
 *
 * They will book seats only if each member of their group can get a seat with row number less than or equal to
 * maxRow. maxRow can vary from group to group.
 * In case there are multiple rows to choose from, the row with the smallest number is chosen. If there are
 * multiple seats to choose in the same row, the seat with the smallest number is chosen.
 * Implement the BookMyShow class:
 *
 * BookMyShow(int n, int m) Initializes the object with n as number of rows and m as number of seats per row.
 * int[] gather(int k, int maxRow) Returns an array of length 2 denoting the row and seat number (respectively) of
 * the first seat being allocated to the k members of the group, who must sit together. In other words, it returns
 * the smallest possible r and c such that all [c, c + k - 1] seats are valid and empty in row r, and r <= maxRow.
 * Returns [] in case it is not possible to allocate seats to the group.
 * boolean scatter(int k, int maxRow) Returns true if all k members of the group can be allocated seats in rows 0
 * to maxRow, who may or may not sit together. If the seats can be allocated, it allocates k seats to the group
 * with the smallest row numbers, and the smallest possible seat numbers in each row. Otherwise, returns false.
 */
public class BookingConcertTickets {

    private final int numRows;
    private final int numCols;
    // Contains the largest column-seat that has been filled in each row.
    // If no seats have been filled in a row, map.get(row) will return null.
    // Note that as per the problem statement, all seats less than or equal the the largest-column-seat-filled
    // will also be filled
    private final Map<Integer, Integer> lastFilledColumnByRow;

    public BookingConcertTickets(int numRows, int numCols) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.lastFilledColumnByRow = Maps.newHashMap();
    }

    public int[] gather(int k, int maxRow) {
        Validatec.greaterThan(numRows, maxRow);
        Validatec.greaterOrEqual(k, 1);

        if (k >= numCols) { return new int[]{}; }

        for (int row=0; row<=maxRow; row++) {
            int numSeatsAvailableInRow = getNumAvailSeatsInRow(row);
            if (numSeatsAvailableInRow >= k) {
                int firstColFilled = lastFilledColumnByRow.getOrDefault(row, -1) + 1;
                int lastColFilled = firstColFilled + k - 1;
                lastFilledColumnByRow.put(row, lastColFilled);
                return new int[]{row, firstColFilled};
            }
        }
        return new int[]{};
    }

    public boolean scatter(int k, int maxRow) {
        Validatec.greaterThan(numRows, maxRow);
        Validatec.greaterOrEqual(k, 1);

        if (!areSeatsAvailable(k, maxRow)) { return false; }

        int numSeatsLeftToReserve = k;
        int curRow = 0;
        while (numSeatsLeftToReserve > 0) {
            int numSeatsAvailableInRow = getNumAvailSeatsInRow(curRow);
            if (numSeatsLeftToReserve >= numSeatsAvailableInRow) {
                // Reserve all remaining seats in row
                numSeatsLeftToReserve -= numSeatsAvailableInRow;
                lastFilledColumnByRow.put(curRow, numCols-1);
                curRow++;
            } else {
                // Only reserve the exact number needed, then done
                int firstColFilled = lastFilledColumnByRow.getOrDefault(curRow, -1) + 1;
                int lastColFilled = firstColFilled + numSeatsLeftToReserve - 1;
                lastFilledColumnByRow.put(curRow, lastColFilled);
                return true;
            }
        }
        throw new IllegalStateException();
    }

    private boolean areSeatsAvailable(int numSeats, int maxRow) {
        Validatec.greaterThan(numRows, maxRow);

        int numSeatsAvailableSoFar = 0;
        for (int row=0; row<=maxRow; row++) {
            int numSeatsAvailableInRow = getNumAvailSeatsInRow(row);
            numSeatsAvailableSoFar += numSeatsAvailableInRow;
            if (numSeatsAvailableSoFar >= numSeats) { return true; }
        }
        return false;
    }

    private int getNumAvailSeatsInRow(int row) {
        Validatec.greaterThan(numRows, row);

        int lastColFilled = lastFilledColumnByRow.getOrDefault(row, -1);
        return numCols - lastColFilled - 1;
    }
}
