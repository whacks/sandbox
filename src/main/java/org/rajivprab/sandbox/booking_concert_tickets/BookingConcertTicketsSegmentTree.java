package org.rajivprab.sandbox.booking_concert_tickets;

import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * https://leetcode.com/problems/booking-concert-tickets-in-groups/
 *
 * See BookingConcertTickets
 * Solving same problem using SegmentTree for O(logN) complexity, instead of O(N), in performing gather and scatter
 */
public class BookingConcertTicketsSegmentTree {

    private final int numRows;
    private final int numCols;
    private final MaxSumSegmentTree seatsAvailableByRow;

    public BookingConcertTicketsSegmentTree(int numRows, int numCols) {
        this.numRows = numRows;
        this.numCols = numCols;
        List<Integer> seatsAvailableInEachRow = IntStream.range(0, numRows).mapToObj(i -> numCols).collect(Collectors.toList());
        this.seatsAvailableByRow = buildSegmentTree(seatsAvailableInEachRow);
    }

    public int[] gather(int k, int maxRow) {
        Validatec.greaterOrEqual(k, 1);

        int smallestRow = seatsAvailableByRow.minIndexWithAtLeast(k);
        if (smallestRow > maxRow) { return new int[]{}; }

        int prevSeatsAvailable = seatsAvailableByRow.get(smallestRow);
        int updatedSeatsAvailable = prevSeatsAvailable - k;
        Validatec.greaterOrEqual(updatedSeatsAvailable, 0);
        seatsAvailableByRow.update(smallestRow, updatedSeatsAvailable);

        int prevSeatsFilled = numCols - prevSeatsAvailable;
        return new int[]{smallestRow, prevSeatsFilled};
    }

    public boolean scatter(int k, int maxRow) {
        Validatec.greaterOrEqual(k, 1);

        if (seatsAvailableByRow.rangeSum(maxRow) < k) { return false; }
        seatsAvailableByRow.consume(k, maxRow);
        return true;
    }

    private static MaxSumSegmentTree buildSegmentTree(List<Integer> elements) {
        return buildSegmentTree(elements, 0, elements.size() - 1);
    }

    private static MaxSumSegmentTree buildSegmentTree(List<Integer> elements, int startIndexInclusive, int endIndexInclusive) {
        Validatec.greaterOrEqual(endIndexInclusive, startIndexInclusive);
        if (startIndexInclusive == endIndexInclusive) {
            return new MaxSumSegmentTree(startIndexInclusive, elements.get(startIndexInclusive));
        }

        int midpoint = (startIndexInclusive + endIndexInclusive) / 2;
        MaxSumSegmentTree leftChild = buildSegmentTree(elements, startIndexInclusive, midpoint);
        MaxSumSegmentTree rightChild = buildSegmentTree(elements, midpoint + 1, endIndexInclusive);
        return new MaxSumSegmentTree(startIndexInclusive, endIndexInclusive, leftChild, rightChild);
    }

    private static class MaxSumSegmentTree {
        private final int startIndexInclusive;
        private final int endIndexInclusive;
        private final MaxSumSegmentTree leftChild;
        private final MaxSumSegmentTree rightChild;

        private int max;
        private int sum;

        private MaxSumSegmentTree(int index, int value) {
            this.startIndexInclusive = index;
            this.endIndexInclusive = index;
            this.leftChild = null;
            this.rightChild = null;
            this.max = value;
            this.sum = value;
        }

        private MaxSumSegmentTree(int startIndexInclusive, int endIndexInclusive, MaxSumSegmentTree left, MaxSumSegmentTree right) {
            this.startIndexInclusive = startIndexInclusive;
            this.endIndexInclusive = endIndexInclusive;
            this.leftChild = left;
            this.rightChild = right;
            refreshParent();
        }

        // Returns Integer.MAX_VALUE if no matching index exists
        public int minIndexWithAtLeast(int val) {
            if (max < val) { return Integer.MAX_VALUE; }
            if (startIndexInclusive == endIndexInclusive) { return startIndexInclusive; }
            if (leftChild != null) {
                int leftResult = leftChild.minIndexWithAtLeast(val);
                // No need to search right-child is valid return. By definition, leftChild will always contain
                // the smaller indexes
                if (leftResult != Integer.MAX_VALUE) { return leftResult; }
            }
            return rightChild.minIndexWithAtLeast(val);
        }

        // Returns the actual num consumed
        public int consume(int desired, int maxIndexInclusive) {
            // Early termination. Prevent wasteful recursion
            if (sum == 0) { return 0; }
            if (startIndexInclusive > maxIndexInclusive) { return 0; }

            if (startIndexInclusive == endIndexInclusive) {
                // leaf node
                Validatec.equals(max, sum);
                if (sum > desired) {
                    sum -= desired;
                    max = sum;
                    return desired;
                } else {
                    int consumed = sum;
                    sum = 0;
                    max = 0;
                    return consumed;
                }
            }

            int leftConsumed = leftChild == null ? 0 : leftChild.consume(desired, maxIndexInclusive);
            desired -= leftConsumed;

            int rightConsumed = 0;
            if (desired > 0 && rightChild != null) {
                rightConsumed = rightChild.consume(desired, maxIndexInclusive);
            }

            refreshParent();
            return leftConsumed + rightConsumed;
        }

        public int rangeSum(int maxIndexInclusive) {
            if (endIndexInclusive <= maxIndexInclusive) { return sum; }
            if (startIndexInclusive > maxIndexInclusive) { return 0; }

            int leftRangeSum = leftChild == null ? 0 : leftChild.rangeSum(maxIndexInclusive);
            int rightRangeSum = rightChild == null ? 0 : rightChild.rangeSum(maxIndexInclusive);
            return leftRangeSum + rightRangeSum;
        }

        public int get(int index) {
            Validatec.isTrue(containsIndex(index));

            // Leaf node
            if (startIndexInclusive == endIndexInclusive) {
                Validatec.equals(max, sum);
                return max;
            }

            if (leftChild != null && leftChild.containsIndex(index)) {
                return leftChild.get(index);
            } else {
                return rightChild.get(index);
            }
        }

        public void update(int index, int newValue) {
            Validatec.isTrue(containsIndex(index));

            // Leaf node
            if (startIndexInclusive == endIndexInclusive) {
                max = newValue;
                sum = newValue;
                return;
            }

            if (leftChild != null && leftChild.containsIndex(index)) {
                leftChild.update(index, newValue);
            } else {
                rightChild.update(index, newValue);
            }
            refreshParent();
        }

        private void refreshParent() {
            int leftSum = leftChild == null ? 0 : leftChild.sum;
            int rightSum = rightChild == null ? 0 : rightChild.sum;
            int leftMax = leftChild == null ? 0 : leftChild.max;
            int rightMax = rightChild == null ? 0 : rightChild.max;
            sum = leftSum + rightSum;
            max = Math.max(leftMax, rightMax);
        }

        private boolean containsIndex(int index) {
            return (index >= startIndexInclusive) && (index <= endIndexInclusive);
        }
    }
}
