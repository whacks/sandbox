package org.rajivprab.sandbox.election_by_jury;

import com.google.common.collect.Streams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;
import org.rajivprab.sandbox.placeholder.Placeholder;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class JurySamplingVariance {
    private static final Logger log = LogManager.getLogger(JurySamplingVariance.class);

    private static final int NUM_SIMULATIONS = 10000;
    private static final Random RNG = new Random();

    public static void main(String ...args) {
        for (double populationPreference=51; populationPreference<60.1; populationPreference++) {
            successRate(populationPreference);
        }
    }

    private static void successRate(double populationPreference) {
        for (long jurySize=16; jurySize<10000; jurySize*=2) {
            long tiebreakJurySize = jurySize + 1;
            double successRate = successRate(populationPreference, tiebreakJurySize);
            log.info("[JurySize={}][PopulationPreference%={}] success-rate = {}%",
                     tiebreakJurySize, populationPreference, successRate);
            if (successRate > 99) {
                return;
            }
        }
    }

    private static double successRate(double populationPreference, long jurySize) {
        long numSuccess = IntStream
                .range(0, NUM_SIMULATIONS)
                .parallel()
                .filter(i -> juryReturnsPopularResult(populationPreference, jurySize))
                .count();
        return numSuccess * 100.0 / NUM_SIMULATIONS;
    }

    private static boolean juryReturnsPopularResult(double populationPreference, long jurySize) {
        Validatec.equals(jurySize % 2, 1L, "Choose odd-sized jury to prevent ties");
        long numJurorsWithPopularResult = LongStream
                .range(0, jurySize)
                .parallel()
                .filter(i -> jurorReturnsPopularResult(populationPreference))
                .count();
        return numJurorsWithPopularResult > (jurySize / 2);
    }

    private static boolean jurorReturnsPopularResult(double populationPreference) {
        Validatec.greaterThan(populationPreference, 1.0, "populationPreference should be a percentage > 1");
        Validatec.greaterThan(100.0, populationPreference, "populationPreference should be a percentage < 100");
        double threshold = populationPreference / 100.0;
        return RNG.nextDouble() <= threshold;
    }
}
