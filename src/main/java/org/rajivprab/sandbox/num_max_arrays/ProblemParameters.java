package org.rajivprab.sandbox.num_max_arrays;

import java.util.Objects;

public class ProblemParameters {
    final int numElements;
    final int maxValue;
    final int searchCost;
    final int maxSeen;

    ProblemParameters(int numElements, int maxValue, int searchCost, int maxSeen) {
        this.numElements = numElements;
        this.maxValue = maxValue;
        this.searchCost = searchCost;
        this.maxSeen = maxSeen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProblemParameters that = (ProblemParameters) o;
        return numElements == that.numElements &&
                maxValue == that.maxValue &&
                searchCost == that.searchCost &&
                maxSeen == that.maxSeen;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numElements, maxValue, searchCost, maxSeen);
    }

    @Override
    public String toString() {
        return "ProblemParameters{" +
                "numElements=" + numElements +
                ", maxValue=" + maxValue +
                ", searchCost=" + searchCost +
                ", maxSeen=" + maxSeen +
                '}';
    }
}
