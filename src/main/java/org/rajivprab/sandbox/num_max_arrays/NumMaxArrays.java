package org.rajivprab.sandbox.num_max_arrays;

import com.google.common.collect.Maps;
import org.rajivprab.cava.Validatec;

import java.math.BigInteger;
import java.util.Map;

/**
 * https://leetcode.com/problems/build-array-where-you-can-find-the-maximum-exactly-k-comparisons/
 */
public class NumMaxArrays {
    private static final BigInteger MOD = BigInteger.valueOf(10).pow(9).add(BigInteger.valueOf(7));

    static final Map<ProblemParameters, BigInteger> cache = Maps.newHashMap();

    public static int numOfArrays(int numElements, int maxValue, int searchCost) {
        BigInteger totalUniqueArrays = numOfArraysHelper(numElements, maxValue, searchCost, 0);
        return totalUniqueArrays.mod(MOD).intValueExact();
    }

    private static BigInteger numOfArraysHelper(int numElements, int maxValue, int searchCost, int maxSeen) {
        Validatec.greaterOrEqual(maxValue, maxSeen);
        Validatec.greaterThan(numElements, 0);

        if (searchCost < 0) { return BigInteger.ZERO; }
        if (searchCost > numElements) { return BigInteger.ZERO; }

        if (numElements == 1) {
            if (searchCost == 1) {
                return BigInteger.valueOf(maxValue - maxSeen);
            } else {
                Validatec.equals(searchCost, 0);
                return BigInteger.valueOf(maxSeen);
            }
        }

        if (maxSeen == maxValue) {
            if (searchCost > 0) {
                return BigInteger.ZERO;
            } else {
                Validatec.equals(searchCost, 0);
                return BigInteger.valueOf(maxSeen).pow(numElements);
            }
        }

        ProblemParameters parameters = new ProblemParameters(numElements, maxValue, searchCost, maxSeen);
        BigInteger result = cache.get(parameters);
        if (result == null) {
            result = numOfArraysWorker(parameters);
            cache.put(parameters, result);
        }
        return result;
    }

    private static BigInteger numOfArraysWorker(ProblemParameters parameters) {
        // Fill in first entry of array with value that is smaller than or equal to the max-seen thus far

        BigInteger numberOfPossibilities = BigInteger.ZERO;
        if (parameters.maxSeen != 0) {
            numberOfPossibilities = numOfArraysHelper(parameters.numElements - 1,
                                                      parameters.maxValue,
                                                      parameters.searchCost,
                                                      parameters.maxSeen);
            numberOfPossibilities = numberOfPossibilities.multiply(BigInteger.valueOf(parameters.maxSeen));
        }

        for (int firstValue = parameters.maxSeen + 1; firstValue <= parameters.maxValue; firstValue++) {
            BigInteger childPossibilities = numOfArraysHelper(parameters.numElements - 1,
                                                       parameters.maxValue,
                                                       parameters.searchCost-1,
                                                       firstValue);
            numberOfPossibilities = numberOfPossibilities.add(childPossibilities);
        }

        Validatec.greaterOrEqual(numberOfPossibilities, BigInteger.ZERO, "Input: " + parameters + ", Output: " + numberOfPossibilities);
        return numberOfPossibilities;
    }
}
