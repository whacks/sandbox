package org.rajivprab.sandbox.contrarian_investor;

import org.javatuples.Pair;

/**
 * Created by rprabhakar on 4/21/16.
 */
public class Portfolio {
    private final double numIndexShares;
    private final double liquidHoldings;

    public static Portfolio buildPortfolio(double numShares, double cash) {
        return new Portfolio(numShares, cash);
    }

    public static Portfolio buildRatioPortfolio(double netWorth, double shareRatio, double sharePrice) {
        double numShares = shareRatio * netWorth / sharePrice;
        double cash = (1 - shareRatio) * netWorth;
        return buildPortfolio(numShares, cash);
    }

    public static Portfolio rebalancePortfolio(Portfolio portfolio, double shareRatio, double sharePrice) {
        return buildRatioPortfolio(portfolio.getTotalMarketValue(sharePrice), shareRatio, sharePrice);
    }

    private Portfolio(double numIndexShares, double liquidHoldings) {
        this.numIndexShares = numIndexShares;
        this.liquidHoldings = liquidHoldings;
    }

    public double getTotalMarketValue(double sharePrice) {
        return liquidHoldings + numIndexShares * sharePrice;
    }

    public double getStockRatio(double sharePrice) {
        return numIndexShares * sharePrice / getTotalMarketValue(sharePrice);
    }

    public String print(double sharePrice) {
        return "MarketValue / StockRatio = " + Pair.with(getTotalMarketValue(sharePrice), getStockRatio(sharePrice));
    }
}
