package org.rajivprab.sandbox.contrarian_investor;

import com.google.common.collect.Lists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 * Simple trade simulator for a contrarian strategy, rebalancing strategy and passive strategy
 *
 * Created by rprabhakar on 4/21/16.
 */
public class TradeSim {
    private static final Logger log = LogManager.getLogger(TradeSim.class);

    private static final String FILE_PATH = "sp500/sp500_daily.csv";

    public static void main(String... args) throws IOException {
        MarketData marketData = new MarketData(ClassLoader.getSystemResourceAsStream(FILE_PATH));
        simTrader(new Contra.PassiveInvestor(), marketData);
        simTrader(new Contra.PassiveRatioInvestor(0.7), marketData);
        simTrader(new Contra.ContrarianInvestor(MarketData.WINDOW_ONE), marketData);
        simTrader(new Contra.ContrarianInvestor(MarketData.WINDOW_TWO), marketData);
        simTrader(new Contra.ContrarianInvestor(MarketData.WINDOW_THREE), marketData);
    }

    private static void simTrader(Contra trader, MarketData marketData) {
        /* Original
        List<Portfolio> traderPortfolios = Lists.newArrayList(Portfolio.buildPortfolio(100, 100));
        for (LocalDate date : marketData.getOrderedDates()) {
            Portfolio curPortfolio = traderPortfolios.get(traderPortfolios.size() - 1);
            log.info("Trader: {}, Date: {}, Portfolio: {}",
                     trader, date, curPortfolio.print(marketData.getClose(date)));
            traderPortfolios.add(trader.getNewPortfolio(curPortfolio, marketData, date));
        }
         */

        // 2023 update:
        Portfolio curPortfolio = Portfolio.buildPortfolio(100, 100);
        LocalDate lastDate = null;
        for (LocalDate date : marketData.getOrderedDates()) {
            curPortfolio = trader.getNewPortfolio(curPortfolio, marketData, date);
            lastDate = date;
        }
        log.info("Trader: {}, Portfolio: {}",
                 trader, curPortfolio.print(marketData.getClose(lastDate)));
    }
}