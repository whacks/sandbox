package org.rajivprab.sandbox.contrarian_investor;

import java.time.LocalDate;

/**
 * Created by rprabhakar on 4/21/16.
 */
public interface Contra {
    Portfolio getNewPortfolio(Portfolio currentPortfolio, MarketData marketData, LocalDate date);

    class PassiveInvestor implements Contra {
        @Override
        public Portfolio getNewPortfolio(Portfolio currentPortfolio, MarketData marketData, LocalDate date) {
            return currentPortfolio;
        }
    }

    class PassiveRatioInvestor implements Contra {
        private final double shareRatio;

        public PassiveRatioInvestor(double shareRatio) {
            this.shareRatio = shareRatio;
        }

        @Override
        public Portfolio getNewPortfolio(Portfolio currentPortfolio, MarketData marketData, LocalDate date) {
            return Portfolio.rebalancePortfolio(currentPortfolio, shareRatio, marketData.getClose(date));
        }
    }

    class ContrarianInvestor implements Contra {
        private final int movingWindow;

        public ContrarianInvestor(int movingWindow) {
            this.movingWindow = movingWindow;
        }

        @Override
        public Portfolio getNewPortfolio(Portfolio currentPortfolio, MarketData marketData, LocalDate date) {
            try {
                double yearlyReturns = marketData.getAverageReturns(date, movingWindow);
                double sharePrice = marketData.getClose(date);
                double currentRatio = currentPortfolio.getStockRatio(sharePrice);
                double longTermSignal = 0.08 - yearlyReturns;
                double dailySignal = longTermSignal / 100;
                double targetRatio = currentRatio * (1 + dailySignal);
                if (targetRatio > 1.0) { targetRatio = 1; }
                if (targetRatio < 0.5) { targetRatio = 0.5; }
                return Portfolio.rebalancePortfolio(currentPortfolio, targetRatio, sharePrice);
            } catch (NullPointerException e) {
                return currentPortfolio;
            }
        }

        @Override
        public String toString() {
            return "Contrarian Investor: " + movingWindow + " days average";
        }
    }
}
