package org.rajivprab.sandbox.contrarian_investor;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.IOExceptionc;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * Market data construct
 *
 * Created by rprabhakar on 4/21/16.
 */
public class MarketData {
    public static final int YEAR_TRADING_DAYS = 50 * 5;

    private static final Logger log = LogManager.getLogger(MarketData.class);
    private static final String DATE = "Date";
    private static final String CLOSE = "Adj Close";

    public static final int WINDOW_ONE = YEAR_TRADING_DAYS;
    public static final int WINDOW_TWO = 3 * YEAR_TRADING_DAYS;
    public static final int WINDOW_THREE = 5 * YEAR_TRADING_DAYS;

    private final List<LocalDate> dates;

    // <Date, 1> represents close-price on that date
    // <Date, 250> represents average-close-price on that date, for the past 250 days
    private final Table<LocalDate, Integer, Double> pastDaysAverageClose = HashBasedTable.create();
    private final Table<LocalDate, Integer, Double> yearlyReturnOverPastAverage = HashBasedTable.create();

    public MarketData(InputStream dataStream) {
        buildClosePrices(dataStream);
        this.dates = buildDates();
        constructPastAverage(WINDOW_ONE);
        constructPastAverage(WINDOW_TWO);
        constructPastAverage(WINDOW_THREE);
        populateYearlyReturnsOverAverage(WINDOW_ONE);
        populateYearlyReturnsOverAverage(WINDOW_TWO);
        populateYearlyReturnsOverAverage(WINDOW_THREE);
    }

    private void putAverage(LocalDate date, int numDays, double avg) {
        pastDaysAverageClose.put(date, numDays, avg);
        log.info("Market date: " + date + ", " + numDays + " days moving-average: " + avg);
    }

    private void putClose(LocalDate date, double close) {
        putAverage(date, 1, close);
    }

    public List<LocalDate> getOrderedDates() {
        return ImmutableList.copyOf(dates);
    }

    public double getAverageClose(LocalDate date, int numDays) {
        return pastDaysAverageClose.get(date, numDays);
    }

    public double getClose(LocalDate date) {
        return getAverageClose(date, 1);
    }

    public double getAverageReturns(LocalDate date, int numDays) {
        return yearlyReturnOverPastAverage.get(date, numDays);
    }

    private void buildClosePrices(InputStream dataStream) {
        try {
            CSVParser parser = new CSVParser(new InputStreamReader(dataStream), CSVFormat.DEFAULT.withHeader());
            for (CSVRecord record : parser.getRecords()) {
                LocalDate date = LocalDate.parse(record.get(DATE));
                Validatec.greaterThan(date.getYear(), 1900, "Bad date parsed: " + date);
                double close = Double.valueOf(record.get(CLOSE));
                Validatec.greaterThan(close, 10.0);
                Validatec.greaterThan(100 * 1000.0, close);
                putClose(date, close);
            }
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    private List<LocalDate> buildDates() {
        List<LocalDate> dates = Lists.newArrayList(pastDaysAverageClose.rowKeySet());
        Collections.sort(dates);
        return ImmutableList.copyOf(dates);
    }

    private void constructPastAverage(int numDaysWindow) {
        LocalDate lastDay = dates.get(dates.size() - 1);
        Validatec.isTrue(!pastDaysAverageClose.contains(lastDay, numDaysWindow), "Computing something already computed before");
        double windowSum = 0;
        for (int i=0; i<numDaysWindow; i++) {
            LocalDate cur = dates.get(i);
            double curClose = getClose(cur);
            windowSum += curClose;
        }
        int curIndex = numDaysWindow;
        while (dates.size() > curIndex) {
            LocalDate cur = dates.get(curIndex);
            double average = windowSum / numDaysWindow;
            putAverage(cur, numDaysWindow, average);

            LocalDate leavingWindowDate = dates.get(curIndex - numDaysWindow);
            double leavingWindowClose = getClose(leavingWindowDate);
            windowSum -= leavingWindowClose;
            double curClose = getClose(cur);
            windowSum += curClose;
            curIndex++;
        }
        Validatec.contains(pastDaysAverageClose.row(lastDay).keySet(), numDaysWindow);
    }

    private void populateYearlyReturnsOverAverage(int numDays) {
        for (LocalDate date : dates) {
            populateYearlyReturnsOverAverage(date, numDays);
        }
    }

    private void populateYearlyReturnsOverAverage(LocalDate date, int numDays) {
        try {
            double curClose = getClose(date);
            double avgClose = getAverageClose(date, numDays);
            double grossReturns = curClose / avgClose - 1.0;
            double numYears = numDays * 0.5 / YEAR_TRADING_DAYS;
            yearlyReturnOverPastAverage.put(date, numDays, grossReturns / numYears);
            log.info("For market-date: " + date + ", over " + numDays + " days window, yearly returns have been: " + grossReturns / numYears);
        } catch (NullPointerException ignored) {}
    }
}
