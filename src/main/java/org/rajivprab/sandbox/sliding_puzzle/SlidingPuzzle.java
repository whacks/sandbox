package org.rajivprab.sandbox.sliding_puzzle;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import org.rajivprab.sandbox.sliding_puzzle.Board.IllegalMoveException;

import java.util.Map;
import java.util.Queue;

/**
 * https://leetcode.com/problems/sliding-puzzle/
 *
 * On a 2x3 board, there are 5 tiles represented by the integers 1 through 5, and an empty square represented by 0.
 *
 * A move consists of choosing 0 and a 4-directionally adjacent number and swapping it.
 *
 * The state of the board is solved if and only if the board is [[1,2,3],[4,5,0]].
 *
 * Given a puzzle board, return the least number of moves required so that the state of the board is solved. If it is
 * impossible for the state of the board to be solved, return -1.
 */
class SlidingPuzzle {
    static int minNumberOfMoves(Table<Integer, Integer, Integer> board) {
        Board myBoard = Board.build(board);
        return new SlidingPuzzle().minNumberOfMoves(myBoard);
    }

    private final Map<Board, Integer> boardsSeenAndMovesNeeded = Maps.newHashMap();

    int minNumberOfMoves(Board board) {
        boardsSeenAndMovesNeeded.put(board, 0);
        Queue<Board> boardsToConsider = Lists.newLinkedList();
        boardsToConsider.add(board);

        while (!boardsToConsider.isEmpty()) {
            Board candidate = boardsToConsider.poll();
            int movesSoFar = boardsSeenAndMovesNeeded.get(candidate);
            if (candidate.isSolved()) { return movesSoFar; }

            try {
                Board child = candidate.moveEmptyLeft();
                Integer prevSeen = boardsSeenAndMovesNeeded.putIfAbsent(child, movesSoFar + 1);
                if (prevSeen == null) { boardsToConsider.add(child); }
            } catch (IllegalMoveException ignored) {}

            try {
                Board child = candidate.moveEmptyRight();
                Integer prevSeen = boardsSeenAndMovesNeeded.putIfAbsent(child, movesSoFar + 1);
                if (prevSeen == null) { boardsToConsider.add(child); }
            } catch (IllegalMoveException ignored) {}

            try {
                Board child = candidate.moveEmptyUp();
                Integer prevSeen = boardsSeenAndMovesNeeded.putIfAbsent(child, movesSoFar + 1);
                if (prevSeen == null) { boardsToConsider.add(child); }
            } catch (IllegalMoveException ignored) {}

            try {
                Board child = candidate.moveEmptyDown();
                Integer prevSeen = boardsSeenAndMovesNeeded.putIfAbsent(child, movesSoFar + 1);
                if (prevSeen == null) { boardsToConsider.add(child); }
            } catch (IllegalMoveException ignored) {}
        }

        return -1;
    }
}
