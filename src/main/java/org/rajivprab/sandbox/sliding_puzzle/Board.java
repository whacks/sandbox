package org.rajivprab.sandbox.sliding_puzzle;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import org.rajivprab.cava.Validatec;

import java.util.Objects;

class Board {
    private final Table<Integer, Integer, Integer> board;
    private final int emptyRow;
    private final int emptyCol;

    static Board build(Table<Integer, Integer, Integer> cells) {
        for (int row=0; row<=1; row++) {
            for (int col=0; col<=2; col++) {
                if (cells.get(row, col) == 0) {
                    return new Board(HashBasedTable.create(cells), row, col);
                }
            }
        }
        throw new IllegalStateException();
    }

    private Board(Table<Integer, Integer, Integer> board, int emptyRow, int emptyCol) {
        this.board = board;
        this.emptyCol = emptyCol;
        this.emptyRow = emptyRow;
    }

    Board moveEmptyLeft() {
        Validatec.notEquals(emptyCol, 0, IllegalMoveException::new);

        Table<Integer, Integer, Integer> newBoard = HashBasedTable.create(board);
        swap(newBoard,
             emptyRow, emptyCol,
             emptyRow, emptyCol - 1);

        return new Board(newBoard, emptyRow, emptyCol - 1);
    }

    Board moveEmptyRight() {
        Validatec.notEquals(emptyCol, 2, IllegalMoveException::new);

        Table<Integer, Integer, Integer> newBoard = HashBasedTable.create(board);
        swap(newBoard,
             emptyRow, emptyCol,
             emptyRow, emptyCol + 1);

        return new Board(newBoard, emptyRow, emptyCol + 1);
    }

    Board moveEmptyUp() {
        Validatec.notEquals(emptyRow, 0, IllegalMoveException::new);

        Table<Integer, Integer, Integer> newBoard = HashBasedTable.create(board);
        swap(newBoard,
             emptyRow, emptyCol,
             emptyRow - 1, emptyCol);

        return new Board(newBoard, emptyRow - 1, emptyCol);
    }

    Board moveEmptyDown() {
        Validatec.notEquals(emptyRow, 1, IllegalMoveException::new);

        Table<Integer, Integer, Integer> newBoard = HashBasedTable.create(board);
        swap(newBoard,
             emptyRow, emptyCol,
             emptyRow + 1, emptyCol);

        return new Board(newBoard, emptyRow + 1, emptyCol);
    }

    boolean isSolved() {
        return board.get(0, 0) == 1
                && board.get(0, 1) == 2
                && board.get(0, 2) == 3
                && board.get(1, 0) == 4
                && board.get(1, 1) == 5
                && board.get(1, 2) == 0;
    }

    private void swap(Table<Integer, Integer, Integer> board,
                      int row1, int col1,
                      int row2, int col2) {
        int orig1 = board.get(row1, col1);
        int orig2 = board.get(row2, col2);
        board.put(row1, col1, orig2);
        board.put(row2, col2, orig1);
    }

    static class IllegalMoveException extends RuntimeException {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Board board1 = (Board) o;
        return emptyRow == board1.emptyRow && emptyCol == board1.emptyCol && board.equals(board1.board);
    }

    @Override
    public int hashCode() {
        return Objects.hash(board, emptyRow, emptyCol);
    }
}
