package org.rajivprab.sandbox.icewind_dale;

import org.rajivprab.cava.Validatec;

/**
 * You get to roll a random number from 0..N.
 * You can then accept your number, or roll again from 0..N-1, recursively, until you get to 0..0
 * Your goal is to maximize your score.
 * Assuming someone follows optimal strategy, what's the expected score?
 * <p>
 * Created by rajivprab on 8/23/17.
 */
public class IcewindDale {
    static double getExpectedRecursive(int maxInclusive) {
        if (maxInclusive == 0) {
            return 0;
        }
        Validatec.greaterThan(maxInclusive, 0);
        double nextExpected = getExpectedRecursive(maxInclusive - 1);
        int minValueToAccept = (int) Math.ceil(nextExpected);
        Validatec.greaterOrEqual(maxInclusive, minValueToAccept);

        int originalNumOutcomes = maxInclusive + 1;
        int numOutcomesToAccept = maxInclusive - minValueToAccept + 1;

        double probabilityOfAccepting = numOutcomesToAccept * 1.0 / originalNumOutcomes;
        double expectedValueAccepted = (minValueToAccept + maxInclusive) / 2.0;

        return probabilityOfAccepting * expectedValueAccepted + (1 - probabilityOfAccepting) * nextExpected;
    }
}
