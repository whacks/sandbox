package org.rajivprab.sandbox.expected_value_paradox;

import com.google.common.collect.ImmutableList;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * https://lambdaclass.com/data_etudes/dont_bet_on_an_ev/
 *
 * Conclusion seems to be that the game has a positive expected-value, but very high variance.
 * When you look at the median vs max outcomes: the median is always worse off than the original.
 * But the max is so high, that it makes up for it, and brings the average up.
 *
 * However, as the number of coin-tosses increases, the odds of getting to that positive outcome decreases exponentially.
 * As long as your sample is large enough for at least one sample to hit that large positive outcome,
 * the average will be positive. But as the number of coin-tosses grows, the sample size will need to grow exponentially
 * in order for at least one sample to land on the positive outcome.
 *
 * Similar game that is more extreme:
 * Whenever you flip tails, you lose everything.
 * But when you flip heads, you 10x your wealth.
 *
 * For 100 coin-tosses, you'll need ~2^100 samples, in order for a single sample to not be bankrupt.
 * But that single sample will finish up with a wealth of 10^100.
 * Thus making the average outcome highly profitable (10^100 / 2^100 >> 1), despite the overwhelming failure rate.
 */
public class ExpectedValueParadox {
    private static final double WINNING_BONUS = 1.5;
    private static final double LOSING_PENALTY = 0.6;

    private static final Random RNG = new Random();
    private static final int SAMPLE_SIZE = 1000 * 1000;

    public static void main(String... args) {
        ImmutableList.of(10, 100, 1000, 10000).forEach(ExpectedValueParadox::runExperiment);
    }

    static void runExperiment(int numCoinTosses) {
        List<Double> wealth = getWealth(numCoinTosses);
        System.out.println("---------------------------------------------------");
        System.out.println("Time to go gambling with $1. Good luck!");
        System.out.println(numCoinTosses + " tosses. Average wealth: " + wealth.stream().mapToDouble(i -> i).average().getAsDouble());
        System.out.println(numCoinTosses + " tosses. Median wealth: " + wealth.get(wealth.size() / 2));
        System.out.println(numCoinTosses + " tosses. Richest wealth: " + wealth.get(0));
        System.out.println("---------------------------------------------------");
    }

    // Returns final wealth in a sorted list, for all samples, with wealthiest at index 0
    private static List<Double> getWealth(int numTosses) {
        return IntStream.range(0, SAMPLE_SIZE)
                        .mapToDouble(i -> getFinalWealth(numTosses))
                        .boxed()
                        .sorted(Collections.reverseOrder())
                        .collect(Collectors.toList());
    }

    private static double getFinalWealth(int numTosses) {
        double wealth = 1.0;
        for (int i = 0; i < numTosses; i++) {
            if (RNG.nextBoolean()) {
                wealth *= WINNING_BONUS;
            } else {
                wealth *= LOSING_PENALTY;
            }
        }
        return wealth;
    }
}
