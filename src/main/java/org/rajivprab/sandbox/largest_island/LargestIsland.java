package org.rajivprab.sandbox.largest_island;

import com.google.common.collect.Table;

/**
 * Q: Determine largest group of adjacent pods from a map.
 * Example map:
 * x====x==x
 * xxx====x=
 * ====x==x=
 * ===xx====
 */
public class LargestIsland {
    public static int getLargestIslandSize(Table<Integer, Integer, Type> map) {
        return new Explorer(map).getLargestIsland();
    }
}
