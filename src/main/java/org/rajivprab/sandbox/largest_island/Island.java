package org.rajivprab.sandbox.largest_island;

import com.google.common.collect.Sets;

import java.util.Set;

class Island {
    Set<Coordinate> coordinates = Sets.newHashSet();

    public void add(Coordinate coordinate) {
        this.coordinates.add(coordinate);
    }
}
