package org.rajivprab.sandbox.largest_island;

import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;

import java.util.Set;

class Explorer {
    private final Table<Integer, Integer, Type> map;
    private final Set<Coordinate> visited = Sets.newHashSet();
    private final Set<Island> islands = Sets.newHashSet();

    Explorer(Table<Integer, Integer, Type> map) {
        this.map = ImmutableTable.copyOf(map);
    }

    int getLargestIsland() {
        if (visited.isEmpty()) {
            traverse();
        }
        return islands.stream().mapToInt(island -> island.coordinates.size()).max().orElse(0);
    }

    private void traverse() {
        for (Cell<Integer, Integer, Type> cell : map.cellSet()) {
            Coordinate coordinate = new Coordinate(cell.getRowKey(), cell.getColumnKey());
            if (cell.getValue() == Type.LAND && !visited.contains(coordinate)) {
                Island island = new Island();
                island.add(coordinate);
                islands.add(island);
                explore(coordinate, island);
            }
        }
    }

    private void explore(Coordinate coordinate, Island island) {
        boolean isNew = visited.add(coordinate);
        if (!isNew) { return; }

        if (map.get(coordinate.x, coordinate.y) != Type.LAND) { return; }

        island.add(coordinate);

        explore(new Coordinate(coordinate.x + 1, coordinate.y), island);
        explore(new Coordinate(coordinate.x - 1, coordinate.y), island);
        explore(new Coordinate(coordinate.x, coordinate.y + 1), island);
        explore(new Coordinate(coordinate.x, coordinate.y - 1), island);
    }
}
