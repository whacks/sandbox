package org.rajivprab.sandbox.two_envelopes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomNumber {
    private static final Random RNG = new Random();

    public static BigInteger get() {
        BigInteger result = BigInteger.ONE;
        int bit = 0;
        while (RNG.nextInt(30) > 0) {
            bit++;
            if (RNG.nextBoolean()) {
                result = result.setBit(bit);
            }
        }
        return result;
    }

    public static void main(String... args) {
        List<BigInteger> list = IntStream.range(0, 100).mapToObj(i -> get()).sorted().collect(Collectors.toList());
        System.out.println("Random numbers: " + list);
    }
}
