package org.rajivprab.sandbox.bayes_vs_frequentists;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * http://www.behind-the-enemy-lines.com/2008/01/are-you-bayesian-or-frequentist-or.html
 */
public class BayesFrequentists {
    private static final long NUM_COINS = 1000000;
    private static final int NUM_FLIPS = 14;
    private static final int NUM_HEADS = 10;

    public static void main(String... args) {
        System.out.println("Frequentist probability of winning: " + frequentistProbabilityOfWinning());
        System.out.println("Fraction of bets actually won (using uniform prior): " +
                                   getFractionBetsWonEmpirically(CoinFactory.usingUniformDistribution()));
        System.out.println("Fraction of bets actually won (using normal prior): " +
                                   getFractionBetsWonEmpirically(CoinFactory.usingNormalDistribution()));
        System.out.println("Fraction of bets actually won (using U shaped prior): " +
                                   getFractionBetsWonEmpirically(CoinFactory.usingUDistribution()));
    }

    private static double getFractionBetsWonEmpirically(CoinFactory factory) {
        Collection<Coin> betCoins = LongStream.range(0, NUM_COINS)
                                              .mapToObj(i -> factory.build())
                                              .filter(coin -> coin.flip(NUM_FLIPS) == NUM_HEADS)
                                              .collect(Collectors.toList());
        long numBetsWon = betCoins.stream().filter(coin -> coin.flip(2) == 2).count();
        return numBetsWon * 1.0 / betCoins.size();
    }

    private static double frequentistProbabilityOfWinning() {
        return Math.pow((NUM_HEADS * 1.0 / NUM_FLIPS), 2);
    }
}
