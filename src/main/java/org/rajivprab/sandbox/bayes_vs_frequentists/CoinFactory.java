package org.rajivprab.sandbox.bayes_vs_frequentists;

import org.apache.commons.math3.distribution.NormalDistribution;

import java.util.Random;
import java.util.function.Supplier;

class CoinFactory {
    private static final Random RNG = new Random();
    private static final NormalDistribution NORMAL_DISTRIBUTION = new NormalDistribution(0.5, 0.2);
    private final Supplier<Coin> builder;

    static CoinFactory usingUniformDistribution() {
        return new CoinFactory(() -> new Coin(RNG.nextDouble()));
    }

    static CoinFactory usingNormalDistribution() {
        return new CoinFactory(() -> new Coin(getRandomSampleFromNormalDistribution()));
    }

    static CoinFactory usingUDistribution() {
        return new CoinFactory(() -> new Coin(getRandomSampleFromUDistribution()));
    }

    private static double getRandomSampleFromUDistribution() {
        double normalSample = getRandomSampleFromNormalDistribution();
        if (normalSample < 0.5) {
            return 0.5 - normalSample;
        } else {
            double deltaFromMedian = normalSample - 0.5;
            return 1.0 - deltaFromMedian;
        }
    }

    private static double getRandomSampleFromNormalDistribution() {
        double percentile = RNG.nextDouble();
        double sample = NORMAL_DISTRIBUTION.inverseCumulativeProbability(percentile);
        if (sample < 0 || sample > 1) {
            return getRandomSampleFromNormalDistribution();
        } else {
            return sample;
        }
    }

    private CoinFactory(Supplier<Coin> builder) {
        this.builder = builder;
    }

    public Coin build() {
        return builder.get();
    }
}
