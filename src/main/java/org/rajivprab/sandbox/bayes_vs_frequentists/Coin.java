package org.rajivprab.sandbox.bayes_vs_frequentists;

import org.rajivprab.cava.Validatec;

import java.util.Random;
import java.util.stream.IntStream;

class Coin {
    private static final Random RNG = new Random();

    private final double weight;

    Coin(double weight) {
        Validatec.greaterThan(weight, 0.0);
        Validatec.greaterThan(1.0, weight);
        this.weight = weight;
    }

    // Flips n times, and returns the number of heads
    int flip(int numFlips) {
        return (int) IntStream.range(0, numFlips).mapToObj(i -> flip()).filter(isHeads -> isHeads).count();
    }

    // Returns true for HEADS
    private boolean flip() {
        return RNG.nextDouble() < weight;
    }
}
