package org.rajivprab.sandbox.boggle;

import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;

import java.util.Set;

/**
 * Given a 2-D grid of letters and a dictionary utilizing a data structure of your choice,
 * produce a set of all valid words that exist in the grid.
 *
 * A world can be constructed of characters that are contiguous in any of the 4 directions. They don't have
 * to be in a single line. Ie, like the game Snake
 */
public class Boggle {
    private final Dictionary dictionary;

    public Boggle(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public Set<String> getWords(Table<Integer, Integer, Character> grid) {
        Set<String> words = Sets.newHashSet();
        for (Cell<Integer, Integer, Character> cell : grid.cellSet()) {
            words.addAll(getWords(grid, cell.getRowKey(), cell.getColumnKey(), ""));
        }
        return words;
    }

    private Set<String> getWords(Table<Integer, Integer, Character> grid, int curRow, int curCol, String fragment) {
        Set<String> words = Sets.newHashSet();
        Character curChar = grid.get(curRow, curCol);
        if (curChar == null) { return words; }
        fragment = fragment + curChar;
        if (dictionary.isWord(fragment)) {
            words.add(fragment);
        }
        if (dictionary.getPotentialNextCharacters(fragment).isEmpty()) {
            return words;
        }
        words.addAll(getWords(grid,curRow+1, curCol, fragment));
        words.addAll(getWords(grid,curRow-1, curCol, fragment));
        words.addAll(getWords(grid, curRow,curCol+1, fragment));
        words.addAll(getWords(grid, curRow,curCol-1, fragment));
        return words;
    }
}
