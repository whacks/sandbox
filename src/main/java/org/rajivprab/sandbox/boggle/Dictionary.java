package org.rajivprab.sandbox.boggle;

import java.util.Set;

// Uses a trie data structure to implement both methods below in O(K) complexity,
// where K is the length of the string-parameter
public class Dictionary {
    public Dictionary(Set<String> allWords) {

    }

    public Set<Character> getPotentialNextCharacters(String charsThusFar) {
        throw new UnsupportedOperationException();
    }

    public boolean isWord(String candidate) {
        throw new UnsupportedOperationException();
    }
}
