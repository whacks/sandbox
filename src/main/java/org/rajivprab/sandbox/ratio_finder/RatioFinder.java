package org.rajivprab.sandbox.ratio_finder;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;

import java.util.List;
import java.util.Set;

public class RatioFinder {
    static double getRatio(String a, String b, List<String> ratios) {
        return new RatioFinder(ratios).getRatio(a, b);
    }

    private final Table<String, String, Double> ratios;

    private RatioFinder(List<String> ratios) {
        this.ratios = buildTable(ratios);
    }

    private double getRatio(String a, String b) {
        return getRatioWorker(a, b, Sets.newHashSet(a));
    }

    private double getRatioWorker(String a, String b, Set<String> seen) {
        if (ratios.contains(a, b)) { return ratios.get(a, b); }
        for (String neighbor : ratios.row(a).keySet()) {
            if (seen.add(neighbor)) {
                Double neighborRatio = getRatioWorker(neighbor, b, seen);
                if (!neighborRatio.isNaN()) {
                    return ratios.get(a, neighbor) * neighborRatio;
                }
            }
        }
        return Double.NaN;
    }

    private static Table<String, String, Double> buildTable(List<String> ratios) {
        Table<String, String, Double> table = HashBasedTable.create();
        for (String ratio : ratios) {
            String[] components = ratio.split(" ");
            double val = Double.parseDouble(components[2]);
            table.put(components[0], components[1], val);
            table.put(components[1], components[0], 1/val);
        }
        return ImmutableTable.copyOf(table);
    }
}
