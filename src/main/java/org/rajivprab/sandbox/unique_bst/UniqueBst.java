package org.rajivprab.sandbox.unique_bst;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javatuples.Pair;
import org.rajivprab.cava.Validatec;

import java.util.List;
import java.util.Map;

/**
 * https://www.interviewbit.com/problems/unique-binary-search-trees/
 *
 * Given A, generate all structurally unique BST’s (binary search trees) that store values 1...A.
 */
public class UniqueBst {
    private static final Logger log = LogManager.getLogger(UniqueBst.class);
    private static final Map<Pair<Integer, Integer>, ImmutableList<TreeNode>> cache = Maps.newConcurrentMap();

    public static List<TreeNode> generateTrees(int a) {
        return getTrees(1, a);
    }

    private static List<TreeNode> getTrees(int startInclusive, int endInclusive) {
        log.info("Cache size: " + cache.size());
        return cache.computeIfAbsent(Pair.with(startInclusive, endInclusive),
                                     ignore -> generateTrees(startInclusive, endInclusive));
    }

    private static ImmutableList<TreeNode> generateTrees(int startInclusive, int endInclusive) {
        if (startInclusive == endInclusive) {
            return ImmutableList.of(TreeNode.leaf(startInclusive));
        } else if (startInclusive > endInclusive) {
            return ImmutableList.of();
        }

        List<TreeNode> result = Lists.newArrayList();
        for (int head = startInclusive; head <= endInclusive; head++) {
            TreeNode headTree = TreeNode.leaf(head);
            List<TreeNode> leftTrees = getTrees(startInclusive, head - 1);
            List<TreeNode> rightTrees = getTrees(head + 1, endInclusive);
            if (leftTrees.isEmpty()) {
                Validatec.notEmpty(rightTrees);
                for (TreeNode cur : rightTrees) {
                    result.add(headTree.withRight(cur));
                    log.info("Updating result. Now with size: " + result.size());
                }
            } else if (rightTrees.isEmpty()) {
                Validatec.notEmpty(leftTrees);
                for (TreeNode cur : leftTrees) {
                    result.add(headTree.withLeft(cur));
                    log.info("Updating result. Now with size: " + result.size());
                }
            } else {
                for (TreeNode left : leftTrees) {
                    TreeNode headTreeWithLeft = headTree.withLeft(left);
                    for (TreeNode right : rightTrees) {
                        result.add(headTreeWithLeft.withRight(right));
                        log.info("Updating result. Now with size: " + result.size());
                    }
                }
            }
        }
        log.info("Returning " + result.size() + " results");
        log.info(result);
        return ImmutableList.copyOf(result);
    }
}
