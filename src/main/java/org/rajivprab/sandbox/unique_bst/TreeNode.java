package org.rajivprab.sandbox.unique_bst;

import org.json.JSONObject;
import org.rajivprab.cava.Validatec;

public interface TreeNode {
    static TreeNode leaf(int val) {
        return new SimpleNode(val, null, null);
    }

    TreeNode withLeft(TreeNode left);
    TreeNode withRight(TreeNode right);

    class SimpleNode implements TreeNode {
        private final int val;
        private final TreeNode left;
        private final TreeNode right;

        private SimpleNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        @Override
        public TreeNode withLeft(TreeNode left) {
            return new SimpleNode(val, left, right);
        }

        @Override
        public TreeNode withRight(TreeNode right) {
            return new SimpleNode(val, left, right);
        }

        @Override
        public String toString() {
            return "SimpleNode{" +
                    "\nval=" + val +
                    ",\nleft=" + left +
                    ",\nright=" + right +
                    '}';
        }
    }

    class JsonNode implements TreeNode {
        private final JSONObject json;

        private JsonNode(int val, JSONObject left, JSONObject right) {
            JSONObject json = new JSONObject();
            json.put("val", val);
            if (left != null) {
                Validatec.greaterThan(val, left.getInt("val"));
                json.put("left", left);
            }
            if (right != null) {
                Validatec.greaterThan(right.getInt("val"), val);
                json.put("right", right);
            }
            this.json = json;
        }

        @Override
        public TreeNode withLeft(TreeNode left) {
            return new JsonNode(json.getInt("val"), ((JsonNode) left).json, json.optJSONObject("right"));
        }

        @Override
        public TreeNode withRight(TreeNode right) {
            return new JsonNode(json.getInt("val"), json.optJSONObject("left"), ((JsonNode) right).json);
        }

        @Override
        public String toString() {
            return json.toString(2);
        }
    }
}
