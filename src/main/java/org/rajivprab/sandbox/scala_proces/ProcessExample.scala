package org.rajivprab.sandbox.scala_proces

import scala.sys.process.{Process, ProcessLogger}

object ProcessExample {
  def main(args: Array[String]): Unit = {
    val command : String = "pwd"
    System.out.println(s"Will be running the following command:\n${command}")

    var stdoutAndErr : Seq[String] = Seq.empty
    val processLogger = ProcessLogger.apply(s => stdoutAndErr = stdoutAndErr.appended(s))
    val createClusterResult = Process(command).!(processLogger)

    System.out.println(s"Result is $createClusterResult")
    System.out.println(s"Output from running the above command:\n${stdoutAndErr.mkString("\n")}")
  }
}
