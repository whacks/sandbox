package org.rajivprab.sandbox.file_sort;

import com.google.common.collect.Lists;
import org.rajivprab.cava.Validatec;

import java.io.*;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

/** Given a file that exceeds your heap-size, sort the file
 */
class FileSort {
    private static final int MAX_FILE_SORT_SIZE = 1000 * 1000 * 1000;

    public File getSorted(File input) {
        try {
            List<File> sortedSubFiles = partition(input, MAX_FILE_SORT_SIZE)
                    .stream()
                    .map(this::sortInMemory)
                    .collect(Collectors.toList());
            return getSorted(sortedSubFiles);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Collection<File> partition(File file, int approximateSize) throws IOException {
        Collection<File> output = Lists.newArrayList();
        Scanner input = new Scanner(file);
        while (true) {
            File subFile = writeBytes(input, approximateSize);
            if (subFile == null) {
                return output;
            } else {
                output.add(subFile);
            }
        }
    }

    private File writeBytes(Scanner input, int approximateSize) throws IOException {
        File output = new File("/tmp/" + UUID.randomUUID());

        Writer writer = new FileWriter(output);
        int bytesWritten = 0;

        while (bytesWritten < approximateSize && input.hasNextLine()) {
            String line = input.nextLine();
            bytesWritten += line.length();
            writer.write(line);     // TODO Write end-of-line char?
        }

        return bytesWritten > 0 ? output : null;
    }

    private File sortInMemory(File file) {
        throw new UnsupportedOperationException();
    }

    private File mergeSortedFiles(File one, File two) throws IOException {
        File output = new File("/tmp/" + UUID.randomUUID());

        Scanner oneScanner = new Scanner(one);
        Scanner twoScanner = new Scanner(one);
        Writer writer = new FileWriter(output);

        String oneLine = getNextLineOrNull(oneScanner);
        String twoLine = getNextLineOrNull(twoScanner);

        while (oneLine != null && twoLine != null) {
            if (oneLine.compareTo(twoLine) < 0) {   // TODO Check if "< 0" or "> 0"
                writer.write(oneLine);
                // TODO Write end-of-line char?
                oneLine = getNextLineOrNull(oneScanner);
            } else {
                writer.write(twoLine);
                // TODO Write end-of-line char?
                twoLine = getNextLineOrNull(twoScanner);
            }
        }

        while (oneLine != null) {
            writer.write(oneLine);
            // TODO Write end-of-line char?
            oneLine = getNextLineOrNull(oneScanner);
        }

        while (twoLine != null) {
            writer.write(twoLine);
            // TODO Write end-of-line char?
            twoLine = getNextLineOrNull(twoScanner);
        }

        return output;
    }

    private String getNextLineOrNull(Scanner scanner) {
        return scanner.hasNextLine() ? scanner.nextLine() : null;
    }

    private File getSorted(List<File> sortedFiles) throws IOException {
        Validatec.notEmpty(sortedFiles);
        if (sortedFiles.size() == 1) {
            return sortedFiles.get(0);
        }

        int midIndex = sortedFiles.size() / 2;
        File first = getSorted(sortedFiles.subList(0, midIndex));
        File second = getSorted(sortedFiles.subList(midIndex, sortedFiles.size()));
        return mergeSortedFiles(first, second);
    }
}
