package org.rajivprab.sandbox.number_contest;

/**
 * Player will ask for redraw anytime the initial number is below a certain threshold
 *
 * Created by rajivprab on 11/12/16.
 */
public class ThresholdPlayer implements Player {
    private final double threshold;

    public ThresholdPlayer(double threshold) {
        this.threshold = threshold;
    }

    @Override
    public boolean askIfRedrawDesired(double initial) {
        return initial < threshold;
    }

    @Override
    public String toString() {
        return "ThresholdPlayer{" +
                "threshold=" + threshold +
                '}';
    }
}
