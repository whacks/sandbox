package org.rajivprab.sandbox.number_contest;

import org.javatuples.Pair;

import java.util.Random;
import java.util.stream.IntStream;

/**
 * You are given a random number between 0 and 1.
 * Upon being told your number, you are allowed to request a new random number.
 * If you request a new random number, you are then committed to that new number you're given.
 * If not, you're committed to your first number.
 *
 * Your opponent is going through this exact same process, in another room.
 * There is no way of knowing what's happening in your opponent's room, or what he's doing, or his strategy.
 * Whoever ends up with a higher number, wins.
 *
 * What would your strategy be?
 *
 * Created by rajivprab on 11/12/16.
 */
public class RiggedGame {
    private static final int NUM_ROUNDS = 10000000;
    private static final int MIN_WIN_THRESHOLD = NUM_ROUNDS / 10000;
    private static final Random RNG = new Random();

    public static Player getLongTermWinner(Player a, double scaleA, Player b, double scaleB) {
        int aWins = IntStream.range(0, NUM_ROUNDS).map(i -> getSingleGameWinner(a, scaleA, b, scaleB) ? 1 : 0).sum();
        int bWins = NUM_ROUNDS - aWins;
        if (Math.abs(aWins - bWins) < MIN_WIN_THRESHOLD) {
            System.out.print("T");
            return getLongTermWinner(a, scaleA, b, scaleB);
        }
        System.out.println("Win percentage for A: " + aWins * 1.0 / NUM_ROUNDS);
        return aWins > bWins ? a : b;
    }

    // Returns true if player A beat player B
    public static boolean getSingleGameWinner(Player a, double scaleA, Player b, double scaleB) {
        return getFinalNumber(a, scaleA) > getFinalNumber(b, scaleB);
    }

    public static double getFinalNumber(Player player, double scale) {
        double num = RNG.nextDouble() * scale;
        if (player.askIfRedrawDesired(num)) {
            num = RNG.nextDouble() * scale;
        }
        return num;
    }

    private static void printWinner(Player a, double scaleA, Player b, double scaleB) {
        System.out.println("=========");
        printSingleWinner(a, scaleA, b, scaleB);
        System.out.println("=========");
    }

    private static void printSingleWinner(Player a, double scaleA, Player b, double scaleB) {
        System.out.println("Match between: " + Pair.with(a, b) + ", Winner is " + getLongTermWinner(a, scaleA, b, scaleB) + "!");
    }

    public static void main(String ... args) {
        printWinner(new ThresholdPlayer(0.75), 1.5, new ThresholdPlayer(0.9), 1);
        printWinner(new ThresholdPlayer(0.75), 1.5, new ThresholdPlayer(0.5), 1);
    }
}
