package org.rajivprab.sandbox.number_contest;

import java.util.stream.IntStream;

/**
 * Training ground for player strategy tinkering
 *
 * Created by rajivprab on 11/12/16.
 */
public class TrainingGround {
    private static final int NUM_ROUNDS = 1000000;

    public static double getAverageScore(Player player) {
        return IntStream.range(0, NUM_ROUNDS)
                .parallel()
                .mapToDouble(i -> Game.getFinalNumber(player))
                .average().orElseThrow(IllegalStateException::new);
    }

    public static void main(String... args) {
        printForPlayer(new RandomPlayer());
        printForPlayer(new OptimistPlayer());
        printForPlayer(EVMaximizer.getPlayer());
        printForPlayer(new ThresholdPlayer(0.55));
        printForPlayer(new ThresholdPlayer(0.60));
        printForPlayer(new ThresholdPlayer(0.61));
        printForPlayer(new ThresholdPlayer(0.62));
        printForPlayer(new ThresholdPlayer(0.625));
        printForPlayer(new ThresholdPlayer(0.65));
        printForPlayer(new ThresholdPlayer(0.70));
    }

    private static void printForPlayer(Player player) {
        System.out.println(player + " averages: " + getAverageScore(player));
    }
}
