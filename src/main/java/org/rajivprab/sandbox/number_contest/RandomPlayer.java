package org.rajivprab.sandbox.number_contest;

import java.util.Random;

/**
 * Created by rajivprab on 11/12/16.
 */
public class RandomPlayer implements Player {
    private static final Random RNG = new Random();

    @Override
    public boolean askIfRedrawDesired(double initial) {
        return RNG.nextBoolean();
    }
}
