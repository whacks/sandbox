package org.rajivprab.sandbox.number_contest;

/**
 * Created by rajivprab on 11/12/16.
 */
public interface Player {
    boolean askIfRedrawDesired(double initial);
}
