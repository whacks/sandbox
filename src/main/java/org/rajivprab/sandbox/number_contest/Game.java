package org.rajivprab.sandbox.number_contest;

import org.javatuples.Pair;

import java.util.Random;
import java.util.stream.IntStream;

/**
 * You are given a random number between 0 and 1.
 * Upon being told your number, you are allowed to request a new random number.
 * If you request a new random number, you are then committed to that new number you're given.
 * If not, you're committed to your first number.
 *
 * Your opponent is going through this exact same process, in another room.
 * There is no way of knowing what's happening in your opponent's room, or what he's doing, or his strategy.
 * Whoever ends up with a higher number, wins.
 *
 * What would your strategy be?
 *
 * Created by rajivprab on 11/12/16.
 */
public class Game {
    private static final int NUM_ROUNDS = 10000000;
    private static final int MIN_WIN_THRESHOLD = NUM_ROUNDS / 10000;
    private static final Random RNG = new Random();

    public static Player getLongTermWinner(Player a, Player b) {
        int aWins = IntStream.range(0, NUM_ROUNDS).map(i -> getSingleGameWinner(a, b) ? 1 : 0).sum();
        int bWins = NUM_ROUNDS - aWins;
        if (Math.abs(aWins - bWins) < MIN_WIN_THRESHOLD) {
            System.out.print("T");
            return getLongTermWinner(a, b);
        }
        return aWins > bWins ? a : b;
    }

    // Returns true if player A beat player B
    public static boolean getSingleGameWinner(Player a, Player b) {
        return getFinalNumber(a) > getFinalNumber(b);
    }

    public static double getFinalNumber(Player player) {
        double num = RNG.nextDouble();
        if (player.askIfRedrawDesired(num)) {
            num = RNG.nextDouble();
        }
        return num;
    }

    private static void printWinner(Player a, Player b) {
        System.out.println("=========");
        printSingleWinner(a, b);
        printSingleWinner(a, b);
        printSingleWinner(a, b);
        printSingleWinner(a, b);
        printSingleWinner(a, b);
        System.out.println("=========");
    }

    private static void printSingleWinner(Player a, Player b) {
        System.out.println("Match between: " + Pair.with(a, b) + ", Winner is " + getLongTermWinner(a, b) + "!");
    }

    public static void main(String ... args) {
        printWinner(new RandomPlayer(), EVMaximizer.getPlayer());
        printWinner(new OptimistPlayer(), EVMaximizer.getPlayer());

        Player kingOfTheHill = EVMaximizer.getPlayer();
        System.out.println("King of the hill is now: " + kingOfTheHill);
        printWinner(new ThresholdPlayer(0.60), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.65), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.70), kingOfTheHill);

        kingOfTheHill = new ThresholdPlayer(0.60);
        System.out.println("King of the hill is now: " + kingOfTheHill);
        printWinner(new ThresholdPlayer(0.55), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.58), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.625), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.65), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.70), kingOfTheHill);

        kingOfTheHill = new ThresholdPlayer(0.625);
        System.out.println("King of the hill is now: " + kingOfTheHill);
        printWinner(new ThresholdPlayer(0.61), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.62), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.63), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.64), kingOfTheHill);

        kingOfTheHill = new ThresholdPlayer(0.61);
        System.out.println("King of the hill is now: " + kingOfTheHill);
        printWinner(new ThresholdPlayer(0.60), kingOfTheHill);
        printWinner(new ThresholdPlayer(0.62), kingOfTheHill);
    }
}
