package org.rajivprab.sandbox.sudoku_solver.ver1;

// Exception used to indicate that a grid was filled with an illegal value
// Or indirectly leads to an illegal board state with no solution
class IllegalFillException extends Exception {
	/**
	 * Eclipse auto-generated
	 */
	private static final long serialVersionUID = 7804424003843029028L;
	
	public int[] _idx = new int[2];
	public int[] _val = new int[2];
	
	public IllegalFillException(int idx0, int val0, int idx1, int val1) {
		_idx[0] = idx0;
		_idx[1] = idx1;
		_val[0] = val0;
		_val[1] = val1;
	}
}