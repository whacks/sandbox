package org.rajivprab.sandbox.sudoku_solver.ver1;

import java.io.File;
import java.util.HashMap;
import java.util.Scanner;

// Parses user-input in order to create a sudoku board and solve it
public class UserInput {

	// Run-args: array of file-names
	public static void main(String[] args) {		
		for (String fileName : args) {
			try { parsePuzzleFile(fileName); }
			catch (Exception e) { 
				System.out.println("Illegal input on file: " + fileName); 
			}
		}
	}
	
	// Given a file-name, parses it, solves it, and then prints it to StdOut
	// Throws exception if file is not formatted properly
	// See Readme for full description
	public static void parsePuzzleFile(String fileName) throws Exception {
		System.out.println("===========================");
		System.out.println("Examining file: " + fileName);
		File sudokuPuzzle = new File(fileName);
		Scanner scanner = new Scanner(sudokuPuzzle);

		// Map that stores all starting grid-values
		HashMap<Integer, Integer> fixedVals = new HashMap<Integer, Integer>();

		// Parse every line in the file
		while (scanner.hasNext()) {
			int row = scanner.nextInt();
			int col = scanner.nextInt();
			int val = scanner.nextInt();

			// Check for illegal values (negative or >9)
			if (row<1 || col<1 || val<1) { 
				scanner.close(); 
				throw new IllegalArgumentException(); 
			}
			if (row>Board.MAX_INT || col>Board.MAX_INT || val>Board.MAX_INT) { 
				scanner.close();
				throw new IllegalArgumentException(); 
			}

			// Populate our map of known-grids
			int idx = Board.getIdx(row-1, col-1);
			fixedVals.put(idx, val);
		}

		// Create a board using the starting known-values and print it
		Board myBoard = new Board(fixedVals);
		System.out.println("Starting board:");
		myBoard.printBoard();

		// Solve the above board and print it
		Board solved = myBoard.solve();
		System.out.println("Solved board:");
		if (solved == null) { System.out.println("No solution exists for file: " + fileName); }
		else { solved.printBoard(); }

		scanner.close();
	}
}
