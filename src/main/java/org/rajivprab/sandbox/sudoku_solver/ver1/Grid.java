package org.rajivprab.sandbox.sudoku_solver.ver1;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

// Represents a single grid, which may be empty or filled
public class Grid {
	// Grid's index and pointer to parent board
	public final int _idx;
	private final Board _parent;

	// Confirmed-value for the grid
	private int _confirmedVal;
	
	// All potential values that this grid could take
	private Set<Integer> _potVals;
	
	// All rows/columns/sub-squares that this grid belongs to
	private List<SubBoard> _mySubBoards;

	public int getConfirmedVal() { return _confirmedVal; }
	public Set<Integer> getPotVals() { return _potVals; }
	
	// Construct a new grid, with specified index 
	// and pointer to the board it belongs to
	public Grid(int myIdx, Board parent) {
		_idx = myIdx;
		_parent = parent;
		_potVals = new HashSet<Integer>();
		for (int i=1; i<=Board.MAX_INT; i++) { _potVals.add(i); }
		SubBoard myRow = parent.getRow(myIdx);
		SubBoard myCol = parent.getCol(myIdx);
		SubBoard mySubSq = parent.getSubSq(myIdx);

		// Keep pointers to my subBoards
		_mySubBoards = new LinkedList<SubBoard>();
		_mySubBoards.add(myRow);
		_mySubBoards.add(myCol);
		_mySubBoards.add(mySubSq);

		// List myself on subBoard's directory
		for (SubBoard sub : _mySubBoards) { sub.addGrid(this); }
	}

	// Tells the grid that val has been taken by some other grid in my neighborhood
	// If this creates illegal config, throws user-exception
	// If this forces my value, call updateNeighbors recursively
	public void remVal(int takenVal) throws IllegalFillException {
		assert takenVal > 0;
		if (_confirmedVal == takenVal) {
			throw new IllegalFillException(_idx, _confirmedVal, -1, takenVal);
		}

		boolean changed = _potVals.remove(takenVal);
		// If nothing got changed, can just return
		if (changed == false) { return; }

		_parent.updateGuessChamp(this);
		// If something got changed, need to check for updated values
		int size = _potVals.size();
		assert size > 0;
		if (size == 1) { 
			int fillVal = _potVals.iterator().next();
			fillGrid(fillVal);
		}
	}

	// Fills grid with a confirmed value
	// Notify all my neighbors
	// And notify my Board
	public void fillGrid(int val) throws IllegalFillException {
		// If already filled, nothing to do
		if (_confirmedVal == val) { return; }
		
		// Check if legal
		if (_confirmedVal>0 && _confirmedVal!=val) { throw new IllegalFillException(_idx, val, _idx, _confirmedVal); }
		if (_potVals.contains(val) == false) { throw new IllegalFillException(_idx, val, -1, -1); }
		
		_parent.clearChamp(this);
		_potVals.clear();
		_confirmedVal = val;
		_parent.newFixedVal(_idx, val);
		updateSubBoards();
	}

	// Update my neighbors of my confirmedVal
	// Expect confirmedVal to be set by the time we get here
	public void updateSubBoards() throws IllegalFillException {
		for (SubBoard sub : _mySubBoards) { sub.notifyNeighbors(this); }
	}
}
