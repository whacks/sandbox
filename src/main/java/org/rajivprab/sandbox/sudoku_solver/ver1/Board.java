package org.rajivprab.sandbox.sudoku_solver.ver1;

import java.util.HashMap;
import java.util.Map;

// Represents a snapshot of 9x9 sudoku board
// with some grids filled
public class Board {
	// Range of integer values
	// Also defines number of rows, columns & sub-squares
	public static final int MAX_INT = 9;
	public static final int MAX_INT_SQRT = 3;

	// All MAX_INT*MAX_INT grids that make up the board
	private Grid[] _myGrid;
	
	// Sub0boards representing all rows/columns/sub-squares
	private SubBoard[] _mySubSq;
	private SubBoard[] _myRows;
	private SubBoard[] _myCols;
	
	// Map of all grids and their fixed values
	private Map<Integer, Integer> _fixedVals;

	// Used to track which grid would be best to gues son
	private int _champIdx;
	private int _champSize;

	// Add new fixed value to a previously empty grid
	public void newFixedVal(int idx, int val) {
		assert val > 0;
		assert val <= MAX_INT;
		assert idx >= 0;
		assert idx < MAX_INT*MAX_INT;
		assert !_fixedVals.containsKey(idx);
		
		_fixedVals.put(idx, val);
	}
	
	// Unit tests
	public static void main(String[] args) {
		// Try a empty Board
		Map<Integer, Integer> fixedVals = new HashMap<Integer, Integer>();
		Board emptyBoard = new Board(fixedVals);

		// Try illegal configs
		// Same number in same row
		fixedVals.put(13, 3);
		fixedVals.put(14, 3);
		try {
			Board illegalBoard = new Board(fixedVals);
			illegalBoard.printBoard();
			assert false;
		}
		catch (IllegalArgumentException e){ }

		// Same number in same col
		fixedVals.clear();
		fixedVals.put(23, 5);
		fixedVals.put(32, 5);
		try {
			Board illegalBoard = new Board(fixedVals);
			illegalBoard.printBoard();
			assert false;
		}
		catch (IllegalArgumentException e) { }

		Board solvedBoard = emptyBoard.solve();
		solvedBoard.printBoard();
	}

	// Construct a new board, using a map of all fixed value grids
	public Board(Map<Integer, Integer> fixedVals) {
		if (fixedVals == null) { throw new IllegalArgumentException(); }

		_champIdx = -1;
		_champSize = 10;
		_mySubSq = new SubBoard[MAX_INT];
		_myRows = new SubBoard[MAX_INT];
		_myCols = new SubBoard[MAX_INT];
		_myGrid = new Grid[MAX_INT*MAX_INT];

		for (int i=0; i<MAX_INT; i++) { _mySubSq[i] = new SubBoard(); }
		for (int i=0; i<MAX_INT; i++) { _myCols[i] = new SubBoard(); }
		for (int i=0; i<MAX_INT; i++) { _myRows[i] = new SubBoard(); }
		for (int i=0; i<MAX_INT*MAX_INT; i++) { _myGrid[i] = new Grid(i, this); }

		_fixedVals = new HashMap<Integer, Integer>();
		
		// Fill each fixed-value into grid incrementally
		// If constructor-provided values are illegal, throw exception
		for (int idx : fixedVals.keySet()) {
			int val = fixedVals.get(idx);
			if (idx >= MAX_INT*MAX_INT || idx < 0) { throw new IllegalArgumentException(); }
			if (val > MAX_INT || val < 1) { throw new IllegalArgumentException(); }
			try { _myGrid[idx].fillGrid(val); }
			catch (IllegalFillException e) { 
				// printBoard();
				throw new IllegalArgumentException();
			}
		}
		
		assert verifyLegal();
	}

	// Prints all rows & columns of the board
	// in aligned & formatted manner
	public void printBoard() {
		for (int row=0; row<MAX_INT; row++) {
			System.out.printf("\n");
			for (int col=0; col<MAX_INT; col++) {
				int idx = getIdx(row, col);
				Grid cur = _myGrid[idx];

				String val;
				if (cur.getConfirmedVal() == 0) { val = " _"; }
				else { val = " " + cur.getConfirmedVal(); }
				System.out.printf(val);
			}
		}
		System.out.printf("\n");
	}

	// Recursively creates copies of itself, and solves the board
	// Finally returns a pointer to the solved board
	public Board solve() {
		// If entire board is populated, return result
		if (_fixedVals.size() == MAX_INT*MAX_INT) { 
			assert verifyLegalSolved();
			return this; 
		}

		// Else, pick a grid to guess on
		// For best performance, pick grid with smallest set of legal values
		int guessGridIdx = getGuessChamp();
		Grid guessGrid = _myGrid[guessGridIdx];
		
		// Iterate through all values that grid could take
		for (int guessVal : guessGrid.getPotVals()) {
			Board newBoard = new Board(_fixedVals);
			Grid newGuessGrid = newBoard._myGrid[guessGridIdx];
			// Try guess to see if it works
			try { 
				newGuessGrid.fillGrid(guessVal);
				assert newBoard.verifyLegal();
				Board result = newBoard.solve();
				if (result != null) { return result; }
			}
			// If we enter catch, guess is not working
			// Abandon it and move to next one
			catch (IllegalFillException excp) { }
		}

		// No Solution exists
		return null;
	}

	// Iterates through all sub-boards
	// And makes sure they are all legal
	private boolean verifyLegal() {
		for (SubBoard sub : _myRows) { 
			boolean success = sub.verifyLegal();
			if (success == false) { return false; }
		}
		for (SubBoard sub : _myCols) { 
			boolean success = sub.verifyLegal();
			if (success == false) { return false; }
		}
		for (SubBoard sub : _mySubSq) { 
			boolean success = sub.verifyLegal();
			if (success == false) { return false; }
		}
		return true;
	}

	// Iterates through all sub-boards
	// And makes sure they are all legal & filled
	// This implies verifyLegal() as well
	private boolean verifyLegalSolved() {
		for (SubBoard sub : _myRows) { 
			boolean success = sub.verifyLegalSolved();
			if (success == false) { return false; }
		}
		for (SubBoard sub : _myCols) { 
			boolean success = sub.verifyLegalSolved();
			if (success == false) { return false; }
		}
		for (SubBoard sub : _mySubSq) { 
			boolean success = sub.verifyLegalSolved();
			if (success == false) { return false; }
		}
		return true;
	}

	// Returns grid-idx with smallest number of potential values
	// But greater than 1
	// This represents the ideal grid to guess a new value with
	private int getGuessChamp() {
		assert _champIdx < MAX_INT*MAX_INT;

		// If champ already exists, return it
		if (_champIdx != -1) {
			Grid champ = _myGrid[_champIdx];
			if (champ.getPotVals().size() > 1) { return _champIdx; }
		}

		// Else, pick first unfilledGrid
		for (int i=0; i<MAX_INT*MAX_INT; i++) {
			if (_myGrid[i].getConfirmedVal() <= 0) { 
				assert _myGrid[i].getPotVals().size() > 1; 
				return i; 
			}  
		}
		assert _fixedVals.size() == 81;
		assert false;
		return -1;
	}

	// If filledGrid is the champ, clear it
	public void clearChamp(Grid filled) {
		if (filled._idx != _champIdx) { return; }
		else {
			_champIdx = -1;
			_champSize = 10;
		}
	}
	
	// Compares grid-upd with the current champion
	// If upd has fewer potential values, it is the new champion
	// If upd is already the champion, and is now filled, set it as null
	public void updateGuessChamp(Grid upd) {
		int size = upd.getPotVals().size();
		if (upd._idx == _champIdx) {
			// Check if guessChamp is now filled
			// champ._confirmedVal will only be populated later, in fillGrid
			// So use size instead
			if (size <= 1) { 
				_champIdx = -1;
				_champSize = 10;
			}
			else { return; }
		}
		else {
			if (size>1 && size<_champSize) {
				_champSize = size;
				_champIdx = upd._idx;
			}
		}
	}

	// Tells you which idx is assigned to grid[row, col] 
	public static int getIdx(int row, int col) { return row*MAX_INT + col; }
	
	// Returns pointer to sub-board where grid-idx resides
	public SubBoard getRow(int idx) { return _myCols[idx / Board.MAX_INT]; }
	public SubBoard getCol(int idx) { return _myRows[idx % Board.MAX_INT]; }
	public SubBoard getSubSq(int idx) { 
		assert idx >= 0;
		assert idx < Board.MAX_INT * Board.MAX_INT;
		int row = idx / Board.MAX_INT;
		int col = idx % Board.MAX_INT;
		int bigRow = row / Board.MAX_INT_SQRT;
		int bigCol = col / Board.MAX_INT_SQRT;
		return _mySubSq[bigRow*Board.MAX_INT_SQRT + bigCol];
	}
}
