package org.rajivprab.sandbox.sudoku_solver.ver1;

import java.util.HashSet;
import java.util.Set;

// Represents a set of 9 grids
// Along the same row/column/sub-square
class SubBoard {
	// Set of empty grids
	private Set<Grid> _emptyGrids;
	
	// Array of grids filled with values
	// To get grid with value-i, look at filledGrids[i]
	// filledGrids[0] should always be empty
	private Grid[] _filledGrids;

	// Construct a new empty sub-board
	public SubBoard() {
		// myGrids = new ArrayList<Grid>();
		_emptyGrids = new HashSet<Grid>();
		_filledGrids = new Grid[Board.MAX_INT+1];
	}

	// Registers grid in set
	public void addGrid(Grid cur) {
		assert _emptyGrids.size() < 9;
		assert cur.getConfirmedVal() == 0;
		_emptyGrids.add(cur);
	}

	// First populate grid's confirmedVal, then notify neighbors of new value
	public void notifyNeighbors(Grid cur) throws IllegalFillException {
		int val = cur.getConfirmedVal();
		if (_filledGrids[val] != null) { 
			Grid conflict = _filledGrids[val];
			throw new IllegalFillException(cur._idx, val, conflict._idx, conflict.getConfirmedVal()); 
		}
		assert _emptyGrids.contains(cur);
		assert val>0 && val<=Board.MAX_INT;

		// Update my directory
		_filledGrids[val] = cur;
		_emptyGrids.remove(cur);

		// Notify my grids about value that is taken
		boolean done = false;
		while (!done) {
			try { 
				for (Grid neighbor : _emptyGrids) { neighbor.remVal(val); }
				done = true;
			}
			catch (java.util.ConcurrentModificationException e) { }
		}
	}

	// Verify that current state is fully legal
	// according to sudoku roles
	public boolean verifyLegal() {
		// if (_emptyGrids.isEmpty() == false) { return false; }
		for (int i=1; i<=Board.MAX_INT; i++) {
			Grid cur = _filledGrids[i];
			if (cur!=null && cur.getConfirmedVal()!=i) { return false; }
		}
		return true;
	}

	// Verify that this sub-board is fully solved, 
	// with all grids having unique values
	// This also implies verifyLegal() is true
	public boolean verifyLegalSolved() {
		if (_emptyGrids.isEmpty() == false) { return false; }
		for (int i=1; i<=Board.MAX_INT; i++) {
			Grid cur = _filledGrids[i];
			if (cur.getConfirmedVal()!=i) { return false; }
		}
		assert verifyLegal();
		return true;
	}
}
