package org.rajivprab.sandbox.sudoku_solver.ver2;

import com.google.common.collect.Lists;
import com.google.common.math.IntMath;
import org.javatuples.Pair;
import org.rajivprab.cava.Validatec;
import org.rajivprab.sandbox.sudoku_solver.ver2.Tile.IllegalValueException;

import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;

/**
 * Board consists of tiles and groups
 * 9*9 tiles
 * 9 + 9 + 9 groups
 * <p>
 * Note that the board object is mutable, and calling setValue mutates it
 * <p>
 * Each tile contains, and has pointers to, 3 groups
 * current value
 * set of possible values it can take (if unfilled)
 * <p>
 * Each group contains, and has pointers to, 9 tiles
 * set of unfilled values
 * <p>
 * Deep-Copying a board -
 * 1. copy over all tiles, their values, and their potential values
 * 2. copy over all groups, and their unfilled values
 * 3. link the newly created tiles to newly created groups, and vice versa
 * 4. No further computation needed, since we have now done an exact copy
 * <p>
 * New computations/derivations are only needed when calling tile.fillValue()
 * <p>
 * Created by rajivprab on 8/10/16.
 */
class Board {
    private static final int WIDTH = Value.values().length;
    private static final int WIDTH_SQRT = IntMath.sqrt(WIDTH, RoundingMode.UNNECESSARY);

    private final Tile[][] tiles = new Tile[WIDTH][WIDTH];  // Index using [row][col]
    private final Group[][] groups = new Group[3][WIDTH];

    Board() {
        for (int row = 0; row < WIDTH; row++) {
            for (int col = 0; col < WIDTH; col++) {
                tiles[row][col] = new Tile(row, col);
            }
        }
        for (int i = 0; i < WIDTH; i++) {
            groups[0][i] = new Group();
            groups[1][i] = new Group();
            groups[2][i] = new Group();
        }
        linkGroupsAndTiles();
    }

    private Board(Board copy) {
        for (int row = 0; row < WIDTH; row++) {
            for (int col = 0; col < WIDTH; col++) {
                tiles[row][col] = new Tile(copy.tiles[row][col]);
            }
        }
        for (int i = 0; i < WIDTH; i++) {
            groups[0][i] = new Group(copy.groups[0][i]);
            groups[1][i] = new Group(copy.groups[1][i]);
            groups[2][i] = new Group(copy.groups[2][i]);
        }
        linkGroupsAndTiles();
    }

    boolean boardIsSolved() {
        Pair<Integer, Integer> minimalTile = getMinimalTileIndex();
        return minimalTile == null;
    }

    Collection<Board> getChildBoards() {
        Pair<Integer, Integer> minimalTile = getMinimalTileIndex();
        Validatec.notNull(minimalTile, "Cannot find any tile that is unset");
        // TODO Enhancement: Keep a set of seen boards, to avoid repeating computations
        List<Board> children = Lists.newArrayList();
        for (Value value : tiles[minimalTile.getValue0()][minimalTile.getValue1()].getPossibleValues()) {
            try {
                Board child = new Board(this).setValue(minimalTile, value);
                children.add(child);
            } catch (IllegalValueException ignored) {}
        }
        return children;
    }

    // Set value for tile Pair.with(row, col), with row/col starting with 0
    Board setValue(Pair<Integer, Integer> index, Value value) {
        Validatec.notNull(index, IllegalArgumentException.class, "Null index given");
        Validatec.notNull(value, IllegalArgumentException.class, "Null value given");
        tiles[index.getValue0()][index.getValue1()].setValue(value);
        return this;
    }

    // Use row/col starting with 0
    Value getValue(int row, int col) {
        return tiles[row][col].getFilledValue();
    }

    // ---------

    private void linkGroupsAndTiles() {
        for (int column = 0; column < WIDTH; column++) {
            for (int row = 0; row < WIDTH; row++) {
                Tile tile = tiles[row][column];
                int miniCol = column / WIDTH_SQRT;
                int miniRow = row / WIDTH_SQRT;
                Group mySquareGroup = groups[2][miniRow * WIDTH_SQRT + miniCol].withTile(tile);
                Group myVerticalGroup = groups[0][column].withTile(tile);
                Group myHorizontalGroup = groups[1][row].withTile(tile);
                tile.withGroup(myHorizontalGroup, mySquareGroup, myVerticalGroup);
            }
        }
    }

    private Pair<Integer, Integer> getMinimalTileIndex() {
        Pair<Integer, Integer> minimalTile = null;
        int minSize = 100;
        for (int column = 0; column < WIDTH; column++) {
            for (int row = 0; row < WIDTH; row++) {
                int size = tiles[column][row].getPossibleValues().size();
                Validatec.isTrue(size != 1);
                if (size == 2) {
                    return Pair.with(column, row);
                }
                if (size > 0 && size < minSize) {
                    minimalTile = Pair.with(column, row);
                    minSize = size;
                }
            }
        }
        return minimalTile;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int row = 0; row < WIDTH; row++) {
            for (int col = 0; col < WIDTH; col++) {
                builder.append(tiles[row][col]).append(" ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

}
