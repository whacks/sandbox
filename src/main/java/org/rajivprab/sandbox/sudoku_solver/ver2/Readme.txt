Solves a sudokuPuzzle, printing out the starting state, and the final solved state
If puzzle cannot be solved, program will display a message saying so

To run the solver, generate a txt file containing the starting state of the sudoku board
And then, from the same dir containing the file, run UserInput.parsePuzzleFile(String fileName)

File format expected: simple text file containing integers in the following format:
every 3 consecutive integers describes one known grid-value in the starting state
format to use: <row> <column> <value>
All values above should range from 1-9
If this format is not followed, method will throw an exception

For examples: see the resources/sudoku folder, or the unit tests