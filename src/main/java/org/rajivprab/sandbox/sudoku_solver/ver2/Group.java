package org.rajivprab.sandbox.sudoku_solver.ver2;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.javatuples.Pair;
import org.rajivprab.cava.Validatec;
import org.rajivprab.sandbox.sudoku_solver.ver2.Tile.IllegalValueException;

import java.util.Collection;
import java.util.Set;

/**
 * Created by rajivprab on 8/10/16.
 */
public class Group {
    private Set<Value> unfilledValues;

    private Collection<Tile> tiles = Lists.newArrayList();

    public Group() {
        this(Sets.newHashSet(Value.values()));
    }

    public Group(Group copy) {
        this(copy.getUnfilledValues());
    }

    private Group(Set<Value> unfilledValues) {
        this.unfilledValues = unfilledValues;
    }

    public Group withTile(Tile tile) {
        tiles.add(tile);
        return this;
    }

    public Set<Value> getUnfilledValues() {
        return Sets.newHashSet(unfilledValues);
    }

    public Group fillValue(Value value) {
        Validatec.isTrue(unfilledValues.remove(value), IllegalValueException.class,
                         "Trying to fill a value that's already taken: " + Pair.with(value, unfilledValues));
        tiles.forEach(tile -> tile.updatePossibleValues(getUnfilledValues()));
        return this;
    }
}
