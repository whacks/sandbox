package org.rajivprab.sandbox.sudoku_solver.ver2;

/**
 * Created by rajivprab on 8/10/16.
 */
public class Sudoku {
    public static Board solve(Board board) {
        if (board.boardIsSolved()) {
            return board;
        }
        for (Board child : board.getChildBoards()) {
            Board solved = solve(child);
            if (solved != null) { return solved; }
        }
        return null;
    }
}
