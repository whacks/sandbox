package org.rajivprab.sandbox.sudoku_solver.ver2;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;

/**
 * An enum representing all the possible values that can fill a single cell
 *
 * Created by rajivprab on 9/18/16.
 */
enum Value {
    ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE;

    private static final BiMap<Value, String> VALUES_TO_DISPLAY = ImmutableBiMap.<Value, String>builder()
            .put(ONE, "1")
            .put(TWO, "2")
            .put(THREE, "3")
            .put(FOUR, "4")
            .put(FIVE, "5")
            .put(SIX, "6")
            .put(SEVEN, "7")
            .put(EIGHT, "8")
            .put(NINE, "9")
            .build();

    public static Value getValue(String display) {
        return VALUES_TO_DISPLAY.inverse().get(display);
    }

    public String getDisplay() {
        return VALUES_TO_DISPLAY.get(this);
    }
}
