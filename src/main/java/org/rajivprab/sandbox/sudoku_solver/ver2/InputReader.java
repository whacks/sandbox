package org.rajivprab.sandbox.sudoku_solver.ver2;

import org.javatuples.Pair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Utility class that reads an input file and generates a board
 *
 * Created by rajivprab on 9/18/16.
 */
public class InputReader {
    public static Board genBoard(File file) {
        try {
            return genBoard(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Illegal file-input provided", e);
        }
    }

    public static Board genBoard(InputStream stream) {
        Board board = new Board();
        try {
            Scanner scanner = new Scanner(stream);
            while (scanner.hasNextLine()) {
                int row = scanner.nextInt() - 1;
                int col = scanner.nextInt() - 1;
                String val = scanner.next();
                board.setValue(Pair.with(row, col), Value.getValue(val));
            }
        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("Illegal input provided", e);
        }
        return board;
    }
}
