package org.rajivprab.sandbox.sudoku_solver.ver2;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.rajivprab.cava.Validatec;

import java.util.Collection;
import java.util.Set;

/**
 * Created by rajivprab on 8/10/16.
 */
public class Tile {
    private final int row, col;

    private Set<Value> possibleValues;
    private Value filledValue;

    private Collection<Group> groups;

    public Tile(int row, int col) {
        this(null, Sets.newHashSet(Value.values()), row, col);
    }

    public Tile(Tile copy) {
        this(copy.filledValue, copy.getPossibleValues(), copy.row, copy.col);
    }

    private Tile(Value value, Set<Value> possibleValues, int row, int col) {
        this.filledValue = value;
        this.possibleValues = possibleValues;
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return filledValue == null ? "0" : filledValue.getDisplay();
    }

    public Tile setValue(Value value) {
        Validatec.isNull(filledValue);
        Validatec.contains(possibleValues, value);
        possibleValues = Sets.newHashSet();     // TODO why does this fail if I do .clear() instead
        filledValue = value;
        return updateGroups();
    }

    private Tile updateGroups() {
        Validatec.notNull(filledValue);
        groups.forEach(group -> group.fillValue(filledValue));
        return this;
    }

    public Tile withGroup(Group group1, Group group2, Group group3) {
        groups = ImmutableList.of(group1, group2, group3);
        return this;
    }

    // Returns empty-collection if tile is already filled
    public Set<Value> getPossibleValues() {
        return Sets.newHashSet(possibleValues);
    }

    public Value getFilledValue() {
        return filledValue;
    }

    public Tile updatePossibleValues(Set<Value> values) {
        possibleValues = Sets.intersection(getPossibleValues(), values);
        Validatec.isTrue(filledValue != null || !possibleValues.isEmpty(),
                         IllegalValueException.class, "No possible values exist");
        if (possibleValues.size() == 1) {
            Value value = possibleValues.iterator().next();
            setValue(value);
        }
        return this;
    }

    public static class IllegalValueException extends RuntimeException {
        public IllegalValueException(String message) {
            super(message);
        }
    }
}
