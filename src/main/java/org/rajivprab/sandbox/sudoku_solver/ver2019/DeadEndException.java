package org.rajivprab.sandbox.sudoku_solver.ver2019;

public class DeadEndException extends RuntimeException {
    public DeadEndException(String message) {
        super(message);
    }
}
