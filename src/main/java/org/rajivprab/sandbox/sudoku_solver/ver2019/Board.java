package org.rajivprab.sandbox.sudoku_solver.ver2019;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.rajivprab.cava.Validatec;

import java.util.Set;

public class Board {
    private final Table<Integer, Integer, Cell> cells = HashBasedTable.create();

    public static Board blank() {
        Cell empty = Cell.blank();  // Reuse same cell since it is being copied in constructor
        Table<Integer, Integer, Cell> cells = HashBasedTable.create();
        for (int rowNum = 1; rowNum <= 9; rowNum++) {
            for (int colNum = 1; colNum <= 9; colNum++) {
                cells.put(rowNum, colNum, empty);
            }
        }
        return new Board(cells);
    }

    private Board(Table<Integer, Integer, Cell> cells) {
        for (Table.Cell<Integer, Integer, Cell> cell : cells.cellSet()) {
            this.cells.put(cell.getRowKey(), cell.getColumnKey(), cell.getValue().copy());
        }
    }

    public Board copy() {
        return new Board(cells);
    }

    public Cell read(int row, int col) {
        validateIndex(row, col);
        return cells.get(row, col);
    }

    public void fill(int row, int col, int value) {
        validateIndex(row, col);
        read(row, col).fill(value);
        for (Cell cell : getNeighbors(row, col)) {
            cell.strikeOut(value);
        }
    }

    public Set<Cell> getNeighbors(final int row, final int col) {
        validateIndex(row, col);
        Set<Cell> neighbors = Sets.newHashSet();

        for (int newRow=1; newRow<=9; newRow++) {
            neighbors.add(read(newRow, col));
        }

        for (int newCol=1; newCol<=9; newCol++) {
            neighbors.add(read(row, newCol));
        }

        int squareMiddleRow = ((row-1) / 3) * 3 + 2;
        int squareMiddleCol = ((col-1) / 3) * 3 + 2;
        for (int newRow=squareMiddleRow-1; newRow<=squareMiddleRow+1; newRow++) {
            for (int newCol=squareMiddleCol-1; newCol<=squareMiddleCol+1; newCol++) {
                neighbors.add(read(newRow, newCol));
            }
        }

        neighbors.remove(read(row, col));
        Validatec.size(neighbors, 20);
        return neighbors;
    }

    private static void validateIndex(int row, int col) {
        Validatec.greaterOrEqual(row, 1);
        Validatec.greaterOrEqual(col, 1);
        Validatec.greaterOrEqual(9, row);
        Validatec.greaterOrEqual(9, col);
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder().append("---------------------------");
        for (int row=1; row<=9; row++) {
            string.append("\n");
            for (int col=1; col<=9; col++) {
                string.append(cells.get(row, col)).append(",");
            }
        }
        string.append("\n---------------------------");
        return string.toString();
    }
}
