package org.rajivprab.sandbox.sudoku_solver.ver2019;

public class Sudoku {
    public static Board solve(Board board) {
        int guessSize = 100;
        int guessRow = -1;
        int guessCol = -1;
        for (int row=1; row<=9; row++) {
            for (int col=1; col<=9; col++) {
                int numPotentialValues = board.read(row, col).getPotentialValues().size();
                if ((numPotentialValues > 0) && (numPotentialValues < guessSize)) {
                    guessRow = row;
                    guessCol = col;
                    guessSize = numPotentialValues;
                }
            }
        }

        if (guessRow == -1) {
            System.out.println("Board is solved\n" + board);
            return board;   // Board is fully solved
        }

        for (int guessValue : board.read(guessRow, guessCol).getPotentialValues()) {
            Board guessBoard = board.copy();    // Make a new board before guessing, so it can be discarded if needed
            try {
                guessBoard.fill(guessRow, guessCol, guessValue);
                return solve(guessBoard);
            } catch (DeadEndException ignored) {}
        }

        throw new DeadEndException("Board cannot be solved");
    }
}
