package org.rajivprab.sandbox.sudoku_solver.ver2019;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.rajivprab.cava.Validatec;

import java.util.Optional;
import java.util.Set;

public class Cell {
    private static final Set<Integer> ALL_VALUES = ImmutableSet.of(1, 2, 3, 4, 5, 6, 7, 8, 9);

    private Integer value;
    private final Set<Integer> potentialValues;

    public static Cell blank() {
        return new Cell(null, ALL_VALUES);
    }

    private Cell(Integer value, Set<Integer> potentialValues) {
        this.value = value;
        this.potentialValues = Sets.newHashSet(potentialValues);
    }

    public void fill(int value) {
        Validatec.contains(potentialValues, value);
        this.value = value;
        potentialValues.clear();
    }

    public Optional<Integer> read() {
        return Optional.ofNullable(value);
    }

    public void strikeOut(int value) {
        if (this.value == null) {
            potentialValues.remove(value);
            Validatec.notEmpty(potentialValues, DeadEndException.class);
        } else {
            Validatec.notEquals(value, this.value);
        }
    }

    public Set<Integer> getPotentialValues() {
        return ImmutableSet.copyOf(potentialValues);
    }

    public Cell copy() {
        return new Cell(value, potentialValues);
    }

    @Override
    public String toString() {
        return read().map(num -> num + "").orElse(" ");
    }
}
