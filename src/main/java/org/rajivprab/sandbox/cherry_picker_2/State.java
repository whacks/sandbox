package org.rajivprab.sandbox.cherry_picker_2;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.Objects;

class State {
    private static final List<Integer> COL_DELTA = ImmutableList.of(-1, 0, 1);

    private final int row;
    private final int robot1Col;
    private final int robot2Col;

    State(int row, int robot1Col, int robot2Col) {
        this.row = row;
        this.robot1Col = robot1Col;
        this.robot2Col = robot2Col;
    }

    Map<State, Integer> getNextStateAndIncrementalScore(List<List<Integer>> grid) {
        Map<State, Integer> nextStates = Maps.newHashMap();
        int numRows = grid.size();
        int numCols = grid.get(0).size();   // Assuming rectangular grid. TODO Add assertion
        int nextRow = row + 1;
        if (nextRow >= numRows) { return nextStates; }
        for (int robot1Delta : COL_DELTA) {
            for (int robot2Delta : COL_DELTA) {
                int nextCol1 = robot1Col + robot1Delta;
                int nextCol2 = robot2Col + robot2Delta;
                if (nextCol1 >= 0 && nextCol1 < numCols && nextCol2 >= 0 && nextCol2 < numCols) {
                    int score = grid.get(nextRow).get(nextCol1) + grid.get(nextRow).get(nextCol2);
                    if (nextCol1 == nextCol2) {
                        // TODO Can we ignore this early-terminate this state? Or can it lead to a max outcome?
                        score = score - grid.get(nextRow).get(nextCol2);    // Avoid double-counting
                    }
                    nextStates.put(new State(nextRow, nextCol1, nextCol2), score);
                }
            }
        }
        return nextStates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return row == state.row &&
                robot1Col == state.robot1Col &&
                robot2Col == state.robot2Col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, robot1Col, robot2Col);
    }

    @Override
    public String toString() {
        return "State{" +
                "row=" + row +
                ", robot1Col=" + robot1Col +
                ", robot2Col=" + robot2Col +
                '}';
    }
}
