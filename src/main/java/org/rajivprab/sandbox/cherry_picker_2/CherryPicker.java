package org.rajivprab.sandbox.cherry_picker_2;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

/**
 * Given a rows x cols matrix grid representing a field of cherries. Each cell in grid represents the number of
 * cherries that you can collect.
 *
 * You have two robots that can collect cherries for you, Robot #1 is located at the top-left corner (0,0) , and
 * Robot #2 is located at the top-right corner (0, cols-1) of the grid.
 *
 * Return the maximum number of cherries collection using both robots  by following the rules below:
 *
 * From a cell (i,j), robots can move to cell (i+1, j-1) , (i+1, j) or (i+1, j+1).
 * When any robot is passing through a cell, It picks it up all cherries, and the cell becomes an empty cell (0).
 * When both robots stay on the same cell, only one of them takes the cherries.
 * Both robots cannot move outside of the grid at any moment.
 * Both robots should reach the bottom row in the grid.
 *
 * https://leetcode.com/problems/cherry-pickup-ii/
 */
public class CherryPicker {
    private static final Logger log = LogManager.getLogger(CherryPicker.class);

    // Table[row, col, numCherries]
    public static int getMaxScore(List<List<Integer>> grid) {
        Map<State, Integer> statesSeen = Maps.newHashMap();   // Only contains highest score for that state
        int highestScoreSeen = Integer.MIN_VALUE;

        Queue<State> statesToConsider = Lists.newLinkedList();
        State startingState = new State(0, 0, getNumCols(grid) - 1);
        int startingScore = grid.get(0).get(0) + grid.get(0).get(getNumCols(grid) - 1);
        statesSeen.put(startingState, startingScore);
        statesToConsider.add(startingState);
        highestScoreSeen = startingScore;

        while (!statesToConsider.isEmpty()) {
            State cur = statesToConsider.poll();
            int score = statesSeen.get(cur);

            for (Entry<State, Integer> candidate : cur.getNextStateAndIncrementalScore(grid).entrySet()) {
                State nextState = candidate.getKey();
                int nextScore = score + candidate.getValue();

                if (!statesSeen.containsKey(nextState)) {
                    statesSeen.put(nextState, nextScore);
                    statesToConsider.add(nextState);
                }

                if (statesSeen.get(nextState) < nextScore) {
                    statesSeen.put(nextState, nextScore);
                    // Do not add to statesToConsider => it should already in queue, since we are using BFS, not DFS
                }

                if (nextScore > highestScoreSeen) {
                    highestScoreSeen = nextScore;
                }
            }
        }

        log.info("All states considered:\n" + Joiner.on("\n").join(statesSeen.entrySet()));
        return highestScoreSeen;
    }

    static int getNumCols(List<List<Integer>> grid) {
        return grid.get(0).size();
    }
}
