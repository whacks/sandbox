package org.rajivprab.sandbox.cuboid_stacking;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.rajivprab.cava.Validatec;

import java.util.*;

class Cuboid {
    private final int length;
    private final int width;
    final int height;

    static Cuboid getOrientedCuboid(List<Integer> dimensions) {
        dimensions = Lists.newArrayList(dimensions);
        Collections.sort(dimensions);

        Validatec.size(dimensions, 3);
        Validatec.greaterOrEqual(dimensions.get(2), dimensions.get(1));
        Validatec.greaterOrEqual(dimensions.get(1), dimensions.get(0));

        return new Cuboid(dimensions.get(0), dimensions.get(1), dimensions.get(2));
    }

    private Cuboid(int length, int width, int height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

    boolean canStackOnTopOf(Cuboid base) {
        return length <= base.length
                && width <= base.width
                && height <= base.height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuboid cuboid = (Cuboid) o;
        return length == cuboid.length && width == cuboid.width && height == cuboid.height;
    }

    @Override
    public int hashCode() {
        return Objects.hash(length, width, height);
    }

    @Override
    public String toString() {
        return String.format("%d x %d x %d", width, length, height);
    }
}
