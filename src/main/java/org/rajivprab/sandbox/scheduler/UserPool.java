package org.rajivprab.sandbox.scheduler;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.UUID;

/**
 * Get User based on token or identifier
 *
 * Created by rprabhakar on 5/16/16.
 */
public class UserPool {
    private final Map<UUID, User> users = Maps.newConcurrentMap();

    public User getUser(String credentialsToken) {
        // TODO Need actual authentication/encryption scheme, instead of using UUID directly as token
        return getUser(UUID.fromString(credentialsToken));
    }

    public User getUser(UUID userID) {
        if (!users.containsKey(userID)) {
            users.put(userID, new User(userID, 1.7, userID + "@jpmorgan.com"));
        }
        return users.get(userID);
    }

    public void registerUser(User user) {
        users.put(user.getUserID(), user);
    }
}
