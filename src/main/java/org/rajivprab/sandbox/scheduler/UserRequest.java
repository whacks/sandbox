package org.rajivprab.sandbox.scheduler;

/**
 * CPU request.
 * Mem request.
 * Timeout.
 * Command-line.
 *
 * Created by rprabhakar on 5/16/16.
 */
public class UserRequest {
    private final double cpuRequest;
    private final double memRequest;
    private final long timeoutSeconds;
    private final String commandLine;

    public UserRequest(double cpuRequest, double memRequest, long timeoutSeconds, String commandLine) {
        this.cpuRequest = cpuRequest;
        this.memRequest = memRequest;
        this.timeoutSeconds = timeoutSeconds;
        this.commandLine = commandLine;
    }

    public double getCpuRequest() {
        return cpuRequest;
    }

    public double getMemRequest() {
        return memRequest;
    }

    public long getTimeoutSeconds() {
        return timeoutSeconds;
    }

    public String getCommandLine() {
        return commandLine;
    }
}
