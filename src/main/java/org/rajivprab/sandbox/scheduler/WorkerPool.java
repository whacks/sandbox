package org.rajivprab.sandbox.scheduler;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.ThreadUtilc;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Assign request.
 * Register worker.
 * Get optimal worker for task
 *
 * Bonus: Get request status
 *
 * Created by rprabhakar on 5/16/16.
 */
public class WorkerPool {
    private static final Logger log = LogManager.getLogger(WorkerPool.class);

    private static final Duration REQ_POLLING_FREQUENCY = Duration.ofSeconds(1);
    private static final Comparator<Worker> MEM_COMPARATOR = new WorkerMemComparator();
    private static final Comparator<Worker> CPU_COMPARATOR = new WorkerCpuComparator();

    private final Map<UUID, Worker> dispatchedJobs = Maps.newConcurrentMap();

    private Scheduler scheduler;
    private Queue<Worker> workersByMemLoad = new PriorityBlockingQueue<>(1, MEM_COMPARATOR);
    private Queue<Worker> workersByCpuLoad = new PriorityBlockingQueue<>(1, CPU_COMPARATOR);

    public WorkerPool() {
        new Thread(() -> pollPendingJobsForever(REQ_POLLING_FREQUENCY)).start();
    }

    // TODO Circular dependency between Scheduler/WorkerPool is ugly. Find a way to break it
    public WorkerPool withScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
        return this;
    }

    public WorkerPool withWorker(Worker worker) {
        this.workersByMemLoad.add(worker);
        this.workersByCpuLoad.add(worker);
        return this;
    }

    public void assignRequest(InternalRequest request) {
        Worker bestCpuWorker = workersByCpuLoad.poll();
        Worker bestMemWorker = workersByMemLoad.poll();

        double cpuConstraint = bestCpuWorker.getCpuLoad() * request.getCpuAllocated();
        double memConstraint = bestMemWorker.getMemLoad() * request.getMemAllocated();

        assignRequest(request, cpuConstraint > memConstraint ? bestCpuWorker : bestMemWorker);
        workersByCpuLoad.add(bestCpuWorker);
        workersByMemLoad.add(bestMemWorker);
    }

    public void assignRequest(InternalRequest request, Worker worker) {
        dispatchedJobs.put(request.getRequestID(), worker);
        worker.runTask(request);
    }

    // TODO Need a graceful way to terminate this while loop if/when the app needs to be exited
    private void pollPendingJobsForever(Duration pollingDuration) {
        while (true) {
            pollAllPendingJobs();
            log.info("Sleeping before next job-status poll");
            ThreadUtilc.sleep(pollingDuration.toMillis());
        }
    }

    private void pollAllPendingJobs() {
        Set<Worker> workersUpdated = Sets.newHashSet();
        for (Map.Entry<UUID, Worker> job : dispatchedJobs.entrySet()) {
            log.info("Polling for status on job: " + job);
            UUID requestID = job.getKey();
            Worker worker = job.getValue();
            workersUpdated.add(worker); // FIXME
            Optional<Result> result = worker.getResult(requestID);
            result.ifPresent(this::notifyCompletion);
        }
        for (Worker worker : workersUpdated) {
            workersByMemLoad.remove(worker);
            workersByMemLoad.add(worker);
            workersByCpuLoad.remove(worker);
            workersByCpuLoad.add(worker);
        }
    }

    private void notifyCompletion(Result result) {
        scheduler.notifyUserOfCompletion(result);
        dispatchedJobs.remove(result.getRequest().getRequestID());
    }

    private static class WorkerMemComparator implements Comparator<Worker> {
        @Override
        public int compare(Worker o1, Worker o2) {
            return Double.valueOf(o1.getMemLoad()).compareTo(o2.getMemLoad());
        }
    }

    private static class WorkerCpuComparator implements Comparator<Worker> {
        @Override
        public int compare(Worker o1, Worker o2) {
            return Double.valueOf(o1.getCpuLoad()).compareTo(o2.getCpuLoad());
        }
    }
}
