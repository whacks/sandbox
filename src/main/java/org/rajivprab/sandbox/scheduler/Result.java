package org.rajivprab.sandbox.scheduler;

import java.time.Instant;

/**
 * UserRequest UUID.
 * Time started.
 * Time completed.
 * Exit Code.
 * Stderr.
 * Stdout.
 *
 * Created by rprabhakar on 5/16/16.
 */
public class Result {
    private final InternalRequest request;
    private final Instant timeStarted;
    private final Instant timeCompleted;
    private final int exitCode;
    private final String stdErr;
    private final String stdOut;

    public Result(InternalRequest request, Instant timeStarted, Instant timeCompleted,
                  int exitCode, String stdErr, String stdOut) {
        this.request = request;
        this.timeStarted = timeStarted;
        this.timeCompleted = timeCompleted;
        this.exitCode = exitCode;
        this.stdErr = stdErr;
        this.stdOut = stdOut;
    }

    public InternalRequest getRequest() {
        return request;
    }

    public Instant getTimeStarted() {
        return timeStarted;
    }

    public Instant getTimeCompleted() {
        return timeCompleted;
    }

    public int getExitCode() {
        return exitCode;
    }

    public String getStdErr() {
        return stdErr;
    }

    public String getStdOut() {
        return stdOut;
    }

    @Override
    public String toString() {
        return "Result{" +
                "request=" + request +
                ", timeStarted=" + timeStarted +
                ", timeCompleted=" + timeCompleted +
                ", exitCode=" + exitCode +
                ", stdErr='" + stdErr + '\'' +
                ", stdOut='" + stdOut + '\'' +
                '}';
    }
}
