package org.rajivprab.sandbox.scheduler;

import com.google.common.collect.Maps;
import com.google.common.util.concurrent.AtomicDouble;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.UUID;

/**
 * User weight.
 * Jobs requested and still running.
 * Notify of job completion
 *
 * Bonus:
 * Track recently completed jobs as well
 *
 * Created by rprabhakar on 5/16/16.
 */
public class User {
    private static final Logger log = LogManager.getLogger(User.class);

    private final UUID userID;
    private final AtomicDouble sumCpuRequested = new AtomicDouble(0);
    private final AtomicDouble sumMemRequested = new AtomicDouble(0);
    private final Map<UUID, InternalRequest> runningJobs = Maps.newConcurrentMap();

    private double userWeight;
    private String email;

    User(UUID userID, double weight, String email) {
        this.userID = userID;
        this.userWeight = weight;
        this.email = email;
    }

    public void notifyUserOfCompletion(Result result) {
        // TODO send email to user's email address
        log.info("Job is done with: " + result + ". Sending notification to: " + this);
    }

    public double addAndGetCpuRequested(double cpuRequested) {
        return sumCpuRequested.addAndGet(cpuRequested);
    }

    public double addAndGetMemRequested(double memRequested) {
        return sumMemRequested.addAndGet(memRequested);
    }

    // TODO needed?
    public void trackJob(InternalRequest request) {
        runningJobs.put(request.getRequestID(), request);
    }

    public double getUserWeight() {
        return userWeight;
    }

    public UUID getUserID() {
        return userID;
    }

    @Override
    public String toString() {
        return "User{" +
                "userID=" + userID +
                '}';
    }
}
