package org.rajivprab.sandbox.scheduler;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.UUID;

/**
 * Accept user request. Return request UUID.
 * Generate internal-request from user-request.
 * Assign the request to the worker-pool.
 * Get Result back from worker-pool.
 * Notify user of Result.
 *
 * Bonus:
 * Query for partial result before completion
 * Convert all classes to interface + base-implementation
 *
 * Created by rprabhakar on 5/16/16.
 */
public class Scheduler {
    private final UserPool userPool;
    private final WorkerPool workerPool;

    private final Map<UUID, InternalRequest> pendingRequests = Maps.newConcurrentMap();

    public Scheduler(UserPool userPool, WorkerPool workerPool) {
        this.userPool = userPool;
        this.workerPool = workerPool;
    }

    public UUID submitRequest(String userToken, UserRequest request) {
        User user = userPool.getUser(userToken);
        InternalRequest internalRequest = InternalRequest.constructRequest(user, request);
        workerPool.assignRequest(internalRequest);
        return internalRequest.getRequestID();
    }

    public void notifyUserOfCompletion(Result result) {
        userPool.getUser(result.getRequest().getUserID()).notifyUserOfCompletion(result);
    }
}
