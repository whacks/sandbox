package org.rajivprab.sandbox.scheduler;

import java.util.UUID;

/**
 * Class is immutable.
 *
 * Request UUID.
 * Timeout.
 * Cpu allocated.
 * Mem allocated.
 * Timeout.
 * Command line.
 * User ID.
 *
 * Created by rprabhakar on 5/16/16.
 */
public class InternalRequest {
    private final UUID requestID = UUID.randomUUID();
    private final UUID userID;
    private final long timeoutSeconds;
    private final double cpuAllocated;
    private final double memAllocated;
    private final String commandLine;

    private InternalRequest(long timeoutSeconds, double cpuAllocated, double memAllocated, String commandLine, UUID userID) {
        this.timeoutSeconds = timeoutSeconds;
        this.cpuAllocated = cpuAllocated;
        this.memAllocated = memAllocated;
        this.commandLine = commandLine;
        this.userID = userID;
    }

    public static InternalRequest constructRequest(User user, UserRequest userRequest) {
        double memAllocation = getAllocation(
                userRequest.getMemRequest(),
                user.addAndGetMemRequested(userRequest.getMemRequest()),
                user.getUserWeight());
        double cpuAllocation = getAllocation(
                userRequest.getCpuRequest(),
                user.addAndGetCpuRequested(userRequest.getCpuRequest()),
                user.getUserWeight());
        return new InternalRequest(
                userRequest.getTimeoutSeconds(), cpuAllocation, memAllocation, userRequest.getCommandLine(), user.getUserID());
    }

    public UUID getRequestID() {
        return requestID;
    }

    public UUID getUserID() {
        return userID;
    }

    public long getTimeoutSeconds() {
        return timeoutSeconds;
    }

    public double getCpuAllocated() {
        return cpuAllocated;
    }

    public double getMemAllocated() {
        return memAllocated;
    }

    public String getCommandLine() {
        return commandLine;
    }

    private static double getAllocation(double currentRequest, double sumRequested, double userWeight) {
        return userWeight * currentRequest / sumRequested;
    }

    @Override
    public String toString() {
        return "InternalRequest{" +
                "requestID=" + requestID +
                ", userID=" + userID +
                ", timeoutSeconds=" + timeoutSeconds +
                ", cpuAllocated=" + cpuAllocated +
                ", memAllocated=" + memAllocated +
                ", commandLine='" + commandLine + '\'' +
                '}';
    }
}
