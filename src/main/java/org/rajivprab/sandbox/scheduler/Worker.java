package org.rajivprab.sandbox.scheduler;

import com.google.common.util.concurrent.AtomicDouble;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;
import java.util.UUID;

/**
 * Request status.
 * CPU load.
 * Mem load.
 * Worker ID.
 * Assign request.
 *
 * Created by rprabhakar on 5/16/16.
 */
public class Worker {
    private static final Logger log = LogManager.getLogger(Worker.class);

    private final UUID workerID = UUID.randomUUID();

    private AtomicDouble cpuLoad = new AtomicDouble(0);
    private AtomicDouble memLoad = new AtomicDouble(0);

    public void runTask(InternalRequest request) {
        // TODO send request to the machine and run the task
        // Process should be assigned cpu/mem utilization proportional to the cpuAssigned and memAssigned in the request
        // Process should be killed by the worker once the timeout is hit
        cpuLoad.addAndGet(request.getCpuAllocated());
        memLoad.addAndGet(request.getMemAllocated());
    }

    public Optional<Result> getResult(UUID requestID) {
        // TODO query the worker machine to get task status and result
        Optional<Result> result = Optional.empty();
        result.ifPresent(this::updateLoadOnCompletion);
        return result;
    }

    void updateLoadOnCompletion(Result result) {
        cpuLoad.addAndGet(result.getRequest().getCpuAllocated() * -1);
        memLoad.addAndGet(result.getRequest().getMemAllocated() * -1);
    }

    public UUID getWorkerID() {
        return workerID;
    }

    public double getCpuLoad() {
        return cpuLoad.get();
    }

    public double getMemLoad() {
        return memLoad.get();
    }

    @Override
    public String toString() {
        return "Worker{" +
                "workerID=" + workerID +
                ", cpuLoad=" + cpuLoad +
                ", memLoad=" + memLoad +
                '}';
    }
}
