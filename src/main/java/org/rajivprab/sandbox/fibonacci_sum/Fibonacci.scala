package org.rajivprab.sandbox.fibonacci_sum

import scala.collection.mutable

object Fibonacci {
  private val values = new mutable.HashMap[Long, Long]();
  values.put(1, 1);
  values.put(2, 2);

  def getIndex(i: Long): Long = {
    if (!values.contains(i)) {
      values.put(i, compute(i))
      assert(values.contains(i-1))
      assert(values.contains(i-2))
    }
    values(i)
  }

  private def compute(i: Long): Long = {
    if (!values.contains(i-1)) {
      values.put(i-1, getIndex(i-1));
    }
    values(i-1) + values(i-2);
  }
}
