package org.rajivprab.sandbox.submatrix_target;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;

import java.util.Optional;

/**
 * See: https://leetcode.com/problems/number-of-submatrices-that-sum-to-target/
 *
 * Given a matrix and a target, return the number of non-empty submatrices that sum to target.
 * A submatrix x1, y1, x2, y2 is the set of all cells matrix[x][y] with x1 <= x <= x2 and y1 <= y <= y2.
 * Two submatrices (x1, y1, x2, y2) and (x1', y1', x2', y2') are different if they have some coordinate that is
 * different: for example, if x1 != x1'.
 *
 * Solution below is O(Row*Row*Col*Col)
 */
public class SubmatrixSumTargetOriginal {

    // Matrix.get(row, column) = value
    public static int getNumSubmatrices(Table<Integer, Integer, Integer> matrix, int target, int numRows, int numCol) {
        return new SubmatrixSumTargetOriginal(matrix, numRows, numCol).getNumSubmatricesForTarget(target);
    }

    private final Table<Integer, Integer, Integer> matrix;
    private final Table<Integer, Integer, Integer> originSumMatrix;
    private final int numRows;
    private final int numColumns;

    private SubmatrixSumTargetOriginal(Table<Integer, Integer, Integer> matrix, int numRows, int numCol) {
        this.matrix = ImmutableTable.copyOf(matrix);
        this.numRows = numRows;
        this.numColumns = numCol;
        this.originSumMatrix = ImmutableTable.copyOf(buildOriginSumMatrix());
    }

    private Table<Integer, Integer, Integer> buildOriginSumMatrix() {
        Table<Integer, Integer, Integer> originSumMatrix = HashBasedTable.create();
        for (int row=0; row<numRows; row++) {
            for (int col=0; col<numColumns; col++) {
                int left = Optional.ofNullable(originSumMatrix.get(row, col-1)).orElse(0);
                int top = Optional.ofNullable(originSumMatrix.get(row-1, col)).orElse(0);
                int diagonal = Optional.ofNullable(originSumMatrix.get(row-1, col-1)).orElse(0);
                int cur = left + top - diagonal + matrix.get(row, col);
                originSumMatrix.put(row, col, cur);
            }
        }
        return originSumMatrix;
    }

    private int getNumSubmatricesForTarget(int target) {
        int result = 0;
        for (Cell<Integer, Integer, Integer> cell : matrix.cellSet()) {
            result += getNumSubmatricesForTarget(cell.getRowKey(), cell.getColumnKey(), target);
        }
        return result;
    }

    private int getNumSubmatricesForTarget(int startRow, int startColumn, int target) {
        int result = 0;
        for (int endRow=startRow; endRow<numRows; endRow++) {
            for (int endCol=startColumn; endCol<numColumns; endCol++) {
                if (getSubmatrixSum(startRow, startColumn, endRow, endCol) == target) { result++; }
            }
        }
        return result;
    }

    private int getSubmatrixSum(int startRow, int startColumn, int endRow, int endColumn) {
        int left = Optional.ofNullable(originSumMatrix.get(endRow, startColumn-1)).orElse(0);
        int top = Optional.ofNullable(originSumMatrix.get(startRow-1, endColumn)).orElse(0);
        int diagonal = Optional.ofNullable(originSumMatrix.get(startRow-1, startColumn-1)).orElse(0);
        return originSumMatrix.get(endRow, endColumn) - left - top + diagonal;
    }
}
