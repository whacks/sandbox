package org.rajivprab.sandbox.largest_square;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.util.List;
import java.util.stream.Collectors;

public class LargestSquare {
    public static int getLargestSquareLength(Table<Integer, Integer, Boolean> map) {
        List<Integer> rows = map.rowKeySet().stream().sorted().collect(Collectors.toList());
        List<Integer> columns = map.columnKeySet().stream().sorted().collect(Collectors.toList());
        int maxRow = rows.get(rows.size() - 1);
        int maxCol = columns.get(columns.size() - 1);

        Table<Integer, Integer, Integer> largestSquareWithCellAsTopLeftCorner = HashBasedTable.create();
        for (int row=maxRow; row>=0; row--) {
            for (int col=maxCol; col>=0; col--) {
                boolean value = map.get(row, col);
                if (value) {
                    Integer bottomRight = largestSquareWithCellAsTopLeftCorner.get(row+1, col+1);
                    if (bottomRight == null) { bottomRight = 0; }

                    Integer right = largestSquareWithCellAsTopLeftCorner.get(row, col+1);
                    if (right == null) { right = 0; }

                    Integer bottom = largestSquareWithCellAsTopLeftCorner.get(row+1, col);
                    if (bottom == null) { bottom = 0; }

                    int myValue = Math.min(Math.min(right, bottom), bottomRight) + 1;
                    largestSquareWithCellAsTopLeftCorner.put(row, col, myValue);
                } else {
                    largestSquareWithCellAsTopLeftCorner.put(row, col, 0);
                }
            }
        }

        List<Integer> largestSquareLengths = largestSquareWithCellAsTopLeftCorner.values().stream().sorted().collect(Collectors.toList());
        return largestSquareLengths.get(largestSquareLengths.size() - 1);
    }
}
