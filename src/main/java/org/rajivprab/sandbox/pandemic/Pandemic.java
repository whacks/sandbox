package org.rajivprab.sandbox.pandemic;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.Validatec;

import java.util.Map;
import java.util.PriorityQueue;
import java.util.stream.IntStream;

/**
 * Created by rprabhakar on 2/25/16.
 */
public class Pandemic {
    public static Map<String, Integer> getClinicAllocations(Map<String, Integer> cityPopulations, int totalClinics) {
        return new ClinicAllocator(cityPopulations, totalClinics).getAllocations();
    }

    private static class ClinicAllocator {
        private final PriorityQueue<City> cities = new PriorityQueue<>();
        private int numClinicsRemaining;

        public ClinicAllocator(Map<String, Integer> cityPopulations, int totalClinics) {
            Validatec.greaterThan(totalClinics, cityPopulations.size() - 1, "Num clinics has to at least match num cities");
            this.numClinicsRemaining = totalClinics;
            double averageService = cityPopulations.values().stream().mapToInt(Integer::intValue).sum() * 1.0 / totalClinics;
            cityPopulations.entrySet().stream().forEach(entry -> createCity(entry.getKey(), entry.getValue(), averageService));
            // cityPopulations.entrySet().stream().forEach(entry -> createCity(entry.getKey(), entry.getValue(), 1));
            IntStream.range(0, numClinicsRemaining).forEach(i -> allocateClinic());
        }

        private void createCity(String name, int pop, double averageService) {
            createCity(name, pop, (int) Math.floor(pop * 1.0 / averageService));
        }

        private void createCity(String name, int pop, int startClinics) {
            cities.add(new City(name, pop, startClinics));
            numClinicsRemaining -= startClinics;
        }

        private void allocateClinic() {
            cities.add(cities.poll().withNewClinic());
        }

        public Map<String, Integer> getAllocations() {
            Map<String, Integer> finalAllocations = Maps.newHashMap();
            cities.stream().forEach(entry -> finalAllocations.put(entry.getName(), entry.getNumClinics()));
            return finalAllocations;
        }

        private static class City implements Comparable<City> {
            private int numClinics;
            private final int population;
            private final String name;

            public City(String name, int population) {
                this(name, population, 1);
            }

            public City(String name, int population, int numClinics) {
                this.name = name;
                this.population = population;
                this.numClinics = numClinics;
            }

            public City withNewClinic() {
                numClinics++;
                return this;
            }

            private double getPopulationServedPerClinic() {
                return population * 1.0 / numClinics;
            }

            public String getName() {
                return name;
            }

            public int getNumClinics() {
                return numClinics;
            }

            @Override
            public int compareTo(City other) {
                Validate.notNull(other);
                return Double.compare(other.getPopulationServedPerClinic(), getPopulationServedPerClinic());
            }
        }
    }
}
