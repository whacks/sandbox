package org.rajivprab.sandbox.pool;

/**
 * Question: Assume there are a set of 10 pool balls, that are of color (R, G), and a single ball that is of color B on a table.
 * Given only the ability to swap any two balls swap(B1, B2), write an algorithm that ensures that no two balls of the same
 * color are touching on the outside of the ring.
 *
 * Example:
 *      B                 R
 *     G R      =>       G G
 *    G R R             R B R
 *   G G R R           G R G R
 */
public class Pool {
}
