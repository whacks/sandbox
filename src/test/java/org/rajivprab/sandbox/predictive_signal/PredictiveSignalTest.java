package org.rajivprab.sandbox.predictive_signal;

import org.junit.Test;

import java.io.IOException;

/**
 * Test class for code
 *
 * Created by rprabhakar on 4/4/16.
 */
public class PredictiveSignalTest {
    private static final String TEST_DATA_PATH = "predictive_signal/ResearchDataset.csv";

    @Test
    public void testDataset() throws IOException {
        PredictiveSignal.parseDataset(TEST_DATA_PATH);
    }
}
