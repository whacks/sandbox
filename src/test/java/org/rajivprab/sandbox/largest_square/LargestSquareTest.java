package org.rajivprab.sandbox.largest_square;

import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import com.google.common.truth.Truth;
import org.junit.Test;

public class LargestSquareTest {
    @Test
    public void baseCase0_false() {
        Table<Integer, Integer, Boolean> map = ImmutableTable.<Integer, Integer, Boolean>builder()
                                                             .put(0, 0, false)
                                                             .build();
        Truth.assertThat(LargestSquare.getLargestSquareLength(map)).isEqualTo(0);
    }

    @Test
    public void baseCase1_false() {
        Table<Integer, Integer, Boolean> map = ImmutableTable.<Integer, Integer, Boolean>builder()
                                                             .put(0, 0, false)
                                                             .put(0, 1, false)
                                                             .put(1, 0, false)
                                                             .put(1, 1, false)
                                                             .build();
        Truth.assertThat(LargestSquare.getLargestSquareLength(map)).isEqualTo(0);
    }

    @Test
    public void baseCase2_false() {
        Table<Integer, Integer, Boolean> map = ImmutableTable.<Integer, Integer, Boolean>builder()
                                                             .put(0, 0, false)
                                                             .put(0, 1, false)
                                                             .put(0, 2, false)
                                                             .put(1, 0, false)
                                                             .put(1, 1, false)
                                                             .put(1, 2, false)
                                                             .put(2, 0, false)
                                                             .put(2, 1, false)
                                                             .put(2, 2, false)
                                                             .build();
        Truth.assertThat(LargestSquare.getLargestSquareLength(map)).isEqualTo(0);
    }

    @Test
    public void baseCase0_true() {
        Table<Integer, Integer, Boolean> map = ImmutableTable.<Integer, Integer, Boolean>builder()
                                                             .put(0, 0, true)
                                                             .build();
        Truth.assertThat(LargestSquare.getLargestSquareLength(map)).isEqualTo(1);
    }

    @Test
    public void baseCase1_true() {
        Table<Integer, Integer, Boolean> map = ImmutableTable.<Integer, Integer, Boolean>builder()
                                                             .put(0, 0, true)
                                                             .put(0, 1, true)
                                                             .put(1, 0, true)
                                                             .put(1, 1, true)
                                                             .build();
        Truth.assertThat(LargestSquare.getLargestSquareLength(map)).isEqualTo(2);
    }

    @Test
    public void baseCase2_true() {
        Table<Integer, Integer, Boolean> map = ImmutableTable.<Integer, Integer, Boolean>builder()
                                                             .put(0, 0, true)
                                                             .put(0, 1, true)
                                                             .put(0, 2, true)
                                                             .put(1, 0, true)
                                                             .put(1, 1, true)
                                                             .put(1, 2, true)
                                                             .put(2, 0, true)
                                                             .put(2, 1, true)
                                                             .put(2, 2, true)
                                                             .build();
        Truth.assertThat(LargestSquare.getLargestSquareLength(map)).isEqualTo(3);
    }

    @Test
    public void square1() {
        Table<Integer, Integer, Boolean> map = ImmutableTable.<Integer, Integer, Boolean>builder()
                                                             .put(0, 0, true)
                                                             .put(0, 1, true)
                                                             .put(0, 2, true)
                                                             .put(1, 0, true)
                                                             .put(1, 1, false)
                                                             .put(1, 2, true)
                                                             .put(2, 0, true)
                                                             .put(2, 1, true)
                                                             .put(2, 2, true)
                                                             .build();
        Truth.assertThat(LargestSquare.getLargestSquareLength(map)).isEqualTo(1);
    }

    @Test
    public void square2() {
        Table<Integer, Integer, Boolean> map = ImmutableTable.<Integer, Integer, Boolean>builder()
                                                             .put(0, 0, false)
                                                             .put(0, 1, true)
                                                             .put(0, 2, true)
                                                             .put(1, 0, true)
                                                             .put(1, 1, true)
                                                             .put(1, 2, true)
                                                             .put(2, 0, true)
                                                             .put(2, 1, true)
                                                             .put(2, 2, true)
                                                             .build();
        Truth.assertThat(LargestSquare.getLargestSquareLength(map)).isEqualTo(2);
    }
}
