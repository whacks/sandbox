package org.rajivprab.sandbox.room_cleaner;

import com.google.common.truth.Truth;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.sandbox.room_cleaner.Map.OutOfMapException;

/**
 * Created by rajivprab on 4/29/17.
 */
public class MapTest {
    @Test
    public void horizontalRectangle() {
        Map map = new Map().setCell(new Coordinates(1, 1), Cell.BLOCKED)
                           .setCell(new Coordinates(-2, -1), Cell.EMPTY);

        Truth.assertThat(map.getCell(new Coordinates(1, 1))).isEqualTo(Cell.BLOCKED);
        Truth.assertThat(map.getCell(new Coordinates(-2, -1))).isEqualTo(Cell.EMPTY);
        Truth.assertThat(map.getCell(new Coordinates(0, 0))).isEqualTo(Cell.EMPTY);

        Truth.assertThat(map.getCell(new Coordinates(1, -1))).isEqualTo(Cell.UNKNOWN);
        assertOutOfMapError(map, -1, 3);
        assertOutOfMapError(map, 0, 1);

    }

    @Test
    public void verticalRectangle() {
        Map map = new Map().setCell(new Coordinates(1, 2), Cell.BLOCKED)
                           .setCell(new Coordinates(-1, -1), Cell.EMPTY);

        Truth.assertThat(map.getCell(new Coordinates(1, 2))).isEqualTo(Cell.BLOCKED);
        Truth.assertThat(map.getCell(new Coordinates(-1, -1))).isEqualTo(Cell.EMPTY);
        Truth.assertThat(map.getCell(new Coordinates(0, 0))).isEqualTo(Cell.EMPTY);

        Truth.assertThat(map.getCell(new Coordinates(1, -1))).isEqualTo(Cell.UNKNOWN);
        assertOutOfMapError(map, 0, 1);
    }

    private static void assertOutOfMapError(Map map, int x, int y) {
        try {
            map.getCell(new Coordinates(x, y));
            Assert.fail("Exception should be thrown");
        } catch (OutOfMapException expected) {}
    }
}
