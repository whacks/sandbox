package org.rajivprab.sandbox.placeholder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class PlaceholderTest {
    private static final Logger log = LogManager.getLogger(PlaceholderTest.class);

    @Test
    public void example1() {
        log.info("Hello world");
    }
}
