package org.rajivprab.sandbox.nuclear_rods;

import com.google.common.collect.Sets;
import com.google.common.truth.Truth;
import org.junit.Test;

/**
 * Unit tests for the solver
 *
 * Created by rprabhakar on 2/28/16.
 */
public class NuclearRodsTest {
    // Rods #1, #2, #4 are connected to each other (2-1-4) so they are in a single group of 3.
    // The sortie to recover them will cost 2 (with isolation chamber capacity 2^2 = 4).
    // Rod #3 is not fused to any other, so it can be recovered at a cost of  1 (with isolation chamber capacity 1^2 = 1).
    @Test
    public void caseOne() {
        Truth.assertThat(NuclearRods.getMinCost("4", "2", "1 2", "1 4")).isEqualTo(3);
    }

    @Test
    public void monolith1() {
        Truth.assertThat(NuclearRods.getMinCost("4", "3", "1 2", "1 4", "3 4")).isEqualTo(2);
    }

    @Test
    public void monolith2() {
        Truth.assertThat(NuclearRods.getMinCost("4", "3", "1 2", "1 4", "4 3")).isEqualTo(2);
    }

    @Test
    public void twoSets() {
        Truth.assertThat(NuclearRods.getMinCost("4", "2", "1 2", "3 4")).isEqualTo(4);
    }

    @Test
    public void mergeSets() {
        Truth.assertThat(NuclearRods.getMinCost("4", "3", "1 2", "3 4", "1 4")).isEqualTo(2);
    }

    @Test
    public void redundantPair() {
        Truth.assertThat(NuclearRods.getMinCost("4", "4", "1 2", "3 4", "1 4", "2 3")).isEqualTo(2);
    }

    @Test
    public void sqrtCalculationRoundDown() {
        Truth.assertThat(NuclearRods.clearSetGetCost(Sets.newHashSet(1, 2))).isEqualTo(2);
    }

    @Test
    public void sqrtCalculationRoundUp() {
        Truth.assertThat(NuclearRods.clearSetGetCost(Sets.newHashSet(1, 2, 3))).isEqualTo(2);
    }

    @Test
    public void sqrtCalculationExact1() {
        Truth.assertThat(NuclearRods.clearSetGetCost(Sets.newHashSet(1))).isEqualTo(1);
    }

    @Test
    public void sqrtCalculationExact2() {
        Truth.assertThat(NuclearRods.clearSetGetCost(Sets.newHashSet(1, 2, 3, 4))).isEqualTo(2);
    }
}
