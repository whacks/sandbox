package org.rajivprab.sandbox.combinatorial;

import com.google.common.truth.Truth;
import org.junit.Test;

public class FactorialTest {
    @Test
    public void factorial_baseTest_withCache() {
        Truth.assertThat(Factorial.get(4)).isEqualTo(4 * 3 * 2);
        Truth.assertThat(Factorial.get(4)).isEqualTo(4 * 3 * 2);
    }

    @Test
    public void sequentialMultiply_baseTest_withCache() {
        Truth.assertThat(Factorial.multiplySequentially(4, 7)).isEqualTo(4 * 5 * 6 * 7);
        Truth.assertThat(Factorial.multiplySequentially(4, 7)).isEqualTo(4 * 5 * 6 * 7);
    }
}
