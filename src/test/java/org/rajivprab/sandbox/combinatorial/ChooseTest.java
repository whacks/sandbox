package org.rajivprab.sandbox.combinatorial;

import com.google.common.truth.Truth;
import org.junit.Test;

public class ChooseTest {

    @Test
    public void compute_23choose10_is1144066_withCaching() {
        Truth.assertThat(Choose.get(23, 10)).isEqualTo(1144066);
    }

    // -------------

    @Test
    public void compute_3choose1_is3_withCaching() {
        Truth.assertThat(Choose.get(3, 1)).isEqualTo(3);
        Truth.assertThat(Choose.get(3, 1)).isEqualTo(3);
    }

    @Test
    public void compute_3choose2_is3_withCaching() {
        Truth.assertThat(Choose.get(3, 2)).isEqualTo(3);
        Truth.assertThat(Choose.get(3, 2)).isEqualTo(3);
    }

    @Test
    public void compute_3choose3_is1_withCaching() {
        Truth.assertThat(Choose.get(3, 3)).isEqualTo(1);
        Truth.assertThat(Choose.get(3, 3)).isEqualTo(1);
    }

    // ------------------

    @Test
    public void compute_4choose1_is4_withCaching() {
        Truth.assertThat(Choose.get(4, 1)).isEqualTo(4);
        Truth.assertThat(Choose.get(4, 1)).isEqualTo(4);
    }

    @Test
    public void compute_4choose2_is6_withCaching() {
        Truth.assertThat(Choose.get(4, 2)).isEqualTo(6);
        Truth.assertThat(Choose.get(4, 2)).isEqualTo(6);
    }

    @Test
    public void compute_4choose3_is4_withCaching() {
        Truth.assertThat(Choose.get(4, 3)).isEqualTo(4);
        Truth.assertThat(Choose.get(4, 3)).isEqualTo(4);
    }

    @Test
    public void compute_4choose4_is1_withCaching() {
        Truth.assertThat(Choose.get(4, 4)).isEqualTo(1);
        Truth.assertThat(Choose.get(4, 4)).isEqualTo(1);
    }
}
