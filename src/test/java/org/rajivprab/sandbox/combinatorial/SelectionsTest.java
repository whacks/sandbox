package org.rajivprab.sandbox.combinatorial;

import com.google.common.truth.Truth;
import org.junit.Test;

public class SelectionsTest {
    @Test
    public void numberCombinations_under100_greaterThanOneMillion() {
        int num = Selections.getPairs();
        Truth.assertThat(num).isEqualTo(4075);
    }
}
