package org.rajivprab.sandbox.car_rental;

import org.junit.Test;

import java.time.Instant;

import static com.google.common.truth.Truth.assertThat;

public class ScheduleTest {
    @Test
    public void overlapsWithStart() {
        Instant start1 = Instant.ofEpochSecond(1000);
        Instant end1 = Instant.ofEpochSecond(2000);

        Instant start2 = Instant.ofEpochSecond(900);
        Instant end2 = Instant.ofEpochSecond(1100);
        assertThat(Schedule.overlaps(start1, end1, start2, end2)).isTrue();
    }

    @Test
    public void overlapsWithEnd() {
        Instant start1 = Instant.ofEpochSecond(1000);
        Instant end1 = Instant.ofEpochSecond(2000);

        Instant start2 = Instant.ofEpochSecond(1900);
        Instant end2 = Instant.ofEpochSecond(2100);
        assertThat(Schedule.overlaps(start1, end1, start2, end2)).isTrue();
    }

    @Test
    public void superset() {
        Instant start1 = Instant.ofEpochSecond(1000);
        Instant end1 = Instant.ofEpochSecond(2000);

        Instant start2 = Instant.ofEpochSecond(900);
        Instant end2 = Instant.ofEpochSecond(2100);
        assertThat(Schedule.overlaps(start1, end1, start2, end2)).isTrue();
    }

    @Test
    public void subset() {
        Instant start1 = Instant.ofEpochSecond(1000);
        Instant end1 = Instant.ofEpochSecond(2000);

        Instant start2 = Instant.ofEpochSecond(1100);
        Instant end2 = Instant.ofEpochSecond(1900);
        assertThat(Schedule.overlaps(start1, end1, start2, end2)).isTrue();
    }

    @Test
    public void isBefore() {
        Instant start1 = Instant.ofEpochSecond(1000);
        Instant end1 = Instant.ofEpochSecond(2000);

        Instant start2 = Instant.ofEpochSecond(100);
        Instant end2 = Instant.ofEpochSecond(900);
        assertThat(Schedule.overlaps(start1, end1, start2, end2)).isFalse();
    }

    @Test
    public void isAfter() {
        Instant start1 = Instant.ofEpochSecond(1000);
        Instant end1 = Instant.ofEpochSecond(2000);

        Instant start2 = Instant.ofEpochSecond(2100);
        Instant end2 = Instant.ofEpochSecond(2900);
        assertThat(Schedule.overlaps(start1, end1, start2, end2)).isFalse();
    }
}
