package org.rajivprab.sandbox.unique_bst;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class UniqueBstTest {
    private static final Logger log = LogManager.getLogger(UniqueBstTest.class);

    @Test
    public void sampleTest() {
        List<TreeNode> results = UniqueBst.generateTrees(3);
        log.info(results);
        assertThat(results).hasSize(5);
    }

    @Ignore
    public void toFive() {
        List<TreeNode> results = UniqueBst.generateTrees(5);
        log.info("Found results: " + results.size());
        log.info(results);
    }

    @Ignore
    public void toSix() {
        List<TreeNode> results = UniqueBst.generateTrees(6);
        log.info("Found results: " + results.size());
        log.info(results);
    }
}
