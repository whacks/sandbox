package org.rajivprab.sandbox.scrabble;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

public class ScrabbleTest {
    @Test
    public void example1() {
        int maxScore = Scrabble.getMaxScore(ImmutableList.of("dog","cat","dad","good"),
                                            ImmutableList.of("a","a","c","d","d","d","g","o","o"),
                                            ImmutableList.of(1,0,9,5,0,0,3,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0));
        Truth.assertThat(maxScore).isEqualTo(23);
    }

    @Test
    public void example2() {
        int maxScore = Scrabble.getMaxScore(ImmutableList.of("xxxz","ax","bx","cx"),
                                            ImmutableList.of("z","a","b","c","x","x","x"),
                                            ImmutableList.of(4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,10));
        Truth.assertThat(maxScore).isEqualTo(27);
    }

    @Test
    public void example3() {
        int maxScore = Scrabble.getMaxScore(ImmutableList.of("leetcode"),
                                            ImmutableList.of("l","e","t","c","o","d"),
                                            ImmutableList.of(0,0,1,1,1,0,0,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,0,0,0));
        Truth.assertThat(maxScore).isEqualTo(0);
    }
}

