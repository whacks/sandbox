package org.rajivprab.sandbox.longest_increasing_subsequence;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;
import scala.Int;

import java.util.List;

public class LongestIncreasingSubsequenceTest {
    @Test
    public void example1() {
        // The longest subsequence that meets the requirements is [1,3,4,5,8].
        // The subsequence has a length of 5, so we return 5.
        // Note that the subsequence [1,3,4,5,8,15] does not meet the requirements because 15 - 8 = 7 is larger than 3.
        test(5, 3, 4,2,1,4,3,4,5,8,15);
    }

    @Test
    public void example2() {
        // The longest subsequence that meets the requirements is [4,5,8,12].
        // The subsequence has a length of 4, so we return 4.
        test(4, 5, 7,4,5,1,8,12,4,7);
    }

    @Test
    public void example3() {
        // The longest subsequence that meets the requirements is [1].
        // The subsequence has a length of 1, so we return 1.
        test(1, 1, 1,5);
    }

    private static void test(int expected, int k, Integer... nums) {
        List<Integer> numsList = ImmutableList.copyOf(nums);
        Truth.assertThat(LongestIncreasingSubsequence.lengthOfLIS(numsList, k)).isEqualTo(expected);
    }
}
