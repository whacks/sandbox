package org.rajivprab.sandbox.dir_sort;

import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.cava.FileUtilc;

import java.io.File;
import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;

public class DirectorySortTest {
    @Test
    public void illegalInput() {
        File input = FileUtilc.getClasspathFile("dir_sort/illegal_input");
        File output = new File("/tmp/" + UUID.randomUUID());
        output.deleteOnExit();

        try {
            DirectorySort.sort(input, output);
            Assert.fail("Should have failed");
        } catch (Exception e) {
            assertThat(e).hasMessageThat().contains("Input files are not sorted");
        }
    }

    @Test
    public void sampleTest() {
        File input = FileUtilc.getClasspathFile("dir_sort/input");
        File expectedOutput = FileUtilc.getClasspathFile("dir_sort/expected_output.txt");
        File output = new File("/tmp/" + UUID.randomUUID());
        output.deleteOnExit();

        DirectorySort.sort(input, output);

        assertThat(FileUtilc.readFileToString(output)).isEqualTo(FileUtilc.readFileToString(expectedOutput));
    }
}
