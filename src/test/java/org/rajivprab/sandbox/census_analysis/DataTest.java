package org.rajivprab.sandbox.census_analysis;

import com.google.common.truth.Truth;
import org.junit.Test;

public class DataTest {
    @Test
    public void getNextLine_shouldHandleCommaInName() {
        String[] columns = Data.getNextLine("\"310M400US33500\",\"Minot, ND Micro Area\",\"75668\",\"(X)\",\"75.668\"");
        Truth.assertThat(columns[0]).isEqualTo("310M400US33500");
        Truth.assertThat(columns[1]).isEqualTo("Minot, ND Micro Area");
        Truth.assertThat(columns[2]).isEqualTo("75668");
        Truth.assertThat(columns[3]).isEqualTo("(X)");
        Truth.assertThat(columns[4]).isEqualTo("75.668");
        Truth.assertThat(columns).hasLength(5);
    }
}
