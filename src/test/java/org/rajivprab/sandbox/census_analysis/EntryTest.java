package org.rajivprab.sandbox.census_analysis;

import com.google.common.truth.Truth;
import org.junit.Test;

public class EntryTest {
    @Test
    public void baseTest1() {
        // 100 people, 50 white, 50 other, 50 marriages
        // Expected number of white-other marriages = 50 * 0.5 = 25 (each White person has 50% chance of marrying other)
        // Expected fraction of white-other marriages = 25 / 50 = 0.5
        // Actual fraction = 0.3
        // Score = 0.3 / 0.5 = 0.6
        Truth.assertThat(getScore(50, 50, 30))
             .isWithin(0.001).of(60);
    }

    @Test
    public void baseTest2() {
        // 100 people, 40 white, 35 other, 25 uncategorized, 50 marriages
        // Expected number of white-other marriages = 40 * 0.35 = 14 (each white person has 35% chance of marrying other)
        // Expected fraction of white-other marriages = 14 / 50 = 0.28
        // Actual fraction = 0.05
        // Score = 0.05 / 0.28 ~ 0.17857
        Truth.assertThat(getScore(40, 35, 5))
             .isWithin(0.001).of(17.857);
    }

    private static double getScore(double whitePercentage, double otherPercentage, double mixedPercentage) {
        return Entry.build("random name", whitePercentage, otherPercentage, mixedPercentage).get().getScore();
    }
}
