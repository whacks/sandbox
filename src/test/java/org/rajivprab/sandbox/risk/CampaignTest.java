package org.rajivprab.sandbox.risk;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.stream.IntStream;

/**
 * Created by rajivprab on 5/21/17.
 */
public class CampaignTest {
    private static final Logger log = LogManager.getLogger(CampaignTest.class);

    @Test
    public void hundred_vs_hundred_attackerMajorEdge() {
        runRepeatedCampaign(50, 50);
    }

    @Test
    public void ten_vs_ten_attackerSlightEdge() {
        runRepeatedCampaign(10, 10);
    }

    @Test
    public void five_vs_five_evenMatch() {
        runRepeatedCampaign(5, 5);
    }

    @Test
    public void three_vs_two() {
        runRepeatedCampaign(3, 2);
    }

    @Test
    public void three_vs_one() {
        runRepeatedCampaign(3, 1);
    }

    @Test
    public void two_vs_two() {
        runRepeatedCampaign(2, 2);
    }

    @Test
    public void two_vs_one() {
        runRepeatedCampaign(2, 1);
    }

    @Test
    public void one_vs_two() {
        runRepeatedCampaign(1, 2);
    }

    private static void runRepeatedCampaign(int numAttackers, int numDefenders) {
        int numCampaigns = 100000;
        long attackerWins = IntStream.range(0, numCampaigns)
                                        .filter(i -> Campaign.attackerWins(numAttackers, numDefenders))
                                     .count();
        log.info(String.format("Starting campaign with %d attackers and %d defenders each", numAttackers, numDefenders));
        log.info("Percentage of campaigns won by attacker: " + attackerWins * 100.0 / numCampaigns);
    }
}
