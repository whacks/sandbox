package org.rajivprab.sandbox.risk;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.Iterator;

/**
 * Created by rajivprab on 5/21/17.
 */
public class OutcomeTest {
    @Test
    public void computeBattleOutcomeWithTies_oneAttackerEach_defenderDies() {
        Iterator<Integer> attackerRolls = ImmutableList.of(6).iterator();
        Iterator<Integer> defenderRolls = ImmutableList.of(3).iterator();
        Outcome outcome = Outcome.compute(attackerRolls, defenderRolls);
        Truth.assertThat(outcome.numDeadAttacker).isEqualTo(0);
        Truth.assertThat(outcome.numDeadDefender).isEqualTo(1);
    }

    @Test
    public void computeBattleOutcomeWithTies_oneAttackerEach_attackerDies() {
        Iterator<Integer> attackerRolls = ImmutableList.of(3).iterator();
        Iterator<Integer> defenderRolls = ImmutableList.of(3).iterator();
        Outcome outcome = Outcome.compute(attackerRolls, defenderRolls);
        Truth.assertThat(outcome.numDeadAttacker).isEqualTo(1);
        Truth.assertThat(outcome.numDeadDefender).isEqualTo(0);
    }

    @Test
    public void computeBattleOutcomeWithTies_oneDeadEach() {
        Iterator<Integer> attackerRolls = ImmutableList.of(6, 1, 1).iterator();
        Iterator<Integer> defenderRolls = ImmutableList.of(3, 1).iterator();
        Outcome outcome = Outcome.compute(attackerRolls, defenderRolls);
        Truth.assertThat(outcome.numDeadAttacker).isEqualTo(1);
        Truth.assertThat(outcome.numDeadDefender).isEqualTo(1);
    }

    @Test
    public void computeBattleOutcomeWithTies_twoDeadAttackers() {
        Iterator<Integer> attackerRolls = ImmutableList.of(5, 4, 1).iterator();
        Iterator<Integer> defenderRolls = ImmutableList.of(6, 5).iterator();
        Outcome outcome = Outcome.compute(attackerRolls, defenderRolls);
        Truth.assertThat(outcome.numDeadAttacker).isEqualTo(2);
        Truth.assertThat(outcome.numDeadDefender).isEqualTo(0);
    }

    @Test
    public void computeBattleOutcomeWithTies_twoDeadDefenders() {
        Iterator<Integer> attackerRolls = ImmutableList.of(3, 2, 1).iterator();
        Iterator<Integer> defenderRolls = ImmutableList.of(2, 1).iterator();
        Outcome outcome = Outcome.compute(attackerRolls, defenderRolls);
        Truth.assertThat(outcome.numDeadAttacker).isEqualTo(0);
        Truth.assertThat(outcome.numDeadDefender).isEqualTo(2);
    }
}
