package org.rajivprab.sandbox.risk;

import com.google.common.truth.Truth;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.Iterator;

/**
 * Created by rajivprab on 5/21/17.
 */
public class BattleTest {
    private static final Logger log = LogManager.getLogger(BattleTest.class);

    @Test
    public void threeVsTwo() {
        runRepeatedBattles(3, 2);
    }

    @Test
    public void threeVsOne() {
        runRepeatedBattles(3, 1);
    }

    @Test
    public void twoVsTwo() {
        runRepeatedBattles(2, 2);
    }

    @Test
    public void twoVsOne() {
        runRepeatedBattles(2, 1);
    }

    @Test
    public void oneVsOne() {
        runRepeatedBattles(1, 1);
    }

    @Test
    public void oneVsTwo() {
        runRepeatedBattles(1, 2);
    }

    private static void runRepeatedBattles(int numAttackers, int numDefenders) {
        Outcome outcome = new Outcome(0, 0);
        for (int i=0; i<100000; i++) {
            outcome = outcome.merge(Battle.getSingleOutcome(numAttackers, numDefenders));
        }
        int totalDeaths = outcome.numDeadAttacker + outcome.numDeadDefender;
        log.info("NumDeadAttackers: " + outcome.numDeadAttacker);
        log.info("NumDeadDefenders: " + outcome.numDeadDefender);
        log.info("Percentage of deaths suffered by attackers: " + outcome.numDeadAttacker * 100.0 / totalDeaths);
        log.info("Percentage of deaths suffered by defenders: " + outcome.numDeadDefender * 100.0 / totalDeaths);
    }

    @Test
    public void minRollIsOneMaxSix() {
        int minRoll = Integer.MAX_VALUE;
        int maxRoll = Integer.MIN_VALUE;
        for (int i=0; i< 100; i++) {
            int roll = Battle.rollDie();
            Truth.assertThat(roll).isAtLeast(1);
            Truth.assertThat(roll).isAtMost(6);
            maxRoll = Math.max(roll, maxRoll);
            minRoll = Math.min(roll, minRoll);
        }
        Truth.assertThat(maxRoll).isEqualTo(6);
        Truth.assertThat(minRoll).isEqualTo(1);
    }

    @Test
    public void dieRollsAreSortedWithHighestFirst() {
        Iterator<Integer> rolls = Battle.rollDie(100).iterator();
        int prevRoll = Integer.MAX_VALUE;
        while (rolls.hasNext()) {
            int roll = rolls.next();
            Truth.assertThat(prevRoll).isAtLeast(roll);
        }
    }
}
