package org.rajivprab.sandbox.my_calendar_three;

import com.google.common.truth.Truth;
import org.junit.Test;

public class MyCalendarThreeTest {
    @Test
    public void example() {
        MyCalendarThree cal = new MyCalendarThree();
        Truth.assertThat(cal.book(10, 20)).isEqualTo(1);
        Truth.assertThat(cal.book(50, 60)).isEqualTo(1);
        Truth.assertThat(cal.book(10, 40)).isEqualTo(2);
        Truth.assertThat(cal.book(5, 15)).isEqualTo(3);
        Truth.assertThat(cal.book(5, 10)).isEqualTo(3);
        Truth.assertThat(cal.book(25, 55)).isEqualTo(3);
    }
}
