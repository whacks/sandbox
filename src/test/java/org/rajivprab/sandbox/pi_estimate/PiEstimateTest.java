package org.rajivprab.sandbox.pi_estimate;

import com.google.common.truth.Truth;
import org.junit.Test;

public class PiEstimateTest {
    @Test
    public void shouldBeApproximatelyCorrect() {
        double estimate = PiEstimate.estimate(1000000);
        Truth.assertThat(estimate).isGreaterThan(3.13);
        Truth.assertThat(estimate).isLessThan(3.15);
    }

    @Test
    public void runMain() {
        PiEstimate.main();
    }
}
