package org.rajivprab.sandbox.topo_sort;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.rajivprab.sandbox.topo_sort.TopologicalSortKahns.Node;

import java.util.Collections;
import java.util.List;

public class TopologicalSortTest {
    private static final Logger log = LogManager.getLogger(TopologicalSortTest.class);

    @Test
    public void basic() {
        Node one = new TopologicalSortKahns.Node("1");
        Node two = new TopologicalSortKahns.Node("2a");
        Node three = new TopologicalSortKahns.Node("2b");
        Node four = new TopologicalSortKahns.Node("3");

        two.dependencies.add(one);
        three.dependencies.add(one);

        four.dependencies.add(two);
        four.dependencies.add(three);

        List<Node> nodes = Lists.newArrayList(one, two, three, four);
        Collections.shuffle(nodes);
        List<Node> ordered = TopologicalSortKahns.topologicalSort(nodes);

        log.info("Ordered nodes:\n{}", Joiner.on("\n").join(ordered));
    }

    @Test
    @Ignore
    public void loop_notHandled_loopsForever() {
        Node one = new TopologicalSortKahns.Node("1");
        Node two = new TopologicalSortKahns.Node("2");
        Node three = new TopologicalSortKahns.Node("3");

        two.dependencies.add(one);
        two.dependencies.add(three);
        three.dependencies.add(two);

        List<Node> nodes = Lists.newArrayList(one, two, three);
        Collections.shuffle(nodes);
        TopologicalSortKahns.topologicalSort(nodes);
    }
}
