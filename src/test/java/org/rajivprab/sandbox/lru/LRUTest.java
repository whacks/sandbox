package org.rajivprab.sandbox.lru;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class LRUTest {
    @Test
    public void populateAndGetSingleEntry() {
        LRU<Integer, String> lru = new LRU<>(1);
        lru.put(1, "one");
        assertThat(lru.get(1)).isEqualTo("one");
    }

    @Test
    public void singleEviction_withInsert() {
        LRU<Integer, String> lru = new LRU<>(1);
        lru.put(1, "one");
        assertThat(lru.get(1)).isEqualTo("one");

        lru.put(2, "two");
        assertThat(lru.get(2)).isEqualTo("two");
        assertThat(lru.get(1)).isNull();
    }

    @Test
    public void noEvictions_getNotPresent() {
        LRU<Integer, String> lru = new LRU<>(1);
        lru.put(1, "one");

        assertThat(lru.get(2)).isNull();
        assertThat(lru.get(1)).isEqualTo("one");
    }

    @Test
    public void getShouldUpdateLru() {
        LRU<Integer, String> lru = new LRU<>(2);
        lru.put(1, "one");
        lru.put(2, "two");
        lru.get(1);

        lru.put(3, "three");

        assertThat(lru.get(2)).isNull();
        assertThat(lru.get(1)).isEqualTo("one");
        assertThat(lru.get(3)).isEqualTo("three");
    }

    @Test
    public void putShouldUpdateLru() {
        LRU<Integer, String> lru = new LRU<>(2);
        lru.put(1, "one");
        lru.put(2, "two");
        lru.put(1, "one!");

        lru.put(3, "three");

        assertThat(lru.get(2)).isNull();
        assertThat(lru.get(1)).isEqualTo("one!");
        assertThat(lru.get(3)).isEqualTo("three");
    }
}
