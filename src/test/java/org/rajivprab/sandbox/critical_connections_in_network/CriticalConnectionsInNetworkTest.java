package org.rajivprab.sandbox.critical_connections_in_network;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.List;
import java.util.Set;

public class CriticalConnectionsInNetworkTest {
    @Test
    public void example1() {
        Set<Set<Integer>> result = solve(4, ImmutableList.of(
                ImmutableList.of(0, 1),
                ImmutableList.of(1, 2),
                ImmutableList.of(0, 2),

                ImmutableList.of(1, 3)
        ));
        Truth.assertThat(result).containsExactly(ImmutableSet.of(3, 1));
        Truth.assertThat(result).hasSize(1);
    }

    @Test
    public void multipleConnections() {
        Set<Set<Integer>> result = solve(5, ImmutableList.of(
                ImmutableList.of(0, 1),
                ImmutableList.of(1, 2),
                ImmutableList.of(0, 2),

                ImmutableList.of(1, 3),

                ImmutableList.of(3, 4)
        ));
        Truth.assertThat(result).containsExactly(
                ImmutableSet.of(1, 3),
                ImmutableSet.of(3, 4));
        Truth.assertThat(result).hasSize(2);
    }

    @Test
    public void multipleClusters() {
        Set<Set<Integer>> result = solve(6, ImmutableList.of(
                ImmutableList.of(0, 1),
                ImmutableList.of(1, 2),
                ImmutableList.of(0, 2),

                ImmutableList.of(1, 3),

                ImmutableList.of(3, 4),
                ImmutableList.of(4, 5),
                ImmutableList.of(3, 5)
        ));
        Truth.assertThat(result).containsExactly(ImmutableSet.of(1, 3));
        Truth.assertThat(result).hasSize(1);
    }

    @Test
    public void noneCritical() {
        Set<Set<Integer>> result = solve(6, ImmutableList.of(
                ImmutableList.of(0, 1),
                ImmutableList.of(1, 2),
                ImmutableList.of(0, 2),

                ImmutableList.of(1, 3),
                ImmutableList.of(0, 5),

                ImmutableList.of(3, 4),
                ImmutableList.of(4, 5),
                ImmutableList.of(3, 5)
        ));
        Truth.assertThat(result).isEmpty();
    }

    private static Set<Set<Integer>> solve(int n, List<List<Integer>> connections) {
        Set<Set<Integer>> mySolution = CriticalConnectionsInNetwork.criticalConnections(n, connections);
        Set<Set<Integer>> idealSolution = IdealSolution.criticalConnections(n, connections);
        Truth.assertThat(mySolution).isEqualTo(idealSolution);
        return mySolution;
    }
}
