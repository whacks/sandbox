package org.rajivprab.sandbox.kth_highest_binary_tree;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.junit.Test;
import org.rajivprab.cava.Validatec;

import java.util.*;

public class RankFinderTest {
    private static final Random RNG = new Random();

    @Test
    public void brainDeadTest() {
        Node tree = new Node(10);
        tree.put(5);
        tree.put(15);
        tree.put(4);
        tree.put(16);
        Truth.assertThat(RankFinder.getRankedEntry(1, tree)).isEqualTo(16);
        Truth.assertThat(RankFinder.getRankedEntry(2, tree)).isEqualTo(15);
        Truth.assertThat(RankFinder.getRankedEntry(3, tree)).isEqualTo(10);
        Truth.assertThat(RankFinder.getRankedEntry(4, tree)).isEqualTo(5);
        Truth.assertThat(RankFinder.getRankedEntry(5, tree)).isEqualTo(4);
    }

    @Test
    public void randomTest() {
        List<Integer> list = buildRandomList();
        Node tree = buildTree(list);

        Collections.sort(list);
        Collections.reverse(list);

        for (int i=0; i<list.size(); i++) {
            int rank = i+1;
            int expectedResult = list.get(i);
            Truth.assertThat(RankFinder.getRankedEntry(rank, tree)).isEqualTo(expectedResult);
        }
    }

    @Test
    public void metaRandom() {
        for (int i=0; i<100; i++) {
            randomTest();
        }
    }

    private List<Integer> buildRandomList() {
        int size = 1 << RNG.nextInt(6);
        Set<Integer> orderedSet = new LinkedHashSet<>();
        for (int i=0; i<size; i++) {
            orderedSet.add(RNG.nextInt());
        }
        return Lists.newArrayList(orderedSet);
    }

    private Node buildTree(List<Integer> list) {
        Validatec.notEmpty(list);
        Node root = new Node(list.get(0));
        for (int i=1; i<list.size(); i++) {
            root.put(list.get(i));
        }
        return root;
    }
}
