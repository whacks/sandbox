package org.rajivprab.sandbox.n_queens;

import com.google.common.collect.ImmutableSet;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.Set;

public class NQueensTest {
    @Test
    public void four() {
        Set<Set<Cell>> result = NQueens.getSolutions(4);
        Set<Cell> solution1 = ImmutableSet.of(
                new Cell(0, 2),
                new Cell(1, 0),
                new Cell(2, 3),
                new Cell(3, 1));
        Set<Cell> solution2 = ImmutableSet.of(
                new Cell(0, 1),
                new Cell(1, 3),
                new Cell(2, 0),
                new Cell(3, 2));
        Truth.assertThat(result).containsExactly(solution1, solution2);
        Truth.assertThat(result).hasSize(2);
    }

    @Test
    public void one() {
        Set<Set<Cell>> result = NQueens.getSolutions(1);
        Set<Cell> solution1 = ImmutableSet.of(new Cell(0, 0));
        Truth.assertThat(result).containsExactly(solution1);
        Truth.assertThat(result).hasSize(1);
    }

    @Test
    public void two() {
        Set<Set<Cell>> result = NQueens.getSolutions(2);
        Truth.assertThat(result).isEmpty();
    }

    @Test
    public void three() {
        Set<Set<Cell>> result = NQueens.getSolutions(3);
        Truth.assertThat(result).isEmpty();
    }
}
