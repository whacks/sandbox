package org.rajivprab.sandbox.sliding_puzzle;

import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import com.google.common.truth.Truth;
import org.junit.Test;

public class SlidingPuzzleTest {
    @Test
    public void example1() {
        Table<Integer, Integer, Integer> board = board(1, 2, 3, 4, 0, 5);
        Truth.assertThat(SlidingPuzzle.minNumberOfMoves(board)).isEqualTo(1);
    }

    @Test
    public void example2() {
        Table<Integer, Integer, Integer> board = board(1, 2, 3, 5, 4, 0);
        Truth.assertThat(SlidingPuzzle.minNumberOfMoves(board)).isEqualTo(-1);
    }

    @Test
    public void example3() {
        Table<Integer, Integer, Integer> board = board(4, 1, 2, 5, 0, 3);
        Truth.assertThat(SlidingPuzzle.minNumberOfMoves(board)).isEqualTo(5);
    }

    @Test
    public void braindead() {
        Table<Integer, Integer, Integer> board = board(1, 2, 3, 4, 5, 0);
        Truth.assertThat(SlidingPuzzle.minNumberOfMoves(board)).isEqualTo(0);
    }

    private static Table<Integer, Integer, Integer> board(int... cells) {
        return ImmutableTable.<Integer, Integer, Integer>builder()
                      .put(0, 0, cells[0])
                      .put(0, 1, cells[1])
                      .put(0, 2, cells[2])
                      .put(1, 0, cells[3])
                      .put(1, 1, cells[4])
                      .put(1, 2, cells[5])
                      .build();
    }
}
