package org.rajivprab.sandbox.ratio_finder;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.List;

public class RatioFinderTest {
    private static final List<String> RATIOS = ImmutableList.of(
            "USD CAD 1.3",
            "Meter Feet 3.3",
            "USD AMD 485.2");

    @Test
    public void simple() {
        runTest("USD", "AMD", 485.2);
    }

    @Test
    public void reverse() {
        runTest("Feet", "Meter", 1/3.3);
    }

    @Test
    public void complex() {
        runTest("CAD", "AMD", 485.2 / 1.3);
    }

    @Test
    public void notFound() {
        Truth.assertThat(RatioFinder.getRatio("USD", "Meter", RATIOS)).isNaN();
    }

    @Test
    public void badInput() {
        Truth.assertThat(RatioFinder.getRatio("Rajiv", "Meter", RATIOS)).isNaN();
    }

    private static void runTest(String a, String b, double expected) {
        Truth.assertThat(RatioFinder.getRatio(a, b, RATIOS)).isWithin(1E-10).of(expected);
    }
}
