package org.rajivprab.sandbox.palindrome_subsequence;

import com.google.common.truth.Truth;
import org.junit.Test;

/**
 * Created by rprabhakar on 2/21/16.
 */
public class FunPalindromeTest {
    // You can select A=aca and B=pmpmp. The product is 3 * 5 = 15, so we return 15.
    @Test
    public void case0() {
        Truth.assertThat(FunPalindrome.getMaxFunScore("acdapmpomp")).isEqualTo(15);
    }

    // You can select A=ababa and B=ekske. The product is 5 * 5 = 25, so we print 25. Another possible solution is A=ababa and B=ekqke which also yields the answer 25.
    @Test
    public void case1() {
        Truth.assertThat(FunPalindrome.getMaxFunScore("axbawbaseksqke")).isEqualTo(25);
    }

    // 'panap' and 'c' == 5 * 1
    @Test
    public void singleLifter() {
        Truth.assertThat(FunPalindrome.getMaxFunScore("panapc")).isEqualTo(5);
    }

    // panap is itself a palindrome, but there is no other subsequence for it to pair with, that doesn't intersect it
    // Hence, can only use 'p' and 'ana'
    @Test
    public void monopoly() {
        Truth.assertThat(FunPalindrome.getMaxFunScore("panap")).isEqualTo(3);
    }

    @Test
    public void nothing() {
        Truth.assertThat(FunPalindrome.getMaxFunScore("p")).isEqualTo(1);
    }

    @Test
    public void stillNothing() {
        Truth.assertThat(FunPalindrome.getMaxFunScore("pxn")).isEqualTo(1);
    }
}
