package org.rajivprab.sandbox.sudoku_solver.ver2019;

import org.junit.Test;

import java.util.Random;

public class SudokuTest {
    private static final Random RNG = new Random();

    @Test
    public void solveBlankBoard() {
        Sudoku.solve(Board.blank());
    }

    @Test
    public void randomSeed() {
        Board board = Board.blank();
        board.fill(RNG.nextInt(9) + 1, RNG.nextInt(9) + 1, RNG.nextInt(9) + 1);
        Sudoku.solve(board);
    }

    @Test
    public void massTest() {
        for (int i=0; i<100; i++) {
            randomSeed();
        }
    }
}
