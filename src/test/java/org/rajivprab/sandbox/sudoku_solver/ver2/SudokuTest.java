package org.rajivprab.sandbox.sudoku_solver.ver2;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests using the files in resources/sudoku
 *
 * Created by rajivprab on 8/10/16.
 */
public class SudokuTest {
    @Test
    public void runEmptyBoard() {
        assertThat(Sudoku.solve(new Board())).isNotNull();
    }

    @Test
    public void parseAndSolveEmptyBoard() {
        assertThat(getSolvedBoard("sudoku/empty_board.txt")).isNotNull();
    }

    @Test
    public void solveHardestProblem() {
        Board solved = getSolvedBoard("sudoku/telegraph_worlds_hardest.txt");
        assertThat(solved.boardIsSolved()).isTrue();
        assertThat(solved.getValue(0, 0)).isEquivalentAccordingToCompareTo(Value.EIGHT);
        assertThat(solved.getValue(1, 2)).isEquivalentAccordingToCompareTo(Value.THREE);
        assertThat(solved.getValue(7, 7)).isEquivalentAccordingToCompareTo(Value.ONE);
    }

    @Test
    public void solveJibbleImpossible() {
        assertThat(getSolvedBoard("sudoku/jibble_impossible.txt")).isNull();
    }

    private static Board getSolvedBoard(String path) {
        return Sudoku.solve(getInputBoard(path));
    }

    private static Board getInputBoard(String path) {
        return InputReader.genBoard(ClassLoader.getSystemResourceAsStream(path));
    }
}
