package org.rajivprab.sandbox.earliest_full_bloom;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.List;

public class EarliestFullBloomTest {
    @Test
    public void example1() {
        List<Integer> plantTime = ImmutableList.of(1, 4, 3);
        List<Integer> growTime = ImmutableList.of(2, 3, 1);
        Truth.assertThat(EarliestFullBloom.solve(plantTime, growTime)).isEqualTo(9);
    }

    @Test
    public void example2() {
        List<Integer> plantTime = ImmutableList.of(1, 2, 3, 2);
        List<Integer> growTime = ImmutableList.of(2, 1, 2, 1);
        Truth.assertThat(EarliestFullBloom.solve(plantTime, growTime)).isEqualTo(9);
    }

    @Test
    public void example3() {
        List<Integer> plantTime = ImmutableList.of(1);
        List<Integer> growTime = ImmutableList.of(1);
        Truth.assertThat(EarliestFullBloom.solve(plantTime, growTime)).isEqualTo(2);
    }
}
