package org.rajivprab.sandbox.kth_min_path;

import com.google.common.truth.Truth;
import org.junit.Test;

/**
 * Created by rprabhakar on 2/19/16.
 */
public class KthMinPathTest {
    @Test
    public void test1() {
        String[] testInput = {
                "2 2 2",
                "2 2 3"
        };
        String[] output = KthMinPath.findPaths(testInput);
        Truth.assertThat(output).asList().containsExactly("HVVH", "VHHV");
    }
}
