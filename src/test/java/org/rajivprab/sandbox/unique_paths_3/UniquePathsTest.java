package org.rajivprab.sandbox.unique_paths_3;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.List;

public class UniquePathsTest {
    @Test
    public void example1() {
        List<Integer> row0 = Lists.newArrayList(1,0,0,0);
        List<Integer> row1 = Lists.newArrayList(0,0,0,0);
        List<Integer> row2 = Lists.newArrayList(0,0,2,-1);
        List<List<Integer>> map = Lists.newArrayList(row0, row1, row2);
        Truth.assertThat(UniquePaths.getNumPaths(map)).isEqualTo(2);
    }

    @Test
    public void example2() {
        List<Integer> row0 = Lists.newArrayList(1,0,0,0);
        List<Integer> row1 = Lists.newArrayList(0,0,0,0);
        List<Integer> row2 = Lists.newArrayList(0,0,0,2);
        List<List<Integer>> map = Lists.newArrayList(row0, row1, row2);
        Truth.assertThat(UniquePaths.getNumPaths(map)).isEqualTo(4);
    }

    @Test
    public void example3() {
        List<Integer> row0 = Lists.newArrayList(0,1);
        List<Integer> row1 = Lists.newArrayList(2,0);
        List<List<Integer>> map = Lists.newArrayList(row0, row1);
        Truth.assertThat(UniquePaths.getNumPaths(map)).isEqualTo(0);
    }
}
