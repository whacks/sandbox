package org.rajivprab.sandbox.stock_returns;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class StockReturnsPredictionsIT {
    @Test
    public void computeProjectedReturns_forYear() {
        double projection = StockReturnsPredictions.projectedReturns(1985);
        // Earnings yield ~= 1/12 = 8.3%
        // GDP Growth ~= 0.3 / 4.03 = 7.4%
        // Projection ~= 15.7%
        assertThat(projection).isWithin(0.01).of(1.157);
    }

    @Test
    public void computeAnnualizedReturns_singleYear() {
        double projection = StockReturnsPredictions.projectedAnnualizedReturns(1985, 1986);
        assertThat(projection).isWithin(0.01).of(1.157);
    }

    @Test
    public void actualPortfolioValue_singleYear() {
        double projection = StockReturnsPredictions.actualPortfolioValue(1985, 1986);
        assertThat(projection).isWithin(0.001).of(1.264);
    }

    @Test
    public void getCorrelation_singleYear() {
        double correlation = StockReturnsPredictions.getCorrelation(1985, 1986);
        // expectedCorrelation = 26.4 / 15.7 = 1.6815
        assertThat(correlation).isWithin(0.01).of(1.68);
    }

    // -------------------------------

    @Test
    public void computeAnnualizedReturns_multipleYears() {
        double projection = StockReturnsPredictions.projectedAnnualizedReturns(1985, 1988);
        assertThat(projection).isWithin(0.01).of(1.133);
    }

    @Test
    public void actualPortfolioValue_multipleYears() {
        double projection = StockReturnsPredictions.actualPortfolioValue(1985, 1988);
        assertThat(projection).isWithin(0.001).of(1.621);
    }

    @Test
    public void getCorrelation_multipleYears() {
        double correlation = StockReturnsPredictions.getCorrelation(1985, 1988);
        // expectedCorrelation = 17.5 / 13.3 = 1.316
        assertThat(correlation).isWithin(0.01).of(1.316);
    }
}
