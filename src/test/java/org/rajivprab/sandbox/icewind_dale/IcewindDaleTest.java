package org.rajivprab.sandbox.icewind_dale;

import com.google.common.truth.Truth;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

/**
 * Unit tests and demo
 *
 * Created by rajivprab on 8/23/17.
 */
public class IcewindDaleTest {
    private static final double TOLERANCE = 0.0000001;
    private static final Logger log = LogManager.getLogger(IcewindDaleTest.class);

    @Test
    public void baseCase() {
        Truth.assertThat(IcewindDale.getExpectedRecursive(0)).isZero();
    }

    @Test
    public void nextToBaseCase() {
        Truth.assertThat(IcewindDale.getExpectedRecursive(1)).isWithin(TOLERANCE).of(0.5);
    }

    @Test
    public void upToTwo() {
        double oddsFirstRollGetting1or2 = 2.0 / 3;
        double expectedValueAcceptedOnFirstRole = 1.5;
        double expectedValue = oddsFirstRollGetting1or2 * expectedValueAcceptedOnFirstRole
                + (1 - oddsFirstRollGetting1or2) * 0.5;
        Truth.assertThat(IcewindDale.getExpectedRecursive(2)).isWithin(TOLERANCE).of(expectedValue);
    }

    @Test
    public void rollFrom0to99() {
        log.info("Average result when rolling a dice numbered from 0-99: " + IcewindDale.getExpectedRecursive(99));
    }
}
