package org.rajivprab.sandbox.num_max_arrays;

import com.google.common.truth.Truth;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class NumMaxArraysTest {
    private static final Logger log = LogManager.getLogger(NumMaxArraysTest.class);

    @Test
    public void example1() {
        test(2, 3, 1, 6);
    }

    @Test
    public void example2() {
        test(5, 2, 3, 0);
    }

    @Test
    public void example3() {
        test(9, 1, 1, 1);
    }

    @Test
    public void example4() {
        test(50, 100, 25, 34549172);
    }

    @Test
    public void example5() {
        test(37, 17, 7, 418930126);
    }

    private static void test(int numElements, int maxValue, int searchCost, int expectedResult) {
        long answer = NumMaxArrays.numOfArrays(numElements, maxValue, searchCost);
        // Solver.cache.entrySet().forEach(e -> log.info("Input: " + e.getKey() + ", Answer: " + e.getValue()));
        Truth.assertThat(answer).isEqualTo(expectedResult);
    }
}
