package org.rajivprab.sandbox.fancy_sequence;

import com.google.common.truth.Truth;
import org.junit.Test;

public class FancySequenceTest {
    @Test
    public void example1() {
        FancySequence instance = new FancySequence();
        instance.append(2);
        instance.addAll(3);
        instance.append(7);
        instance.multAll(2);

        Truth.assertThat(instance.getIndex(0)).isEqualTo(10);
        Truth.assertThat(instance.getIndex(1)).isEqualTo(14);
        Truth.assertThat(instance.getIndex(2)).isEqualTo(-1);
        Truth.assertThat(instance.getIndex(3)).isEqualTo(-1);

        instance.addAll(3);
        instance.append(10);
        instance.multAll(2);

        Truth.assertThat(instance.getIndex(0)).isEqualTo(26);
        Truth.assertThat(instance.getIndex(1)).isEqualTo(34);
        Truth.assertThat(instance.getIndex(2)).isEqualTo(20);
        Truth.assertThat(instance.getIndex(3)).isEqualTo(-1);

        instance.append(42);

        Truth.assertThat(instance.getIndex(0)).isEqualTo(26);
        Truth.assertThat(instance.getIndex(1)).isEqualTo(34);
        Truth.assertThat(instance.getIndex(2)).isEqualTo(20);
        Truth.assertThat(instance.getIndex(3)).isEqualTo(42);
    }

    @Test
    public void example1_optimal() {
        FancySequenceOptimal instance = new FancySequenceOptimal();
        instance.append(2);
        instance.addAll(3);
        instance.append(7);
        instance.multAll(2);

        Truth.assertThat(instance.getIndex(0)).isEqualTo(10);
        Truth.assertThat(instance.getIndex(1)).isEqualTo(14);
        Truth.assertThat(instance.getIndex(2)).isEqualTo(-1);
        Truth.assertThat(instance.getIndex(3)).isEqualTo(-1);

        instance.addAll(3);
        instance.append(10);
        instance.multAll(2);

        Truth.assertThat(instance.getIndex(0)).isEqualTo(26);
        Truth.assertThat(instance.getIndex(1)).isEqualTo(34);
        Truth.assertThat(instance.getIndex(2)).isEqualTo(20);
        Truth.assertThat(instance.getIndex(3)).isEqualTo(-1);

        instance.append(42);

        Truth.assertThat(instance.getIndex(0)).isEqualTo(26);
        Truth.assertThat(instance.getIndex(1)).isEqualTo(34);
        Truth.assertThat(instance.getIndex(2)).isEqualTo(20);
        Truth.assertThat(instance.getIndex(3)).isEqualTo(42);
    }
}
