package org.rajivprab.sandbox.proto;

import com.google.common.truth.Truth;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.rajivprab.sandbox.proto.address_book.PersonAddressBook.AddressBook;
import org.rajivprab.sandbox.proto.address_book.PersonAddressBook.Person;

public class AddressBookTest {
    private static final Logger log = LogManager.getLogger(AddressBookTest.class);

    @Test
    public void serializeAndDerialize_shouldGetBackOriginal() throws InvalidProtocolBufferException {
        Person personF = Person.newBuilder().setName("Yamuna").setId(2013).build();
        Person personM = Person.newBuilder().setName("Sudha").setId(2023).build();
        AddressBook addressBook = AddressBook.newBuilder()
                                             .addPeople(personF)
                                             .addPeople(personM)
                                             .build();

        ByteString Fserialized = personF.toByteString();
        ByteString Mserialized = personM.toByteString();
        ByteString addressBookSerialized = addressBook.toByteString();

        log.info("PersonF:\n" + Fserialized);
        log.info("PersonM:\n" + Mserialized);
        log.info("AddressBook:\n" + addressBookSerialized);

        Truth.assertThat(Person.parseFrom(Fserialized)).isEqualTo(personF);
        Truth.assertThat(Person.parseFrom(Mserialized)).isEqualTo(personM);
        Truth.assertThat(AddressBook.parseFrom(addressBookSerialized)).isEqualTo(addressBook);
    }
}
