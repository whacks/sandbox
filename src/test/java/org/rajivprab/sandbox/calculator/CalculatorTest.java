package org.rajivprab.sandbox.calculator;

import com.google.common.truth.Truth;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void number() {
        Truth.assertThat(num(4.9).evaluate()).isEqualTo(4.9);
    }

    @Test
    public void sum() {
        Truth.assertThat(new SumNode(num(1), num(1.5)).evaluate()).isEqualTo(2.5);
    }

    @Test
    public void divide() {
        Truth.assertThat(DivideNode.build(num(1.5), num(2.0)).evaluate()).isEqualTo(0.75);
    }

    @Test
    public void complex() {
        Node mult = new MultiplyNode(num(6), num(4));
        Node plus = new SumNode(mult, num(3));
        Node minus = SubtractNode.build(num(5), num(3));
        Node divide = DivideNode.build(plus, minus);

        Truth.assertThat(divide.evaluate()).isEqualTo(13.5);
    }

    private static NumberNode num(double val) {
        return new NumberNode(val);
    }
}
