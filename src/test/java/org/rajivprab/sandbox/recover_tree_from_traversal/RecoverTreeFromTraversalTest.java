package org.rajivprab.sandbox.recover_tree_from_traversal;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.List;
import java.util.Queue;

public class RecoverTreeFromTraversalTest {
    @Test
    public void getDepthValues() {
        List<Pair<Integer, Integer>> result =
                RecoverTreeFromTraversal.getDepthValues("1-2--3--4-5--6--7");
        Truth.assertThat(result).hasSize(7);
        Truth.assertThat(result).containsExactly(
                Pair.of(0, 1),
                Pair.of(1, 2),
                Pair.of(2, 3),
                Pair.of(2, 4),
                Pair.of(1, 5),
                Pair.of(2, 6),
                Pair.of(2, 7));
    }

    @Test
    public void example1() {
        runTest("1-2--3--4-5--6--7", "1,2,5,3,4,6,7");
    }

    @Test
    public void example2() {
        runTest("1-2--3---4-5--6---7", "1,2,5,3,null,6,null,4,null,7");
    }

    @Test
    public void example3() {
        runTest("1-401--349---90--88", "1,401,null,349,88,90");
    }

    private static String stripTrailingNull(String output) {
        while (output.endsWith(",null")) {
            output = output.substring(0, output.length() - 5);
        }
        return output;
    }

    private static String printTreeBreadthFirstWithNulls(Node root) {
        Queue<Node> queue = Lists.newLinkedList();
        queue.add(root);
        StringBuilder result = new StringBuilder();
        while (!queue.isEmpty()) {
            Node entry = queue.poll();
            if (result.length() != 0) { result.append(","); }
            if (entry == null) {
                result.append("null");
            } else {
                result.append(entry.value);
                queue.add(entry.leftChild);
                queue.add(entry.rightChild);
            }
        }
        return result.toString();
    }

    private static int getMaxDepth(Node root, int curDepth) {
        int leftDepth = -1;
        int rightDepth = -1;
        if (root.leftChild != null) {
            leftDepth = getMaxDepth(root.leftChild, curDepth + 1);
        }
        if (root.rightChild != null) {
            rightDepth = getMaxDepth(root.rightChild, curDepth + 1);
        }
        return Math.max(leftDepth, Math.max(rightDepth, curDepth));
    }

    private static void runTest(String input, String output) {
        Truth.assertThat(
                stripTrailingNull(
                        printTreeBreadthFirstWithNulls(
                                RecoverTreeFromTraversal.recover(input))))
             .isEqualTo(output);
    }
}
