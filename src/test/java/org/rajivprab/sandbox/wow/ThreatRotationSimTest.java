package org.rajivprab.sandbox.wow;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.rajivprab.sandbox.wow.Strategy.*;

public class ThreatRotationSimTest {
    private static final Logger log = LogManager.getLogger(ThreatRotationSimTest.class);
    private static final double TIME_LIMIT = 2 * 60;

    @Test
    public void perfectSlam() {
        run(new PerfectSlam());
    }

    @Test
    public void perfectRevenge() {
        run(new PerfectRevenge());
    }

    @Test
    public void revengeFirst() {
        run(new RevengeFirst());
    }

    @Test
    public void shieldSlamFirst() {
        run(new ShieldSlamFirst());
    }

    @Test
    public void slamSunderOnly() {
        run(new SlamSunderOnly());
    }

    @Test
    public void sunderOnly() {
        run(new SunderOnly());
    }

    private static void run(Strategy strategy) {
        double tps = ThreatRotationSim.getTps(strategy, TIME_LIMIT);
        log.info("TPS from " + strategy.getClass().getSimpleName() + " is " + tps);

        double tpsPerRage = ThreatRotationSim.getTpsPerRage(strategy, TIME_LIMIT);
        log.info("TPS/rage from " + strategy.getClass().getSimpleName() + " is " + tpsPerRage);
    }
}
