package org.rajivprab.sandbox.chery_picker_2;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;
import org.rajivprab.sandbox.cherry_picker_2.CherryPicker;

import java.util.List;

public class CherryPickerTest {
    @Test
    public void example1() {
        List<List<Integer>> grid = ImmutableList.of(
                ImmutableList.of(3, 1, 1),
                ImmutableList.of(2, 5, 1),
                ImmutableList.of(1, 5, 5),
                ImmutableList.of(2, 1, 1));
        Truth.assertThat(CherryPicker.getMaxScore(grid)).isEqualTo(24);
    }

    @Test
    public void example2() {
        List<List<Integer>> grid = ImmutableList.of(
                ImmutableList.of(1, 0, 0, 0, 0, 0, 1),
                ImmutableList.of(2, 0, 0, 0, 0, 3, 0),
                ImmutableList.of(2, 0, 9, 0, 0, 0, 0),
                ImmutableList.of(0, 3, 0, 5, 4, 0, 0),
                ImmutableList.of(1, 0, 2, 3, 0, 0, 6));
        Truth.assertThat(CherryPicker.getMaxScore(grid)).isEqualTo(28);
    }

    @Test
    public void example3() {
        List<List<Integer>> grid = ImmutableList.of(
                ImmutableList.of(1, 0, 0, 3),
                ImmutableList.of(0, 0, 0, 3),
                ImmutableList.of(0, 0, 3, 3),
                ImmutableList.of(9, 0, 3, 3));
        Truth.assertThat(CherryPicker.getMaxScore(grid)).isEqualTo(22);
    }

    @Test
    public void example4() {
        List<List<Integer>> grid = ImmutableList.of(
                ImmutableList.of(1, 1),
                ImmutableList.of(1, 1));
        Truth.assertThat(CherryPicker.getMaxScore(grid)).isEqualTo(4);
    }
}
