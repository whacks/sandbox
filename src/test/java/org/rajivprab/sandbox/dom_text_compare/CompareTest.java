package org.rajivprab.sandbox.dom_text_compare;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class CompareTest {
    @Test
    public void emptyHtml_shouldReturnTrue() {
        assertThat(Compare.textEquals(getNodeWithEmptyChild(), getNodeWithEmptyChild())).isTrue();
    }

    @Test
    public void emptyHtml_compareWithBlankHtml_shouldReturnTrue() {
        assertThat(Compare.textEquals(getNodeWithEmptyChild(), HtmlNode.buildNode(""))).isTrue();
    }

    @Test
    public void emptyHtml_compareWithNonEmpty_shouldReturnFalse() {
        assertThat(Compare.textEquals(getNodeWithEmptyChild(), HtmlNode.buildNode("a"))).isFalse();
    }

    @Test
    public void sameTextButDifferentOrder_shouldReturnTrue() {
        HtmlNode abc_def_hij = HtmlNode.buildNode(HtmlNode.buildNode("abc"),
                                              HtmlNode.buildNode("def"),
                                              HtmlNode.buildNode("hij"));
        HtmlNode abcd_efh_ij = HtmlNode.buildNode(HtmlNode.buildNode("abcd"),
                                                  HtmlNode.buildNode("efh"),
                                                  HtmlNode.buildNode("ij"));
        assertThat(Compare.textEquals(abc_def_hij, abcd_efh_ij)).isTrue();
    }

    private static HtmlNode getNodeWithEmptyChild() {
        return HtmlNode.buildNode(HtmlNode.buildNode());
    }
}
