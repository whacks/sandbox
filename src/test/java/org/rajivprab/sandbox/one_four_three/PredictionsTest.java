package org.rajivprab.sandbox.one_four_three;

import org.junit.Test;

import java.util.LinkedList;

import static com.google.common.truth.Truth.assertThat;

public class PredictionsTest {
    @Test
    public void sampleInput() {
        LinkedList<Integer> list = new LinkedList<>();
        list.add(3);
        list.add(4);
        assertThat(Predictions.getLetterCombinations(list))
                .containsExactly("dg", "dh", "di", "eg", "eh", "ei", "fg", "fh", "fi");
    }
}
