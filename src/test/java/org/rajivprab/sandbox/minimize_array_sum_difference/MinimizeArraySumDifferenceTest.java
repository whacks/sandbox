package org.rajivprab.sandbox.minimize_array_sum_difference;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

public class MinimizeArraySumDifferenceTest {
    @Test
    public void example1() {
        Truth.assertThat(compute(3, 9, 7, 3)).isEqualTo(2);
    }

    @Test
    public void example2() {
        Truth.assertThat(compute(-36, 36)).isEqualTo(72);
    }

    @Test
    public void example3() {
        Truth.assertThat(compute(2, -1, 0, 4, -2, -9)).isEqualTo(0);
    }

    @Test
    public void bigExample1() {
        Truth.assertThat(compute(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)).isEqualTo(1);
    }

    @Test
    public void bigExample2() {
        Truth.assertThat(compute(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)).isEqualTo(0);
    }

    private static int compute(Integer... nums) {
        // return MinimizeArraySumDifferenceNaive.minimumDifference(ImmutableList.copyOf(nums));
        return MinimizeArraySumDifferenceMeetInMiddle.minimumDifference(ImmutableList.copyOf(nums));
    }
}
