package org.rajivprab.sandbox.fibonacci_sum

import org.scalatest.flatspec.AnyFlatSpec

class FibonacciSpec extends AnyFlatSpec {
  "Fibonacci" should "produce 1 on index 1" in {
    assert(Fibonacci.getIndex(1) == 1)
  }

  it should "produce 2 on index 2" in {
    assert(Fibonacci.getIndex(2) == 2)
  }

  it should "produce 5 on index 3" in {
    assert(Fibonacci.getIndex(4) == 5)
  }

  it should "produce 5 on index 30" in {
    assert(Fibonacci.getIndex(30) == 1346269)
  }

  it should "produce 5 on euler-question" in {
    assert(Main.solve() == 4613732)
  }
}
