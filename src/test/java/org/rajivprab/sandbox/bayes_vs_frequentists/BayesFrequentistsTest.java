package org.rajivprab.sandbox.bayes_vs_frequentists;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.junit.Test;

public class BayesFrequentistsTest {
    private static final Log log = LogFactory.getLog(BayesFrequentists.class);

    @Test
    public void runMain() {
        BayesFrequentists.main();
    }

    @Test
    public void normalDistributionPercentiles() {
        NormalDistribution distribution = new NormalDistribution(0.5, 0.2);
        log.info("1st percentile is: " + distribution.inverseCumulativeProbability(0.01));
        log.info("10th percentile is: " + distribution.inverseCumulativeProbability(0.10));
        log.info("20th percentile is: " + distribution.inverseCumulativeProbability(0.20));
        log.info("30th percentile is: " + distribution.inverseCumulativeProbability(0.30));
        log.info("40th percentile is: " + distribution.inverseCumulativeProbability(0.40));
        log.info("50th percentile is: " + distribution.inverseCumulativeProbability(0.50));
        log.info("60th percentile is: " + distribution.inverseCumulativeProbability(0.60));
        log.info("70th percentile is: " + distribution.inverseCumulativeProbability(0.70));
        log.info("80th percentile is: " + distribution.inverseCumulativeProbability(0.80));
        log.info("90th percentile is: " + distribution.inverseCumulativeProbability(0.90));
        log.info("99th percentile is: " + distribution.inverseCumulativeProbability(0.99));
    }
}
