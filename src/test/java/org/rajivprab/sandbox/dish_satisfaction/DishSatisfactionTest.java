package org.rajivprab.sandbox.dish_satisfaction;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

public class DishSatisfactionTest {
    @Test
    public void example1() {
        runTest(14, -1, -8, 0, 5, -9);
    }

    @Test
    public void example2() {
        runTest(20, 4, 3, 2);
    }

    @Test
    public void example3() {
        runTest(0, -1, -4, -5);
    }

    @Test
    public void example4() {
        runTest(35, -2, 5, -1, 0, 3, -3);
    }

    private static void runTest(int expectedOutput, Integer... dishSatisfaction) {
        Truth.assertThat(DishSatisfaction.maxSatisfaction(ImmutableList.copyOf(dishSatisfaction)))
             .isEqualTo(expectedOutput);
    }
}
