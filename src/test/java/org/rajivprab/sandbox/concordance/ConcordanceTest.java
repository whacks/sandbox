package org.rajivprab.sandbox.concordance;

import com.google.common.truth.Truth;
import org.junit.Test;

/**
 * Unit tests for the concordance
 *
 * Created by rprabhakar on 4/2/16.
 */
public class ConcordanceTest {
    @Test(expected = IllegalArgumentException.class)
    public void foundCommaInEntry() {
        new Concordance.Entry("word,");
    }

    @Test(expected = IllegalArgumentException.class)
    public void foundSpaceInEntry() {
        new Concordance.Entry("word ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void foundUpperCaseInEntry() {
        new Concordance.Entry("Word");
    }

    @Test(expected = IllegalArgumentException.class)
    public void foundNewLineInEntry() {
        new Concordance.Entry("word\n");
    }

    @Test
    public void shouldParseProblemText() {
        String text = "Given an 5 arbitrary text document written in English, write a program that will generate a\n" +
                "concordance , i.e. an\talphabetical list of all 123 word occurrences, labeled with word frequencies!\n" +
                "Bonus: label each word with the sentence numbers in which each occurrence appeared.";
        Concordance concordance = new Concordance(text);
        concordance.printConcordance();
        Truth.assertThat(concordance.concordance.get("a").getSentences()).containsExactly(1, 1);
        Truth.assertThat(concordance.concordance.get("an").getSentences()).containsExactly(1, 1);
        Truth.assertThat(concordance.concordance.get("concordance").getSentences()).containsExactly(1);
        Truth.assertThat(concordance.concordance.get("bonus").getSentences()).containsExactly(2);
        Truth.assertThat(concordance.concordance.get("word").getSentences()).containsExactly(1, 1, 2);
        Truth.assertThat(concordance.concordance.get("i.e.").getSentences()).containsExactly(1);
        Truth.assertThat(concordance.concordance.get("123")).isNull();
    }
}
