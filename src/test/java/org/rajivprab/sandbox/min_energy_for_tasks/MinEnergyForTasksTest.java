package org.rajivprab.sandbox.min_energy_for_tasks;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.javatuples.Pair;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

public class MinEnergyForTasksTest {
    @Test
    public void example1() {
        Truth.assertThat(runWithInputs(1, 2, 2, 4, 4, 8)).isEqualTo(8);
    }

    @Test
    public void example2() {
        Truth.assertThat(runWithInputs(1,3,2,4,10,11,10,12,8,9)).isEqualTo(32);
    }

    @Test
    public void example3() {
        Truth.assertThat(runWithInputs(1,7,2,8,3,9,4,10,5,11,6,12)).isEqualTo(27);
    }

    @Test
    public void tricky() {
        Truth.assertThat(runWithInputs(2, 6, 6, 9, 1, 1, 1, 6, 1, 3)).isEqualTo(12);
    }

    @Test
    public void extreme() {
        Truth.assertThat(runWithInputs(5, 10,
                                       1, 7)).isEqualTo(11);
    }

    private static int runWithInputs(Integer... value) {
        List<Pair<Integer, Integer>> actualAndMinEnergy = Lists.newArrayList();

        for (int i=0; i<value.length; i+=2) {
            actualAndMinEnergy.add(Pair.with(value[i], value[i+1]));
        }

        Collections.shuffle(actualAndMinEnergy);

        return compute(actualAndMinEnergy);
    }

    private static int compute(List<Pair<Integer, Integer>> actualAndMinEnergy) {
        return MinEnergyForTasksSimple.compute(actualAndMinEnergy);
    }
}
