package org.rajivprab.sandbox.flip_zero_matrix;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.List;

public class FlipZeroMatrixTest {
    @Test
    public void example1() {
        List<Boolean> row0 = ImmutableList.of(false, false);
        List<Boolean> row1 = ImmutableList.of(false, true);
        List<List<Boolean>> matrix = ImmutableList.of(row0, row1);
        Truth.assertThat(FlipZeroMatrix.minStepsNeeded(matrix)).isEqualTo(3);
    }

    @Test
    public void example2() {
        List<Boolean> row0 = ImmutableList.of(false);
        List<List<Boolean>> matrix = ImmutableList.of(row0);
        Truth.assertThat(FlipZeroMatrix.minStepsNeeded(matrix)).isEqualTo(0);
    }

    @Test
    public void example3() {
        List<Boolean> row0 = ImmutableList.of(true, true, true);
        List<Boolean> row1 = ImmutableList.of(true, false, true);
        List<Boolean> row2 = ImmutableList.of(false, false, false);
        List<List<Boolean>> matrix = ImmutableList.of(row0, row1, row2);
        Truth.assertThat(FlipZeroMatrix.minStepsNeeded(matrix)).isEqualTo(6);
    }

    @Test
    public void example4() {
        List<Boolean> row0 = ImmutableList.of(true, false, false);
        List<Boolean> row1 = ImmutableList.of(true, false, false);
        List<List<Boolean>> matrix = ImmutableList.of(row0, row1);
        Truth.assertThat(FlipZeroMatrix.minStepsNeeded(matrix)).isEqualTo(-1);
    }
}
