package org.rajivprab.sandbox.longest_substring;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.Iterator;

/**
 * Created by rajivprab on 12/21/16.
 */
public class LongestSubstringTest {
    @Test
    public void singleChar() {
        Truth.assertThat(LongestSubstring.getLongestSubstring(getStream("x"), 10)).isEqualTo("x");
    }

    @Test
    public void simple1char() {
        Truth.assertThat(LongestSubstring.getLongestSubstring(getStream("baaaaaaah"), 1)).isEqualTo("aaaaaaa");
    }

    @Test
    public void simple2chars() {
        Truth.assertThat(LongestSubstring.getLongestSubstring(getStream("bbaaaaaaah"), 2)).isEqualTo("bbaaaaaaa");
    }

    @Test
    public void simple2charsVariant2() {
        Truth.assertThat(LongestSubstring.getLongestSubstring(getStream("baaaaaaahh"), 2)).isEqualTo("aaaaaaahh");
    }

    @Test
    public void harder2Chars() {
        Truth.assertThat(LongestSubstring.getLongestSubstring(getStream("ababcbcbaaabbdef"), 2)).isEqualTo("baaabb");
    }

    @Test
    public void longInput() {
        Truth.assertThat(LongestSubstring.getLongestSubstring(getStream("abcdeffghijklll"), 6)).isEqualTo("ghijklll");
    }

    @Test
    public void longerInput() {
        Truth.assertThat(LongestSubstring.getLongestSubstring(getStream("abcdeffghijklllm"), 7)).isEqualTo("ffghijklll");
    }

    private static Iterator<Character> getStream(String input) {
        return Lists.charactersOf(input).iterator();
    }
}
