package org.rajivprab.sandbox.min_index_sum;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.List;

public class MinIndexSumTest {
    @Test
    public void example1() {
        List<String> one = ImmutableList.of("Sh", "Tap", "BK", "KFC");
        List<String> two = ImmutableList.of("Piatti", "Grill", "Steakhouse", "Sh");
        Truth.assertThat(MinIndexSum.getMinIndexes(one, two)).containsExactly("Sh");
    }

    @Test
    public void example2() {
        List<String> one = ImmutableList.of("Sh", "Tap", "BK", "KFC");
        List<String> two = ImmutableList.of("KFC", "Sh", "BK");
        Truth.assertThat(MinIndexSum.getMinIndexes(one, two)).containsExactly("Sh");
    }

    @Test
    public void example3() {
        List<String> one = ImmutableList.of("Sh", "Tap", "BK", "KFC");
        List<String> two = ImmutableList.of("KFC", "BK", "Tap", "Sh");
        Truth.assertThat(MinIndexSum.getMinIndexes(one, two)).containsExactly("BK", "KFC", "Sh", "Tap")
             .inOrder();
    }

    @Test
    public void example4() {
        List<String> one = ImmutableList.of("Sh", "Tap", "BK", "KFC");
        List<String> two = ImmutableList.of("KNN", "KFC", "BK", "Tap", "Sh");
        Truth.assertThat(MinIndexSum.getMinIndexes(one, two)).containsExactly("BK", "KFC", "Sh", "Tap")
             .inOrder();
    }
}
