package org.rajivprab.sandbox.separate_numbers;

import com.google.common.truth.Truth;
import org.junit.Test;

public class SeparateNumbersTest {

    /**
     * Explanation: You could have written down the numbers:
     * 3, 27
     * 327
     */
    @Test
    public void example1() {
        test("327", 2);
    }

    // Explanation: No numbers can have leading zeros and all numbers must be positive.
    @Test
    public void example2() {
        test("094", 0);
    }

    // Explanation: No numbers can have leading zeros and all numbers must be positive.
    @Test
    public void example3() {
        test("0", 0);
    }

    @Test
    public void all1s() {
        // 1, 1, 1, 1, 1, 1
        // 1, 1, 1, 1, 11
        // 1, 1, 1, 111
        // 1, 1, 11, 11
        // 1, 1, 1111
        // 1, 11, 111
        // 1, 11111
        // 11, 11, 11
        // 11, 1111
        // 111, 111
        // 111111
        test("111111", 11);
    }

    @Test
    public void decreasing() {
        // 9, 8765
        // 98, 765
        // 98765
        test("98765", 3);
    }

    private static void test(String num, int expected) {
        // Truth.assertThat(SeparateNumbersNaive.numberOfCombinations(num)).isEqualTo(expected);
        Truth.assertThat(SeparateNumbersBetter.numberOfCombinations(num)).isEqualTo(expected);
    }
}
