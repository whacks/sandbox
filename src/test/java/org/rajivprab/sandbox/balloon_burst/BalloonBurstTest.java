package org.rajivprab.sandbox.balloon_burst;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

public class BalloonBurstTest {
    @Test
    public void providedExample() {
        Truth.assertThat(BalloonBurst.getMax(ImmutableList.of(3, 1, 5, 8))).isEqualTo(167);
    }

    @Test
    public void providedExample_withOptimal() {
        Truth.assertThat(OptimalBalloonBurst.getMax(ImmutableList.of(3, 1, 5, 8))).isEqualTo(167);
    }
}
