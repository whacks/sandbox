package org.rajivprab.sandbox.multiples_3_5

import org.scalatest.flatspec.AnyFlatSpec

class MultiplesSpec extends AnyFlatSpec {
  "Multiples" should "be 23 for 3s and 5s under 10" in {
    assert(Main.sum(3, 5, 10) == 23)
  }

  "Multiples" should "be 233168 for 3s and 5s under 1000" in {
    assert(Main.sum(3, 5, 1000) == 233168)
  }
}
