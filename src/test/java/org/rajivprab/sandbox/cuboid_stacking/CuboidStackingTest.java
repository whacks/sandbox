package org.rajivprab.sandbox.cuboid_stacking;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

public class CuboidStackingTest {
    @Test
    public void example1() {
        runTest(190,
                50,45,20,
                95,37,53,
                45,23,12);
    }

    @Test
    public void example2() {
        runTest(76,
                38,25,45,
                76,35,3);
    }

    @Test
    public void example3() {
        runTest(102,
                7,11,17,
                7,17,11,
                11,7,17,
                11,17,7,
                17,7,11,
                17,11,7);
    }

    @Test
    public void oneCube_returnLength() {
        runTest(3,
                3,3,3);
    }

    @Test
    public void oneCuboid_returnLongestLength() {
        runTest(3,
                1,2,3);
    }

    @Test
    public void longColumn() {
        runTest(15,
                1,2,3,
                1,2,3,
                1,2,3,
                1,2,3,
                1,2,3);
    }

    @Test
    public void pyramid() {
        runTest(30,
                7,8,9,
                6,7,8,
                5,6,7,
                4,5,6);
    }

    private static void runTest(int expectedHeight, Integer... cuboids) {
        Truth.assertThat(getMaxHeight(cuboids)).isEqualTo(expectedHeight);
    }

    private static int getMaxHeight(Integer... cuboids) {
        return CuboidStacking.getMaxHeight(getLists(cuboids));
    }

    private static List<List<Integer>> getLists(Integer... cuboids) {
        List<List<Integer>> lists = Lists.newArrayList();
        for (int i=0; i<cuboids.length; i+=3) {
            List<Integer> dimensions = Lists.newArrayList(cuboids[i], cuboids[i+1], cuboids[i+2]);
            Collections.shuffle(dimensions);
            lists.add(dimensions);
        }
        Collections.shuffle(lists);
        return ImmutableList.copyOf(lists);
    }
}
