package org.rajivprab.sandbox.pandemic;

import com.google.common.collect.ImmutableMap;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.Map;

/**
 * Created by rprabhakar on 2/25/16.
 */
public class PandemicTest {
    @Test
    public void oneCity() {
        Map<String, Integer> allocations = Pandemic.getClinicAllocations(ImmutableMap.of("a", 101), 2);
        Truth.assertThat(allocations).hasSize(1);
        Truth.assertThat(allocations).containsEntry("a", 2);
    }

    @Test
    public void twoCity() {
        Map<String, Integer> allocations = Pandemic.getClinicAllocations(ImmutableMap.of("a", 20, "b", 70), 9);
        Truth.assertThat(allocations).hasSize(2);
        Truth.assertThat(allocations).containsEntry("a", 2);
        Truth.assertThat(allocations).containsEntry("b", 7);
    }

    @Test
    public void twoCityOddNumbers() {
        Map<String, Integer> allocations = Pandemic.getClinicAllocations(ImmutableMap.of("a", 21, "b", 69), 9);
        Truth.assertThat(allocations).hasSize(2);
        Truth.assertThat(allocations).containsEntry("a", 2);
        Truth.assertThat(allocations).containsEntry("b", 7);
    }

    @Test
    public void bigCities() {
        Map<String, Integer> allocations =
                Pandemic.getClinicAllocations(ImmutableMap.of("a", 20000, "b", 70000, "c", 10000), 10000);
        Truth.assertThat(allocations).hasSize(3);
        Truth.assertThat(allocations).containsEntry("a", 2000);
        Truth.assertThat(allocations).containsEntry("b", 7000);
        Truth.assertThat(allocations).containsEntry("c", 1000);
    }

    @Test
    public void bigCitiesNotRounded() {
        Map<String, Integer> allocations =
                Pandemic.getClinicAllocations(ImmutableMap.of("a", 20001, "b", 70001, "c", 10001), 10000);
        Truth.assertThat(allocations).hasSize(3);
        Truth.assertThat(allocations).containsEntry("a", 2000);
        Truth.assertThat(allocations).containsEntry("b", 7000);
        Truth.assertThat(allocations).containsEntry("c", 1000);
    }
}
