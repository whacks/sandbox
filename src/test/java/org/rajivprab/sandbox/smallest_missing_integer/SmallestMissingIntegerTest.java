package org.rajivprab.sandbox.smallest_missing_integer;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SmallestMissingIntegerTest {
    private static final Log log = LogFactory.getLog(SmallestMissingIntegerTest.class);
    private static final Random RNG = new Random();

    @Test
    public void allPresent() {
        test(Lists.newArrayList(0, 1, 2), 3);
    }

    @Test
    public void allPresent_noZero() {
        test(Lists.newArrayList(1, 2, 3), 4);
    }

    @Test
    public void zeroPresent_OneMissing() {
        test(Lists.newArrayList(0, 2, 3), 1);
    }

    @Test
    public void OneMissing_allBiggerNumbers() {
        test(Lists.newArrayList(2, 3, 5), 1);
    }

    @Test
    public void multipleMissing() {
        test(Lists.newArrayList(1, 4, 5), 2);
    }

    private void test(List<Integer> input, int expected) {
        Collections.shuffle(input, RNG);

        // Should ignore all 0s and -1s gracefully
        while (RNG.nextBoolean()) {
            input.add(RNG.nextInt(2) * -1);
            Collections.shuffle(input, RNG);
        }

        // Should handle duplicate elements gracefully
        while (RNG.nextBoolean()) {
            input.add(input.get(0));
            Collections.shuffle(input, RNG);
        }

        log.info("Starting test with input: " + input);
        Truth.assertThat(SmallestMissingInteger.getSmallestMissing(input)).isEqualTo(expected);
    }
}
