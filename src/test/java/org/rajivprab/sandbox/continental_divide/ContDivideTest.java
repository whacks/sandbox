package org.rajivprab.sandbox.continental_divide;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.javatuples.Pair;
import org.junit.Test;

import java.util.List;

/**
 * Created by rprabhakar on 2/15/16.
 */
public class ContDivideTest {
    @Test
    public void noneFound() {
        List<Integer> row0 = Lists.newArrayList(1, 4, 2, 6, 7);
        List<Integer> row1 = Lists.newArrayList(5, 2, 6, 3, 7);
        List<Integer> row2 = Lists.newArrayList(2, 5, 2, 6, 7);
        List<List<Integer>> map = Lists.newArrayList(row0, row1, row2);
        Truth.assertThat(ContDivide.getContDivides(map)).isEmpty();
    }

    @Test
    public void someFound() {
        List<Integer> row0 = Lists.newArrayList(1, 4, 2, 6);
        List<Integer> row1 = Lists.newArrayList(2, 2, 3, 3);
        List<Integer> row2 = Lists.newArrayList(2, 5, 2, 6);
        List<List<Integer>> map = Lists.newArrayList(row0, row1, row2);
        Truth.assertThat(ContDivide.getContDivides(map))
                .containsExactly(Pair.with(0, 3), Pair.with(1, 2), Pair.with(1, 3), Pair.with(2, 3));
    }
}
