package org.rajivprab.sandbox.min_cost_hire_workers;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import org.junit.Test;

public class MinCostToHireWorkersTest {
    @Test
    public void example1() {
        Truth.assertThat(MinCostToHireWorkers.getCost(
                ImmutableList.of(10, 20, 5),
                ImmutableList.of(70, 50, 30),
                2
        )).isWithin(0.01).of(105.0);
    }

    @Test
    public void example2() {
        Truth.assertThat(MinCostToHireWorkers.getCost(
                ImmutableList.of(3, 1, 10, 10, 1),
                ImmutableList.of(4, 8, 2, 2, 7),
                3
        )).isWithin(0.01).of(30.67);
    }
}
