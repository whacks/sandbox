package org.rajivprab.sandbox.recover_tree;

import com.google.common.truth.Truth;
import org.junit.Test;

public class RecoverTreeTest {
    @Test
    public void example1() {
        Truth.assertThat(RecoverTree.parse("1-2--3--4-5--6--7")).containsExactly(1, 2, 5, 3, 4, 6, 7);
    }

    @Test
    public void example2() {
        Truth.assertThat(RecoverTree.parse("1-2--3---4-5--6---7")).containsExactly(1, 2, 5, 3, null, 6, null, 4, null, 7);
    }

    @Test
    public void example3() {
        Truth.assertThat(RecoverTree.parse("1-401--349---90--88")).containsExactly(1, 401, null, 349, 88, 90);
    }

    @Test
    public void numDashes() {
        Truth.assertThat(RecoverTree.getNumDashes("---31", 0)).isEqualTo(3);
    }

    @Test
    public void getNum() {
        Truth.assertThat(RecoverTree.getNum("---31", 3)).isEqualTo(31);
        Truth.assertThat(RecoverTree.getNum("---31-", 3)).isEqualTo(31);
    }
}
