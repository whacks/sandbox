package org.rajivprab.sandbox.scheduler;

import org.junit.Test;
import org.rajivprab.cava.ThreadUtilc;

import java.time.Duration;
import java.util.UUID;

/**
 * Created by rprabhakar on 5/16/16.
 */
public class SchedulerTest {
    @Test
    public void basicDemo() {
        UUID user1 = UUID.randomUUID();
        WorkerPool workerPool = new WorkerPool();
        Scheduler scheduler = new Scheduler(new UserPool(), workerPool);
        workerPool.withScheduler(scheduler)
                .withWorker(new WorkerMock()).withWorker(new WorkerMock()).withWorker(new WorkerMock());
        ThreadUtilc.sleep(1);
        scheduler.submitRequest(user1.toString(), new UserRequest(10, 10, 1000, "./balancedScript.sh"));
        ThreadUtilc.sleep(1);
        scheduler.submitRequest(user1.toString(), new UserRequest(10, 10, 1000, "./balancedScript.sh"));
        ThreadUtilc.sleep(1);
        scheduler.submitRequest(user1.toString(), new UserRequest(10, 10, 1000, "./balancedScript.sh"));
        ThreadUtilc.sleep(1);
        scheduler.submitRequest(UUID.randomUUID().toString(), new UserRequest(10, 10, 1000, "./balancedScript.sh"));
        ThreadUtilc.sleep(1);
        scheduler.submitRequest(UUID.randomUUID().toString(), new UserRequest(10, 10, 1000, "./balancedScript.sh"));
        ThreadUtilc.sleep(1);
        scheduler.submitRequest(UUID.randomUUID().toString(), new UserRequest(10, 10, 1000, "./balancedScript.sh"));
        ThreadUtilc.sleep(1);
        ThreadUtilc.sleep(1);
        scheduler.submitRequest(user1.toString(), new UserRequest(10, 10, 1000, "./balancedScript.sh"));
        ThreadUtilc.sleep(1);
        scheduler.submitRequest(user1.toString(), new UserRequest(10, 10, 1000, "./balancedScript.sh"));
        scheduler.submitRequest(user1.toString(), new UserRequest(0.01, 100, 1000, "./memHeavyScript.sh"));
        ThreadUtilc.sleep(1);
        scheduler.submitRequest(user1.toString(), new UserRequest(100, 0.01, 1000, "./cpuHeavyScript.sh"));

        ThreadUtilc.sleep(Duration.ofSeconds(20).toMillis());
    }
}
