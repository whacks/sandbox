package org.rajivprab.sandbox.scheduler;

import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

/**
 * Created by rprabhakar on 5/16/16.
 */
public class WorkerMock extends Worker {
    private static final Logger log = LogManager.getLogger(WorkerMock.class);

    private static final Instant TIME_STARTED = Instant.now();
    private final Map<UUID, InternalRequest> reqs = Maps.newConcurrentMap();

    @Override
    public void runTask(InternalRequest request) {
        log.info("Request assigned to worker: " + this + ", request: " + request);
        reqs.put(request.getRequestID(), request);
        super.runTask(request);
    }

    @Override
    public Optional<Result> getResult(UUID requestID) {
        Optional<Result> result = new Random().nextInt(5) == 1 ?
                Optional.of(new Result(reqs.get(requestID), TIME_STARTED, Instant.now(), 0, "stderr.log", "stdout.log")) :
                Optional.empty();
        // log.info("Get result mocking out: " + result);
        result.ifPresent(this::updateLoadOnCompletion);
        return result;
    }
}
