package org.rajivprab.sandbox.url_shortener;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class UniqueStringTest {
    @Test
    public void shouldIterateThroughAllCharacters_andThenLoopAround() {
        UniqueString uniqueString = new UniqueString();
        loopThroughAllCharacters(uniqueString, "");
        loopThroughAllCharacters(uniqueString, "1");
        loopThroughAllCharacters(uniqueString, "2");
    }

    private static void loopThroughAllCharacters(UniqueString uniqueString, String prefix) {
        for (char i = '0'; i <= '9'; i++) {
            assertThat(uniqueString.getUniqueString()).isEqualTo(prefix + i);
        }
        for (char i = 'a'; i <= 'z'; i++) {
            assertThat(uniqueString.getUniqueString()).isEqualTo(prefix + i);
        }
        for (char i = 'A'; i <= 'Z'; i++) {
            assertThat(uniqueString.getUniqueString()).isEqualTo(prefix + i);
        }
    }
}
