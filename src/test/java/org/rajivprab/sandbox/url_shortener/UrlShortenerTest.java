package org.rajivprab.sandbox.url_shortener;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class UrlShortenerTest {
    @Test
    public void shorten_thenExpand_shouldTrackUsage() {
        UrlShortener urlShortener = new UrlShortener();
        String fullUrl0 = "www.abcd.com";
        String fullUrl1 = "https://efgh.com";

        String shortenedUrl0 = urlShortener.shorten(fullUrl0);
        String shortenedUrl1 = urlShortener.shorten(fullUrl1);
        assertThat(shortenedUrl0).isEqualTo(UrlShortener.URL_PREFIX + "0");
        assertThat(shortenedUrl1).isEqualTo(UrlShortener.URL_PREFIX + "1");

        assertThat(urlShortener.getFullUrl(ImmutableList.of("1", "0")))
                .containsExactly(fullUrl1, fullUrl0);
    }

    @Test
    public void shouldTrackClicks() {
        UrlShortener urlShortener = new UrlShortener();
        String fullUrl = "test-url";
        String shortenedUrl0 = urlShortener.shorten(fullUrl);

        assertThat(shortenedUrl0).isEqualTo(UrlShortener.URL_PREFIX + "0");
        assertThat(urlShortener.getFullUrl(ImmutableList.of("0"))).containsExactly(fullUrl);
        assertThat(urlShortener.getFullUrl(ImmutableList.of("0"))).containsExactly(fullUrl);
        assertThat(urlShortener.suffixToNumClicks.get("0").get()).isEqualTo(2);
    }

    @Test
    public void shortenSameUrlTwice_shouldGetSameSuffix() {
        UrlShortener urlShortener = new UrlShortener();
        String fullUrl = "www.abcd.com";

        String shortenedUrl0 = urlShortener.shorten(fullUrl);
        String shortenedUrl1 = urlShortener.shorten(fullUrl);

        assertThat(shortenedUrl0).isEqualTo(UrlShortener.URL_PREFIX + "0");
        assertThat(shortenedUrl0).isEqualTo(shortenedUrl1);

        assertThat(urlShortener.getFullUrl(ImmutableList.of("0"))).containsExactly(fullUrl);
    }
}
