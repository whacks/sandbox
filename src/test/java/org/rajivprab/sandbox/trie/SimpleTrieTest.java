package org.rajivprab.sandbox.trie;

import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.Optional;

/**
 * Created by rajivprab on 6/24/17.
 */
public class SimpleTrieTest {
    @Test
    public void noValueFound_returnsEmptyOptional() {
        Trie<Character, String> dictionary = Trie.newSimpleTrie();
        isEmpty(dictionary.getValue('d', 'o', 'g'));
        isEmpty(dictionary.remove('d', 'o', 'g'));
    }

    @Test
    public void variousBaseCases() {
        Trie<Character, String> dictionary = Trie.newSimpleTrie();

        isEmpty(dictionary.put("dog", 'd', 'o', 'g'));
        hasValue(dictionary.getValue('d', 'o', 'g'), "dog");
        Truth.assertThat(dictionary.getKeys()).containsExactly('d');

        isEmpty(dictionary.put("dot", 'd', 'o', 't'));
        hasValue(dictionary.getValue('d', 'o', 't'), "dot");
        Truth.assertThat(dictionary.get('d', 'o').get().getKeys()).containsExactly('g', 't');

        isEmpty(dictionary.put("dogged", 'd', 'o', 'g', 'g', 'e', 'd'));
        hasValue(dictionary.getValue('d', 'o', 'g'), "dog");
        hasValue(dictionary.getValue('d', 'o', 'g', 'g', 'e', 'd'), "dogged");

        hasValue(dictionary.remove('d', 'o', 'g'), "dog");
        hasValue(dictionary.getValue('d', 'o', 'g', 'g', 'e', 'd'), "dogged");
        Truth.assertThat(dictionary.get('d', 'o').get().getKeys()).containsExactly('g', 't');

        hasValue(dictionary.put("anarchy",'d', 'o', 'g', 'g', 'e', 'd'), "dogged");
        hasValue(dictionary.getValue('d', 'o', 'g', 'g', 'e', 'd'), "anarchy");
    }

    private static <V> void isEmpty(Optional<V> optional) {
        Truth.assertThat(optional).isEqualTo(Optional.empty());
    }

    private static <V> void hasValue(Optional<V> optional, V value) {
        Truth.assertThat(optional).isEqualTo(Optional.of(value));
    }
}
