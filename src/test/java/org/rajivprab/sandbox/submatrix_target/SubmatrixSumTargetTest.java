package org.rajivprab.sandbox.submatrix_target;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Table;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.List;

public class SubmatrixSumTargetTest {

    @Test
    public void example1() {
        List<List<Integer>> input = ImmutableList.of(
                ImmutableList.of(0,1,0),
                ImmutableList.of(1,1,1),
                ImmutableList.of(0,1,0)
        );
        runTest(input, 0, 4);
    }

    @Test
    public void example2() {
        List<List<Integer>> input = ImmutableList.of(
                ImmutableList.of(1,-1),
                ImmutableList.of(-1,1)
        );
        runTest(input, 0, 5);
    }

    @Test
    public void example3() {
        List<List<Integer>> input = ImmutableList.of(
                ImmutableList.of(904)
        );
        runTest(input, 0, 0);
    }

    private static void runTest(List<List<Integer>> input, int target, int expected) {
        Table<Integer, Integer, Integer> inputTable = buildInput(input);
        int resultOriginal = SubmatrixSumTargetOriginal.getNumSubmatrices(
                inputTable, target, input.size(), input.get(0).size());
        int resultOptimized = SubmatrixSumTargetOptimized.getNumSubmatrices(
                inputTable, target, input.size(), input.get(0).size());
        Truth.assertThat(resultOriginal).isEqualTo(expected);
        Truth.assertThat(resultOptimized).isEqualTo(expected);
    }

    private static Table<Integer, Integer, Integer> buildInput(List<List<Integer>> input) {
        Table<Integer, Integer, Integer> table = HashBasedTable.create();
        for (int row=0; row<input.size(); row++) {
            List<Integer> curRow = input.get(row);
            for (int col=0; col<curRow.size(); col++) {
                table.put(row, col, curRow.get(col));
            }
        }
        return table;
    }
}
