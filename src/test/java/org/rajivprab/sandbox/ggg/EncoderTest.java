package org.rajivprab.sandbox.ggg;

import com.google.common.truth.Truth;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.rajivprab.cava.FileUtilc;

import java.util.Map;

/**
 * Created by rajivprab on 6/24/17.
 */
public class EncoderTest {
    private static final Logger log = LogManager.getLogger(EncoderTest.class);

    @Test
    public void encodeThenDecode_example1() {
        runTest("Hello, world!");
    }

    @Test
    public void encodeThenDecode_example2() {
        runTest(FileUtilc.readClasspathFile("ggg/encode_input2.txt"));
    }

    private static void runTest(String humanMessage) {
        Map<Character, String> encoding = Encoder.generateEncoding(humanMessage);
        String encodingString = Encoder.printEncoding(encoding);
        String alienMessage = Encoder.getAlienMessage(humanMessage, encoding);
        String decodedMessage = Decoder.decode(encodingString, alienMessage);

        log.info("Human message:\n" + humanMessage);
        log.info("Human to alien encoding: " + encodingString);
        log.info("Alien message: " + alienMessage);
        log.info("Decoded Human message:\n" + decodedMessage);

        Truth.assertThat(decodedMessage).isEqualTo(humanMessage);
    }
}
