package org.rajivprab.sandbox.ggg;

import com.google.common.truth.Truth;
import org.junit.Test;

/**
 * Created by rajivprab on 6/24/17.
 */
public class DecoderTest {
    @Test
    public void exampleOne() {
        Truth.assertThat(Decoder.decode(
                "H GgG d gGg e ggG l GGg o gGG r Ggg w ggg", "GgGggGGGgGGggGG, ggggGGGggGGggGg!"))
             .isEqualTo("Hello, world!");
    }

    @Test
    public void exampleTwo() {
        Truth.assertThat(Decoder.decode(
                "a GgG d GggGg e GggGG g GGGgg h GGGgG i GGGGg l GGGGG m ggg o GGg p Gggg r gG y ggG",
                "GGGgGGGgGGggGGgGggG /gG/GggGgGgGGGGGgGGGGGggGGggggGGGgGGGgggGGgGggggggGggGGgG!"))
             .isEqualTo("hooray /r/dailyprogrammer!");
    }
}
