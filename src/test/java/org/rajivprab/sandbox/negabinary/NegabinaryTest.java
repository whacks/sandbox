package org.rajivprab.sandbox.negabinary;

import com.google.common.truth.Truth;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class NegabinaryTest {
    @Test
    public void exampleZero() {
        Truth.assertThat(Negabinary.getBaseMinusTwo(0)).isEqualTo("0");
    }

    @Test
    public void examplePositive() {
        Truth.assertThat(Negabinary.getBaseMinusTwo(1)).isEqualTo("1");
        Truth.assertThat(Negabinary.getBaseMinusTwo(2)).isEqualTo("110");
        Truth.assertThat(Negabinary.getBaseMinusTwo(3)).isEqualTo("111");
        Truth.assertThat(Negabinary.getBaseMinusTwo(4)).isEqualTo("100");
        Truth.assertThat(Negabinary.getBaseMinusTwo(5)).isEqualTo("101");
        Truth.assertThat(Negabinary.getBaseMinusTwo(6)).isEqualTo("11010");
    }

    @Test
    public void exampleNegative() {
        Truth.assertThat(Negabinary.getBaseMinusTwo(-1)).isEqualTo("11");
        Truth.assertThat(Negabinary.getBaseMinusTwo(-2)).isEqualTo("10");
        Truth.assertThat(Negabinary.getBaseMinusTwo(-3)).isEqualTo("1101");
        Truth.assertThat(Negabinary.getBaseMinusTwo(-4)).isEqualTo("1100");
        Truth.assertThat(Negabinary.getBaseMinusTwo(-5)).isEqualTo("1111");
        Truth.assertThat(Negabinary.getBaseMinusTwo(-15)).isEqualTo("110001");
    }
}
