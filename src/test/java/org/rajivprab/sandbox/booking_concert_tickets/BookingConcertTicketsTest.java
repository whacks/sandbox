package org.rajivprab.sandbox.booking_concert_tickets;

import com.google.common.truth.Truth;
import org.junit.Test;

public class BookingConcertTicketsTest {
    @Test
    public void example1_simpleMap() {
        BookingConcertTickets instance = new BookingConcertTickets(2, 5);
        Truth.assertThat(instance.gather(4, 0)).asList().containsExactly(0, 0);
        Truth.assertThat(instance.gather(2, 0)).asList().isEmpty();
        Truth.assertThat(instance.scatter(5, 1)).isTrue();
        Truth.assertThat(instance.scatter(5, 1)).isFalse();

        // More checks by me
        Truth.assertThat(instance.gather(1, 1)).asList().containsExactly(1, 4);
        Truth.assertThat(instance.scatter(1, 1)).isFalse();
    }

    @Test
    public void example1_segmentTree() {
        BookingConcertTicketsSegmentTree instance = new BookingConcertTicketsSegmentTree(2, 5);
        Truth.assertThat(instance.gather(4, 0)).asList().containsExactly(0, 0);
        Truth.assertThat(instance.gather(2, 0)).asList().isEmpty();
        Truth.assertThat(instance.scatter(5, 1)).isTrue();
        Truth.assertThat(instance.scatter(5, 1)).isFalse();

        // More checks by me
        Truth.assertThat(instance.gather(1, 1)).asList().containsExactly(1, 4);
        Truth.assertThat(instance.scatter(1, 1)).isFalse();
    }
}
