package org.rajivprab.sandbox.strong_password_checker;

import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

public class StrongPasswordCheckerTest {
    @Test
    public void example1() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("a"))
                .isEqualTo(5);
    }

    @Test
    public void example2() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aA1"))
             .isEqualTo(3);
    }

    @Test
    public void example3() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("1337C0d3"))
             .isEqualTo(0);
    }

    @Test
    public void missingSpecialCharacters() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("leetcode"))
             .isEqualTo(2);
    }

    @Test
    public void tooShort_andBreakSequenceWith2Inserts_and2specialCharacters() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aaaaa"))
             .isEqualTo(2);
    }

    @Test
    public void tooShortBy1_andBreakSequenceWith1Inserts_and2specialCharacters() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aaabb"))
             .isEqualTo(2);
    }

    @Test
    public void tooShortBy1_andBreakSequenceWith1Inserts_and1specialCharacter() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aaa11"))
             .isEqualTo(1);
    }

    @Test
    public void tooShortBy3_andBreakSequenceWith1Inserts_and2specialCharacters() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aaa"))
             .isEqualTo(3);
    }

    @Test
    public void tooLong() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded(RandomStringUtils.randomAlphanumeric(25)))
             .isEqualTo(5);
    }

    @Test
    public void tooLong_repeatedCharacters_onlyDeleteRepeated() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aaau123456789U123456789bbb"))
             .isEqualTo(6);
    }

    @Test
    public void tooLong_repeatedCharacters_missingSpecialIfDeleted_deleteOthersInstead() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aaa01234567890123456789BBB"))
             .isEqualTo(6);
    }

    @Test
    public void tooLong_repeatedCharacters_needToAddressBoth() {
        // Convert to aa1aa2aa3aa4aa5aa6aa. Delete 5 characters first, then mutate 6
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aaaaaaaaaaaaaaaaaaaaaaaaa"))
             .isEqualTo(11);
    }

    @Test
    public void tooLong_missingSpecialCharacters_needToAddressBoth() {
        // Delete 10 characters. Then mutate 2
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("012345678901234567890123456789"))
             .isEqualTo(12);
    }

    @Test
    public void tooLong_repeatedCharacters_addressBothWith1Deletion() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("01234567890123456NNNn"))
             .isEqualTo(1);
    }

    @Test
    public void tooLong_repeatedCharacters_addressBothWith2Deletion() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("01234567890123456sEEEE"))
             .isEqualTo(2);
    }

    @Test
    public void tooLong_repeatedCharacters_addressWith3Deletion() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("01234567890123456sEEEEE"))
             .isEqualTo(3);
    }

    @Test
    public void tooLong_multipleRepeatedSequences_addressWith3Deletion() {
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("0123456789012345sssEEEE"))
             .isEqualTo(3);
    }

    @Test
    public void tooLong_repeatedCharacters_shouldDeleteOne_andReplaceEvery3rd() {
        // Convert to: aa1aa2aa3aa4aa5aa6aa
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aaaaaaaaaaaaaaaaaaaaa"))
             .isEqualTo(7);
    }

    @Test
    public void tooLong_repeatedCharacters_twoDifferent_shouldDeleteOne_andReplaceEvery3rd() {
        // Convert to: aa1aa2aa3abb4bb5bb6b
        Truth.assertThat(StrongPasswordChecker.minChangesNeeded("aaaaaaaaaabbbbbbbbbbb"))
             .isEqualTo(7);
    }
}
